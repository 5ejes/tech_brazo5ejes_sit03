﻿#pragma warning disable 0649
using UnityEngine;

namespace NSSeguridad
{
    [CreateAssetMenu(fileName = "Security Data", menuName = "NSSeguridad/Security Data", order = 0)]
    public class SOSecurityData : ScriptableObject
    {
        public string aulaUserFirstName;

        public string aulaUserLastName;

        public string aulaClassName;

        public string aulaClassId;

        public string aulaSchoolName;

        public string aulaUsername;

        public string aulaUserPassword;

        public string licenseNumber;

        public string ltiDatos;

        /// <summary>
        /// campos usuario con valores que el usuario a ingresa
        /// </summary>
        public string inputUser;

        /// <summary>
        /// campo curso que contiene el valor de la contraseña ingresada por el usuario
        /// </summary>
        public string inputPassword;

        public bool isLogin;
    }
}