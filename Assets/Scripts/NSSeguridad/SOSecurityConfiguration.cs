﻿#pragma warning disable 0649
using UnityEngine;

namespace NSSeguridad
{
    [CreateAssetMenu(fileName = "Security configuration", menuName = "NSSeguridad/Security Configuration", order = 0)]
    public class SOSecurityConfiguration : ScriptableObject
    {
        public string licenceUrl;

        public bool isModeMonoUser;

        public bool isSecurityOn;

        public bool isModeClassroom;

        public bool isConectionActive;

        public bool isLtiActive;

        public string fileName;

        [Header("URLs")] public string urlClassroom = "http://190.84.230.23:3000";

        public string urlScoreLti = "https://lti.servercloudlabs.com/lti_launcher/setScore";

        public string validLtiUrl = "lti.servercloudlabs.com";

        public string WebAulaUrl;
    }
}