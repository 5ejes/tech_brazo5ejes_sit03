﻿#pragma warning disable 0649
using System.Collections;
using UnityEngine;
using NSInterfaz;
using NSSeguridad;


namespace NSTraduccionIdiomas
{
    /// <summary>
    /// Clase que controla la seleccion de idiomas
    /// </summary>
    public class ClsControladorIdiomas : MonoBehaviour
    {
        public bool bilingueIngEsp;

        [SerializeField] private SOSecurityData refSoSecurityData;

        public enum idioma
        {
            espaniol,
            ingles,
            portugues
        }

        public idioma IdiomaActual;

        private void Start()
        {
            DiccionarioIdiomas.CreateInstance();
            DiccionarioIdiomas.Instance.CargarDiccionarios();
            DiccionarioIdiomas.Instance.SetIdiom(IdiomsList.Spanish);
            //DiccionarioIdiomas.Instance.seOnDiccionarioCargado.AddListener(MostrarVentanaInicial);
        }

        private void MostrarVentanaInicial()
        {
            if (refSoSecurityData.isLogin)
                Debug.Log("Evento cargo datos");//refPanelInterfazBienvenida.ShowPanel();
        }

        private void HandleDelegateOnBoxMessageButtonAccept()
        {
            Application.Quit();
        }

        private void ActivarVentanaCorrespondiente()
        {
            StartCoroutine(CouActivarPanel());
        }

        private IEnumerator CouActivarPanel()
        {
            yield return new WaitForSeconds(0.5f);

            switch (IdiomaActual)
            {
                case idioma.espaniol:
                    DiccionarioIdiomas.Instance.SetIdiom(IdiomsList.Spanish);
                    break;

                case idioma.ingles:
                    DiccionarioIdiomas.Instance.SetIdiom(IdiomsList.English);
                    break;

                case idioma.portugues:
                    DiccionarioIdiomas.Instance.SetIdiom(IdiomsList.portugues);
                    break;
            }

            /*if (bilingueIngEsp)
                refPanelInterfazSeleccionLenguajeInglesEspaniol.ShowPanel();
            else
                Debug.Log("Evento cargo datos");//refPanelInterfazBienvenida.ShowPanel();
                //refPanelInterfazBienvenida.ShowPanel();
                */
        }
    }
}