﻿#pragma warning disable 0649
using System.Collections.Generic;
using System.Xml;
using UnityEngine;

public class ClsCargaDeXMl : MonoBehaviour
{
    #region members

    public string[] NombreArchivoACargar;

    private Dictionary<string, string> diccionarioConLoDelrAchivo;

    #endregion

    #region monoBehaviour

    private void Start()
    {
        diccionarioConLoDelrAchivo = new Dictionary<string, string>();
    }

    #endregion

    #region private methods

    private void mtdLeerXML(string nombreDelArchivo)
    {
        diccionarioConLoDelrAchivo = new Dictionary<string, string>();
        var dir = System.IO.Path.Combine(Application.streamingAssetsPath, nombreDelArchivo + ".xml");

        XmlDocument reader = new XmlDocument();
        reader.LoadXml(System.IO.File.ReadAllText(dir));

        XmlNodeList _list = reader.ChildNodes[0].ChildNodes;

        for (int i = 0; i < _list.Count; i++)
            diccionarioConLoDelrAchivo.Add(_list[i].Name, _list[i].InnerXml);
    }

    #endregion

    #region public methods

    public Dictionary<string, string> CargarDiccionarioEspaniol()
    {
        mtdLeerXML(NombreArchivoACargar[0]);
        return diccionarioConLoDelrAchivo;
    }

    public Dictionary<string, string> CargarDiccionarioIngles()
    {
        mtdLeerXML(NombreArchivoACargar[1]);
        return diccionarioConLoDelrAchivo;
    }

    public Dictionary<string, string> CargarDiccionarioPortugues()
    {
        mtdLeerXML(NombreArchivoACargar[1]);
        return diccionarioConLoDelrAchivo;
    }

    #endregion
}