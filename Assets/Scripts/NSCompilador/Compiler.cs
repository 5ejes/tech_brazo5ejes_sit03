﻿using System.Collections;
using System.Collections.Generic;
using NSEditorAcciones;
using UnityEngine;

namespace NSCompilador
{
	public class Compiler
	{
		public static ExecutionSequence Compile(params LogicBlock[] argBlocks)
		{
			List<LogicBlock> listBlocks = new List<LogicBlock>();

			int startIndex = 0;
			// Proceso todos los bloques de los parametros
			RecursiveProcessBlocks(argBlocks, ref startIndex, listBlocks);

			return new ExecutionSequence(listBlocks.ToArray());
		}

		private static void RecursiveProcessBlocks(LogicBlock[] argBlocks, ref int argIndex, List<LogicBlock> argListLogicBlocks)
		{
			// Recorro cada bloque
			foreach (var block in argBlocks)
			{
				// Según cada bloque
				switch (block)
				{
					default:
						// Agrego el bloque actual a la lista
						argListLogicBlocks.Add(block);
					// Defino y aumento el índice
					block.ExecutionOrder = argIndex;
					argIndex++;
					break;
					case IfStatement _ifStatement:

							// Agrego el bloque actual a la lista
							argListLogicBlocks.Add(block);
						// Defino y aumento el índice
						block.ExecutionOrder = argIndex;
						argIndex++;

						// Asigno el orden de ejecución para bloques if
						RecursiveProcessBlocks(_ifStatement.IfExecutionBlocks, ref argIndex, argListLogicBlocks);

						// Casteo el bloque en un bloque if Else
						var _ifElseStatement = _ifStatement as IfElseStatement;

						// Si es un IfElse
						if (_ifElseStatement != null)
						{
							// Asigno el orden de ejecución para bloques else
							RecursiveProcessBlocks(_ifElseStatement.ElseExecutionBlocks, ref argIndex, argListLogicBlocks);
						}
						break;
				}
			}
		}

		private static int CheckStatements(List<LogicBlock> listBlocks, ref int currentIndex, LogicBlock logic)
		{
			switch (logic)
			{

				// Si el bloque lógico es un IfStatement
				case IfStatement _ifStatement:
					// Recorro los bloques de ejecución del IfStatment
					for (int j = 0; j < _ifStatement.IfExecutionBlocks.Length; j++)
					{
						// Obtengo el bloque actual
						LogicBlock currentLogic = _ifStatement.IfExecutionBlocks[j];
						// Aumento el índice actual
						currentIndex++;
						// Defino el orden de ejecución
						currentLogic.ExecutionOrder = currentIndex;
						// Agrego el bloque a la lista
						listBlocks.Add(currentLogic);
					}

					// Intento convertir el if en un if else
					var _ifElseStatement = _ifStatement as IfElseStatement;

					// Si la conversión no es nula
					if (_ifElseStatement != null)
					{
						// Recorro todos los bloques de ejecución else
						for (int j = 0; j < _ifElseStatement.ElseExecutionBlocks.Length; j++)
						{
							// Obtengo el bloque actual
							LogicBlock currentLogic = _ifElseStatement.ElseExecutionBlocks[j];
							// Aumento el indice actual
							currentIndex++;
							// Defino el orden de ejecución
							currentLogic.ExecutionOrder = currentIndex;
							// Agrego el bloque a la lista
							listBlocks.Add(currentLogic);

						}
					}

					break;
				default:
					break;
			}

			return currentIndex;
		}

		public void SendToExecution(ExecutionSequence argSequence)
		{
			throw new System.NotImplementedException();
		}
	}
}