﻿using System.Collections;
using System.Collections.Generic;
using NSUtilities;
using UnityEngine;
using UnityEngine.Events;

namespace NSInspectorSender
{
	public class Vector3InspectorSender : AbstractInspectorSender<Vector3>
	{
		[System.Serializable]	public class Vector3Event : UnityEvent<Vector3> { }

		[Header("Event")]
		[SerializeField] private Vector3Event _onSendAction = new Vector3Event();
		public override void Send()
		{
			// Ejecuto el evento
			_onSendAction.WrapInvoke(Value);
		}
	}
}
