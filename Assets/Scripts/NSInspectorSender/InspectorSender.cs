﻿using System.Collections;
using System.Collections.Generic;
using NSUtilities;
using UnityEngine;
using UnityEngine.Events;

namespace NSInspectorSender
{
	public class InspectorSender : BaseInspectorSender
	{
		[SerializeField] private UnityEvent _onSendAction = new UnityEvent();
		public override void Send()
		{
			_onSendAction.WrapInvoke();
		}
	}
}