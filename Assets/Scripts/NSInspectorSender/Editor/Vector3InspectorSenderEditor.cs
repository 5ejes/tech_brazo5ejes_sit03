﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using NSInspectorSender;

[CustomEditor(typeof(Vector3InspectorSender))]
public class Vector3InspectorSenderEditor : Editor
{
	private Vector3InspectorSender _editorTarget;

	public Vector3InspectorSender EditorTarget
	{
		get
		{
			if (_editorTarget == null) _editorTarget = (Vector3InspectorSender)target;
			return _editorTarget;
		}
		set => _editorTarget = value;
	}

	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();

		if (GUILayout.Button("Send"))
		{
			EditorTarget.Send();
		}
	}
}
