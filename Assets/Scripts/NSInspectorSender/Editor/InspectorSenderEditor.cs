﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using NSUtilities;
using NSInspectorSender;

[CustomEditor(typeof(InspectorSender))]
public class InspectorSenderEditor : GenericEditorTargetBase<InspectorSender>
{
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();

		if (GUILayout.Button("Send"))
		{
			EditorTarget.Send();
		}
	}
}

