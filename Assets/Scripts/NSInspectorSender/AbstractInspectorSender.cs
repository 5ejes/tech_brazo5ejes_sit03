﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSInspectorSender
{
	public abstract class AbstractInspectorSender<T> : BaseInspectorSender
	{
		[Header("Value")]
		[SerializeField] private T value;

		public T Value { get => value; set => this.value = value; }
	}
}