using UnityEngine;

namespace NSContextosLocalesBrazo
{
	public class ArmLocalContextDataWriter : MonoBehaviour
	{
		[Header("References")]
		[SerializeField]private RoboticViewer _roboticViewer;
		public ArmLocalContext CurrentContext { get; set; }
		public void WriteToRoboticViewer()
		{
			if (CurrentContext == null) return;
			NSInspectorValue.BoolInspectorValue gripValueState = CurrentContext.GripValueState;
			if (gripValueState == null) return;

			// Escribo el resultado del grip 
			WriteGripState(gripValueState.Value);
		}

		public void RemoveCurrentContextReference()
		{
			CurrentContext = null;
		}

		private void WriteGripState(bool currentGripState)
		{
			if(_roboticViewer == null) return;

			SetViewerGripperInput(currentGripState);
		}

		private void SetViewerGripperInput(bool gripState)
		{
						// Si el viewer es nulo
			if (_roboticViewer == null)
			{
				// Lanzo el error
				Debug.LogError("Viewer is not defined", this);
				return;
			}

			// Modifico el valor de gripper input
			_roboticViewer.digitalInputs[2] = gripState;
		}
	}
}
