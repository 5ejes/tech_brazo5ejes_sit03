﻿using System.Collections;
using System.Collections.Generic;
using Modular;
using NSInterfazControlPLC;
using NSUtilities;
using UnityEngine;

namespace NSContextosLocalesBrazo
{
	public class ArmLocalContextManager : MonoBehaviour
	{
		[Header("Events")]
		[SerializeField] private ArmLocalContextEvent _onContextReceived = new ArmLocalContextEvent();
		private ArmLocalContext _currentContext;

		/// <summary>
		/// Contexto brazo local actual de la escena
		/// </summary>
		/// <value>Contexto local que se quiere definir</value>
		public ArmLocalContext CurrentContext
		{
			get => _currentContext;
			set
			{
				_currentContext = value;
				// Invoco el evento al definir el valor del contexto actual
				_onContextReceived.WrapInvoke(value);
			}
		}

		/// <summary>
		/// Elimina la cámara activa del contexto actual
		/// </summary>
		public void CleanCurrentOnCameraManager()
		{
			// Si el contexto es nulo
			if (IsCurrentContextNull()) return;

			// Elimino la referencia a la cámara actual en el contexto activo
			CurrentContext.LocalCameraManager.CleanCurrentCamera();
		}

		/// <summary>
		/// Define una cámara virtual actual como activa
		/// </summary>
		/// <param name="name">Nombre de la cámara registrada</param>
		public void SetCameraAsActive(string name)
		{

			// Verifico si no es nulo
			if (IsCurrentContextNull()) return;

			// Busco en el Camera Manager local
			CurrentContext.LocalCameraManager.SetCameraAsActive(name);
		}

		/// <summary>
		/// Reseteo los cubos de la práctica
		/// </summary>
		public void ResetCubes()
		{
			// Verifico si no es nulo
			if (IsCurrentContextNull()) return;

			// Reseteo los cubos de la situación actual
			CurrentContext.LocalCubeManager.ResetCubes();
		}

		/// <summary>
		/// Conecta el grafcetOutputInterface al contexto y el grafcetReader
		/// </summary>
		/// <param name="_grafcetOutput">Interfaz a la que vamos a conectar</param>
		public void ConnectGrafcetOutput(grafcetOutputInterface _grafcetOutput)
		{
			// Verifico si no es nulo
			if (IsCurrentContextNull()) return;

			// Conecto el grafcet output al brazo
			CurrentContext.GrafcetReader.GrafcetOutput = _grafcetOutput;
		}

		/// <summary>
		/// Borra la referencia al grafcet Engine, interrumpiendo su ejecución
		/// </summary>
		public void DeleteGrafcetEngine()
		{
			// Verifico si no es nulo
			if (IsCurrentContextNull()) return;

			// Elimino la referencia al grafcet Controller
			CurrentContext.GrafcetReader?.GrafcetOutput?.RemoveController();
		}

		/// <summary>
		/// ¿Está definido un contexto actuál?
		/// </summary>
		/// <returns>'true' si está definido, si no 'false'</returns>
		private bool IsCurrentContextNull()
		{
			// verifica si el contexto actual está definido

			if (CurrentContext == null)
			{
				Debug.LogError($"The current context is not defined : {gameObject}");
			}

			return CurrentContext == null;
		}

		/// <summary>
		/// Borra la referencia del grafcet al brazo
		/// </summary>
		public void DeleteArmGrafcetReference()
		{
			// Borro la referencia al Grafcet Output del brazo
			CurrentContext.GrafcetReader.GrafcetOutput = null;
		}

		/// <summary>
		/// Reinicia las prioridades de todas las cámaras
		/// </summary>
		public void ResetAllCameraPriorities()
		{
			// Verifico si existe el contexo
			if (IsCurrentContextNull()) return;

			// Reinicio las prioridades de todas las cámaras del contexto activo a su valor por defecto
			int defaultPriority = 0;
			CurrentContext.LocalCameraManager.SetCameraPriority(defaultPriority, CurrentContext.LocalCameraManager.GetAllCameras());
		}

		/// <summary>
		/// Envía al brazo su posición inicial
		/// </summary>
		public void CurrentArmGoToHome()
		{
			// Compruebo si el contexto no es nulo
			if (IsCurrentContextNull()) return;

			// Envio el brazo a la posición de inicio
			CurrentContext.PositionHandler?.GoToHome();
		}

		[ContextMenu("Return to Error Position Current Context")]
		/// <summary>
		/// Envía el brazo a su posición inicial
		/// </summary>
		public void CurrentArmReturnErrorPosition()
		{
			// Comprueblo si el contexto no es nulo
			if (IsCurrentContextNull()) return;
			ArmPositionHandler positionHandler = CurrentContext.PositionHandler;
			if (!positionHandler) return;

			NSInspectorValue.BoolInspectorValue gripValueState = CurrentContext.GripValueState;
			if (!gripValueState) return;

			float maximumTopHeight = 0.6f;

			var currentArmHeightPosition = positionHandler.GripTip.position;
			currentArmHeightPosition = positionHandler.ConvertToLocalPosition(currentArmHeightPosition);
			currentArmHeightPosition.y = maximumTopHeight;

			SequenceStack stackedOrder = new SequenceStack(new IStackExecutableSequence[]{
				new SecondDelayExecutableSequence(() => {}, 1f),
				new BaseStackExecutableSequence(
					() =>
					{
						positionHandler.MoveToPositionUnrestricted(currentArmHeightPosition.ToAbsolute());
					},
					 () =>
					 {
						 var localPosition = positionHandler.ConvertToLocalPosition(positionHandler.GripTip.position);
						 return (localPosition.y > 0.45f);
					 }),
					 new SecondDelayExecutableSequence(
							() => gripValueState.Value = false,
							1f
						),
					 new BaseStackExecutableSequence(
						 () =>
						 {
							 // Obtengo la posición de error
								var errorReturnPosition = positionHandler.ErrorReturnPosition;
								errorReturnPosition = positionHandler.ConvertToLocalPosition(errorReturnPosition).ToAbsolute();

								positionHandler.MoveToPositionUnrestricted(errorReturnPosition);
						 },
						 () => true
					 )
			});

			StackSequenceProcessor.ExecuteStack(stackedOrder);

			// Envio el brazo a la posición de error
			// CurrentContext.PositionHandler?.ReturnToErrorPosition();
		}

		/// <summary>
		/// Rota la cámara FreeLook del contexto actual
		/// </summary>
		/// <param name="degrees">Grados que va a rotar la cámara </param>
		public void RotateFreelookAroundPivot(float degrees)
		{
			// Compruebo si el contexto no es nulo
			if (IsCurrentContextNull()) return;

			// Roto la cámara freelook
			CurrentContext.LocalFreelook.RotateAroundPivot(degrees);
		}

		/// <summary>
		/// Cancela la rotación del freelook del contexto actual
		/// </summary>
		public void CancelFreelookRotation()
		{
			// Comprueblo si el contexto no es nulo
			if (IsCurrentContextNull()) return;

			// Cancelo la rotación
			CurrentContext.LocalFreelook.CancelRotation();
		}

		/// <summary>
		/// Fuerzo una actualización de la posición del FreeLook
		/// </summary>
		public void ForceUpdateFreeLook()
		{
			// Si el contexto existe
			if (IsCurrentContextNull()) return;

			// Fuerzo la actualización del freelook del contexto
			CurrentContext.LocalFreelook.ForceUpdateFreelookCamera();
		}

		/// <summary>
		/// Ejecuta la función local del play button en cada contexto
		/// </summary>
		public void ExecutePlayButtonAction()
		{
			// Si el contexto existe
			if (IsCurrentContextNull()) return;

			// Ejecuto el evento play del contexto actual
			CurrentContext.PlayButtonHandler.WrapInvoke();
		}

		public void StopCurrentArmMovement()
		{
			if (IsCurrentContextNull()) return;
			CurrentContext.PositionHandler.StopTranslation();
		}
	}
}
