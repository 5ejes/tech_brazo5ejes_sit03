﻿using NSScriptableValues;
using UnityEngine;

namespace NSGenericValueInspector
{
	public class ValueFloatInspector : MonoBehaviour
	{
		[Header("Value Float")]
		[SerializeField] private ValueFloat _valueFloat;

		public ValueFloat Value
		{
			get
			{
				if (_valueFloat == null) Debug.LogError($"The Value Float of this instance is not defined : {this}", this);
				return _valueFloat;
			}
		}

		public void Increment()
		{
			if (Value) Value.Value++;
		}

		public void IncrementBy(float amount)
		{
			if (Value) Value.Value += amount;
		}

		public void Decrement()
		{
			if (Value) Value.Value--;
		}

		public void DecrementBy(float amount)
		{
			if (Value) Value.Value -= amount;
		}

		public void SetAmount(float amount)
		{
			if (Value) Value.Value = amount;
		}

		public float GetAmount() => Value ? Value.Value : float.MinValue;
	}
}
