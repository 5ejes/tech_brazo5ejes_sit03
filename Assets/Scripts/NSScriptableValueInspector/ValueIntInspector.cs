﻿using NSScriptableValues;
using UnityEngine;

namespace NSGenericValueInspector
{
	public class ValueIntInspector : MonoBehaviour
	{
		[Header("Value Float")]
		[SerializeField] private ValueInt _valueInt;

		public ValueInt Value
		{
			get
			{
				if (_valueInt == null) Debug.LogError($"The Value Float of this instance is not defined : {this}", this);
				return _valueInt;
			}
		}

		public void Increment()
		{
			if (Value) Value.Value++;
		}

		public void IncrementBy(int amount)
		{
			if (Value) Value.Value += amount;
		}

		public void Decrement()
		{
			if (Value) Value.Value--;
		}

		public void DecrementBy(int amount)
		{
			if (Value) Value.Value -= amount;
		}

		public void SetAmount(int amount)
		{
			if (Value) Value.Value = amount;
		}

		public float GetAmount() => Value ? Value.Value : float.MinValue;
	}
}
