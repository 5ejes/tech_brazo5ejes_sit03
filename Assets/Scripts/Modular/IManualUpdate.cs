namespace Modular
{
    public interface IManualUpdate
    {
        bool AllowUpdate { get; set; }
        void CallUpdate();
    }
}
