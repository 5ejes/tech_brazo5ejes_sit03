﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Modular
{
	public class StackSequenceProcessor : MonoBehaviour
	{
		private static StackSequenceProcessor _instance;
		private static StackSequenceProcessor Instance
		{
			get
			{
				if (_instance == null) _instance = new GameObject("StackExecutableInstance").AddComponent<StackSequenceProcessor>();
				return _instance;
			}
		}
		private bool _executingSequence;
		private bool _executingStack;
		private IStackExecutableSequence _currentSequence;


		public static void InterruptCurrentSuquence()
		{
			Instance.InterrupExecutionSequence();
			Instance._currentSequence = null;
		}
		public static bool IsSequenceExecuting() => Instance._executingSequence;
		public static bool IsStackExecuting() => Instance._executingStack;
		public static IStackExecutableSequence GetCurrentSequence() => Instance._currentSequence;
		public static GameObject GetSceneInstanceGameObject() => Instance.gameObject;
		public static MonoBehaviour GetMonoBehaviour() => Instance;
		public static void ExecuteStack(SequenceStack stack)
		{
			StackSequenceProcessor instance = Instance;

			if (stack == null)
			{
				Debug.LogError($"The stack passed as parameter is null", instance);
				return;
			}

			instance.StartCoroutine(instance.ProcessSequence(stack));
		}

		private IEnumerator ProcessSequence(SequenceStack stack)
		{
			_currentSequence = null;

			if (stack == null) yield break;
			if (stack.IsQueueEmpty()) yield break;

			_executingStack = true;

			while (!stack.IsQueueEmpty())
			{
				IStackExecutableSequence sequence = stack.Unstack();
				if (sequence == null) yield break;

				_currentSequence = sequence;
				_executingSequence = true;

				sequence.ExecuteCurrentSequence();
				yield return new WaitUntil(sequence.EndSequenceRequirement);

				_executingSequence = false;
			}

			_executingStack = false;
			_executingSequence = false;
		}
		private void InterrupExecutionSequence()
		{
			StopCoroutine("ProcessSequence");
			_executingSequence = false;
		}

	}
	public class SequenceStack
	{
		public SequenceStack(IStackExecutableSequence[] sequenceArray)
		{
			int length = sequenceArray.Length;
			for (int sequenceItemIndex = 0; sequenceItemIndex < length; sequenceItemIndex++)
			{
				_sequenceQueue.Enqueue(sequenceArray[sequenceItemIndex]);
			}
		}
		private Queue<IStackExecutableSequence> _sequenceQueue = new Queue<IStackExecutableSequence>();
		public int QueueCount() => _sequenceQueue.Count;
		public bool IsQueueEmpty() => _sequenceQueue.Count == 0;
		public IStackExecutableSequence Unstack() => _sequenceQueue.Dequeue();
	}
	public interface IStackExecutableSequence
	{
		bool Initialized { get; }
		Func<bool> EndSequenceRequirement { get; }
		Action ExecutionSequence { get; }

		void ExecuteCurrentSequence();
	}

	public class BaseStackExecutableSequence : AbstractStackExecutableSequence
	{
		public BaseStackExecutableSequence(Action sequenceAction, Func<bool> endSequenceRequirement) : base(sequenceAction)
		{
			EndSequenceRequirement = endSequenceRequirement;
		}
	}

	public class SecondDelayExecutableSequence : AbstractStackExecutableSequence
	{
		private float SecondsDelay { get; }
		private bool _delayDone;
		public SecondDelayExecutableSequence(Action sequenceAction, float secondsDelay) : base(sequenceAction)
		{
			SecondsDelay = secondsDelay;
			EndSequenceRequirement = () => this._delayDone;

			OnExecute += DelayEventHandler;
		}
		private void DelayEventHandler(IStackExecutableSequence sequence)
		{
			StackSequenceProcessor.GetMonoBehaviour().StartCoroutine(StartDelay());
		}
		private IEnumerator StartDelay()
		{
			_delayDone = false;

			yield return new WaitForSeconds(SecondsDelay);
			
			OnExecute -= DelayEventHandler;
			_delayDone = true;
		}
	}

	public abstract class AbstractStackExecutableSequence : IStackExecutableSequence
	{
		public delegate void ExecutableSequenceEventHandler(IStackExecutableSequence sequence);

		public bool Initialized { get; protected set; }
		public Func<bool> EndSequenceRequirement { get; protected set; }
		public Action ExecutionSequence { get; protected set; }
		public event ExecutableSequenceEventHandler OnExecute;

		public AbstractStackExecutableSequence(Action sequenceAction)
		{
			ExecutionSequence = sequenceAction;
		}

		public void CheckInitialization()
		{
			Initialized = ExecutionSequence != null && EndSequenceRequirement != null;
		}

		public void ExecuteCurrentSequence()
		{
			CheckInitialization();
			if (!Initialized)
			{
				Debug.LogError($"Current sequence is not initialized properly, please create a new sequence using 'new' passing non-null parameters : {this}");
				return;
			}

			// Ejecuto la acción
			ExecutionSequence();

			// Comunico el evento si existe
			if (OnExecute != null) OnExecute(this);
		}
	}
}