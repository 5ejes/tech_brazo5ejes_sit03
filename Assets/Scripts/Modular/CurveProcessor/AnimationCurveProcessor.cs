﻿using NSScriptableValues;
using UnityEngine;

namespace Modular.CurveProcessor
{
	public class AnimationCurveProcessor : AbstractCurveProcessor, IValue<AnimationCurve>
	{

		[Header("Settings")]
		[SerializeField] private AnimationCurve _curve;

		[Header("Debug")]
		[SerializeField] private float _curvePercent;

		public AnimationCurve Value
		{
			get => _curve;
			set => _curve = value;
		}

		[ContextMenu("Print Percent Curve")]
		private void PrintCurvePercent()
		{
			var curveValue = _curve.Evaluate(_curvePercent);
			float value = Mathf.LerpUnclamped(MinValue, MaxValue, curveValue);

			Debug.Log($"The value is -> {value}", this);
		}
	}
}