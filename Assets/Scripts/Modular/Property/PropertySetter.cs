﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using NSUtilities;
using UnityEngine;
using static NSScriptableEvent.ScriptableEventUnityObject;
using TypeObject = UnityEngine.Object;

namespace Modular.Property
{

	public class PropertySetter : MonoBehaviour
	{
		[Header("Settings")]
		[SerializeField] private TypeObject _objectValue;
		[SerializeField] private string _parseType;
		[SerializeField] private string _propertyName;

		[SerializeField] private UnityObjectEvent _onValueSet;

		public TypeObject ObjectValue { get => _objectValue; set => _objectValue = value; }
		public string ParseType { get => _parseType; set => _parseType = value; }
		public string PropertyName { get => _propertyName; set => _propertyName = value; }

		private PropertyInfo _property;

		public void SetValue(TypeObject value)
		{
			// Defino la propiedad local 
			DefineProperty();

			// Si la propiedad existe
			if (_property != null)
			{
				// Obtengo el tipo de la propiedad
				var propertyType = _property.GetType();

				// Cambio el objeto entregado en el tipo compatible con la propiedad
				var convertedValue = Convert.ChangeType(value, propertyType);

				// Si el valor existe
				if (convertedValue != null)
				{
					// Si el valor de la propiedad puede ser escrito
					if (_property.CanWrite)
					{
						// Transmito el valor a la propiedad
						_property.SetValue(ObjectValue, convertedValue);

						// Transmito el valor a través del evento
						_onValueSet.WrapInvoke(value);
					}
					else
					{
						// Si el valor no puede ser escrito
						ErrorLog($"The property can't be written");
						return;
					}
				}
				else
				{
					// Si el valor no existe o no pudo ser convertido
					ErrorLog($"The value isn't compatible, can't be converted to {propertyType}");
					return;
				}
			}
			else
			{
				// Si la propiedad no existe
				ErrorLog($"The property is null at value set time");
				return;
			}
		}

		/// <summary>
		/// Define la propiedad según los parámetros
		/// </summary>
		private void DefineProperty()
		{
			// Si la propiedad es nula
			if (_property == null)
			{
				// Defino la propiedad
				var typeToParse = Type.GetType(ParseType);

				// Si el tipo no es nulo
				if (typeToParse != null)
				{
					// Obtengo la propiedad

					// Si el nombre no está vacio ni es nulo
					if (!string.IsNullOrEmpty(PropertyName))
					{
						// Busco la propiedad
						_property = typeToParse.GetProperty(PropertyName);

						// Si la propiedad sigue siendo nula
						if (_property == null)
						{
							// Lanzo un error
							ErrorLog($"The property {PropertyName} couldn't be found");
							return;
						}
					}
					else
					{
						// Si el nombre de la propiedad no está definido
						ErrorLog($"The property name string isn't defined");
						return;
					}
				}
				else
				{
					// Si el tipo es nulo
					ErrorLog($"The type {ParseType} couldn't be found");
					return;
				}
			}
		}

		private void ErrorLog(string msg)
		{
			// Lanzo un error

			// Defino el mensaje de error
			var selectedMsg = (!string.IsNullOrEmpty(msg)) ? msg : "An error has been reported";

			// Logueo el mensaje seleccionado
			Debug.LogError($"{selectedMsg} : {gameObject}");
		}
	}
}