﻿using UnityEngine;

namespace Modular
{
	public class TimeController : MonoBehaviour
	{
		[SerializeField] private float _timeValue = 1f;
		private float _currentTimeValue = -100f;

		private void Update(){
			if(HasTimeValueChanged())
			{
				_currentTimeValue = _timeValue;
				UpdateTimeValue(_currentTimeValue);
			}
		}
		private bool HasTimeValueChanged() => _timeValue != _currentTimeValue;

		private void UpdateTimeValue(float timeValue)
		{
			Time.timeScale = timeValue;
		}

	}
}
