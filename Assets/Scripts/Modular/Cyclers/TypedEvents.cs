﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Modular
{

	[System.Serializable]
	public class IntEvent : UnityEvent<int> { }

	[System.Serializable]
	public class FloatEvent : UnityEvent<float> { }

	[System.Serializable]
	public class Vector3Event : UnityEvent<Vector3> { }

	[System.Serializable]
	public class Vector2Event : UnityEvent<Vector2> { }

	[System.Serializable]
	public class BoolEvent : UnityEvent<bool> { }

	[System.Serializable]
	public class GameObjectEvent : UnityEvent<GameObject> { }

	[System.Serializable]
	public class ColliderEvent : UnityEvent<Collider> { }

	[System.Serializable]
	public class CollisionEvent : UnityEvent<Collision> { }

	[System.Serializable]
	public class TransformEvent : UnityEvent<Transform> { }

	[System.Serializable]
	public class AnimationCurveEvent : UnityEvent<AnimationCurve> { };

}