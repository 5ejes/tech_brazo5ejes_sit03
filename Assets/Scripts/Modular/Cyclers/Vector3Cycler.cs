﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Modular.Cyclers
{

	public class Vector3Cycler : Cycler<Vector3>
	{
		[Header("Settings")]
		[SerializeField] private Vector3Event _dataPass = new Vector3Event();

		public override UnityEvent<Vector3> OnDataPass
		{
			get
			{
				return _dataPass;
			}
		}
	}
}