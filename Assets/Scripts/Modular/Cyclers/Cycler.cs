﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Modular.Cyclers
{
	public abstract class Cycler<T> : MonoBehaviour
	{

		[Header("Automation")]
		[Header("Settings")]
		[SerializeField] private bool _playOnStart = false;
		[SerializeField] private float _delayBeforeStart = 1f;
		[SerializeField] private float _interval = 1f;
		[SerializeField] private int _cycleCountLimit = 0;
		[Space]
		[SerializeField] private List<T> _dataset = new List<T>();
		public List<T> Data
		{
			get => _dataset;
			set => _dataset = value;
		}

		public abstract UnityEvent<T> OnDataPass
		{
			get;
		}

		public int CurrentCallIndex => _index;

		private int _index;
		private bool _CO_CycleData = false;

		private void Start()
		{
			if (_playOnStart)
				StartCoroutine(ExecutedDelay(_delayBeforeStart, CycleData()));
		}

		private IEnumerator ExecutedDelay(float delay, IEnumerator yieldedCoroutine)
		{
			yield return new WaitForSeconds(delay);

			// Si el callback no es nulo
			if (yieldedCoroutine != null)
			{
				// Invoco el callback
				StartCoroutine(yieldedCoroutine);
			}
		}

		private IEnumerator CycleData()
		{
			// Si ya se está ejecutando
			if (_CO_CycleData) yield break;

			// Marco la rutina como activa
			_CO_CycleData = true;

			// Defino el número de ciclos transcurridos
			int currentCyclesElapsed = 0;

			// Defino la cláusula de finalización del bucle
			// Si la cuenta limite para los ciclos es un número diferente de 0, defino la clásula como el numero de ciclos transcurridos sea menor a los ciclos limite definidos
			// Si no, entonces no hay un limite, se ejecuta indefinidamente
			bool finalized = (Mathf.Abs(_cycleCountLimit) > 0) ? currentCyclesElapsed < _cycleCountLimit : true;

			// Mientras se cumpla la cláusula
			while (finalized)
			{
				// Si es el primer ciclo transcurrido
				if (currentCyclesElapsed == 0)
					// Decremento el dato en 1 para asegurar que el primer dato sea n
					// Next devuelve el dato siguiente al actual : n+1
					_index--;

				// Obtengo el dato
				var currentData = Next();

				// Si el evento no es nulo, invoco el evento
				if (OnDataPass != null && OnDataPass.GetPersistentEventCount() > 0)
					OnDataPass.Invoke(currentData);
				else
					Debug.LogWarning($"The event is empty, but even tho, the coroutine is executing : {gameObject.name}");

				// Aumento el ciclo transcurrido
				currentCyclesElapsed++;

				yield return new WaitForSeconds(_interval);

				// Evalúo la clausula
				finalized = (Mathf.Abs(_cycleCountLimit) > 0) ? currentCyclesElapsed < _cycleCountLimit : true;
			}

			// Marco la rutina como finalizada
			_CO_CycleData = false;

		}

		public void ExecutePassNextData()
		{
			ExecutePass(Next());
		}

		public void ExecutePass(T invokeArgument)
		{

			if (OnDataPass != null)
				OnDataPass.Invoke(invokeArgument);
		}

		public void ExecutePassPreviousData()
		{
			ExecutePass(Previous());
		}

		public void ForceSetIndex(int value)
		{
			// Fuerza el valor del índice al valor especificado
			_index = value;
		}

		public T Next()
		{
			// Aumento el indice
			_index++;
			return GetData(_index, _dataset);
		}

		public T Previous()
		{
			// Disminuyo el indice
			_index--;
			return GetData(_index, _dataset);
		}

		private T GetData(int index, List<T> dataset)
		{
			// Si hay al menos un elemento
			if (dataset.Count > 0)
			{
				// Si hay más de 1 elemento
				if (dataset.Count > 1)
				{
					// Limito el índice al tamaño del arreglo
					int listIndex = index % dataset.Count;

					// retorno el elemento actual
					return dataset[listIndex];
				}
				else
				{
					// Si solo hay un elemento

					// Retorno el primer y único índice
					return dataset[0];
				}
			}
			else
			{
				// Si no hay ningun elemento en la lista de datos

				// Logueo un mensaje de advertencia en pantalla
				Debug.LogWarning($"The dataset is currently empty, no data will be returned : {gameObject.name}");

				// Devuelvo el valor por defecto T
				return default(T);
			}
		}
	}
}