﻿using System.Collections;
using System.Collections.Generic;
using NSUtilities;
using UnityEngine;
using static NSScriptableEvent.ScriptableEventColor;

namespace Modular
{
	public class SimpleColorSet : MonoBehaviour
	{
		[SerializeField] private Color color = Color.white;
		[SerializeField] private ColorEvent _onColorSet;

		public void Set()
		{
			// Ejecuto el evento con el parámetro dado
			_onColorSet.WrapInvoke(color);
		}
	}
}
