﻿using System;
using System.Collections;
using System.Collections.Generic;
using Modular;
using NSBrazo5Ejes.NSDraggingBehaviour;
using UnityEngine;
using UnityEngine.Events;

public class CylinderOutOfBoundsEvaluator : MonoBehaviour
{

	[Header("References")]
	[SerializeField] private DragMoveComponent _dragMoveComponent = null;
	[Header("Values")]
	[SerializeField] private Transform[] _watchValues = new Transform[0];
	[Header("Settings")]
	[SerializeField] private Transform _minValue = null;
	[SerializeField] private Transform _maxValue = null;
	[Space]
	[SerializeField] private float _timeout = 2f;
	[Header("Constraints")]
	[SerializeField] private bool _ignoreX = false;
	[SerializeField] private bool _ignoreY = false;
	[SerializeField] private bool _ignoreZ = false;

	[Header("Update Settings")]
	[SerializeField] private bool _allowUpdate = true;
	[SerializeField] private float _updateSeconds = 0f;
	[Header("Events")]
	[SerializeField] private TransformEvent _onValueOutOfBounds = null;
	[SerializeField] private UnityEvent _onCylinderOutOfBoundsTimeout = null;

	private List<Transform> _outOfBoundsValues = new List<Transform>();

	private float _nextUpdateTime = 0f;

	public Transform[] WatchValues { get => _watchValues; set => _watchValues = value; }

	// private void Start()
	// {
	//   // Defino la longitud de los valores a vigilar
	//   int length = _watchValues.Length;
	// }

	private void Update()
	{

		// Si no puedo actualizar, retorno
		if (!CanUpdate()) return;

		// Si los segundos de actualización son mayores a 0
		if (_updateSeconds > 0f)
		{
			// Actualizo el tiempo de actualización
			_nextUpdateTime = Time.time + _updateSeconds;
		}

		// Ejecuto la evaluación
		WatchOutOfBounds(_watchValues);

		return;
	}

	private void WatchOutOfBounds(Transform[] watchValues)
	{
		// Si los valores están definidos
		if (watchValues == null) return;

		// Recorro el arreglo de valores en los parámetros
		int length = watchValues.Length;
		for (int i = 0; i < length; i++)
		{
			// Obtengo el valor actual a comparar
			Transform currentValue = watchValues[i];

			// Si el valor actual no se ha marcado como activo, continuo
			if (!currentValue.gameObject.activeInHierarchy) continue;

			// Defino si el valor actual está fuera de los límites
			bool isValueOutOfBounds = _outOfBoundsValues.Contains(currentValue);

			// Si no está dentro de los límites de la situación
			if (!IsInside(currentValue.position, _minValue, _maxValue))
			{

				// Si el objeto no ha sido marcado como fuera de los límites
				if (!isValueOutOfBounds)
				{
					// Agrego el objeto a la lista de objetos fuera de los límites
					_outOfBoundsValues.Add(currentValue);

					// Inicio un llamado con el valor
					StartCoroutine(WatchSeconds(currentValue, _timeout));

					// Comunico el evento
					_onValueOutOfBounds?.Invoke(currentValue);
				}

			}
			else
			{
				// El valor se encuentra en los límites

				// Si el objeto se encuentra en la lista
				if (isValueOutOfBounds)
					_outOfBoundsValues.Remove(currentValue);

			}
		}

	}

	private IEnumerator WatchSeconds(Transform currentValue, float timeout)
	{
		// Si el elemento actual no existe, aborto la operación
		if (currentValue == null) yield break;

		// TODO: Acceso repetido a diferentes componentes, refactorizar función

		// Obtengo el componente draggable
		DraggableComponent draggable = currentValue.GetComponent<DraggableComponent>();
		if (_dragMoveComponent != null && !_dragMoveComponent.IsOnDraggedComponentsList(draggable)) yield break;

		// Determino si el valor actual está dentro de los elementos fuera de los límites
		if (!_outOfBoundsValues.Contains(currentValue)) yield break;

		// Si los segundos son mayores a 0
		if (timeout > 0f)
		{
			// Espero los segundos especificados
			yield return new WaitForSeconds(timeout);
		}

		yield return null;

		// Si el componente de raycasting está definido
		if (_dragMoveComponent != null)
		{

			// Si el elemento fue resgistrado anteriormente sobre el plano
			// De la situación (Válido para operar)

			// Verifico si el draggable se encuentra sobre el plano
			if (_dragMoveComponent.IsOnDraggedComponentsList(draggable))
			{
				// Si está en la lista

				// TODO: Cambiar el orden del IF y mover este elemento por fuera del IF actual para que el ELSE quede
				// Como condición de la misma evaluación

				// Si el objeto está por dentro de los límites después del timeout, retorno
				if (!_outOfBoundsValues.Contains(currentValue)) yield break;
				
				// Invoco el evento
				_onCylinderOutOfBoundsTimeout?.Invoke();

				// Remuevo la referencia de la lista
				_outOfBoundsValues.Remove(currentValue);
			}

		}
		else if (_outOfBoundsValues.Contains(currentValue))
		{
			// Evaluo nuevamente si los elementos están por fuera de los límites
			// Llamo un evento
			_onCylinderOutOfBoundsTimeout?.Invoke();

			// Remuevo el objeto de la lista
			_outOfBoundsValues.Remove(currentValue);
		}
	}

	private bool CanUpdate()
	{
		// Retorno si puedo actualizar, 
		// y el tiempo es mayor al tiempo de actualización 
		// o los segundos son menores o iguales a 0
		return _allowUpdate && (Time.time >= _nextUpdateTime || _updateSeconds <= 0f);
	}

	private bool IsInside(Vector3 worldPosition, Transform min, Transform max)
	{
		if (min == null || max == null)
		{
			Debug.LogError("Bounds not defined", this);
			return true;
		}

		// Evaluo cada dimensión de los vectores por el mínimo y el máximo
		bool insideX = _ignoreX ? true : IsInsideBounds(worldPosition.x, min.position.x, max.position.x);
		bool insideY = _ignoreY ? true : IsInsideBounds(worldPosition.y, min.position.y, max.position.y);
		bool insideZ = _ignoreZ ? true : IsInsideBounds(worldPosition.z, min.position.z, max.position.z);

		// Si la evaluación retorna verdadero (Los valores se encuentran dentro de las dimensiones determinadas)
		return insideX && insideY && insideZ;
	}

	private bool IsInsideBounds(float point, float minPoint, float maxPoint)
	{
		// Hallo el mínimo y el máximo
		float min = Mathf.Min(minPoint, maxPoint);
		float max = Mathf.Max(minPoint, maxPoint);

		// Si el punto es mayor o igual al mínimo y menor o igual al máximo
		return (point >= min && point <= max);
	}
}