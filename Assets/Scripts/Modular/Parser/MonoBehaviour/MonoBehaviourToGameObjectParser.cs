﻿using NSUtilities;
using UnityEngine;

namespace Modular.Parser
{
	public class MonoBehaviourToGameObjectParser : GenericParser<MonoBehaviour>
	{
		[SerializeField] private GameObjectEvent _onResultValue;

		public GameObjectEvent OnResultValue { get => _onResultValue; set => _onResultValue = value; }

		public override void Parse(MonoBehaviour valueToParse)
		{
			OnParseAction.WrapInvoke();
			
			if (valueToParse == null)
			{
				Debug.LogError($"The value to parse is null : {this}", this);
				return;
			}

			// Convierto el valor
			OnResultValue.WrapInvoke(valueToParse.gameObject);
		}
	}
}
