﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Clase que define los límites de un transform con dos referencias transform
/// </summary>
public class TransformLimits : MonoBehaviour
{
	[Header("Límites")]

	[Tooltip("Ambos límites deben ser hijos del TransformLimits")]
	[SerializeField] private Transform _maxLimit;
	[Tooltip("Ambos límites deben ser hijos del TransformLimits")]
	[SerializeField] private Transform _minLimit;

	/// <summary>
	/// Límite mínimo definido
	/// </summary>
	/// <value></value>
	public Transform MinLimit { get => _minLimit; set => _minLimit = value; }
	/// <summary>
	/// Límite máximo definido
	/// </summary>
	/// <value></value>
	public Transform MaxLimit { get => _maxLimit; set => _maxLimit = value; }

	/// <summary>
	/// Obtiene la posición mínima del mínimo o el máximo
	/// </summary>
	/// <returns></returns>
	public float GetLimitHeight()
	{
		// Retorno cualquiera de ambas que sea mínimo
		return Mathf.Min(MaxLimit.position.y, MinLimit.position.y);
	}

	/// <summary>
	/// Obtiene el offset hasta min, tomando en cuenta su rotación
	/// </summary>
	/// <returns></returns>
	public Vector3 GetOffsetToMin()
	{
		// Obtengo el offset hasta min limit
		var offset = MinLimit.position - GetCenterVector();

		// Roto el offset y lo retorno
		return transform.rotation * offset;
	}

	/// <summary>
	/// Obtiene el offset hasta max, tomando en cuenta su rotación
	/// </summary>
	/// <returns></returns>
	public Vector3 GetOffsetToMax()
	{
		// Obtengo el offset hasta min limit
		var offset = MaxLimit.position - GetCenterVector();

		// Roto el offset y lo retorno
		return transform.rotation * offset;
	}

	/// <summary>
	/// Obtengo los límites
	/// </summary>
	/// <returns>Un arreglo de vector3 con los límites</returns>
	public Vector3[] GetLimitBounds()
	{
		// Almaceno las posiciones de cada punto
		var min = MinLimit.localPosition;
		var max = MaxLimit.localPosition;

		min.Scale(transform.localScale);
		max.Scale(transform.localScale);

		// Obtengo el quaternion de la rotación
		var rotation = transform.rotation;

		// Defino la posición
		var position = transform.position;

		// Retorno todos los límites
		return new Vector3[]
		{
			rotation * new Vector3(min.x, min.y, min.z) + position,
			rotation * new Vector3(max.x, min.y, min.z) + position,
			rotation * new Vector3(min.x, max.y, min.z) + position,
			rotation * new Vector3(max.x, max.y, min.z) + position,
			rotation * new Vector3(min.x, min.y, max.z) + position,
			rotation * new Vector3(max.x, min.y, max.z) + position,
			rotation * new Vector3(min.x, max.y, max.z) + position,
			rotation * new Vector3(max.x, max.y, max.z) + position
		};
	}

	/// <summary>
	/// Obtiene los límites locales
	/// </summary>
	/// <returns>Un Arreglo de vectores locales</returns>
	public Vector3[] GetLocalLimitBounds()
	{
		// Almaceno las posiciones de cada punto
		var min = MinLimit.localPosition;
		var max = MaxLimit.localPosition;

		min.Scale(transform.localScale);
		max.Scale(transform.localScale);

		// Obtengo el quaternion de la rotación
		var rotation = transform.rotation;

		// Defino la posición
		var position = transform.position;

		// Retorno todos los límites
		return new Vector3[]
		{
			rotation * new Vector3(min.x, min.y, min.z),
			rotation * new Vector3(max.x, min.y, min.z),
			rotation * new Vector3(min.x, max.y, min.z),
			rotation * new Vector3(max.x, max.y, min.z),
			rotation * new Vector3(min.x, min.y, max.z),
			rotation * new Vector3(max.x, min.y, max.z),
			rotation * new Vector3(min.x, max.y, max.z),
			rotation * new Vector3(max.x, max.y, max.z)
		};
	}

	/// <summary>
	/// Obtiene los límites ignorando la dimension Y
	/// </summary>
	/// <returns>Un arreglo de vectores límite</returns>
	public Vector3[] GetLimitBoundsXZ()
	{
		// Genero los datos del transform
		var position = transform.position;
		var rotation = transform.rotation;
		var scale = transform.localScale;

		// Obtengo las posiciones del min y el max
		var min = MinLimit.localPosition;
		var max = MaxLimit.localPosition;

		// Escalo los vectores
		min.Scale(scale);
		max.Scale(scale);

		// Obtengo la altura mínima
		var minHeight = GetMinFromVector3Index(1, min, max);


		// Genero todos los límites
		return new Vector3[]
		{
			rotation * new Vector3(min.x, minHeight, min.z) + position,
			rotation * new Vector3(max.x, minHeight, min.z) + position,
			rotation * new Vector3(min.x, minHeight, max.z) + position,
			rotation * new Vector3(max.x, minHeight, max.z) + position
		};
	}

	/// <summary>
	/// Obtengo los límites locales ignorando el eje Y
	/// </summary>
	/// <returns>Un arreglo de Vectores representando los límites locales</returns>
	public Vector3[] GetLocalLimitBoundsXZ()
	{
		// Genero los datos del transform
		var rotation = transform.rotation;
		var scale = transform.localScale;

		// Obtengo las posiciones del min y el max
		var min = MinLimit.localPosition;
		var max = MaxLimit.localPosition;

		// Escalo los vectores
		min.Scale(scale);
		max.Scale(scale);

		// Obtengo el mínimos de las dos posiciones
		var minHeight = GetMinFromVector3Index(1, min, max);

		// Genero todos los límites
		return new Vector3[]
		{
			rotation * new Vector3(min.x, minHeight, min.z),
			rotation * new Vector3(max.x, minHeight, min.z),
			rotation * new Vector3(min.x, minHeight, max.z),
			rotation * new Vector3(max.x, minHeight, max.z)
		};
	}

	public Vector3[] GetLocalCenterLimitBoundsXZ()
	{

		// Genero los datos del transform
		var rotation = transform.rotation;
		var scale = transform.localScale;

		// Obtengo las posiciones del min y el max
		var min = MinLimit.localPosition;
		var max = MaxLimit.localPosition;

		// Escalo los vectores
		min.Scale(scale);
		max.Scale(scale);

		// Obtengo el mínimos de las dos posiciones
		var minHeight = GetMinFromVector3Index(1, min, max);

		// Genero todos los límites
		return new Vector3[]
		{
			rotation * new Vector3(min.x, minHeight, 0f),
			rotation * new Vector3(max.x, minHeight, 0f),
			rotation * new Vector3(0f, minHeight, min.z),
			rotation * new Vector3(0f, minHeight, max.z)
		};
	}

	/// <summary>
	/// Genero todos los límites centrales en X además de Z
	/// </summary>
	/// <returns>Un arreglo con todos los puntos generados</returns>
	public Vector3[] GetCompleteLocalBoundsXZ()
	{
		// Genero los datos del transform
		var rotation = transform.rotation;
		var scale = transform.localScale;

		// Obtengo las posiciones del min y el max
		var min = MinLimit.localPosition;
		var max = MaxLimit.localPosition;

		// Escalo los vectores
		min.Scale(scale);
		max.Scale(scale);

		// Obtengo el mínimos de las dos posiciones
		var minHeight = GetMinFromVector3Index(1, min, max);

		// Genero todos los límites
		return new Vector3[]
		{
			rotation * new Vector3(min.x, minHeight, 0f),
			rotation * new Vector3(max.x, minHeight, 0f),
			rotation * new Vector3(0f, minHeight, min.z),
			rotation * new Vector3(0f, minHeight, max.z),
			rotation * new Vector3(min.x, minHeight, min.z),
			rotation * new Vector3(max.x, minHeight, min.z),
			rotation * new Vector3(min.x, minHeight, max.z),
			rotation * new Vector3(max.x, minHeight, max.z)
		};
	}
	/// <summary>
	/// Genero todos los límites centrales en X además de Z
	/// </summary>
	/// <returns>Un arreglo con todos los puntos generados</returns>
	public Vector3[] GetCompleteLocalBounds(Quaternion rotation = default)
	{
		// Genero los datos del transform
		var scale = transform.localScale;

		// Obtengo las posiciones del min y el max
		var min = MinLimit.localPosition;
		var max = MaxLimit.localPosition;

		// Escalo los vectores
		min.Scale(scale);
		max.Scale(scale);

		// Obtengo el mínimos de las dos posiciones
		var minHeight = GetMinFromVector3Index(1, min, max);
		var maxHeight = GetMaxFromVector3Index(1, min, max);

		// Genero todos los límites
		return new Vector3[]
		{
			rotation * new Vector3(min.x, minHeight, 0f),
			rotation * new Vector3(max.x, minHeight, 0f),
			rotation * new Vector3(0f, minHeight, min.z),
			rotation * new Vector3(0f, minHeight, max.z),
			rotation * new Vector3(min.x, minHeight, min.z),
			rotation * new Vector3(max.x, minHeight, min.z),
			rotation * new Vector3(min.x, minHeight, max.z),
			rotation * new Vector3(max.x, minHeight, max.z),
			rotation * new Vector3(min.x, maxHeight, 0f),
			rotation * new Vector3(max.x, maxHeight, 0f),
			rotation * new Vector3(0f, maxHeight, min.z),
			rotation * new Vector3(0f, maxHeight, max.z),
			rotation * new Vector3(min.x, maxHeight, min.z),
			rotation * new Vector3(max.x, maxHeight, min.z),
			rotation * new Vector3(min.x, maxHeight, max.z),
			rotation * new Vector3(max.x, maxHeight, max.z),
			rotation * new Vector3(max.x, minHeight, max.z),
			rotation * new Vector3(min.x, 0f, 0f),
			rotation * new Vector3(max.x, 0f, 0f),
			rotation * new Vector3(0f, 0f, min.z),
			rotation * new Vector3(0f, 0f, max.z),
			rotation * new Vector3(min.x, 0f, min.z),
			rotation * new Vector3(max.x, 0f, min.z),
			rotation * new Vector3(min.x, 0f, max.z),
			rotation * new Vector3(max.x, 0f, max.z)
		};
	}

	private float GetMinFromVector3Index(int dimensionIndex, params Vector3[] argBounds)
	{

		// Si supera las dimensiones establecidas por el Vector3
		if (dimensionIndex > 3) return 0f;

		// Defino la variable para el valor mínimo
		var minValue = float.MaxValue;

		// Recorro cada posición
		foreach (Vector3 position in argBounds)
		{
			// Evaluo el x de cada posición
			minValue = Mathf.Min(position[dimensionIndex], minValue);
		}

		// Retorno el menor en X
		return minValue;
	}

	private float GetMaxFromVector3Index(int dimensionIndex, params Vector3[] argBounds)
	{

		// Si supera las dimensiones establecidas por el Vector3
		if (dimensionIndex > 3) return 0f;

		// Defino la variable para el valor mínimo
		var maxValue = float.MinValue;

		// Recorro cada posición
		foreach (Vector3 position in argBounds)
		{
			// Evaluo el x de cada posición
			maxValue = Mathf.Max(position[dimensionIndex], maxValue);
		}

		// Retorno el menor en X
		return maxValue;
	}

	/// <summary>
	/// Retorna el vector entre el mínimo y el máximo
	/// </summary>
	/// <returns>Vector3 con el vector apuntando de min a max</returns>
	public Vector3 GetMinToMaxDirection()
	{
		return (MaxLimit.position - MinLimit.position);
	}


	/// <summary>
	/// Obtiene el Vector que conforma el centro entre los límites
	/// </summary>
	/// <returns></returns>
	public Vector3 GetCenterVector()
	{
		// Retorno el centro entre los dos vectores
		return Vector3.Lerp(MaxLimit.position, MinLimit.position, 0.5f);
	}



}
