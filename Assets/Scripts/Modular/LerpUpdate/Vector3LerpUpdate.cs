﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Modular.LerpUpdate
{
		[System.Serializable]
	public class Vector3LerpUpdate
	{

		public Vector3LerpUpdate(float timeout, Vector3 current, Vector3 target)
		{
			// Defino los parámetros
			Timeout = timeout;
			Current = current;
			Target = target;
		}
		public float Elapsed { get; private set; } = 0f;
		public float Timeout { get; set; }
		public Vector3 Current
		{
			get => _current; set
			{
				// Defino el valor actual y registro un valor de inicio
				_current = value;
			}
		}
		private Vector3 _current;
		public Vector3 LerpValue
		{
			get
			{
				_lerpValue = UseSLerp
				? Vector3.Slerp(Current, Target, Percent)
				: Vector3.Lerp(Current, Target, Percent);

				return _lerpValue;
			}
		}
		private Vector3 _lerpValue;

		public Vector3 Target { get; set; }
		public float Percent => Elapsed / (float)Timeout;
		public bool UseSLerp { get; set; } = false;

		public void Update(float timeDelta)
		{

			// Si el tiempo transcurrido es menor al tiempo límite
			if (Elapsed >= Timeout) return;

			// Obtengo el valor absoluto
			timeDelta = Mathf.Abs(timeDelta);
			// Aumento el tiempo transcurrido
			Elapsed += timeDelta;

			// Fijo el valor
			Elapsed = Mathf.Clamp(Elapsed, 0, Timeout);
		}

		public void InverseUpdate(float timeDelta)
		{
			// Si el tiempo transcurrido es menor al tiempo límite
			if (Elapsed <= 0) return;

			// Obtengo el valor absoluto
			timeDelta = Mathf.Abs(timeDelta);
			// Aumento el tiempo transcurrido
			Elapsed -= timeDelta;

			// Fijo el valor
			Elapsed = Mathf.Clamp(Elapsed, 0, Timeout);
		}

		public void Reset()
		{
			Elapsed = 0f;
		}

	}
}
