﻿using UnityEngine;
using UnityEngine.Events;

namespace Modular.Switcher
{
	public class IntValueSwitcher : MonoBehaviour
	{
		[System.Serializable] private class SwitchIntStatement
		{
			[SerializeField] private int _switchValue;
			[SerializeField] private IntEvent _response;
			public int SwitchValue { get => _switchValue; set => _switchValue = value; }
			public IntEvent Response { get => _response; set => _response = value; }
		}

		[Header("Switchs")]
		[SerializeField] private SwitchIntStatement[] _statements;

		[Header("Default Response")]
		[SerializeField] private IntEvent _defaultResponse = new IntEvent();

		[Header("Event")]
		[SerializeField] private UnityEvent _endEvaluation = new UnityEvent();


		public void Evaluate(int value)
		{
			// Busco el switch statement con el valor actual
			var foundSwitchStatement = System.Array.Find(_statements, state => state.SwitchValue.Equals(value));

			// Si encontré una respuesta para el valor dado
			if(foundSwitchStatement != null)
			{
				// Invoco la respuesta
				foundSwitchStatement.Response?.Invoke(value);
			}else
			{
				// Si no encontré respuesta, convoco el valor por defecto
				_defaultResponse?.Invoke(value);
			}

			// Inicio el evento de finalización
			_endEvaluation?.Invoke();
		}
	}
}
