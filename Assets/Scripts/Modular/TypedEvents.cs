﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Modular
{

  [System.Serializable]
  public class ErrorDefinitionEvent : UnityEvent<ErrorDefinition> { };

  [System.Serializable]
  public class GrafcetControllerEvent : UnityEvent<grafcetController> { };

  [System.Serializable]
  public class GrafcetOutputInterfaceEvent : UnityEvent<NSInterfazControlPLC.grafcetOutputInterface> { };

}