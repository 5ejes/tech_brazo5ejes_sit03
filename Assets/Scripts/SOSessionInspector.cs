﻿using Modular;
using NSCalificacionSituacion;
using NSUtilities;
using UnityEngine;

public class SOSessionInspector : MonoBehaviour
{
	[SerializeField] private SOSessionData _sessionData;
	[Header("Events")]
	[SerializeField] private IntEvent _onSetAttempts;
	public SOSessionData SessionData
	{
		get
		{
			if (!_sessionData) Debug.LogError($"SOSessionData for this instance, is not defined : {this}", this);
			return _sessionData;
		}
		set => _sessionData = value;
	}

	private float _incrementProtectionTime = 0.5f;
	private float _nextIncrementValidTime = 0f;
	public bool CanIncrement => Time.time > _nextIncrementValidTime;
	public string TimeSituationS => SessionData.TimeSituationString;
	public float TimeSituationF => SessionData.timeSituationFloat;
	public byte QuantityAttempts => SessionData.quantityAttempts;

	public IntEvent OnSetAttempts { get => _onSetAttempts; set => _onSetAttempts = value; }

	public void IncrementAttempts()
	{
		if (SessionData && CanIncrement)
		{
			SessionData.quantityAttempts++;
			OnSetAttempts.WrapInvoke((int)SessionData.quantityAttempts);
			_nextIncrementValidTime = Time.time + _incrementProtectionTime;
		}
	}
	public void DecrementAttempts()
	{
		if (SessionData && CanIncrement)
		{
			SessionData.quantityAttempts--;
			OnSetAttempts.WrapInvoke((int)SessionData.quantityAttempts);
			_nextIncrementValidTime = Time.time + _incrementProtectionTime;
		}
	}

	public void IncrementAttemptsBy(int amount)
	{
		if (SessionData && CanIncrement)
		{
			SessionData.quantityAttempts += (byte)amount;
			OnSetAttempts.WrapInvoke((int)SessionData.quantityAttempts);
			_nextIncrementValidTime = Time.time + _incrementProtectionTime;
		}
	}

	public void DecrementAttemptsBy(int amount)
	{
		if (SessionData && CanIncrement)
		{
			SessionData.quantityAttempts -= (byte)amount;
			OnSetAttempts.WrapInvoke((int)SessionData.quantityAttempts);
			_nextIncrementValidTime = Time.time + _incrementProtectionTime;
		}
	}

	public void SetAttempts(int value)
	{
		if (SessionData && CanIncrement)
		{
			SessionData.quantityAttempts = (byte)value;
			OnSetAttempts.WrapInvoke((int)SessionData.quantityAttempts);
			_nextIncrementValidTime = Time.time + _incrementProtectionTime;
		}
	}

	public void ResetAttempts()
	{
		if (SessionData && CanIncrement)
		{
			SessionData.quantityAttempts = byte.MinValue;
			OnSetAttempts.WrapInvoke((int)SessionData.quantityAttempts);
			_nextIncrementValidTime = Time.time + _incrementProtectionTime;
		}
	}

  public void UpdateAttemps()
  {
    // Actualizo los valores de los intentos
    if(SessionData)
    {
      OnSetAttempts.WrapInvoke((int)SessionData.quantityAttempts);
    }
  }

}
