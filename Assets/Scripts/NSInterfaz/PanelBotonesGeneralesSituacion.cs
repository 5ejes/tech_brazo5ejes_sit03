﻿#pragma warning disable 0649
using System;
using System.Collections;
using NSAvancedUI;
using NSBoxMessage;
using NSCuaderno;
using NSSceneLoading;
using NSScriptableEvent;
using NSSituacionGeneral;
using NSTraduccionIdiomas;
using NSUtilities;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace NSInterfaz
{
	public class PanelBotonesGeneralesSituacion : AbstractSingletonPanelUIAnimation<PanelBotonesGeneralesSituacion>
	{
		[SerializeField] private Cuaderno refCuaderno;

		[SerializeField] private ScriptableEvent seExitSituation;

		[SerializeField] private ScriptableEvent seOpenUIPlc;

		[SerializeField] private ScriptableEvent seRestartSituation;

		[Header("Events")]
		[SerializeField] private UnityEvent _onExitSituationConfirm;
		[SerializeField] private UnityEvent _onExitSituationCancel;
		[SerializeField] private UnityEvent _onRestartSituationConfirm;
		[SerializeField] private UnityEvent _onRestartSituationCancel;

		private IEnumerator Start()
		{
			//PanelBienvenidaSituacion.Instance.ShowPanel();
			yield return new WaitForSeconds(1);
			refCuaderno.InitCuaderno();
		}

		public void OnButtonBeginEvaluation()
		{
			BoxMessageManager.Instance.CreateBoxMessageDecision(DiccionarioIdiomas.Instance.Traducir("mensajeComenzarEvaluacion"), DiccionarioIdiomas.Instance.Traducir("TextCancelar"), DiccionarioIdiomas.Instance.Traducir("TextComenzar"), ConfirmBeginEvaluation);
		}

		public void OnButtonReiniciarSituacion()
		{
			BoxMessageManager.Instance.CreateBoxMessageDecision(DiccionarioIdiomas.Instance.Traducir("mensajeReiniciarPractica"), DiccionarioIdiomas.Instance.Traducir("TextCancelar"), DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"), RestartSituationConfirm, RestartSituationCancel);
		}

		public void OnButtonReturnBackSituation()
		{
			BoxMessageManager.Instance.CreateBoxMessageDecision(DiccionarioIdiomas.Instance.Traducir("mensajeAbandonarPractica"), DiccionarioIdiomas.Instance.Traducir("TextCancelar"), DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"), ConfirmExitSituation, CancelExitSituation);
		}

		public void OnButtonOpenPLC()
		{
			seOpenUIPlc.ExecuteEvent();
		}

		private void RestartSituationConfirm()
		{
			if (seRestartSituation)
				seRestartSituation.ExecuteEvent();
			_onRestartSituationConfirm.WrapInvoke();
		}
		private void RestartSituationCancel()
		{
			_onRestartSituationCancel.WrapInvoke();
		}

		private void ConfirmReturnBackSituation()
		{
			if (seExitSituation)
				seExitSituation.ExecuteEvent();
		}

		private void ConfirmExitSituation()
		{
			_onExitSituationConfirm.WrapInvoke();
		}

		private void CancelExitSituation()
		{
			_onExitSituationCancel.WrapInvoke();
		}

		private void ConfirmBeginEvaluation()
		{
			PanelEvaluacion.Instance.ShowPanel(true);
		}
	}
}