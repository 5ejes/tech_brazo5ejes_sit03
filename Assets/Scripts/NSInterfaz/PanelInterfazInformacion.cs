﻿using System;
using NSAvancedUI;
using UnityEngine;
using UnityEngine.UI;

namespace NSInterfaz
{
    public class PanelInterfazInformacion : AbstractSingletonPanelUIAnimation<PanelInterfazInformacion>
    {
        #region members

        [SerializeField] private ToggleGroup toggleGroupButtons;

        [SerializeField] private Toggle toggleSituacion;
        
        [SerializeField] private Toggle toggleProcedimiento;
        
        [SerializeField] private Toggle toggleEcuaciones;
        #endregion

        private void Awake()
        {
            toggleSituacion.group = toggleGroupButtons;
            toggleProcedimiento.group = toggleGroupButtons;
            toggleEcuaciones.group = toggleGroupButtons;
        }

        private void OnEnable()
        {
            toggleSituacion.isOn = true;
            toggleProcedimiento.isOn = false;
            toggleEcuaciones.isOn = false;
        }

        #region public methods
        
        #endregion
    }
}