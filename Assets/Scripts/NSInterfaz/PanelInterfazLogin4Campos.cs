﻿#pragma warning disable 0649
using NSAvancedUI;
using NSBoxMessage;
using NSSeguridad;
using NSTraduccionIdiomas;
using System;
using NSScriptableEvent;
using UnityEngine;
using UnityEngine.UI;
using NSSingleton;
using TMPro;

namespace NSInterfaz
{
	public class PanelInterfazLogin4Campos : AbstractSingletonPanelUIAnimation<PanelInterfazLogin4Campos>
	{
		#region members

		[SerializeField] private TMP_InputField inputFieldNombre;

		[SerializeField] private TMP_InputField inputFieldCurso;

		[SerializeField] private TMP_InputField inputFieldIdCurso;

		[SerializeField] private TMP_InputField inputFieldInstituto;

		[SerializeField] private ScriptableEventInt seOnLogin;

		[SerializeField] private ScriptableEvent seOnInputsLogin4InputsCorrect;

		[SerializeField] private SOSecurityData refSoSecurityData;

		[SerializeField] private SOSecurityConfiguration refSoSecurityConfiguration;

		[SerializeField] private string[] cmd;

		private string correoMOno;

		private string nombreMono;

		private string instituMono;

		#endregion

		#region monoBehaviour

		private void Start()
		{
			inputFieldNombre.text = DiccionarioIdiomas.Instance.Traducir("TextPlaceholderLogin");
			inputFieldCurso.text = DiccionarioIdiomas.Instance.Traducir("TextPlaceHolderCurso");
			inputFieldIdCurso.text = DiccionarioIdiomas.Instance.Traducir("TextPlaceholderIDCurso");
			inputFieldInstituto.text = DiccionarioIdiomas.Instance.Traducir("TextPlaceholderInstitucion");

#if UNITY_WEBGL
            if (refSoSecurityConfiguration.isLtiActive)
            {
                inputFieldNombre.interactable = false;
                inputFieldCurso.interactable = false;
                inputFieldIdCurso.interactable = false;
                inputFieldInstituto.interactable = false;
            }
#endif

			if (refSoSecurityConfiguration.isModeMonoUser)
			{
#if UNITY_EDITOR || UNITY_STANDALONE
				// Obtenemos los argumentos enviados a la aplicación de escritorio.
				cmd = System.Environment.CommandLine.Split(',');

				try
				{
					if (cmd.Length > 5)
					{
						nombreMono = cmd[3];
						instituMono = cmd[4];
						correoMOno = cmd[5];
					}
				}
				catch
				{
					BoxMessageManager.Instance.CreateBoxMessageInfo("esto es lo que hay dentro de la cmd " + cmd, "ACEPTAR");
				}
#elif UNITY_ANDROID || UNITY_IPHONE
                try
                {
					AndroidJavaClass UnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer"); 
					AndroidJavaObject currentActivity = UnityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

					if (currentActivity != null)
					{
                        AndroidJavaObject intent = currentActivity.Call<AndroidJavaObject>("getIntent");

                        if (intent != null)
                        {
                            nombreMono = safeCallStringMethod(intent, "getStringExtra", "nombre");
                            instituMono = safeCallStringMethod(intent, "getStringExtra", "institucion");
                            correoMOno = safeCallStringMethod(intent, "getStringExtra", "correo");
                        }
                    }
				}
                catch (Exception e)
                {
					Debug.Log(e.ToString());
				}
#endif
			}
		}

		private void OnEnable()
		{
			seOnLogin.Subscribe(loguin);
		}

		private void OnDisable()
		{
            refSoSecurityData.aulaUsername = inputFieldNombre.text;
            refSoSecurityData.aulaUserFirstName = inputFieldNombre.text;
            refSoSecurityData.aulaClassName = inputFieldCurso.text;
            refSoSecurityData.aulaClassId = inputFieldIdCurso.text;
            refSoSecurityData.aulaSchoolName = inputFieldInstituto.text;

            seOnLogin.Unsubscribe(loguin);
		}

		private void Update()
		{
#if UNITY_WEBGL
            if (refSoSecurityConfiguration.isLtiActive)
            {
                inputFieldNombre.text = refSoSecurityData.aulaUserFirstName;
                inputFieldCurso.text = refSoSecurityData.aulaClassName;
                inputFieldIdCurso.text = refSoSecurityData.aulaClassId;
                inputFieldInstituto.text = refSoSecurityData.aulaSchoolName;
            }

#elif UNITY_ANDROID || UNITY_IPHONE || UNITY_EDITOR || UNITY_STANDALONE
			if (refSoSecurityConfiguration.isModeMonoUser)
			{
				inputFieldNombre.text = nombreMono;
				inputFieldInstituto.text = instituMono;
			}
#endif
		}

		#endregion

		#region static methods

		public static string safeCallStringMethod(AndroidJavaObject javaObject, string methodName, params object[] args)
		{
			if (args == null)
				args = new object[] { null };

			IntPtr methodID = AndroidJNIHelper.GetMethodID<string>(javaObject.GetRawClass(), methodName, args, false);
			jvalue[] jniArgs = AndroidJNIHelper.CreateJNIArgArray(args);

			try
			{
				IntPtr returnValue = AndroidJNI.CallObjectMethod(javaObject.GetRawObject(), methodID, jniArgs);
				if (IntPtr.Zero != returnValue)
				{
					var val = AndroidJNI.GetStringUTFChars(returnValue);
					AndroidJNI.DeleteLocalRef(returnValue);
					return val;
				}
			}
			finally
			{
				AndroidJNIHelper.DeleteJNIArgArray(args, jniArgs);
			}

			return null;
		}

		#endregion

		#region public methods

		public void loguin(int op)
		{
			if (op == 0)
				ShowPanel(false);
		}

		/// <summary>
		/// metodo que activa el inicio de sesion offline
		/// </summary>
		public void ButtonLoginOffline()
		{
			if (inputFieldNombre.text != "" && inputFieldCurso.text != "" && inputFieldInstituto.text != "" && inputFieldIdCurso.text != "")
			{
				seOnInputsLogin4InputsCorrect.ExecuteEvent();
				FillSecurityData();
			}
			else
				BoxMessageManager.Instance.CreateBoxMessageInfo(DiccionarioIdiomas.Instance.Traducir("mensajeCamposNecesarios"), DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"));

		}

		public void FillSecurityData()
		{
			// TODO: Verificar que los datos ingresados correspondan en el campo que se requieren
			if (refSoSecurityData == null) return;

			refSoSecurityData.aulaUserFirstName = inputFieldNombre.text;
			refSoSecurityData.aulaClassName = inputFieldCurso.text;
			refSoSecurityData.aulaClassId = inputFieldIdCurso.text;
			refSoSecurityData.aulaSchoolName = inputFieldInstituto.text;
		}

		#endregion
	}


}