﻿#pragma warning disable 0649
using System;
using NSBoxMessage;
using NSInterfaz;
using NSScriptableEvent;
using NSScriptableValues;
using NSSeguridad;
using NSTraduccionIdiomas;
using UnityEngine;
using UnityEngine.Events;

namespace NSInitSimulator
{
    public class InitSimulator : MonoBehaviour
    {
        [Header("Dictionary")] [SerializeField]
        private ScriptableEvent seOnDictionaryLoaded;

        public UnityEvent EventDictionaryLoaded;

        [Tooltip("¿Tiene el simulador ventana para la seleccion de idiomas?")]
        [SerializeField] public ValueBool simulatorIsBilingual;
        
        [SerializeField] private ScriptableEvent seOnPanelSelectIdiomClose;
        
        [Header("Login")]
        [SerializeField] private ScriptableEvent seOnPanelWellcomeClose;

        public IdiomsList actualIdiom_init;

        public GameObject plcObject;

        private void Awake()
        {
            switch (actualIdiom_init)
            {
                case IdiomsList.Spanish:
                    Manager.Instance.globalLanguage = BaseSimulator.LanguageList.Spanish;
                    plcObject.GetComponent<baseSystem>().Language = BaseSimulator.LanguageList.Spanish;
                    break;

                case IdiomsList.English:
                    Manager.Instance.globalLanguage = BaseSimulator.LanguageList.English;
                    plcObject.GetComponent<baseSystem>().Language = BaseSimulator.LanguageList.English;
                    break;

                case IdiomsList.portugues:
                    Manager.Instance.globalLanguage = BaseSimulator.LanguageList.Portuguese;
                    plcObject.GetComponent<baseSystem>().Language = BaseSimulator.LanguageList.Portuguese;
                    break;
            }
        }


        private void Start()
        {
            seOnDictionaryLoaded.Subscribe(OnDictionaryLoaded);
            seOnPanelSelectIdiomClose.Subscribe(OnPanelSelectIdiomClose);
            DiccionarioIdiomas.CreateInstance();
            DiccionarioIdiomas.Instance.idiomaActual = actualIdiom_init;
            DiccionarioIdiomas.Instance.CargarDiccionarios();
            DiccionarioIdiomas.Instance.SetIdiom(actualIdiom_init);
        }


        private void OnDictionaryLoaded()
        {
            if (simulatorIsBilingual)
                PanelInterfazSeleccionLenguajeInglesEspaniol.Instance.ShowPanel();
            else
                OnButtonAceptarPanelWellcome();

               // BoxMessageManager.Instance.CreateBoxMessageInfo(DiccionarioIdiomas.Instance.Traducir("TextBienvenida"), DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"), OnButtonAceptarPanelWellcome);
        }

        public void OnPanelSelectIdiomClose()
        {
            PanelInterfazSeleccionLenguajeInglesEspaniol.Instance.ShowPanel(false);
            OnButtonAceptarPanelWellcome();
            //BoxMessageManager.Instance.CreateBoxMessageInfo(DiccionarioIdiomas.Instance.Traducir("TextBienvenida"), DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"), OnButtonAceptarPanelWellcome);
        }
        
        private void OnButtonAceptarPanelWellcome()
        {
            seOnPanelWellcomeClose.ExecuteEvent();
        }
    }
}