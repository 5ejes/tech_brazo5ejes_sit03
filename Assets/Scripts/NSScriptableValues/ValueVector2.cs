#pragma warning disable 0660
#pragma warning disable 0661
using UnityEngine;


namespace NSScriptableValues
{
    [CreateAssetMenu(fileName = "Vector2", menuName = "NSScriptableValue/Vector2", order = 0)]
    public class ValueVector2 : AbstractScriptableValue<Vector2>
    {
        #region sobre carga operadores

        public static implicit operator Vector2(ValueVector2 argValueA)
        {
            return argValueA.value;
        }
        
        public static ValueVector2 operator +(ValueVector2 argValueA, ValueVector2 argValueB)
        {
            argValueA.Value += argValueB.value;
            return argValueA;
        }

        public static ValueVector2 operator -(ValueVector2 argValueA, ValueVector2 argValueB)
        {
            argValueA.Value -= argValueB.value;
            return argValueA;
        }

        public static ValueVector2 operator *(ValueVector2 argValueA, float argValueB)
        {
            argValueA.Value *= argValueB;
            return argValueA;
        }

        public static ValueVector2 operator /(ValueVector2 argValueA, float argValueB)
        {
            argValueA.Value /= argValueB;
            return argValueA;
        }

        public static bool operator >(ValueVector2 argValueA, ValueVector2 argValueB)
        {
            return argValueA.value.magnitude > argValueB.value.magnitude;
        }

        public static bool operator <(ValueVector2 argValueA, ValueVector2 argValueB)
        {
            return argValueA.value.magnitude < argValueB.value.magnitude;
        }

        public static bool operator ==(ValueVector2 argValueA, ValueVector2 argValueB)
        {
            return argValueA.value == argValueB.value;
        }

        public static bool operator !=(ValueVector2 argValueA, ValueVector2 argValueB)
        {
            return argValueA.value != argValueB.value;
        }

        public static bool operator >=(ValueVector2 argValueA, ValueVector2 argValueB)
        {
            return argValueA.value.magnitude >= argValueB.value.magnitude;
        }

        public static bool operator <=(ValueVector2 argValueA, ValueVector2 argValueB)
        {
            return argValueA.value.magnitude <= argValueB.value.magnitude;
        }

        #endregion
    }
}