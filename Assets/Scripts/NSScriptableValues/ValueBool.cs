﻿using UnityEngine;

namespace NSScriptableValues
{
    [CreateAssetMenu(fileName = "Bool", menuName = "NSScriptableValue/Bool", order = 0)]
    public class ValueBool : AbstractScriptableValue<bool>
    {
        #region sobre carga operadores

        public static implicit operator bool(ValueBool argValueA)
        {
            return argValueA.value;
        }
        #endregion
    }
}