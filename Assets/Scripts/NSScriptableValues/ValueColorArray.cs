﻿using UnityEngine;

namespace NSScriptableValues
{
    [CreateAssetMenu(fileName = "ColorArray", menuName = "NSScriptableValue/ColorArray", order = 0)]
    public class ValueColorArray : AbstractScriptableValue<Color[]>
    {
        #region sobre carga operadores

        public static implicit operator Color[](ValueColorArray argValueA)
        {
            return argValueA.value;
        }
        #endregion
    }
}