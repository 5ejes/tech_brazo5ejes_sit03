using UnityEngine;

namespace NSScriptableValues
{
    [CreateAssetMenu(fileName = "ErrorDefinition", menuName = "NSScriptableValue/ErrorDefinition", order = 0)]
    public class ValueErrorDefinition : AbstractScriptableValue<ErrorDefinition>
    {
        public static implicit operator ErrorDefinition(ValueErrorDefinition argValueA)
        {
            return argValueA.value;
        }

				public void SetAsNull()
				{
					Value = null;
				}
    }
}