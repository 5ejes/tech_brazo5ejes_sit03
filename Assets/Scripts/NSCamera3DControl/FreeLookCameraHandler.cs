﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.Events;
using NSUtilities;

public class FreeLookCameraHandler : MonoBehaviour
{
	[Header("Events")]
	[SerializeField] private UnityEvent _onActiveFreelook;
	[SerializeField] private UnityEvent _onActiveOther;
	[Header("Settings")]
	[Tooltip("Pivote utilizado para rotar la cámara")]
	[SerializeField] private Transform _pivot;
	[SerializeField] private CinemachineVirtualCamera _freelookCamera;

	public bool Rotating { get; set; } = false;

	public float CurrentRotationDegrees { get; set; } = 0f;


	private CinemachineVirtualCamera _currentCamera;

	/// <summary>
	/// La cámara Freelook actualizada por el script
	/// </summary>
	/// <value>La cámara que se definirá como FreeLook</value>
	public CinemachineVirtualCamera FreelookCamera
	{
		get
		{
			// Si no existe, hallo la referencia en el objeto
			if (_freelookCamera) _freelookCamera = GetComponent<CinemachineVirtualCamera>();
			return _freelookCamera;
		}
		set => _freelookCamera = value;
	}

	public void ForceUpdateFreelookCamera()
	{
		// Si hay una cámara activa
		if (_currentCamera != null)
		{

			// Si no es la mísma cámara
			if (!_currentCamera.Equals(FreelookCamera))
			{
				// Copio la posición y la rotación de la cámara actual activa
				transform.rotation = _currentCamera.transform.rotation;
				transform.position = _currentCamera.transform.position;

				_currentCamera = FreelookCamera;
			}
		}
	}

	public void UpdateFreelookCamera(CinemachineVirtualCamera currentActive)
	{

		// Si la cámara actual no corresponde a esta cámara
		if (!currentActive.Equals(FreelookCamera))
		{
			// Defino la cámara actual
			_currentCamera = currentActive;

			StartCoroutine(Delay(2f, () =>
			{

				// Si durante el delay la cámara activa aún es diferente a la FreeLookCamera
				if (!_currentCamera.Equals(FreelookCamera))
				{
					// Copio la posición y la rotación de la cámara actual activa
					transform.rotation = _currentCamera.transform.rotation;
					transform.position = _currentCamera.transform.position;
				}
			}));

			// Ejecuto el evento
			_onActiveOther.WrapInvoke();
		}
		else
		{
			// Si es la cámara actual

			// Ejecuto el evento
			_onActiveFreelook.WrapInvoke();
		}
	}

	private IEnumerator Delay(float delay, System.Action callback)
	{
		// Espero los segundos especificados
		yield return new WaitForSeconds(delay);

		// Ejecuto el callback si no es nulo
		if (callback != null) callback();
	}

	public void RotateAroundPivot(float degrees)
	{
		// Si pivot existe
		if (_pivot)
		{
			// Defino los parámetros de rotación
			Rotating = true;
			CurrentRotationDegrees = degrees;
		}
	}

	public void CancelRotation()
	{
		// Cancelo la rotación de la cámara
		Rotating = false;
		CurrentRotationDegrees = 0f;
	}

	private void Update()
	{
		// Si está rotando y la rotación no es igual a 0f
		if (Rotating && Mathf.Abs(CurrentRotationDegrees) > float.Epsilon)
		{
			// Roto alrededor del pivote, en el eje y, con el tiempo delta
			transform.RotateAround(_pivot.position, Vector3.up, CurrentRotationDegrees * Time.deltaTime);
		}
	}
}
