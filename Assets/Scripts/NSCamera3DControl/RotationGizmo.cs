﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSCamera3DControl
{
	public class RotationGizmo : MonoBehaviour
	{
		[Header("Settings")]
		[SerializeField] private Material _lineMaterial;

		private LineRenderer outputLineRenderer;
		private float distance = 1f;
		private int segments = 12;

		private Material _copyMat;

		private void Start()
		{

			_copyMat = new Material(_lineMaterial);

			var _outline = CreateLineRenderer("Outline");
			var _zSphere = CreateLineRenderer("ZSphere");

			BuildCircle(_outline, segments, distance);
			BuildCircle(_zSphere, segments, distance * 0.8f);
		}

		private LineRenderer CreateLineRenderer(string name)
		{
			outputLineRenderer = new GameObject(name).AddComponent<LineRenderer>();
			outputLineRenderer.transform.parent = transform;

			outputLineRenderer.transform.localPosition = Vector3.zero;

			outputLineRenderer.loop = true;
			outputLineRenderer.useWorldSpace = false;

			outputLineRenderer.startWidth = .1f;
			outputLineRenderer.endWidth = .1f;

			outputLineRenderer.startColor = outputLineRenderer.endColor = Color.white;

			outputLineRenderer.material = _copyMat;

			return outputLineRenderer;
		}

		private void BuildCircle(LineRenderer argTarget, int segments, float distance)
		{
			float ratio = Mathf.PI * 2 / (float) segments;

			argTarget.positionCount = segments;

			List<Vector3> listPositions = new List<Vector3>();

			for (int i = 0; i < segments; i++)
			{
				var position = Vector3.zero;

				float currentPortion = ratio * i;
				position.x = Mathf.Sin(currentPortion) * distance;
				position.y = Mathf.Cos(currentPortion) * distance;

				listPositions.Add(position);
			}

			argTarget.SetPositions(listPositions.ToArray());
		}
	}
}