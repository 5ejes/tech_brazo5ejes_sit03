﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using NSUtilities;

namespace NSCamera3DControl
{

	public class ViewsCube : MonoBehaviour
	{
		[Header("Camera and Ray Settings")]
		[SerializeField] private Camera _rayCamera;
		[SerializeField] private float _maxDistance = 10f;
		[SerializeField] private LayerMask _cubeLayer;

		[Header("Viewport Position Settings")]
		[Range(0f, 1f)]
		[SerializeField] private float _viewportPositionX = 0.5f;
		[Range(0f, 1f)]
		[SerializeField] private float _viewportPositionY = 0.5f;
		[Space]
		[Range(0f, 1f)]
		[SerializeField] private float _relativeDistancePercent = .75f;

		[Header("Events")]
		[SerializeField] private UnityEvent _onSelectRight = new UnityEvent();
		[SerializeField] private UnityEvent _onSelectTop = new UnityEvent();
		[SerializeField] private UnityEvent _onSelectFront = new UnityEvent();

		private Vector2 _storedViewportPosition = -Vector2.one;
		private float _storedRelativeDistancePercent = -1f;

		// Defino cada uno de los nombres
		public const string TOP_VIEW = "TOP";
		public const string BOTTOM_VIEW = "BOTTOM";
		public const string FRONT_VIEW = "FRONT";
		public const string BACK_VIEW = "BACK";
		public const string LEFT_VIEW = "LEFT";
		public const string RIGHT_VIEW = "RIGHT";

		private void Update()
		{

			// Creo un vector con los valores X y Y de la posición en el viewport del cubo
			var viewportPosition = new Vector2(_viewportPositionX, _viewportPositionY);

			// Si el valor del viewport es distinto al almacenado
			if (viewportPosition != _storedViewportPosition || _relativeDistancePercent != _storedRelativeDistancePercent)
			{
				// Proceso el cambio
				// Obtengo la posición del cubo en pantalla y la actualizo
				transform.position = GetCubeScreenPosition();

				// Actualizo el valor almacenado
				_storedViewportPosition = viewportPosition;
				_storedRelativeDistancePercent = _relativeDistancePercent;
			}

			// Si doy click con el mouse
			if (Input.GetMouseButtonDown(0))
			{
				// Si la camara es nula, retorno
				if (_rayCamera == null) throw new System.Exception("Ray Camera is null");

				// Lanzo un rayo desde la cámara
				var ray = _rayCamera.ScreenPointToRay(Input.mousePosition);
				RaycastHit hit;

				// Uso el rayo para detectar la cara del cubo
				if (Physics.Raycast(ray, out hit, _maxDistance, _cubeLayer, QueryTriggerInteraction.Collide))
				{
					// Logueo en consola en 
					ProcessHitNormal(hit.normal);
				}
			}
		}

		private void ProcessHitNormal(Vector3 argHitNormal)
		{
			// Logueo los datos

			// Obtengo el nombre del plano de la normal según el transform dado
			var name = GetNormalName(argHitNormal);

			// Creo un item para la vista contexto
			ViewContextItem viewContextItem = ViewContextItem.FreeLook;

			// Defino el item según el nombre según la normal dada
			switch (name)
			{
				case TOP_VIEW:
				case BOTTOM_VIEW:
					_onSelectTop.WrapInvoke();
					break;
				case FRONT_VIEW:
				case BACK_VIEW:
					_onSelectFront.WrapInvoke();
					break;
				case LEFT_VIEW:
				case RIGHT_VIEW:
					_onSelectRight.WrapInvoke();
					break;
				default:
					break;
			}

		}

		private string GetNormalName(Vector3 argHitNormal, Transform argTransformOverride = null)
		{
			// Creo un nombre vacío
			var name = string.Empty;

			// Uso el override para el transform, si no es nulo
			// En caso de nulo, simplemente uso el valor por defecto
			var useTransform = argTransformOverride != null ? argTransformOverride : transform;

			#region Implementación manual
			/* 
			// Comparo los argumentos con todos los valores
			if (argHitNormal == useTransform.up) name = "Up";
			else if (argHitNormal == -useTransform.up) name = "Down";
			else if (argHitNormal == useTransform.right) name = "Right";
			else if (argHitNormal == -useTransform.right) name = "Left";
			else if (argHitNormal == useTransform.forward) name = "Front";
			else if (argHitNormal == -useTransform.forward) name = "Back";
			*/
			#endregion

			#region Implementación por proximidad

			// Obtengo el nombre del plano más cercano a la normal
			name = GetNearestPlaneName(argHitNormal, useTransform);

			#endregion

			// Retorno el nombre
			return name;
		}

		private string GetNearestPlaneName(Vector3 argHitNormal, Transform argTransform)
		{
			// Arriba, abajo, frente, atrás, izquierda, derecha

			// Obtengo los productos punto
			var dotProducts = GetDotProducts(argHitNormal, argTransform);

			// Defino el mínimo valor y el índice inicial
			float minValue = float.MaxValue;
			int index = -1;

			// Recorro los productos punto
			for (int i = 0; i < dotProducts.Length; i++)
			{
				// Obtengo el producto punto actual
				float currentProduct = dotProducts[i];

				// Calculo la diferencia absoluta
				float diff = Mathf.Abs(currentProduct - 1);

				// Si el valor absoluto de la diferencia es menor que el valor más pequeño
				if (diff < minValue)
				{

					// Redefino el valor más pequeño
					minValue = diff;

					// Guardo el índice actual
					index = i;
				}
			}

			// Si el índice es valido, retorno el nombre
			// Si no es válido, retorno string Empty
			return GetNameFromIndex(index);

		}

		private string GetNameFromIndex(int index)
		{
			// Creo una variable de retorno
			string nameOutput = string.Empty;

			// Arriba, abajo, frente, atrás, izquierda, derecha
			switch (index)
			{
				case 0:
					nameOutput = TOP_VIEW;
					break;
				case 1:
					nameOutput = BOTTOM_VIEW;
					break;
				case 2:
					nameOutput = FRONT_VIEW;
					break;
				case 3:
					nameOutput = BACK_VIEW;
					break;
				case 4:
					nameOutput = LEFT_VIEW;
					break;
				case 5:
					nameOutput = RIGHT_VIEW;
					break;
			}

			// Retorno la variable
			return nameOutput;
		}

		private static float[] GetDotProducts(Vector3 argHitNormal, Transform argTransform)
		{
			// Calculo los 6 productos punto
			return new float[]
			{
				Vector3.Dot(argHitNormal, argTransform.up),
					Vector3.Dot(argHitNormal, -argTransform.up),
					Vector3.Dot(argHitNormal, argTransform.forward),
					Vector3.Dot(argHitNormal, -argTransform.forward),
					Vector3.Dot(argHitNormal, argTransform.right),
					Vector3.Dot(argHitNormal, -argTransform.right)
			};
		}

		private Vector3 GetCubeScreenPosition()
		{

			// Si la camara del ray es nula
			if (_rayCamera == null)
			{
				throw new System.Exception("The Ray Camera is null at position set");
			}

			// Obtengo la posición segun la ubicación en el viewport
			return _rayCamera.ViewportToWorldPoint(new Vector3(_viewportPositionX, _viewportPositionY, _maxDistance * _relativeDistancePercent));

		}
	}
}