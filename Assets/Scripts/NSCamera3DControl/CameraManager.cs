﻿using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using NSUtilities;
using UnityEngine;

namespace NSCamera3DControl
{
	/// <summary>
	/// Clase encargada de manejar las prioridades en las cámaras Cinemachine registradas
	/// </summary>
	public class CameraManager : MonoBehaviour
	{
		[Header("Events")]
		[SerializeField] private CinemachineVirtualCameraEvent _onSetActiveCamera = new CinemachineVirtualCameraEvent();
		[Header("Cameras")]
		[SerializeField] private List<CinemachineVirtualCamera> _cameras = new List<CinemachineVirtualCamera>();

		[Header("Settings")]
		[Tooltip("Si se define la primera cámara registrada como activa por defecto")]
		[SerializeField] private bool _enableFirstOnAwake = false;

		/// <summary>
		/// Obtengo las cámaras registradas
		/// </summary>
		public List<CinemachineVirtualCamera> Cameras => _cameras;

		private CinemachineVirtualCamera _currentActive = null;
		public CinemachineVirtualCamera Current => _currentActive;

		private static readonly int _selectedCameraPriority = 10;
		private static readonly int _defaultPriority = 0;

		private void Awake()
		{

			// Si defino la primera cámara por defecto
			if (_enableFirstOnAwake)
			{
				// Defino la prioridad por defecto a todas las cámaras
				SetCameraPriority(_defaultPriority, GetAllCameras());

				// Si la lista de cámaras no es nula
				if (Cameras != null)
				{
					// Si existe al menos una cámara
					if (Cameras.Count > 0)
						// Defino la prioridad de la primera cámara como la activa
						SetCameraPriority(_selectedCameraPriority, Cameras[0]);

					// Defino la cámara actual como activa
					_currentActive = Cameras[0];

					// Ejecuto el evento
					_onSetActiveCamera.WrapInvoke(_currentActive);
				}
			}
		}

		/// <summary>
		/// Obtengo todas las cámaras en forma de arreglo
		/// </summary>
		/// <returns>Las cámaras registradas en la lista</returns>
		public CinemachineVirtualCamera[] GetAllCameras()
		{
			// Retorno el grupo de cámaras registrado
			return Cameras.ToArray();
		}

		/// <summary>
		/// Defino la prioridad de las cámaras dadas por parámetros
		/// </summary>
		/// <param name="priority">Prioridad a dar a la cámara</param>
		/// <param name="argCameras">Cámaras a modificar su prioridad</param>
		public void SetCameraPriority(int priority, params CinemachineVirtualCamera[] argCameras)
		{
			foreach (var camera in argCameras)
			{
				// Defino la prioridad de las caramaras dadas en los parámetros
				camera.Priority = priority;
			}
		}

		/// <summary>
		/// Elimina la referencia a la cámara actual
		/// </summary>
		public void CleanCurrentCamera()
		{
			// Elimino la referencia a la cámara actual
			_currentActive = null;
		}

		/// <summary>
		/// Busca y define la cámara activa según el parámetro dado
		/// </summary>
		/// <param name="name">Nombre de la cámara virtual a activasr</param>
		public void SetCameraAsActive(string name)
		{
			// Si la cámara activa está definida
			if (Current)
			{
				// Si la cámara a la que voy a definir como activa está activa
				if (Current.name.Equals(name)) return;
			}
			
			// Reestablezco las prioridades de todas las cámaras
			SetCameraPriority(_defaultPriority, GetAllCameras());

			// Si la lista no es nula
			if (Cameras != null)
			{
				// Si la lista contiene al menos un elemento
				if (Cameras.Count > 0)
				{
					// Busco la cámara que coincida con nuestro filtro
					var selectedCamera = Cameras.Find(camera => camera.gameObject.name.Equals(name));

					// Si se encuentra una cámara
					if (selectedCamera != null)
					{
						// Defino la prioridad de la cámara actual
						SetCameraPriority(_selectedCameraPriority, selectedCamera);

						// Defino la cámara seleccionada como la activa
						_currentActive = selectedCamera;

						// Ejecuto el evento
						_onSetActiveCamera.WrapInvoke(_currentActive);
					}
					else
					{
						// No se econtró la cámara
						// Lanzo un error y cancelo la operación
						Debug.LogError($"No camera with {name} as name was found : {gameObject}");
						return;
					}

				}
				else
				{
					// La lista está vacía
					// Lanzo un error y cancelo la operación
					Debug.Log($"Camera List is empty : {gameObject}");
					return;
				}
			}
			else
			{
				// La lista es nula
				// Lanzo un error y cancelo la operación
				Debug.Log($"Camera list is undefined : {gameObject}");
				return;
			}
		}
	}
}