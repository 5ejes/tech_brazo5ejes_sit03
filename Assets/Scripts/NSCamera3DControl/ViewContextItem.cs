﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSCamera3DControl
{
	public enum ViewContextItem
	{
		Top,
		Right,
		Front,
		FreeLook
	}
}