﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformLineDrawer : MonoBehaviour
{

	[System.Serializable]
	public class TransformLineDrawingData
	{
		private Transform[] _points;
		public Transform[] Points { get => _points; set => _points = value; }
		public TransformLineDrawingData(params Transform[] transformValues)
		{
			Points = transformValues;
		}
	}
	private static TransformLineDrawer _instance;
	private static TransformLineDrawer Instance
	{
		get
		{
			if (_instance == null)
			{
				_instance = new GameObject("Transform Line Drawer").AddComponent<TransformLineDrawer>();
			}

			return _instance;
		}
	}

	[Header("Draw Settings")]
	[SerializeField] private Color _sphereColor = Color.white;
	[SerializeField] private Color _lineColor = Color.cyan;
	[Space]
	[SerializeField] private float _radius = 0.1f;

	private List<TransformLineDrawingData> _lineDrawingData = new List<TransformLineDrawingData>();

	private bool _enableDraw = true;
	public static bool EnableDraw
	{
		get => Instance._enableDraw;
		set => Instance._enableDraw = value;
	}

	private void OnEnable()
	{
		if (_instance == null)
			_instance = this;
		else if (_instance != this)
			Destroy(_instance);

	}
	private void OnDrawGizmos()
	{
		if (_lineDrawingData == null) return;
		if (!EnableDraw) return;
		int dataCount = _lineDrawingData.Count;

		for (int lineDataIndex = 0; lineDataIndex < dataCount; lineDataIndex++)
		{
			var points = _lineDrawingData[lineDataIndex].Points;
			if (points == null) return;

			int count = points.Length;
			for (int pointIndex = 0; pointIndex < count; pointIndex++)
			{
				Transform currentPoint = points[pointIndex];
				if (currentPoint == null) return;

				Vector3 currentPointPosition = currentPoint.position;
				if (pointIndex < count - 1)
				{
					Vector3 nextPointPosition = points[pointIndex + 1].position;

					Gizmos.color = _lineColor;
					Gizmos.DrawLine(currentPointPosition, nextPointPosition);
				}

				Gizmos.color = _sphereColor;
				Gizmos.DrawWireSphere(currentPointPosition, _radius);
			}
		}
	}

	public static void AddData(TransformLineDrawingData data)
	{
		if (data == null) return;
		Instance._lineDrawingData.Add(data);

	}
}