﻿using UnityEngine;

public interface IOverrideTransform
{
  Transform OverrideTransform { get; set; }
  bool UseOverrideTransform { get; set; }
}
