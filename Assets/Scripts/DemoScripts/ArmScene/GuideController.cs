﻿using Modular;
using System;
using UnityEngine;

namespace NSBrazo5Ejes
{
	public class GuideController : MonoBehaviour, IManualUpdate
	{
		private static readonly float REVOLUTION_VALUE = 90f;
		private static readonly float MIN_VALUE_THRESHOLD = float.Epsilon;

		private static readonly float _pitchHorizontalValue = -90f;
		private static readonly float _pitchVerticalValue = 0f;
		private static readonly float _yawRotationValue = 0f;

		[Header("References")]
		[SerializeField] private Transform _gripRotationGuide;
		[SerializeField] private Transform _rootBrazo;
		[SerializeField] private Transform _baseRotatoria;
		[Header("Distance Settings")]
		[SerializeField] private Transform _target;
		[SerializeField] private Transform _gripTip;

		[Header("Values")]
		[SerializeField] private float _gripPitch = 0f;
		[SerializeField] private float _gripYaw = 0f;
		[Header("Settings")]
		[SerializeField] private float _quarterRevolutionPerSecond = 1f;
		[Header("Visual Grip")]
		[SerializeField] private Transform _visualGrip;
		[SerializeField] private Transform _visualArmGripParent;
		[Header("Update Settings")]
		[SerializeField] private bool _callOnUpdate = false;
		[Header("Events")]
		[SerializeField] private BoolEvent _Eje5SetValue;
		[SerializeField] private BoolEvent _Eje5EndTransitionValue;
		[Header("Debug")]
		[SerializeField] private bool _drawDebug = false;

		private float _currentGripPitch = 0f;
		private float _currentGripYaw = 0f;
		private bool _currentHorizontalValue = false;

		private bool _evaluateTransitionEnd = true;
		private Vector3 _storedGripGuidePosition;
		private Quaternion _storedGripGuideRotation;
		private float _distanceToVertical = float.MinValue;

		private Vector3 _targetPosition = Vector3.zero;
		public float DistanceToVerticalGrip
		{
			get
			{
				if (_distanceToVertical < 0f)
				{
					_distanceToVertical = (Target.position - GripTip.position).magnitude;
				}
				return _distanceToVertical;
			}
			private set => _distanceToVertical = value;
		}

		public Transform Target { get => _target; set => _target = value; }
		public Transform GripTip { get => _gripTip; set => _gripTip = value; }

		// Para alterar el modo horizontal o vertical usar SetRotationMode(bool horizontal)
		public bool HorizontalValue { get => _currentHorizontalValue; private set => _currentHorizontalValue = value; }
		public bool AllowUpdate { get => enabled; set => enabled = value; }

		private void Start()
		{
			UpdateGuideGripRotation();

			_gripYaw = 0f;
			_gripPitch = _pitchVerticalValue;
		}

		private bool CanUpdateArmTargetPosition()
		{

			// Defino si puedo actualizar el elemento
			bool canUpdate = !_storedGripGuidePosition.Equals(_gripRotationGuide.position) || !_storedGripGuideRotation.Equals(_gripRotationGuide.rotation);

			// Actualizo los datos
			if (canUpdate)
			{
				_storedGripGuidePosition = _gripRotationGuide.position;
				_storedGripGuideRotation = _gripRotationGuide.rotation;
			}

			return canUpdate;
		}

		private void UpdateArmTargetPosition()
		{

			// Defino el vector de dirección hacía el objetivo
			Vector3 from = (_gripRotationGuide.position - _rootBrazo.position);

			// Aplano el vector en Y
			from.y = 0f;
			from.Normalize();

			// Defino el vector de referencia para la rotación
			Vector3 to = _rootBrazo.rotation * Vector3.left;
			float localUpRotation = Vector3.SignedAngle(from, to, Vector3.up);

			// Aplico un offset basado en la rotación del brazo, menos unos ajustes de configuración
			// Y el 360 para que el wrapping sea completo
			float wrapAroundValue = 360f;
			float offset = (_rootBrazo.eulerAngles.y - 90f) + wrapAroundValue;

			// Defino el ángulo basado en la rotación del punto eje hasta el objetivo, y lo muevo para que coincida
			// Con los valores de rotación de la escena
			float angles = Mathf.Abs((localUpRotation - offset) % wrapAroundValue);

			// Defino la dirección de rotación según el ángulo de guía en Z
			Quaternion rotationToTarget = Quaternion.AngleAxis(angles, Vector3.up);

			// Defino la dirección de rotación
			var rotationDir = rotationToTarget *
				Quaternion.AngleAxis(_gripRotationGuide.eulerAngles.z, Vector3.right)
				* Vector3.up;

			if (_drawDebug) Debug.DrawRay(_gripRotationGuide.position, rotationDir * DistanceToVerticalGrip, Color.red);

			// Aplico la posición basado en la dirección hacia la que se debe dibujar
			_targetPosition = _gripRotationGuide.position + rotationDir * DistanceToVerticalGrip;
		}

		private void Update()
		{
			if (!_callOnUpdate) return;
			CallUpdate();
		}

		private void UpdateGuideGripRotation()
		{
			// Si no existe el componente de rotación guía
			if (!_gripRotationGuide || !_rootBrazo) return;


			Quaternion rotation;

			// Actualizo la rotación del objeto
			rotation = Quaternion.AngleAxis(-_currentGripYaw, Vector3.up)
			* Quaternion.AngleAxis(_rootBrazo.eulerAngles.y, Vector3.up)
			* Quaternion.AngleAxis(_currentGripPitch, Vector3.forward);

			_gripRotationGuide.localRotation = rotation;

			// Actualizo los valores actuales de CurrentGrip

			// Si la transición es horizontal
			float maxDelta = Time.deltaTime * _quarterRevolutionPerSecond * REVOLUTION_VALUE;

			float angle = -GetLocalUpTransitionRotation();

			float targetYaw = _currentHorizontalValue ? 90f : angle;
			float targetPitch = _currentHorizontalValue ? _pitchHorizontalValue : _pitchVerticalValue;

			if (HorizontalValue)
			{
				_currentGripYaw = Mathf.MoveTowards(_currentGripYaw, targetYaw, maxDelta);
				// Compruebo el margen de porcentaje y la sincronización
				if (Mathf.Abs(_currentGripYaw - targetYaw) < MIN_VALUE_THRESHOLD)
				{
					_currentGripPitch = Mathf.MoveTowards(_currentGripPitch, targetPitch, maxDelta);

					// Evaluo la transición si no hay movimiento y si se requiere una evaluación
					if (Mathf.Abs(_currentGripPitch - targetPitch) < float.Epsilon && _evaluateTransitionEnd)
					{
						// Marco la evaluación como completa
						_evaluateTransitionEnd = false;

						// Invoco un evento al finalizar la transición horizontal
						_Eje5EndTransitionValue?.Invoke(_currentHorizontalValue);
					}
				}
			}
			else
			{
				// Si la transición es vertical
				_currentGripPitch = Mathf.MoveTowards(_currentGripPitch, targetPitch, maxDelta);
				if (Mathf.Abs(_currentGripPitch - targetPitch) < MIN_VALUE_THRESHOLD)
				{
					_currentGripYaw = Mathf.MoveTowards(_currentGripYaw, targetYaw, maxDelta);

					// Evaluo la transición si no hay movimiento y si se requiere una evaluación
					if (Mathf.Abs(_currentGripYaw - targetYaw) < float.Epsilon && _evaluateTransitionEnd)
					{
						// Marco la evaluación como completa
						_evaluateTransitionEnd = false;

						// Invoco un evento al finalizar la transición horizontal
						_Eje5EndTransitionValue?.Invoke(_currentHorizontalValue);
					}
				}
			}
		}

		private float GetLocalUpTransitionRotation()
		{
			Vector3 from = _baseRotatoria.rotation * Vector3.forward;
			Vector3 to = _rootBrazo.rotation * Vector3.left;

			if (_drawDebug)
			{
				// Debug.DrawRay(transform.parent.position, from, Color.green);
				// Debug.DrawRay(transform.parent.position, to, Color.red);
			}
			float offsetAngle = Vector3.SignedAngle(from, to, Vector3.up);
			return offsetAngle;
		}

		private bool CanUpdateGripRotation()
		{
			// Si el grip rotation guide existe y ha cambiado el roll o el yaw
			bool canUpdateGripRotation = _gripRotationGuide && _rootBrazo
			&& (Mathf.Abs(_gripYaw - _currentGripYaw) > MIN_VALUE_THRESHOLD || Mathf.Abs(_gripPitch - _currentGripPitch) > MIN_VALUE_THRESHOLD);

			// Si no puede actualizarse la rotación del grip
			if (!canUpdateGripRotation)
			{
				// Defino los valores a los valores objetivo exacto
				_currentGripYaw = _gripYaw;
				_currentGripPitch = _gripPitch;
				UpdateGuideGripRotation();
			}

			return canUpdateGripRotation;
		}

		private void RotateGrip(Vector3 rotation)
		{
			if (!_visualArmGripParent || !_visualGrip) return;
			Quaternion parentGripRotation = Quaternion.AngleAxis(_visualArmGripParent.eulerAngles.x, Vector3.right) * Quaternion.AngleAxis(_visualArmGripParent.eulerAngles.z, Vector3.forward);

			Vector3 gripSampleDirection = _visualArmGripParent.forward;
			float offsetAngle = Vector3.SignedAngle(gripSampleDirection, transform.rotation * Vector3.right, Vector3.up);

			_visualGrip.localRotation = Quaternion.Euler(rotation);
		}

		public void CallUpdate()
		{
			// Solo ejecuto el update después del n frane
			if (Time.frameCount < 2) return;


			UpdateGuideGripRotation();
			if (_visualGrip) RotateGrip(Vector3.up * -_gripRotationGuide.eulerAngles.y);


			// Actulizo la posición en caso de poder actualizarla
			if (CanUpdateArmTargetPosition())
			{
				UpdateArmTargetPosition();
			}

			Target.position = Vector3.MoveTowards(Target.position, _targetPosition, Time.deltaTime * 2f);
		}

		public void ToHorizontal()
		{
			_gripPitch = _pitchHorizontalValue;
			// _gripYaw = 0f;
		}

		public void ToVertical()
		{
			_gripPitch = _pitchVerticalValue;
			// _gripYaw = 0f;
		}

		public void SetRotationMode(bool horizontal)
		{

			if (horizontal) ToHorizontal();
			else ToVertical();

			HorizontalValue = horizontal;

			// Marco la evaluación de transición como pendiente
			_evaluateTransitionEnd = true;

			_Eje5SetValue?.Invoke(HorizontalValue);
		}

		public float SignedAngleFromDirection(Vector3 fromdir, Vector3 todir, Vector3 referenceup)
		{ // calculates the the angle between two direction vectors, with a referenceup a sign in which direction it points can be calculated (clockwise is positive and counter clockwise is negative)
			Vector3 planenormal = Vector3.Cross(fromdir, todir); // calculate the planenormal (perpendicular vector)
			float angle = Vector3.Angle(fromdir, todir); // calculate the angle between the 2 direction vectors (note: its always the smaller one smaller than 180°)
			float orientationdot = Vector3.Dot(planenormal, referenceup); // calculate wether the normal and the referenceup point in the same direction (>0) or not (<0), http://docs.unity3d.com/Documentation/Manual/ComputingNormalPerpendicularVector.html
			if (orientationdot > 0.0f) // the angle is positive (clockwise orientation seen from referenceup)
				return angle;
			return -angle; // the angle is negative (counter-clockwise orientation seen from referenceup)
		}
	}
}
