﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using NSUtilities;
using System;

using Random = UnityEngine.Random;
using static NSScriptableEvent.ScriptableEventTransformArray;
using NSBrazo5Ejes.NSExerciseGenerator;
using NSBrazo5Ejes;
using NSBrazo5Ejes.NSTransportBelt;

namespace NSBrazo5Ejes
{

	/// <summary>
	/// Clase que interviene en la inicialización de cada cubo
	/// </summary>
	public class CubeDataReference
	{

		/// <summary>
		/// Componente Cube del objeco
		/// </summary>
		public Cube CubeComponent { get; }

		/// <summary>
		/// Posición inicial del objeto
		/// </summary>
		public Vector3 InitialPosition { get; }
		/// <summary>
		/// Rotación inicial del objeto
		/// </summary>
		public Quaternion InitialRotation { get; }

		/// <summary>
		/// Indica si la referencia actual debe comenzar inactiva
		/// </summary>
		/// <value></value>
		public bool StartDisabled { get; set; } = false;

		/// <summary>
		/// Constructor para la clase
		/// </summary>
		/// <param name="cube">Componente el cual se va a almacenar como instancia</param>
		public CubeDataReference(Cube cube)
		{
			CubeComponent = cube;

			// Si se usa el overrideTransform y existe una referencia
			if (cube.OverrideTransform && cube.UseOverrideTransform)
			{
				// Defino cada valor
				InitialPosition = cube.OverrideTransform.position;
				InitialRotation = cube.OverrideTransform.rotation;
			}
			else
			{
				// Defino cada valor
				InitialPosition = cube.transform.position;
				InitialRotation = cube.transform.rotation;
			}
		}

		/// <summary>
		/// Convierte Cube a CubeDataReference
		/// </summary>
		/// <param name="cube">Componente Cube</param>
		public static implicit operator CubeDataReference(Cube cube)
		{
			// Convierto el componente Cube en CubeDataReference
			return new CubeDataReference(cube);
		}

		/// <summary>
		/// Restauro el cubo a la posición y rotación registradas
		/// </summary>
		public void Restore()
		{
			// Si se usa el override transform y existe una referencia
			if (CubeComponent.OverrideTransform && CubeComponent.UseOverrideTransform)
			{

				// Defino por cada cubo, su posición a la que está almacenada
				CubeComponent.OverrideTransform.position = InitialPosition;

				// Y la rotación que está igualmente almacenada
				CubeComponent.OverrideTransform.rotation = InitialRotation;
			}
			else
			{

				// Defino por cada cubo, su posición a la que está almacenada
				CubeComponent.transform.position = InitialPosition;

				// Y la rotación que está igualmente almacenada
				CubeComponent.transform.rotation = InitialRotation;
			}

			// Defino el estado de activación inicial del cubo
			CubeComponent.gameObject.SetActive(!StartDisabled);
		}
	}
	public class CubeManager : MonoBehaviour
	{

		[Header("References")]
		[SerializeField] private GameObject _cubeGO;
		[SerializeField] private LocalPositionParser _parser;
		[SerializeField] private BeltTunnel _beltTunnel;

		[SerializeField] private bool _generateInTunnel;

		[Header("Limits")]
		[SerializeField] private Transform _minLimit;
		[SerializeField] private Transform _maxLimit;

		[Header("Events")]
		[SerializeField] private TransformArrayEvent _onCubesGenerated = new TransformArrayEvent();
		[SerializeField] private UnityEvent _onCubeResetStart = new UnityEvent();
		[SerializeField] private UnityEvent _onCubeResetEnd = new UnityEvent();


		/// <summary>
		/// Limites vectoriales del entorno
		/// </summary>
		/// <returns></returns>
		private static readonly Vector3 _limitVector = new Vector3(0.8f, 0.5f, 0.8f);

		private List<CubeDataReference> _cubeDataReferences = new List<CubeDataReference>();

		/// <summary>
		/// Genera los cubos dentro del tunel especificado si este existe
		/// </summary>
		/// <value></value>
		public bool GenerateInTunnel { get => _generateInTunnel; set => _generateInTunnel = value; }

		private void OnEnable()
		{
			// Inicializo los cubos que existan en el editor
			ReadCubesFromChildren();

		}

		public void ProcessRoutine(AbstractExerciseRoutine routine)
		{
			// Generar cubos
			GenerateCubesOnPositions(routine.Positions.ToArray());
		}

		public void GenerateCubesOnPositions(Vector3[] cubePositions)
		{

			// Genero un offset aleatorio
			int offset = Random.Range(0, 2);

			// Inicializo las posiciones
			for (int i = 0; i < cubePositions.Length; i++)
			{
				// Obtengo la posición
				var currentPosition = cubePositions[i];

				// Creo el cubo en la posición dada y el Color ID Dado
				int selectedColorID = (i + offset) % 3;
				CreateCube(currentPosition, selectedColorID);
			}

			// Convierto las referencias a Transform
			var cubeArray = ParseCubeReferences(_cubeDataReferences.ToArray());

			// Envio los cubos generados via el evento
			_onCubesGenerated.WrapInvoke(cubeArray);
		}

		/// <summary>
		/// Convierte referencias CubeReferences[] a Transform[]
		/// </summary>
		/// <param name="cubeArray">Arreglo de CubeReferences a los cuales convertir</param>
		/// <returns>Un Arreglo con las referencias Transform</returns>
		private Transform[] ParseCubeReferences(params CubeDataReference[] cubeArray)
		{
			// Genero una variable de output
			var output = new List<Transform>();

			// Proceso los elementos dados
			foreach (var cube in cubeArray)
			{
				// Añado el transform de la referencia
				output.Add(cube.CubeComponent.transform);
			}

			// Retorno el output ya procesado
			return output.ToArray();
		}

		/// <summary>
		/// Elimino todos los cubos presentes en la lista (de la escena)
		/// </summary>
		public void DeleteAllCubes()
		{
			// La cuenta de la lista
			int count = _cubeDataReferences.Count;

			// Si hay más de un elemento presente en la lista
			if (count > 0)
			{

				// Recorro los cubos
				for (int i = count - 1; i >= 0; i--)
				{
					// Obtengo el cubo
					var currentCubeData = _cubeDataReferences[i];

					// Destruyo el objeto
					Destroy(currentCubeData.CubeComponent.gameObject);
				}

				// Limpio la lista
				_cubeDataReferences.Clear();
			}
		}

		/// <summary>
		/// Creo un cubo sobre la posición indicada
		/// </summary>
		/// <param name="localPosition">Posición válida en la que se va a ubicar el cubo</param>
		/// <param name="ColorID">Identificador de color del cubo a crear</param>
		public void CreateCube(Vector3 localPosition, int ColorID = 0)
		{
			// Si el prefab del cubo es nulo

			if (!_cubeGO)
			{
				// Lanzo un error, cancelo la ejecución
				Debug.LogError($"The prefab used to instanciate the cubes is null : {gameObject}");
				return;
			}

			// Si los cubos se generan por fuera del tunel
			if (!_generateInTunnel)
			{

				// Verifico que la posición sea válida

				// Ajusto la posición
				localPosition = AdjustPosition(localPosition, 0.05f);

				// Si la posición es válida
				if (ValidatePosition(localPosition))
				{

					// Obtengo la posición global basado en una coordenada local dada
					Vector3 parsedPosition;
					if (_minLimit && _maxLimit)
						parsedPosition = _parser.ParseVector(localPosition, _minLimit, _maxLimit);
					else
						parsedPosition = _parser.ParseVector(localPosition);
					// Instancio un nuevo cubo en la posición definida
					var spawnedGo = Instantiate(_cubeGO, Vector3.zero, Quaternion.identity, transform);

					// Obtengo los signos
					var difference = (_maxLimit.position - _minLimit.position).normalized;

					// Defino el offset referente al tamaño de los cubos
					Vector3 scaleOffset = (spawnedGo.transform.localScale.y / 2f) * Vector3.up;

					// Si todas las dimensiones de la diferencia son diferentes a cero
					if (difference.x != 0 && difference.y != 0 && difference.z != 0)
					{
						// Obtengo los signos de cada dimensión del vector de diferencia
						float signX = Mathf.Sign(difference.x);
						float signY = Mathf.Sign(difference.y);
						float signZ = Mathf.Sign(difference.z);

						// Modifico al igual el offset de escala
						scaleOffset.x *= signX;
						scaleOffset.y *= signY;
						scaleOffset.z *= signZ;
					}

					// Defino la posición global del elemento aplicando el offset de escala
					spawnedGo.transform.position = parsedPosition + scaleOffset;

					// Obtengo el componente cubo del objeto instanciado
					var cube = spawnedGo.GetComponent<Cube>();

					// Si el componente existe
					if (cube)
					{
						// Asigno el Color ID del cubo
						cube.ColorID = ColorID;

						// Agrego el cubo a la lista
						_cubeDataReferences.Add(cube);
					}
					else
					{
						// El componente no existe
						Debug.LogError($"The required component {typeof(Cube)} doesn't exist in the instantiated GO: {gameObject}");
						return; // Cancelo la operación
					}

					// Si el componente no existe, cancelo la operación
				}
			}
			else
			{
				// Si las posiciones seleccionadas deben ir dentro del tunel

				// Si la banda no está definida
				if (!_beltTunnel)
				{
					// Lanzo un error y retorno
					Debug.LogError($"La referencia al tunel de la banda transportadora no ha sido definida : {gameObject}");
					return;
				}

				// Obtengo la posición global basado en una coordenada local dada
				Vector3 parsedPosition;
				if (_minLimit && _maxLimit)
					parsedPosition = _parser.ParseVector(localPosition, _minLimit, _maxLimit);
				else
					parsedPosition = _parser.ParseVector(localPosition);

				// Instancio un nuevo cubo en la posición definida
				var spawnedGo = Instantiate(_cubeGO, parsedPosition, Quaternion.identity, transform);

				// Obtengo el componente cubo del objeto instanciado
				var cube = spawnedGo.GetComponent<Cube>();

				// Si el componente existe
				if (cube)
				{
					// Genero el ID de color del Cubo
					cube.ColorID = ColorID;

					// Creo la referencia
					var cubeReference = new CubeDataReference(cube);

					// Marco la referencia como empezar inactiva
					cubeReference.StartDisabled = true;

					// Agrego el cubo a la lista
					_cubeDataReferences.Add(cubeReference);

					// Lo anexo al tunel de la banda definida
					_beltTunnel.Store(spawnedGo.transform);
				}
				else
				{
					// El componente no existe
					Debug.LogError($"The required component {typeof(Cube)} doesn't exist in the instantiated GO: {gameObject}");
					return; // Cancelo la operación
				}
			}
			// Si no es válida, simplemente no realiza la operación
		}

		/// <summary>
		/// Valida la posición dada según los límites establecidos
		/// </summary>
		/// <param name="target">La posición a validar</param>
		/// <returns></returns>
		private bool ValidatePosition(Vector3 target)
		{

			// Si los limites mínimos no están definidos
			if (!_minLimit) return false;

			// Hago una validación en cada una de las dimensiones del vector
			var wrongX = target.x < 0f || target.x > _limitVector.x;
			var wrongY = target.y < 0f || target.y > _limitVector.y;
			var wrongZ = target.z < 0f || target.z > _limitVector.z;

			// Retorno la booleana resultante
			// Ninguno de los ejes está fuera de los límites (true)
			// Uno o más ejes están fuera de los límites (false)
			return (!wrongX && !wrongY && !wrongZ);

		}

		/// <summary>
		/// Ajusta la posición del vector a los limites establecidos
		/// </summary>
		/// <param name="target">Posición a ajustar</param>
		/// <param name="steps">Número de ajuste al que se va a someter el vector</param>
		/// <returns>El vector ajustado</returns>
		private Vector3 AdjustPosition(Vector3 target, float steps = 0.05f)
		{

			// Hago un ajuste hacia el número más cercano multiplo de steps
			target.x = Mathf.Round(target.x / steps) * steps;
			target.y = Mathf.Round(target.y / steps) * steps;
			target.z = Mathf.Round(target.z / steps) * steps;

			// Retorno el valor
			return target;
		}

		/// <summary>
		/// Obtengo los cubos que son hijos del transform actual
		/// </summary>
		private void ReadCubesFromChildren()
		{
			// Obtengo el número de hijos
			int childCount = transform.childCount;

			// Reinicio la lista
			_cubeDataReferences.Clear();

			// Recorro la cuenta de los hijos
			for (int i = 0; i < childCount; i++)
			{
				// Obtengo el hijo
				var child = transform.GetChild(i);

				// Obtengo el componente Cubo
				var childCube = child.GetComponent<Cube>();

				// Si el componente existe
				if (childCube != null)
				{

					// Lo agrego a la lista
					_cubeDataReferences.Add(childCube);
				}
			}
		}

		/// <summary>
		/// Define la posición a los cubos indicados
		/// </summary>
		/// <param name="argIndex">Índice del cubo registrado</param>
		/// <param name="localPosition">Posición para el cubo dentro del area</param>
		public void SetCubePosition(int argIndex, Vector3 localPosition)
		{
			// Si el índice es válido y la posición es válida
			if (argIndex < _cubeDataReferences.Count && ValidatePosition(localPosition))
			{
				// Obtengo la referencia a los datos del cubo y modifico su posición local
				_cubeDataReferences[argIndex].CubeComponent.transform.localPosition = localPosition;
			}
			else
			{
				// Lanzo un error
				Debug.LogError($"One or both argument can't be processed : {gameObject}");
			}
		}

		/// <summary>
		/// Reseteo las posiciones y rotaciones de todos los cubos registrados
		/// </summary>
		public void ResetCubes()
		{

			// Al iniciar el reinicio de los cubos
			if (_onCubeResetStart != null) _onCubeResetStart.Invoke();

			// Obtengo la cuenta de los cubos
			int count = _cubeDataReferences.Count;

			// Recorro el número de cubos
			for (int i = 0; i < count; i++)
			{

				// Reinicio las propiedades del cubo
				CubeDataReference cubeDataReference = _cubeDataReferences[i];
				cubeDataReference.CubeComponent.Reset();

				// Restauro el cubo
				cubeDataReference.Restore();
			}

			// Si la referencia al belt tunnel existe
			if (_beltTunnel != null)
			{
				// Reinicio el registro
				_beltTunnel.CreateQueue();
			}

			// Al terminar el reinicio de los cubo
			if (_onCubeResetEnd != null) _onCubeResetEnd.Invoke();
		}
	}
}