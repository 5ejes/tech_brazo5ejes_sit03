﻿using System.Collections;
using System.Collections.Generic;
using NSBoxMessage.Inspector;
using NSUtilities;
using UnityEngine;
using static NSScriptableEvent.ScriptableEventErrorDefinition;

public class ErrorDefinitionBoxActionSender : GenericBoxActionSender<ErrorDefinition>
{
	[Header("Events")]
	[SerializeField] private ErrorDefinitionEvent _onArgSend;
	public override void Send(ErrorDefinition arg)
	{
		// Si el argumento existe
		if (arg != null)
		{
			// Si el inspector existe
			if (Inspector != null)
			{
				// Convierto el inspector en inspector de error
				var errorInspector = (BoxErrorInspector)Inspector;

				// Si existe el inspector de error
				if (errorInspector)
				{
					// Hago una llamada sobrescrita del evento aceptar
					errorInspector.ComunicateErrorOverride(arg, OnAcceptButtonCall);

					// Invoco el envío del argumento definido
					_onArgSend.WrapInvoke(arg);
				}
			}
		}
	}
}
