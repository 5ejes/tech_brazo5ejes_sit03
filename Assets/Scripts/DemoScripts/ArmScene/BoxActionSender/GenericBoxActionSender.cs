﻿using System.Collections;
using System.Collections.Generic;
using NSBoxMessage.Inspector;
using NSUtilities;
using UnityEngine;
using UnityEngine.Events;

public abstract class GenericBoxActionSender<T>: MonoBehaviour
{
  [Header("References")]
  [SerializeField] private BoxMessageInspector _inspector;
  [Header("Events")]
  [SerializeField] private UnityEvent _onAcceptButtonCall;
  [SerializeField] private UnityEvent _onCancelButtonCall;
  [Header("Settings")]
  [SerializeField] private bool _autoRemoveDelegatesOnArgSend = true;

  public BoxMessageInspector Inspector { get => _inspector; set => _inspector = value; }
  public UnityEvent OnAcceptButtonCall { get => _onAcceptButtonCall; }
  public UnityEvent OnCancelButtonCall { get => _onCancelButtonCall; }
  public bool AutoRemoveDelegatesOnArgSend { get => _autoRemoveDelegatesOnArgSend; set => _autoRemoveDelegatesOnArgSend = value; }

  private UnityAction _acceptAction;
  private UnityAction _cancelAction;

  public abstract void Send(T arg);
}
