﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReferenceSafeZoneGridDrawer : MonoBehaviour
{
	[Header("Settings")]
	[SerializeField] private Transform _min;
	[SerializeField] private Transform _max;

	[Header("Drawing Settings")]
	[SerializeField] private bool _drawGizmos = true;
	[Space]
	[SerializeField] private bool _gridMode = true;

	[SerializeField] private float _cubeSize = 0.1f;
	[SerializeField] private Color _gridColor = Color.cyan;

	private void OnDrawGizmos()
	{
		if (_min && _max && _drawGizmos)
		{
			var difference = _max.position - _min.position;
			Vector3 absDifference = Vector3.zero;

			absDifference.x = Mathf.Abs(difference.x);
			absDifference.y = Mathf.Abs(difference.y);
			absDifference.z = Mathf.Abs(difference.z);

			if (_gridMode && absDifference != Vector3.zero)
			{
				Vector3 division;

				division.x = absDifference.x / _cubeSize;
				division.y = absDifference.y / _cubeSize;
				division.z = absDifference.z / _cubeSize;

				int countX, countY, countZ;

				// countX = Mathf.RoundToInt(division.x);
				// countY = Mathf.RoundToInt(division.y);
				// countZ = Mathf.RoundToInt(division.z);

				countX = 8;
				countY = 5;
				countZ = 8;

				var signX = Mathf.Sign(difference.x);
				var signY = Mathf.Sign(difference.y);
				var signZ = Mathf.Sign(difference.z);

				for (int x = 0; x < countX; x++)
				{
					for (int y = 0; y < countY; y++)
					{
						for (int z = 0; z < countZ; z++)
						{
							var currentPosition = new Vector3(signX * x * (absDifference.x / countX),
								signY * y * absDifference.y / countY, signZ * z * absDifference.z / countZ
							);

							currentPosition += _min.position;
							currentPosition += new Vector3(signX * _cubeSize / 2, signY * _cubeSize / 2, signZ * _cubeSize / 2);

							Gizmos.color = _gridColor;
							Gizmos.DrawWireCube(currentPosition, _cubeSize * Vector3.one);
						}
					}
				}
			}
			else
			{
				Gizmos.color = _gridColor;

				var position = _min.position + (difference / 2);

				Gizmos.DrawCube(position, difference);

				var wireColor = _gridColor;
				wireColor.a = 1f;

				Gizmos.color = wireColor;
				Gizmos.DrawWireCube(position, absDifference);
			}
		}
	}
}