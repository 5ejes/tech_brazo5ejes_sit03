﻿using NSInterfazControlPLC;
using UnityEngine;

public class GripGuideWriter : MonoBehaviour
{
	[Header("References")]
	[SerializeField] private grafcetOutputInspector _grafcetOutput;

	[Header("Options")]
	[SerializeField] private bool _enableWrite;
	public bool EnableWrite
	{
			get { return _enableWrite; }
			set { _enableWrite = value; }
	}
	

	public void WriteEje5Data(bool value)
	{
		// Si el script no está activo o el output no está definido
		// No realizo la acción
		if(!EnableWrite || _grafcetOutput == null) return;
		
		// Escribo el valor del quinto eje
		_grafcetOutput.WriteEje5State(value);
	}
}
