﻿using System.Collections;
using System.Collections.Generic;
using NSUtilities;
using UnityEngine;
using static NSScriptableEvent.ScriptableEventErrorDefinition;

public class ErrorDefinitionFilterInspector : MonoBehaviour
{
	[System.Serializable]
	public class ErrorDefinitionFilter
	{
		[Header("Settings"), SerializeField] private string errorCode;
		[SerializeField] private ErrorDefinitionEvent _responde;
		[SerializeField] private bool _filterActive = true;

		public ErrorDefinitionFilter(string errorCode)
		{
			// Defino el codigo de error y creo una respuesta al evento
			ErrorCode = errorCode;
			_responde = new ErrorDefinitionEvent();
		}

		public string ErrorCode { get => errorCode; set => errorCode = value; }
		public ErrorDefinitionEvent Responde { get => _responde; }
		public bool FilterActive { get => _filterActive; set => _filterActive = value; }

		public void FireResponse(ErrorDefinition errorDefinition)
		{
			// Ejecuto la respuesta
			Responde.WrapInvoke(errorDefinition);
		}
	}

	[Header("Default Event Response")]
	[SerializeField] private ErrorDefinitionEvent _defaultResponse;
	[Header("Settings")]
	[SerializeField] private List<ErrorDefinitionFilter> _filters = new List<ErrorDefinitionFilter>();
	public List<ErrorDefinitionFilter> Filters { get => _filters; }
	public ErrorDefinitionEvent DefaultResponse { get => _defaultResponse; }

	public ErrorDefinitionFilter GetFilter(string code)
	{
		ErrorDefinitionFilter output = null;
		if (string.IsNullOrEmpty(code)) return output;

		// Hallo el filtro que coincide
		output = Filters.Find(listFilter => listFilter.ErrorCode.ToLower().Equals(code.ToLower()));
		return output;
	}

	public void EnableFilter(string code)
	{
		var filter = GetFilter(code);
		if(filter == null) return;
		
		filter.FilterActive = true;
	}

	public void DisableFilter(string code)
	{
		var filter = GetFilter(code);
		if(filter == null) return;

		filter.FilterActive = false;
	}

	public void Filter(ErrorDefinition definition)
	{
		// Si la lista de filtros está definida
		if (_filters != null)
		{
			// Si al menos existe un filtro
			if (_filters.Count > 0)
			{
				// Busco el filtro que coincida con mis parámetros
				var filter = _filters.Find(listFilter => listFilter.ErrorCode.ToLower().Equals(definition.ErrorCode.ToLower()));

				// Si el filtro existe
				if (filter != null)
				{
					// Si el filtro no está activo
					if (!filter.FilterActive) return;

					// Ejecuto la respuesta
					filter.FireResponse(definition);
				}
				else
				{
					// Si el filtro no existe

					// Ejecuto la respuesta por defecto
					DefaultResponse.WrapInvoke(definition);
				}
			}
		}
	}

}
