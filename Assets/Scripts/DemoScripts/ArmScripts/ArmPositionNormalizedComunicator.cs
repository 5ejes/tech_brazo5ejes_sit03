﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Modular;
using NSUtilities;
using UnityEngine;
using System;

public class ArmPositionNormalizedComunicator : MonoBehaviour
{
	[Header("Arm Position Handler")]
	[SerializeField] private Transform _maxLimit;
	[SerializeField] private Transform _minLimit;
	[SerializeField] private Transform _gripTipPosition;

	[Header("Settings")]
	[SerializeField] private int _updateFrame = 0;
	[SerializeField] private Vector3 _armNormalizedPosition;

	[Header("Events")]
	[SerializeField] private Vector3Event _onArmPositionUpdate;
	[SerializeField] private FloatEvent _onXCoordinateUpdate;
	[SerializeField] private FloatEvent _onYCoordinateUpdate;
	[SerializeField] private FloatEvent _onZCoordinateUpdate;

	public void Update()
	{

		// Si el refresh frame es mayor a 0
		if (_updateFrame > 0)
		{
			// Evaluo el framecount

			// Si es distinto del update frame, retorno
			if (Time.frameCount % _updateFrame != 0) return;
		}

		// Si el griptip no ha sido definido, retorno
		if (_gripTipPosition == null) return;

		// Si el límite mínimo y el límite máximo no están definidos, no se puede realizar la operación
		if (_minLimit == null || _maxLimit == null) return;

		_armNormalizedPosition = GetNormalizedPosition(_gripTipPosition.position);

		// Comunico la posición vectorial
		_onArmPositionUpdate.WrapInvoke(_armNormalizedPosition);

		// Comunico la posición en x, y, además de en, z
		_onXCoordinateUpdate.WrapInvoke(_armNormalizedPosition.x);
		_onYCoordinateUpdate.WrapInvoke(_armNormalizedPosition.y);
		_onZCoordinateUpdate.WrapInvoke(_armNormalizedPosition.z);
	}

	private Vector3 GetNormalizedPosition(Vector3 worldPosition)
	{
		Vector3 output = new Vector3();

		Vector3 minPosition, maxPosition;

		minPosition = _minLimit.position;
		maxPosition = _maxLimit.position;

		output.x = Mathf.InverseLerp(minPosition.x, maxPosition.x, worldPosition.x);
		output.y = Mathf.InverseLerp(minPosition.y, maxPosition.y, worldPosition.y);
		output.z = Mathf.InverseLerp(minPosition.z, maxPosition.z, worldPosition.z);

		return output;
	}
}