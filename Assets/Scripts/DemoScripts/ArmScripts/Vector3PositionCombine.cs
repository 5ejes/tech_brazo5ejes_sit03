﻿using System;
using Modular;
using NSUtilities;
using UnityEngine;

public class Vector3PositionCombine : MonoBehaviour
{
	[Header("Values")]
	[SerializeField] private float _xValue;
	[SerializeField] private float _yValue;
	[SerializeField] private float _zValue;

	[Header("Settings")]
	[SerializeField] private int _updateFrame;

	[Header("Events")]
	[SerializeField] private Vector3Event _onVector3Combine;
	private float _previousXValue;
	private float _previousYValue;
	private float _previousZValue;


	private Vector3 _combinationVector;

	public float XValue { get => _xValue; set => _xValue = value; }
	public float YValue { get => _yValue; set => _yValue = value; }
	public float ZValue { get => _zValue; set => _zValue = value; }

	private void SetValues(float x, float y, float z)
	{
		_xValue = x;
		_yValue = y;
		_zValue = z;
	}

	private void Update()
	{
		// Si los frames de actualización son superiores a cero y el frame no coincide con el módulo de frame actual, retorno
		if (_updateFrame > 0 && Time.frameCount % _updateFrame != 0) return;

		if (HasAnyValueChanged())
		{
			UpdateAllPreviousValues();
			CombineVector3();

			_onVector3Combine.WrapInvoke(_combinationVector);
		}
	}

	private void UpdateAllPreviousValues()
	{
		_previousXValue = XValue;
		_previousYValue = YValue;
		_previousZValue = ZValue;
	}

	private bool HasAnyValueChanged()
	{
		return (_previousXValue != XValue || _previousYValue != YValue || _previousZValue != ZValue);
	}

	private void CombineVector3()
	{
		_combinationVector.x = XValue;
		_combinationVector.y = YValue;
		_combinationVector.z = ZValue;
	}
}
