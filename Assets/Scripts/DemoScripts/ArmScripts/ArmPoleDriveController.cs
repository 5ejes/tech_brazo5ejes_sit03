﻿using NSUtilities;
using UnityEngine;

namespace NSBrazo5Ejes
{

	public class ArmPoleDriveController : MonoBehaviour
	{

		[Header("References")]
		[SerializeField] private ArmPoleHandler _poleHandler;
		[SerializeField] private ArmPositionHandler _positionHandler;


		[Header("Interpolation Values")]
		[SerializeField] private float _horizontalMax = 1f;
		[SerializeField] private float _horizontalMin = -1f;
		[Space]
		[SerializeField] private float _verticalMax = 1.5f;
		[SerializeField] private float _verticalMin = 1.5f;
		[Header("Position Distance Value Setting")]
		[SerializeField] private float _minCheckDistance = 0.025f;
		[Header("Call Settings")]
		[SerializeField] private bool _callOnUpdate = true;
		[SerializeField] private float _callEach = 0.0025f;

		private Vector3 _positionRegistered = -Vector3.one;

		public bool CallOnUpdate { get => _callOnUpdate; set => _callOnUpdate = value; }
		public float CallEach { get => _callEach; set => _callEach = value; }

		private float _nextUpdateCall = 0f;

		private void Update()
		{
			if (!CallOnUpdate) return;
			if (CallEach > 0)
			{
				if (Time.time < _nextUpdateCall) return;
				_nextUpdateCall = Time.time + CallEach;
			}

			// Ejecuto el update de pesos del polo
			SimplePoleWeightUpdate();
		}

		private void SimplePoleWeightUpdate()
		{

			// Obtengo los pesos
			Vector3 targetPosition = _poleHandler.LeafEnd.position;

			// Si la dimensión de la posición registrada es mayor al mínimo
			if ((targetPosition - _positionRegistered).sqrMagnitude > Mathf.Pow(_minCheckDistance, 2))
			{
				// Convierto la posición a local
				targetPosition = _positionHandler.ConvertToLocalPosition(targetPosition);

				// Fijo la posición a valores concretos
				targetPosition.ToAbsolute().SnapVector(0.05f);

				// Convierto la posición concreta en global nuevamente
				targetPosition = _positionHandler.ConvertToWorldPosition(targetPosition);

				// Registro la posición actual
				_positionRegistered = targetPosition;

				// Obtengo los pesos según la posición procesada
				var weights = _poleHandler.GetLimitWeights(targetPosition);

				// Obtengo los valores de acuerdo a los pesos de movimiento (de 0 a 1 donde 0 es muy cerca o abajo, y 1 es muy lejos o arriba)
				var horizontalValue = Mathf.Lerp(_horizontalMin, _horizontalMax, weights.x);
				var verticalValue = Mathf.Lerp(_verticalMin, _verticalMax, weights.y);

				// Defino los valores objetivo
				_poleHandler.HorizontalOffset = horizontalValue;
				_poleHandler.VerticalOffset = verticalValue;
			}
		}
	}
}
