﻿using UnityEngine;

namespace NSBrazo5Ejes
{
	public class TransformHolder : MonoBehaviour
	{
		[Header("Settings")]
		[SerializeField] private bool _populateWithChildren = true;
		[Header("Values")]
		[SerializeField] private Transform[] _values;
		public Transform[] Values { get => _values; set => _values = value; }
		private Transform _transform;

		private void Awake()
		{
			_transform = transform;

			if (_populateWithChildren)
			{
				ResolveChildren();
			}
		}

		private void ResolveChildren()
		{
			if (!_transform) return;

			int count = _transform.childCount;
			Values = new Transform[count];

			for (int i = 0; i < count; i++)
			{
				Values[i] = _transform.GetChild(i);
			}
		}
	}
}
