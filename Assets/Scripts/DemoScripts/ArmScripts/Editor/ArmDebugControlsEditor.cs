﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CanEditMultipleObjects]
[CustomEditor(typeof(ArmDebugControls))]
public class ArmDebugControlsEditor : Editor
{

	private ArmDebugControls _editorTarget;

	void OnEnable()
	{
		// Casteo el editor target
		_editorTarget = (ArmDebugControls)target;
	}

	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();
		GUILayout.FlexibleSpace();
		if (GUILayout.Button("Mover Grip"))
		{
			if (_editorTarget != null)
			{
				_editorTarget.MoveOrderSend(_editorTarget.Data);
			}
		}

		if (GUILayout.Button("Activar Grip"))
		{
			if (_editorTarget != null)
			{
				_editorTarget.GripButtonPress();
			}
		}
	}



}
