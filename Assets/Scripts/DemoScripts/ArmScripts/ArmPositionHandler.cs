﻿#pragma warning disable 0649
using System;
using System.Collections;
using System.Collections.Generic;
using NSUtilities;
using UnityEngine;
using UnityEngine.Events;
using static NSInspectorSender.Vector3InspectorSender;
using static NSScriptableEvent.ScriptableEventBool;

public class ArmPositionHandler : MonoBehaviour
{
	private const float _minDistanceThreshold = 0.000001f;
	private const float _minDistanceMoveThreshold = 0.00025f;

	public static readonly Vector3 MIN_LIMITS = new Vector3(0, 0, 0);
	public static readonly Vector3 MAX_LIMITS = new Vector3(0.8f, 0.5f, 0.8f);

	private static readonly Vector3 _minVector3ValueCorrection = new Vector3(0f, 0.05f, 0f);

	[Header("References")]
	[SerializeField] private Transform _target;
	[SerializeField] private Transform _gripTip;
	[SerializeField] private Transform _gripRotationGuide;

	[Header("Grips")]
	[SerializeField] private Transform _gripOffset;
	[Space]
	/// <summary>
	/// Transform usado para convertir coordenadas WorldSpace en LocalSpace de manera adecuada, para que funcionen con el brazo
	/// </summary>
	[SerializeField] private Transform _positioningMinConverter;
	[Header("Settings")]
	[SerializeField] private float _safeZoneValue = 1.35f;
	[Space]
	[SerializeField] private Transform[] _armBounds;

	[Space]
	[Header("Positioning Settings")]
	[SerializeField] private float _armSpeed;
	[Space]
	[SerializeField] private float _steps = 0.05f;
	[Header("Bounds")]
	[SerializeField] private Transform _min;
	[SerializeField] private Transform _max;
	[Header("Actions")]
	[SerializeField] private UnityEvent _onPositionStart = new UnityEvent();
	[SerializeField] private UnityEvent _onPositionEnd = new UnityEvent();
	[Space]
	[SerializeField] private UnityEvent _onGoToHomeStart = new UnityEvent();
	[SerializeField] private UnityEvent _onGoToHomeEnd = new UnityEvent();
	[SerializeField] private UnityEvent _onArmPositioning = new UnityEvent();

	[Header("Error Events")]
	[SerializeField] private UnityEvent _onWrongCoordinatePassed = new UnityEvent();

	[Header("Position Events")]
	[SerializeField] private Vector3Event _onLocalTargetPosition = new Vector3Event();
	[SerializeField] private Vector3Event _onWorldTargetPosition = new Vector3Event();

	[Header("Draw Gizmos")]
	[SerializeField] private bool _drawDebug;

	private ArmGripController _gripController;
	public ArmGripController GripController
	{
		get
		{
			if (!_gripController) _gripController = GetComponent<ArmGripController>();
			return _gripController;
		}
	}

#pragma warning restore 0649

	private bool _Co_MoveToPosition = false;
	private bool _Co_CyclePositions = false;

	private TransformBound[] _gripRotationTransformBounds;
	private string _logText = string.Empty;

	private Vector3 baseTargetPosition;
	private bool _lastUseGrip;
	private bool _storedUseHorizontalPreviousValue = false;
	private Vector3 _currentWorldPosition;
	private Vector3 _currentLocalPosition;
	private Vector3 _lastLocalPosition;

	private Vector3 _storedGripGuidePosition = Vector3.one * -1f;
	private Quaternion _storedGripGuideRotation = Quaternion.identity;

	private Coroutine _horizontalDelayWaitForCoroutine = null;
	private Coroutine _waitForCoroutine;

	private IKSolver _ikSolver = null;

	private WaitForSeconds _horizontalTransitionYield;

	private bool _stopMovement = false;
	private bool _horizontalMovementTransition = false;

	private float _distanceToVerticalGrip = float.MinValue;

	public Transform PositioningMinConverter { get => _positioningMinConverter; set => _positioningMinConverter = value; }
	public Transform Target { get => _target; }

	public Transform GripOffset
	{
		get
		{
			return _gripOffset;
		}
	}

	public Vector3 CurrentWorldPosition { get => _currentWorldPosition; set => _currentWorldPosition = value; }
	public Vector3 CurrentLocalPosition { get => _currentLocalPosition; set => _currentLocalPosition = value; }
	public Vector3 LastLocalPosition { get => _lastLocalPosition; set => _lastLocalPosition = value; }
	public Transform GripTip { get => _gripTip; private set => _gripTip = value; }
	public float SafeZoneValue { get => _safeZoneValue; set => _safeZoneValue = value; }
	public Vector3 ErrorReturnPosition { get => _errorReturnPosition; }
	public IKSolver IkSolver
	{
		get
		{
			// Si el ik solver no está definido
			if (_ikSolver == null)
			{
				// Si el target (Transform de IKSolver) existe
				if (Target != null)
				{
					// Obtengo el ik solver via GetComponent
					_ikSolver = Target.GetComponent<IKSolver>();
				}
			}

			return _ikSolver;
		}
	}

	public float DistanceToVerticalGrip
	{
		get
		{
			if (_distanceToVerticalGrip < 0)
			{
				_distanceToVerticalGrip = (Target.position - _gripOffset.position).magnitude;
			}
			return _distanceToVerticalGrip;
		}
		set => _distanceToVerticalGrip = value;
	}

	private Vector3 _errorReturnPosition = new Vector3();

	private TransformBound[] GenerateBounds(Transform[] argTransformArray)
	{
		if (argTransformArray != null)  // Si existe la referencia a los bounds
		{
			if (argTransformArray.Length != 0)  // Si existe al menos un transform
			{
				// Verificio la longitud
				int armBoundsLength = argTransformArray.Length;
				// Creo un nuevo arreglo de bounds

				// Creo una lista temporal de los límites
				List<TransformBound> tempList = new List<TransformBound>();

				// Por el número de arm bounds registrados
				for (int armBoundIndex = 0; armBoundIndex < armBoundsLength; armBoundIndex++)
				{
					// Obtengo el transform actual registrado
					Transform currentArmBound = argTransformArray[armBoundIndex];

					// Lo anexo a la lista
					// Hay una conversión explicita de Transform a TransformBound
					tempList.Add(currentArmBound);
				}

				// retorno el arreglo
				return tempList.ToArray();

			}
			else  // Si no hay bounds que verificar
			{
				// Declaro la evaluación como verdadera
				return null;
			}
		}
		else  // El arreglo de transform no existe
		{
			return null;
		}
	}

	private void GenerateCheckNewBounds()
	{
		// Si hay transform registrados como límites
		if (_armBounds != null)
		{
			// Obtengo la longitud de los transform del brazo registrados
			int armTransformLength = _armBounds.Length;

			// Si hay al menos un transform registrado en el arreglo
			if (armTransformLength > 0)
			{
				// Si ya hay un arreglo de límites de transformación generados
				if (_gripRotationTransformBounds != null)
				{
					// Si ya se ha generado los transform bounds
					// Verifico las longitudes
					int transformBoundsLength = _gripRotationTransformBounds.Length;
					// Si hay una diferencia
					if (!armTransformLength.Equals(transformBoundsLength))
					{
						// Genero un nuevo arreglo 
						_gripRotationTransformBounds = GenerateBounds(_armBounds);
					}

				}
				else
				{
					// No hay límites de transformación generados

					// Genero un nuevo arreglo 
					_gripRotationTransformBounds = GenerateBounds(_armBounds);
				}
			}
		}
	}

	[ContextMenu("Debug -> Print Distance To Tip")]
	private void PrintDistanceToTip()
	{
		Vector3 localAnchor = new Vector3(0, 0.05f, 0.4f);
		var localTipPositition = CurrentLocalPosition;
		float distance = Vector3.Distance(localAnchor, localTipPositition);

		Debug.Log($"Distance to tip: {distance}");
	}

	private void WaitFor(float seconds, Action callback)
	{
		// Si no hay un callback, retorno
		if (callback == null) return;
		// Si no hay que esperar o el valor es erroneo
		if (seconds <= 0)
		{
			callback();
			return;
		}

		// Creo la coroutine
		var coroutine = WaitForCo(seconds, callback);
		_waitForCoroutine = StartCoroutine(coroutine);
	}

	private IEnumerator WaitForCo(float seconds, Action callback)
	{

		// Si el callback es nulo, interrumpo
		if (callback == null) yield break;
		// Si no hay que esperar o el valor es erroneo
		if (seconds <= 0)
		{
			callback();
			yield break;
		}

		// Espero los segundos especificados
		yield return new WaitForSeconds(seconds);

		// Ejecuto el callback
		callback();
	}

	private bool IsPointInside(Vector3 position, TransformBound[] bounds, bool ANDEvaluation = false)
	{
		// Si la referencia a los límites no es nula
		if (bounds != null)
		{
			// Si hay al menos un límite
			int length = bounds.Length;
			if (length > 0)
			{
				// Genero un arreglo de los resultados
				bool[] results = new bool[length];

				// Por cada límite
				for (int boundIndex = 0; boundIndex < length; boundIndex++)
				{
					// Obtengo el límite actual
					TransformBound currentBound = bounds[boundIndex];

					// Verifico si el punto está dentro del límite
					results[boundIndex] = currentBound.IsInside(position);
				}

				// Genero el resultado 
				// Si la evaluación AND está activada, solo retorno true si todos los límites evaluan true
				// Si la evaluación AND está desactivada, retorno true si al menos un límite evaluó true
				return ANDEvaluation ? results.ANDEvaluation() : results.OREvaluation();
			}
		}

		// No hay ningún límite, retorno false;
		return false;
	}

	private Vector3 ClampVector(Vector3 targetVector, Vector3? minClampVector = null, Vector3? maxClampVector = null)
	{
		// Defino los vectores de salida y evaluación
		Vector3 output = Vector3.zero;
		Vector3 minVector, maxVector;

		// Defino, si existe, el vector máximo, y el mínimo
		// En caso de no existir los defino al vector de entrada
		minVector = minClampVector ?? targetVector;
		maxVector = maxClampVector ?? targetVector;

		// Defino la variable de salida con los valores mínimos y máximos
		output.x = Mathf.Clamp(targetVector.x, minVector.x, maxVector.x);
		output.y = Mathf.Clamp(targetVector.y, minVector.y, maxVector.y);
		output.z = Mathf.Clamp(targetVector.z, minVector.z, maxVector.z);

		return output;
	}

	private Vector3 AdjustVector(Vector3 target)
	{

		// Defino los steps (Número entre el que se va a mover)
		var steps = _steps;

		// Hago un ajuste hacia el número más cercano multiplo de steps
		target.x = Mathf.Round(target.x / steps) * steps;
		target.y = Mathf.Round(target.y / steps) * steps;
		target.z = Mathf.Round(target.z / steps) * steps;

		// Compruebo los límites de cada dimensión
		bool isValidVector = IsValidLocalPosition(target);
		if (isValidVector)
		{
			// Ejecuto el error de coordenada inválida
			_onWrongCoordinatePassed.WrapInvoke();

			// Defino el target como el valor por defecto mínimo
			// target = Vector3.up * 0.05f;
			target = Vector3.zero;
			return target;
		}

		// Si el valor no tiene números fuera de rango

		// Retorno el valor
		return target;
	}

	private bool IsValidLocalPosition(Vector3 target)
	{
		bool wrongX = target.x < MIN_LIMITS.x || target.x > MAX_LIMITS.x;
		bool wrongY = target.y < MIN_LIMITS.y || target.y > MAX_LIMITS.y;
		bool wrongZ = target.z < MIN_LIMITS.z || target.z > MAX_LIMITS.z;

		// Si alguno de los ejes no está en el valor predeterminado
		return wrongX || wrongY || wrongZ;
	}

	private void TrackPoint(Vector3 position, Space space = Space.World)
	{
		// Si empieza en coordenadas locales
		if (space.Equals(Space.Self))
			// Transformo la posición a coordenadas globales
			position = ConvertToWorldPosition(position);

		// // Obtengo la dirección hacia la grip
		// Vector3 direction = new Vector3();
		// direction = Target.position - _gripTip.position;

		// Target.position = position + (direction.normalized * _distanceToGripTip);
		// Debug.DrawRay(_gripTip.position, direction, Color.blue);

		Target.position = position;

	}

	public void StopTranslation()
	{
		// Si el delay del brazo en modo horizontal está activo
		if (_horizontalDelayWaitForCoroutine != null)
			StopCoroutine(_horizontalDelayWaitForCoroutine);	// Lo cancelo

		// Si la corutina de espera está activa
		if(_waitForCoroutine != null)
			StopCoroutine(_waitForCoroutine);	// Cancelo la espera

		// Detengo la translación del brazo
		_stopMovement = true;
		_Co_MoveToPosition = false;
	}

	public void GoToHome()
	{

		#region Implementación antigua

		// // muevo el brazo a la posición home
		// MoveToPosition(_min.position);

		#endregion

		// Ejecuto el evento de inicio de reinicio de posiciones
		if (_onGoToHomeStart != null) _onGoToHomeStart.Invoke();

		// Creo el arreglo de tres puntos para dirigirme a HOME
		Vector3 upTarget = new Vector3(Target.position.x, _max.position.y, Target.position.z);
		Vector3 upHome = new Vector3(_min.position.x, _max.position.y, _min.position.z);
		Vector3 Home = _min.position;

		// Convierto los elementos en coordenadas locales
		upTarget = ConvertToLocalPosition(upTarget);
		upHome = ConvertToLocalPosition(upHome);
		Home = ConvertToLocalPosition(Home);

		var positionsToHome = new Vector3[3]
		{
			// Posición por encima del target
			upTarget.ToAbsolute(),
				// Posición por encima de HOME
				upHome.ToAbsolute(),
				// Posición de HOME
				Home.ToAbsolute()
		};

		// Actualizo la posición home como la actual y la última posición
		CurrentLocalPosition = LastLocalPosition = Home.ToAbsolute();

		// Entrego el arreglo de las posiciones a la corutina
		CyclePositions(positionsToHome, () =>
		{
			// Si existe, invoco el callback de finalización
			if (_onGoToHomeEnd != null) _onGoToHomeEnd.Invoke();
		});
	}

	public float GetMaximumTopHeight()
	{
		return _max.position.y + _safeZoneValue;
	}

	public void CyclePositions(Vector3[] argPositions, System.Action onEndCallback = null)
	{
		// Inicio la corutina
		var coroutine = CyclePositionsCo(argPositions, onEndCallback);
		StartCoroutine(coroutine);
	}

	public void MoveToPosition(Vector3 argTargetPosition)
	{
		// Si hay alguna transición activa, la detengo
		StopTranslation();

		// Creo los límites para definir la dirección relativa
		var limitDir = GetLimitDirection();

		// Ajusto el Vector de posición
		argTargetPosition = AdjustVector(argTargetPosition);

		// Limito el vector
		argTargetPosition = ClampVector(argTargetPosition, _minVector3ValueCorrection);


		var localAbsolute = argTargetPosition.ToAbsolute();

		// Almaceno la ultima posición local y la actual
		LastLocalPosition = CurrentLocalPosition;
		CurrentLocalPosition = localAbsolute;

		// Ejecuto el evento
		_onLocalTargetPosition.WrapInvoke(localAbsolute);

		// Obtengo la diferencia

		// Defino la dirección de movimiento según los límites
		argTargetPosition = ApplyLimitDirection(argTargetPosition, limitDir);

		// Inicializo la posición relativa
		argTargetPosition += _min.position;

		// Substraigo el valor 0.05 para ajustar el valor requerido mínimo (offset)
		argTargetPosition.y -= 0.05f;

		Debug.DrawRay(argTargetPosition, Vector3.one.normalized * 0.1f, Color.red, 10f);

		// Resuelvo las rotaciones

		#region Implementación de rotaciones

		// Roto la posición
		argTargetPosition = RotatePointAround(_min.position, argTargetPosition, transform.rotation);

		// Almaceno la posición original
		baseTargetPosition = argTargetPosition;

		#endregion

		// Genero nuevos puntos
		// GenerateCheckNewBounds();

		/* Se omite la comprobación de uso horizontal*/
		// Compruebo si la posición está dentro de algun límite
		// UseHorizontalGrip = IsPointInside(argTargetPosition, _gripRotationTransformBounds, false);

		// Envió un evento notificando el valor de UseHorizontalGrip después de su comprobación
		// _onHorizontalGripEvaluation.WrapInvoke(UseHorizontalGrip);

		//// Aplico el offset
		// var gripOffset = GetOffsetToGrip(argTargetPosition);
		// argTargetPosition += gripOffset;

		// Genero la distancia entre la posición objetivo y la posición actual del brazo
		var distance = Vector3.Distance(Target.position, (Vector3)argTargetPosition);

		// Ejecuto el evento
		_onWorldTargetPosition.WrapInvoke(argTargetPosition);
		CurrentWorldPosition = argTargetPosition;

		// Si la distancia corresponde a un valor mayor al límite
		if (distance > _minDistanceThreshold)
		{

			WaitFor(Time.deltaTime, () =>
			{
				// Defino la corutina
				var coroutine = MoveToPositionCo(_gripRotationGuide, argTargetPosition, _armSpeed * Time.deltaTime);

				// Ejecuto la corutina
				StartCoroutine(coroutine);
			});

		}
		else
		{
			// Ejecuto la secuencia de finalización de la posición
			if (_onPositionEnd != null) _onPositionEnd.Invoke();
		}
	}


	public void MoveToPositionUnrestricted(Vector3 argTargetPosition, Action onTranslationEnd = null)
	{

		// Si hay alguna transición activa, la detengo
		StopTranslation();

		// Debug.Log($"Target Position : {argTargetPosition.ToString("0.00")}", this);

		// Creo los límites para definir la dirección relativa
		var limitDir = GetLimitDirection();

		var localAbsolute = argTargetPosition.ToAbsolute();

		//// Evaluo si la posición es válida
		// bool isValid = IsValidLocalPosition(argTargetPosition);

		// Limito el vector
		argTargetPosition = ClampVector(argTargetPosition, _minVector3ValueCorrection);

		// Almaceno la posición actual y la anterior
		LastLocalPosition = CurrentLocalPosition;
		CurrentLocalPosition = localAbsolute;

		// Ejecuto el evento
		_onLocalTargetPosition.WrapInvoke(localAbsolute);

		// Obtengo la diferencia

		// Defino la dirección de movimiento según los límites
		argTargetPosition = ApplyLimitDirection(argTargetPosition, limitDir);

		// Inicializo la posición relativa
		argTargetPosition += _min.position;

		// Substraigo el valor 0.05 para ajustar el valor requerido mínimo (offset)
		argTargetPosition.y -= 0.05f;

		Debug.DrawRay(argTargetPosition, Vector3.one.normalized * 0.1f, Color.red, 10f);

		// Resuelvo las rotaciones

		#region Implementación de rotaciones

		argTargetPosition = RotatePointAround(_min.position, argTargetPosition, transform.rotation);

		// Almaceno la posición original
		baseTargetPosition = argTargetPosition;

		#endregion

		// Genero nuevos puntos
		// GenerateCheckNewBounds();

		/* Se omite la comprobación de uso horizontal*/
		// Compruebo si la posición está dentro de algun límite
		// UseHorizontalGrip = IsPointInside(argTargetPosition, _gripRotationTransformBounds, false);

		// Envió un evento notificando el valor de UseHorizontalGrip después de su comprobación
		// _onHorizontalGripEvaluation.WrapInvoke(UseHorizontalGrip);

		// Si grip offset está definido
		// var gripOffset = GetOffsetToGrip(argTargetPosition);
		// argTargetPosition += gripOffset;

		// Genero la distancia entre la posición objetivo y la posición actual del brazo
		var distance = Vector3.Distance(Target.position, (Vector3)argTargetPosition);

		// Ejecuto el evento
		_onWorldTargetPosition.WrapInvoke(argTargetPosition);
		CurrentWorldPosition = argTargetPosition;

		// Si la distancia corresponde a un valor mayor al límite
		if (distance > _minDistanceThreshold)
		{

			WaitFor(Time.deltaTime, () =>
			{
				// Defino la corutina
				var coroutine = MoveToPositionCo(_gripRotationGuide, argTargetPosition, _armSpeed * Time.deltaTime, onTranslationEnd);

				// Ejecuto la corutina
				StartCoroutine(coroutine);
			});

		}
		else
		{
			// Ejecuto la secuencia de finalización de la posición
			if (_onPositionEnd != null) _onPositionEnd.Invoke();
		}
	}

	private Vector3 GetOffsetToGrip(Vector3 worldPosition)
	{
		Vector3 offset = Vector3.zero;
		// Si grip offset está definido
		if (GripOffset != null)
		{
			// Obtengo el offset hacia el grip
			offset = Vector3.up * DistanceToVerticalGrip;
		}

		return offset;
	}
	public Vector3 RotatePointAround(Vector3 pivotPoint, Vector3 position, Quaternion rotation)
	{

		// Genero un vector que apunte hacia la posición objetivo sin rotar
		var positionDifference = position - pivotPoint;

		// Creo una copia del vector
		var newDiff = positionDifference;

		// Obtengo los límites
		var limitDir = GetLimitDirection();

		// Multiplico por los límites predefinidos
		newDiff.x *= Mathf.Sign(-limitDir.x);
		newDiff.z *= Mathf.Sign(limitDir.z);

		// Roto el vector
		newDiff = rotation * newDiff;

		// Obtengo la nueva posición objetivo, aplicando el vector sobre el punto pivote
		return newDiff + pivotPoint;
	}

	public Vector3 ApplyLimitDirection(Vector3 position, Vector3 limitDirection)
	{
		position.x *= Mathf.Sign(limitDirection.x);
		position.y *= Mathf.Sign(limitDirection.y);
		position.z *= Mathf.Sign(limitDirection.z);

		return position;
	}

	public Vector3 GetLimitDirection()
	{
		return (_max.position - _min.position).normalized;
	}

	public IEnumerator MoveToPositionCo(Transform argTarget, Vector3 argTargetPosition, float argMaxDistanceDelta, Action onTranslationEndCallback = null)
	{

		// Si la corutina está en ejecución. no la vuelvo a ejecutar
		if (_Co_MoveToPosition) yield break;
		_Co_MoveToPosition = true;
		_stopMovement = false;

		// Invoco la acción de inicio del posicionamiento
		if (_onPositionStart != null)
			_onPositionStart.Invoke();

		// Defino la posición inicial como la posición actual del objeto
		Vector3 startingPoint = argTarget.position;

		// Creo un valor para determinar la distancia aceptada entre un punto y su transcurso
		var distanceThreshold = 0.2f;
		var maxSpeedReduction = 0.75f;

		// Creo dos distancias, una distancia al inicio, y una distancia al final
		float distanceToStart, distanceToEnd;

		float usedDistanceDelta;

		// Defino una variable de distancia con el valor máximo
		float distance;

		do
		{
			// Modifico ambas distancias
			Vector3 position = argTarget.position;

			distanceToStart = Vector3.Distance(startingPoint, position);
			distanceToEnd = Vector3.Distance(argTargetPosition, position);

			if (distanceToEnd < distanceThreshold)
			{
				var weight = 1f - (distanceToEnd / distanceThreshold);
				usedDistanceDelta = argMaxDistanceDelta - (argMaxDistanceDelta * maxSpeedReduction * weight);

			}
			else if (distanceToStart < distanceThreshold)
			{
				var weight = 1f - (distanceToStart / distanceThreshold);
				usedDistanceDelta = argMaxDistanceDelta - (argMaxDistanceDelta * maxSpeedReduction * weight);
			}
			else
			{
				usedDistanceDelta = argMaxDistanceDelta;
			}

			usedDistanceDelta = argMaxDistanceDelta;

			distance = Vector3.Distance(position, argTargetPosition);
			position = Vector3.MoveTowards(position, argTargetPosition, usedDistanceDelta);

			argTarget.position = position;

			// Ejecuto el evento de movimiento
			_onArmPositioning.WrapInvoke();


			yield return null;
		}
		while (distance > _minDistanceMoveThreshold && !_stopMovement);

		// Si no se detiene la translación actual
		if (!_stopMovement)
			argTarget.position = argTargetPosition;

		_onArmPositioning.WrapInvoke();


		if (_onPositionEnd != null)
			_onPositionEnd.Invoke();

		if (onTranslationEndCallback != null) onTranslationEndCallback.Invoke();

		_Co_MoveToPosition = false;
		_stopMovement = false;
	}
	private IEnumerator CyclePositionsCo(Vector3[] argPositions, System.Action onEndCallback = null)
	{
		// Si la corutina ya ha iniciado, salgo de ella
		if (_Co_CyclePositions || argPositions.Length == 0) yield break;

		// Marco la corutina como iniciada
		_Co_CyclePositions = true;

		// Creo un índice iniciando por 0
		int index = 0;

		// Creo un callback temporal para el manejo del siguiente índice
		var callback = new UnityAction(() =>
		{
			// Incremento el índice en uno
			index++;
		});

		// Agrego un callback temporal mientras ciclo las posiciones
		_onPositionEnd.AddListener(callback);

		// Mientras el índice sea menor al número de posiciones
		do
		{
			// Obtengo la posición actual
			var currentPosition = argPositions[index];

			// Si ningun movimiento no se está llevando a cabo
			if (!_Co_MoveToPosition)
			{
				// Me muevo hacia la posición actual
				MoveToPosition(currentPosition);
			}

			// Nota:
			// El índice es controlado por el callback
			// El while no tiene control sobre el índice

			yield return null;

		} while (index < argPositions.Length);

		// Elimino el callback
		_onPositionEnd.RemoveListener(callback);

		// Marco la corutina como finalizada
		_Co_CyclePositions = false;

		// Si el callback ha sido definido
		if (onEndCallback != null)
		{
			// Ejecuto el callback
			onEndCallback();
		}
	}
	public void SetInitialStoredPositions()
	{
		LastLocalPosition = CurrentLocalPosition = ConvertToLocalPosition(Target.position).ToAbsolute();
	}
	public void SetErrorPosition()
	{
		// Obtengo la posición objetivo
		_errorReturnPosition = GetCurrentPosition();
		Vector3 errorLocalPosition = ConvertToLocalPosition(_errorReturnPosition);
		
		// Fijo los límites de la posición objetivo a los puntos entre los límites
		errorLocalPosition.x = Mathf.Clamp(errorLocalPosition.x, MIN_LIMITS.x, MAX_LIMITS.x);
		errorLocalPosition.y = Mathf.Clamp(errorLocalPosition.y, MIN_LIMITS.y, MAX_LIMITS.y);
		errorLocalPosition.z = Mathf.Clamp(errorLocalPosition.z, MIN_LIMITS.z, MAX_LIMITS.z);
		
		// Corrijo la altura de la posición de error si está debajo de un threshold
		if (errorLocalPosition.y < 0.2f) errorLocalPosition.y = 0.2f;

		// Defino la posición local y la ultima posición local a la posición de error
		CurrentLocalPosition = LastLocalPosition = errorLocalPosition;

		// Defino la posición de error global basado en la configuración de la posición de error local dada por la posición de la grip
		_errorReturnPosition = ConvertToWorldPosition(errorLocalPosition);
	}

	private Vector3 GetCurrentPosition()
	{
		var offset = Target.position - GripOffset.position;
		Vector3 vector3 = Target.position - offset;
		return vector3;
	}

	[ContextMenu("Test Unrestricted")]
	public void Test()
	{
		MoveToPositionUnrestricted(MAX_LIMITS);
	}

	public void ReturnToErrorPosition()
	{
		// ** Este método puede ser no confiable, es decir, generar errores de translación **
		// TODO: Revisar y testear casos de uso

		// Obtengo el vector de diferencia entre la posición de retorno y la posición del objetivo
		var distance = _errorReturnPosition - Target.position;

		// Si la distancia es mayor a un máximo, retorno
		if (distance.sqrMagnitude > 2.5f) return;

		var targetPosition = ConvertToLocalPosition(_errorReturnPosition);

		// Si la posición 'y' objetivo es menor al valor
		if (targetPosition.y < 0.35f) targetPosition.y = 0.35f; // Defino la posición 'y' objetivo como el valor

		// Actualizo la posición actual y la ultima registrada con el objetivo
		CurrentLocalPosition = LastLocalPosition = targetPosition.ToAbsolute();

		// Muevo el brazo sin restricciones (PUEDE SER PELIGROSO)
		// ** Ocasiona BUGS visuales si la posición inicial de error no es la adecuada o ocurre un fallo en la programación
		MoveToPositionUnrestricted(targetPosition, () =>
		{
			_onGoToHomeEnd.WrapInvoke();
		});
	}

	public void LiftArm()
	{
		// Si existe el positioning min converter
		if (!PositioningMinConverter)
		{
			Debug.LogError($"Positioning Min Converter is null : {this}", this);
			return;
		}
		// Levanta el brazo si supera no supera un threshold mínimo
		var currentArmPosition = ConvertToLocalPosition(GripTip.position);

		// Si la posición actual del brazo está por encima del threshold de altura
		if (currentArmPosition.y > 0.2f) return;

		// Levanto el brazo
		currentArmPosition.y = 0.2f;

		// Muevo la posición del brazo
		MoveToPosition(currentArmPosition);
	}

	public Vector3 ConvertToLocalPosition(Vector3 worldPosition)
	{
		return PositioningMinConverter.InverseTransformPoint(worldPosition);
	}

	public Vector3 ConvertToWorldPosition(Vector3 localPosition)
	{
		return PositioningMinConverter.TransformPoint(localPosition);
	}

	private void OnDrawGizmos()
	{
		if (!_drawDebug) return;

		if (GripOffset != null)
		{
			Gizmos.color = Color.red;
			var gripTipPosition = RotatePointAround(transform.position, GripOffset.position, transform.rotation);
			Gizmos.DrawWireSphere(gripTipPosition, 0.1f);
		}

		Color blue = Color.blue;
		blue.a = 0.2f;
		Gizmos.color = blue;
		var localAnchor = new Vector3(0, 0.05f, 0.4f);
		Gizmos.DrawSphere(ConvertToWorldPosition(localAnchor), 0.385f);

		Gizmos.color = Color.yellow;
		var localPosition = ApplyLimitDirection(CurrentLocalPosition, GetLimitDirection());
		Vector3 worldPosition = ConvertToWorldPosition(localPosition);
		worldPosition = RotatePointAround(_min.position, worldPosition, transform.rotation);
		Gizmos.DrawWireSphere(worldPosition, 0.025f);
	}

	private void OnGUI()
	{
		if (!_drawDebug) return;
		if (!string.IsNullOrEmpty(_logText))
		{
			GUI.skin.textField.fontSize = 40 / Screen.width;
			GUILayout.TextField(_logText);
		}

	}

}