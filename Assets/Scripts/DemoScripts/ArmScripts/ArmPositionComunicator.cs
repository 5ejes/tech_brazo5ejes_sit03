﻿using System.Collections;
using System.Collections.Generic;
using Modular;
using NSUtilities;
using UnityEngine;

public class ArmPositionComunicator : MonoBehaviour
{
  [Header("Arm Position Handler")]
  [SerializeField] private ArmPositionHandler _positionHandler;
	[SerializeField]private Transform _gripTipPosition;

  [Header("Settings")]
  [SerializeField] private int _updateFrame = 0;

  [SerializeField] private bool _convertOutputToCentimeters = true;
  [Space]
  [SerializeField] private Vector3 _armPosition;

  /// <summary>
  /// Referencia al Arm Position Handler que contiene las referencias para ubicar el brazo
  /// </summary>
  /// <value></value>
  public ArmPositionHandler PositionHandler
  {
    get
    {
      if (_positionHandler == null) _positionHandler = GetComponent<ArmPositionHandler>();
      return _positionHandler;
    }
    set => _positionHandler = value;
  }

  [Header("Events")]
  [SerializeField] private Vector3Event _onArmPositionUpdate;
  [SerializeField] private FloatEvent _onXCoordinateUpdate;
  [SerializeField] private FloatEvent _onYCoordinateUpdate;
  [SerializeField] private FloatEvent _onZCoordinateUpdate;

  public void Update()
  {
    // Si el position handler es nulo, retorno
    if (PositionHandler == null) return;

    // Si el refresh frame es mayor a 0
    if (_updateFrame > 0)
    {
      // Evaluo el framecount

      // Si es distinto del update frame, retorno
      if (Time.frameCount % _updateFrame != 0) return;
    }

    // Obtengo la posición convertida
    var localArmPosition = PositionHandler.ConvertToLocalPosition(_gripTipPosition.position).ToAbsolute();

    // Comunico la posición
    _armPosition = localArmPosition;

    float snap = 0.01f;

    // Hago snap del vector
    _armPosition = _armPosition.SnapVector(snap);

    // Si se convierte la salida a centímetros
    if (_convertOutputToCentimeters)
    {
      // Escalo el vector por la unidad de conversión
      _armPosition.Scale(Vector3.one * 100);
    }

    // Comunico la posición vectorial
    _onArmPositionUpdate.WrapInvoke(_armPosition);

    // Comunico la posición en x, y, además de en, z
    _onXCoordinateUpdate.WrapInvoke(_armPosition.x);
    _onYCoordinateUpdate.WrapInvoke(_armPosition.y);
    _onZCoordinateUpdate.WrapInvoke(_armPosition.z);
  }

}
