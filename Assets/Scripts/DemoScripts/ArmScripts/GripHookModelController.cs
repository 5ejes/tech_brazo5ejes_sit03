﻿using System;
using System.Collections;
using Modular.LerpUpdate;
using UnityEngine;

public class GripHookModelController : MonoBehaviour
{
	[Header("References")]
	[SerializeField] private Transform _rightGripPiece;
	[SerializeField] private Transform _leftGripPiece;
	[Space]
	[SerializeField] private Transform _rightTipGripRay;
	[SerializeField] private Transform _leftTipGripRay;

	[Space]
	[SerializeField] private Transform _rightRotationReference;
	[SerializeField] private Transform _leftRotationReference;
	[Header("Rotation Values")]
	[SerializeField] private float _closedRotationValue = 0f;
	[SerializeField] private float _openRotationValue = 30f;

	[SerializeField] private float _gripAnimationTimeout = 0.3f;
	[Header("Ray Settings")]
	[SerializeField] private LayerMask _detectionLayer;
	[SerializeField] private Transform _gripLeafEnd;
	[Header("Grip Settings")]
	[SerializeField] private float _gripOffset = 2f;

	private bool _stopRightGripUpdate = true;
	private bool _stopLeftGripUpdate = true;
	public bool GripAnimationStop => _stopLeftGripUpdate && _stopRightGripUpdate;

	private bool _open = true;
	private bool _inspectAnimationStop = false;

	private Vector3 _rotationVector;

	private float _leftAngle = 0f;
	private float _rightAngle = 0f;
	private float _currentLeftAngle = 0f;
	private float _currentRightAngle = 0f;
	private static readonly float distance = 0.095f;
	private static readonly float _inspectionMinThreshold = 0.01f;
	private static readonly float _updateTime = 0.1f;

	private Vector3 direction;

	private float _nextUpdateTime = 0f;

	private float _offset = 0f;
	private float _inspectionLeftAngle = 0f;
	private float _inspectionRightAngle = 0f;

	private Vector3 a, b = new Vector3();

	private void Start()
	{
		// Calculo el offset al ángulo		
		float x = _rightGripPiece.position.x - _rightTipGripRay.position.x;
		float y = _rightGripPiece.position.y - _rightTipGripRay.position.y;
		_offset = Mathf.Atan(x / y) * Mathf.Rad2Deg;
	}

	private void Update()
	{

		// Si la garra está marcada para estar cerrada
		if (!_open)
		{
			// Si el tiempo de actualización se ha cumplido
			if (Time.time > _nextUpdateTime)
			{
				// Defino el próximo tiempo de actualización
				_nextUpdateTime = Time.time + _updateTime;

				// Defino la dirección considerando la rotación del objeto
				direction = _gripLeafEnd.rotation * Vector3.forward * -1;

				// Obtengo los ángulos basados en la posición, la dirección y la distancia del rayo desde un punto de la garra
				_leftAngle = GetTargetAngle(distance, direction, _gripLeafEnd.position - direction * distance, _leftGripPiece, _leftRotationReference, _leftAngle);
				_rightAngle = GetTargetAngle(distance, -direction, _gripLeafEnd.position + direction * distance, _rightGripPiece, _rightRotationReference, _rightAngle);
			}
		}

		// Creo un valor para la interpolación entre el objetivo y el valor de la garra actual
		_currentLeftAngle = Mathf.MoveTowards(_currentLeftAngle, _leftAngle, _gripAnimationTimeout != 0 ? 1 / _gripAnimationTimeout : 1);
		_currentRightAngle = Mathf.MoveTowards(_currentRightAngle, _rightAngle, _gripAnimationTimeout != 0 ? 1 / _gripAnimationTimeout : 1);

		// Comunico la rotación del vector a las piezas
		SetLeftLocalRotation(_currentLeftAngle + _offset + _gripOffset);
		SetRightLocalRotation(_currentRightAngle + -_offset + -_gripOffset);

		// Si está marcado para inspeccionar la finalización de la animación
		if (_inspectAnimationStop)
		{
			// Verifico que la diferencia sea menor al threshold definido
			_stopRightGripUpdate = Math.Abs(_currentRightAngle - _inspectionRightAngle) < _inspectionMinThreshold;
			_stopLeftGripUpdate = Mathf.Abs(_currentLeftAngle - _inspectionLeftAngle) < _inspectionMinThreshold;

			// Defino los ángulos delta de inspección
			_inspectionLeftAngle = _currentLeftAngle;
			_inspectionRightAngle = _currentRightAngle;

			// Defino la animación para detenerse
			_inspectAnimationStop = !(_stopLeftGripUpdate && _stopRightGripUpdate);
		}
	}

	private float GetTargetAngle(float distance, Vector3 direction, Vector3 position, Transform gripPiece, Transform rotationReference, float defaultAngle = 0f)
	{
		// Defino la información del rayo
		RaycastHit info;

		// Casteo una esfera dado el radio de la esfera, la dirección y posición
		if (Physics.SphereCast(position, 0.01f, direction, out info, distance, _detectionLayer, QueryTriggerInteraction.Collide))
		{

			// Defino el vector a, referencia de 0 grados de rotación
			a = rotationReference.position - gripPiece.position;
			a.Normalize();

			// Defino el vector b, el vector entre la posición del grip y el punto generado
			b = info.point - gripPiece.position;
			b.Normalize();

			// Defino el ángulo según la rotación del grip
			float angle = Vector3.SignedAngle(a, b, _gripLeafEnd.right);

			// Debug.DrawRay(info.point, Vector3.up * 0.01f, Color.red);
			// Debug.DrawRay(gripPiece.position, a, Color.yellow);
			// Debug.DrawRay(gripPiece.position, b, Color.yellow);
			
			// Retorno el ángulo resultante
			return angle;
		}
		else
		{
			// Si no se ha detectado colisión alguna

			// Retorno el valor dado por defecto
			return defaultAngle;
		}

	}

	private void SetLocalRotation(float angle, Transform piece)
	{
		// Modifico el valor x del vector
		_rotationVector.x = angle;

		// Aplico la rotación a la pieza dada
		piece.localEulerAngles = _rotationVector;
	}

	private void SetLeftLocalRotation(float angle)
	{
		// Aplico la rotación
		SetLocalRotation(angle, _leftGripPiece);
	}

	private void SetRightLocalRotation(float angle)
	{
		// Aplico la rotación
		SetLocalRotation(angle, _rightGripPiece);
	}

	private void ResetAnimationStopInspectionValues()
	{
		// Defino la inspección para determinar cuando se deja de mover el elemento
		_inspectAnimationStop = true;

		// Reinicio los valores para inspeccionar correctamente
		_stopLeftGripUpdate = _stopRightGripUpdate = false;
		_inspectionLeftAngle = _inspectionRightAngle = float.MaxValue;
	}
	public void Open()
	{
		// Marco la garra como abierta
		_open = true;

		// Restablezco los valores de inspección
		ResetAnimationStopInspectionValues();
		
		// Defino los ángulos para la animación
		_leftAngle = _openRotationValue;
		_rightAngle = -_openRotationValue;
	}


	public void Close()
	{
		// Marco la garra como cerrada
		_open = false;

		// Restablezco los valores de inspección
		ResetAnimationStopInspectionValues();

		// Defino los ángulos para la animación
		_leftAngle = _closedRotationValue;
		_rightAngle = -_closedRotationValue;
	}
}