﻿using Modular;
using Modular.LerpUpdate;
using NSArm3Axis;
using NSUtilities;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ArmGripController : MonoBehaviour
{
	private static readonly float _MIN_FORCE = 1.5f;
	private static readonly float _MAX_FORCE = 5f;
	private static readonly float DOT_VERTICAL_THRESHOLD = 0.85f;
	private static readonly float ROTATION_HEIGHT_THRESHOLD = 0.2f;


	[Header("Grip Transform")]
	[SerializeField] private Transform _grip;
	[Header("Settings")]
	[SerializeField] private float _errorMargin = 0.1f;
	[SerializeField] private LayerMask _gripableObjectsLayer = -1;

	[Header("Visual Adjustment")]
	[SerializeField] private Transform _visualGrip;
	[SerializeField] private Transform _visualArmGripParent;

	[Space]
	[SerializeField] private GripHookModelController _gripHookModelController;

	[Header("Misc")]
	[SerializeField] private bool _forceCubeToFollowGrip = true;

	[SerializeField] private Transform _localPositionConverter;
	[Space]
	[SerializeField] private GameObject _rightGripperCollisionPoint = null;
	[SerializeField] private GameObject _leftGripperCollisionPoint = null;
	[SerializeField] private float _deactivationTimeout = 0.1f;
	[SerializeField] private UnityEvent _onWrongGripToCube;
	[Header("Actions")]
	[SerializeField] private UnityEvent _onGripActivated = new UnityEvent();
	[SerializeField] private GameObjectEvent _onGripDeactivated = new GameObjectEvent();
	[Space]
	[SerializeField] private GameObjectEvent _onGripCastGet = new GameObjectEvent();
	[SerializeField] private BoolEvent _onGripTransitionEndState = new BoolEvent();

	/// <summary>
	/// Se ejecuta al final de cualquiera de las acciones
	/// </summary>
	/// <returns></returns>
	[SerializeField] private UnityEvent _onActionEnd = new UnityEvent();
	public UnityEvent OnGripActivated => _onGripActivated;
	public GameObjectEvent OnGripDeactivated => _onGripDeactivated;
	public GameObjectEvent OnGripCastGet => _onGripCastGet;

	public bool IsOnHorizontalModeTransition { get; set; } = false;

	[Header("Debug")]
	[SerializeField] private bool _drawVisuals;
	[SerializeField] private bool _gripHold;
	[SerializeField] private float _range;

	private AbstractCollectableObject _currentGrab;
	public AbstractCollectableObject CurrentGrab
	{
		get => _currentGrab;
		set => _currentGrab = value;
	}
	private Transform _transform;

	public bool GripHold { get => _gripHold; private set => _gripHold = value; }
	public bool ForceCubeToFollowGrip { get => _forceCubeToFollowGrip; set => _forceCubeToFollowGrip = value; }

	// Valores antiguos
	//private Vector3 _parentHorizontalModeGripRotation = new Vector3(-270f, 0f, 0f);
	//private Vector3 _parentVerticalModeGripRotation = new Vector3(-180, 0f, 0f);

	private Vector3 _parentHorizontalModeGripRotation = new Vector3(90f, 0f, 0f);
	private Vector3 _parentVerticalModeGripRotation = new Vector3(0f, 0f, 0f);
	private Vector3 _gripHorizontalModeRotation = new Vector3(0, 90f, 0f);
	private Vector3 _gripVerticalModeRotation = new Vector3(0, 0f, 0f);

	private Vector3 _lastGrabPosition;
	private Quaternion _initialGrabRotation = Quaternion.identity;
	private Quaternion _initialGripRotation = Quaternion.identity;
	private readonly Collider[] _results = new Collider[16];
	private int _currentDeadCount = 0;
	private readonly float _deadCountResetTime = 0.2f;
	private float _elapsed = 0f;

	private List<Transform> _evaluatedDistanceGrabTouchList = new List<Transform>();

	private Vector3LerpUpdate _gripLerp;
	private Vector3LerpUpdate _parentGripLerp;
	private GameObject _hitGripCastGameObject;

	private float _cubeAngleOffset = 0f;
	private float _targetOffset = 0f;

	private float _threshold = 55f;
	private float _snap = 90f;
	private static readonly float _gripRotationSpeed = 75f;
	private Vector3 _lastPosition;
	private Vector3 _deltaPosition;

	private bool _markToCast = false;

	private bool _evaluateTransitionEnd = false;
	private void Awake()
	{
		_transform = transform;
		_lastPosition = _grip.position;
	}

	private void Start()
	{
		// Creo las interpolaciones vectoriales
		_gripLerp = new Vector3LerpUpdate(1f, _gripVerticalModeRotation, _gripHorizontalModeRotation);
		_parentGripLerp = new Vector3LerpUpdate(1f, _parentVerticalModeGripRotation, _parentHorizontalModeGripRotation);

		// Hago una actualización inicial del estado del grip
		SetGrip(GripHold);
	}

	private void Update()
	{
		// Actualizo los valores delta
		_deltaPosition = _grip.position - _lastPosition;
		_lastPosition = _grip.position;
	}
	private IEnumerator ListenCubeMove(Transform target, Action<Vector3> finalPositionCallback)
	{
		if (target == null) yield break;
		if (_evaluatedDistanceGrabTouchList.Contains(target)) yield break;
		_evaluatedDistanceGrabTouchList.Add(target);

		Vector3 lastFramePosition = target.position;
		float threshold = float.Epsilon * float.Epsilon;
		WaitForSeconds wait = new WaitForSeconds(0.1f);
		yield return null;

		int notMovingFrames = 0;
		do
		{
			yield return wait;

			float sqrDistance = (target.position - lastFramePosition).sqrMagnitude;
			Debug.Log($"SqrDistance -> {sqrDistance}");


			if (sqrDistance < threshold)
			{
				notMovingFrames++;
			}
			else
			{
				notMovingFrames = 0;
			}

			lastFramePosition = target.position;
		} while (notMovingFrames > 10);
		Debug.Log($"Not moving!");

		// Obtengo la posición final
		finalPositionCallback?.Invoke(target.position);
		_evaluatedDistanceGrabTouchList.Remove(target);
	}

	private void LateUpdate()
	{
		// Hallo la distancia al grip
		var dirToTarget = (_visualGrip.position - _transform.position);

		// La aplano en el eje Y y la normalizo
		dirToTarget.y = 0f;
		dirToTarget.Normalize();

		//* Si el controlador del modelo no es nulo y la animación de control no se ha detenido, retorno
		if (_gripHookModelController != null)
		{
			if (!_gripHookModelController.GripAnimationStop)
				return;
		}

		// Evaluo el fin de la transición
		if(_evaluateTransitionEnd)
		{
			// Marco como evaluado
			_evaluateTransitionEnd = false;

			// Invoco el evento de finalización
			_onGripTransitionEndState?.Invoke(GripHold);
		}

		// Si la orden de hacer un cast se ha definido
		if (_markToCast)
		{
			// Desactivo esa flag
			_markToCast = false;

			// Hago un cast del grip
			GripCast(_grip.position, _errorMargin, (_currentGrab) =>
			{
				// Desactivo o activo la colisión de las pinzas de acuerdo al resultado del cast
				SetGripperColliderState(_currentGrab == null);
			});
		}

		// Aplico las rotaciones correctivas al objeto sostenido por la grip
		UpdatePositionCurrentGrab(_grip.position);

		// Aplico la rotación al grab actual
		RotateCurrentGrab(CurrentGrab, _grip);
	}


	private bool IsHorizontal(Transform target)
	{
		return Mathf.Abs(Vector3.Dot(target.rotation * Vector3.up, Vector3.up)) < 0.05f;
	}

	private void RotateCurrentGrab(AbstractCollectableObject currentGrab, Transform grip)
	{
		if (currentGrab == null) return;

		// Creo el cuaternion de ajuste a la rotación
		Quaternion offset = Quaternion.identity;

		// Muevo el offset hacia el target según el delta dado
		_cubeAngleOffset = Mathf.MoveTowards(_cubeAngleOffset, _targetOffset, Time.deltaTime * _gripRotationSpeed);

		// Aplico el offset creando un quaternion del eje y con los ángulos procesados
		offset = Quaternion.AngleAxis(_cubeAngleOffset, Vector3.up);

		// Obtengo la diferencia de rotación al momento de agarrar el objeto y la rotación actual
		Quaternion difference = _initialGripRotation * Quaternion.Inverse(_grip.rotation);
		// Aplico la inversión de esa diferencia y le sumo la rotación al agarrar el cubo
		currentGrab.Target.rotation = Quaternion.Inverse(difference) * offset * _initialGrabRotation;
	}

	private float GetTargetOffsetAngle(Transform target, Transform grip)
	{
		// Aplico el offset según la rotación de la grip
		float offsetAngle = grip.eulerAngles.y;

		// Obtengo los angulos en euler
		Vector3 targetEulerAngles = target.eulerAngles;

		// Calculo la dirección del objeto con el Vector forward
		var cubeCurrentDir = GetForwardFlattenDir(targetEulerAngles);

		// Hallo el snap y le aplico el offset
		float roundWeight = _threshold / _snap;
		targetEulerAngles.y = SnapTo(targetEulerAngles.y - offsetAngle, _snap, roundWeight) + offsetAngle;

		// Hago una copia del ángulo del cubo
		var targetEulerDir = GetForwardFlattenDir(targetEulerAngles);

		// Calculo el ángulo entre el ángulo actual del target y el ángulo objetivo
		// Con el Vector up
		float cubeAngle = GetGrabAutoAdjustmentAngle(targetEulerDir, cubeCurrentDir);

		// Aplico el offset dando el ángulo negativo
		return -cubeAngle;
	}

	private Vector3 GetForwardFlattenDir(Vector3 targetEulerAngles)
	{
		Vector3 output = Quaternion.Euler(targetEulerAngles) * Vector3.forward;
		// Aplano en el eje y
		output.y = 0f;
		output.Normalize();

		return output;
	}
	private float SnapTo(float value, float snap, float roundWeight)
	{
		return WeightedRound(value / snap, roundWeight) * snap;
	}

	private float WeightedRound(float value, float weight)
	{
		float prevValue = Mathf.Floor(value);
		float nextValue = prevValue + 1;

		float weightValue = Mathf.Lerp(prevValue, nextValue, 1 - weight);
		return value <= weightValue ? prevValue : nextValue;
	}

	private float GetGrabAutoAdjustmentAngle(Vector3 adjustmentTargetDir, Vector3 cubeDir)
	{
		return Vector3.SignedAngle(adjustmentTargetDir, cubeDir, Vector3.up);
	}

	private bool IsValidContact(Collider currentContact, int parentRecursionLevel)
	{
		Transform contactTransform = currentContact.transform;
		Transform currentGrabTransform = CurrentGrab.transform;
		if (contactTransform.Equals(currentGrabTransform)) return false;

		// Recorro los padres
		var parent = contactTransform.parent;
		return IsValidContactWithParent(CurrentGrab.transform, parent, parentRecursionLevel);
	}

	private bool IsValidContactWithParent(Transform target, Transform parent, int parentRecursionLevel)
	{
		// Asumo que el contacto es válido
		bool isValid = true;

		// SI el padre no es nulo, hago las comprobaciones
		if (parent != null)
		{
			// Compruebo si el target es el parent dado
			if (target.Equals(parent)) return false;

			// Compruebo si el nivel de recusión ya alcanzó 0
			if (parentRecursionLevel < 0) return false;

			// Compruebo con el parent del parent
			isValid = IsValidContactWithParent(target, parent.parent, parentRecursionLevel - 1);
		}

		return isValid;
	}

	private void SetGripperColliderState(bool state)
	{
		if (!_rightGripperCollisionPoint || !_leftGripperCollisionPoint) return;

		// Defino el estado de los puntos de colision
		_rightGripperCollisionPoint.SetActive(state);
		_leftGripperCollisionPoint.SetActive(state);
	}

	private void OnDrawGizmos()
	{
		if (_drawVisuals)
		{

			if (_grip)
			{
				Gizmos.color = Color.blue;
				Gizmos.DrawWireSphere(_grip.position, _errorMargin);
			}
		}
	}

	private void GripCast(Vector3 position, float errorMargin, Action<AbstractCollectableObject> gripCollectableCallback = null)
	{

		// Obtengo los objetos según un radio
		var hits = Physics.OverlapSphere(position, errorMargin, _gripableObjectsLayer, QueryTriggerInteraction.Collide);

		// Recorro cada objeto
		int length = hits.Length;
		for (int i = 0; i < length; i++)
		{
			// Obtengo el colisionador actual
			Collider hit = hits[i];

			// Obtengo el GameObject del hit actual
			GameObject hitGo = hit.gameObject;

			// Obtengo la distancia desde la posición del objeto
			float distance = Vector3.Distance(hitGo.transform.position, _grip.position);

			// Si la distancia supera el margen de error, descarto el elemento actual y continúo
			if (distance > _errorMargin) continue;

			// Obtengo el RigidBody
			var collectableCube = hitGo.GetComponent<Cube>();

			if (!collectableCube) collectableCube = hitGo.GetComponentInParent<Cube>();
			if (!collectableCube) collectableCube = hitGo.GetComponentInChildren<Cube>();

			// Compruebo que el objeto que obtuve exista como Cube
			if (collectableCube == null) return;

			// Compruebo si el cubo no ha sido agarrado del centro
			if (!collectableCube.ValidCatch(position, errorMargin))
			{
				// Transmito el error
				_onWrongGripToCube.WrapInvoke();
				return;
			}

			var collectableObject = (AbstractCollectableObject)collectableCube;

			// Si existe el RigidBody
			if (collectableObject)
			{

				// Ejecuto la acción si está definida
				if (OnGripCastGet != null) OnGripCastGet.Invoke(hitGo);

				// Almaceno el objeto que se detectó para que no interfiera con la colisión a la hora de ser agarrado
				_hitGripCastGameObject = hitGo;

				// Actualizo los overrides del Abstract CollectableObject
				collectableCube.UpdateBaseOverrides();

				// Lo defino como el grab actual
				// Y lo marco como quinemático
				CurrentGrab = collectableObject;


				// Restablezco la rotación del cubo
				_cubeAngleOffset = 0f;

				_initialGrabRotation = CurrentGrab.Target.rotation;
				_initialGripRotation = _grip.rotation;

				_targetOffset = GetTargetOffsetAngle(CurrentGrab.Target, _visualGrip);

				collectableObject.CollectObject(_grip.position);

				// Las colisiones del cubo contra otros cubos mientras se sostiene en la grip, quedan deshabilitadas
				// collectableCube.CollisionEnter += GrabObjectCollisionTest;

				// Obtengo el cubo del objeto coleccionable
				// Si el cubo no es nulo
				if (collectableCube != null)
					collectableCube.State = CubeState.Trasladandose;

				// Activo el colisionador de entorno
				collectableCube.SetEnvironmentColliderStatus(true);

				// Rompo el ciclo
				break;
			}
		}

		// Ejecuto el callback con el objeto
		gripCollectableCallback?.Invoke(CurrentGrab);

	}
	private void GripRelease()
	{
		// Si existe un objeto agarrado por el grip
		if (CurrentGrab)
		{

			// Lo suelto generando la simulación
			// Elimino la referencia ya que no está siendo sostenido
			CurrentGrab.ReleaseObject();

			// Obtengo el cubo del objeto colectible
			var cube = (Cube)CurrentGrab;

			// Si el cubo existe, actualizo su estado
			if (cube) cube.State = CubeState.Final;

			// Desactivo el colisionador de entorno
			cube.SetEnvironmentColliderStatus(false);

			Ray catchSpinRay = new Ray(CurrentGrab.Target.position, Vector3.down);
			RaycastHit hitInfo;

			if (Physics.Raycast(catchSpinRay, out hitInfo, 0.55f, Physics.AllLayers, QueryTriggerInteraction.Ignore))
			{
				float distance = hitInfo.distance;

				if (distance > ROTATION_HEIGHT_THRESHOLD)
				{
					Quaternion rotation = CurrentGrab.Target.rotation;

					float z = Mathf.Sign(UnityEngine.Random.Range(-1f, 1f));
					float x = Mathf.Sign(UnityEngine.Random.Range(-1f, 1f));
					Vector3 randomFlatDirection = new Vector3(x, 0f, z).normalized;

					float randomForce = UnityEngine.Random.Range(_MIN_FORCE, _MAX_FORCE);
					CurrentGrab.CollectableRigidbody.AddTorque(rotation * randomFlatDirection * randomForce, ForceMode.VelocityChange);
				}
			}

			// Elimino las referencias a los objetos
			_hitGripCastGameObject = null;
			CurrentGrab = null;

			_initialGrabRotation = Quaternion.identity;
			_initialGripRotation = Quaternion.identity;

			// Las colisiones quedan deshabilitadas
			// cube.CollisionEnter -= GrabObjectCollisionTest;
		}
	}
	private IEnumerator WaitFor(float seconds, Action callback)
	{
		// Si no se ha definido un callback, no hay razón para esperar
		if (callback == null) yield break;

		// Espero los segundos definidos por parámetro
		yield return new WaitForSeconds(seconds);

		// Invoco el callback
		callback?.Invoke();
	}

	public void ActivateGrip()
	{
		// Marco como activado el grip
		GripHold = true;

		// Marco el grip para hacer un cast la proxima vez que la grip se cierre
		_markToCast = true;

		// Marco para que se evalue el final de la transición
		_evaluateTransitionEnd = true;

		// Ejecuto la acción de activación del grip
		if (OnGripActivated != null) OnGripActivated.Invoke();

		// Desactivo la colisión de la grip
		SetGripperColliderState(false);

		// Cuando finalice la acción
		if (_onActionEnd != null) _onActionEnd.Invoke();
	}

	public void DeactivateGrip()
	{
		// Marco como desactivado el grip
		GripHold = false;

		// Ejecuto la espera para reactivar las colisiones de las pinz	
		StartCoroutine(WaitFor(_deactivationTimeout, () =>
		{
			// Si hay un objeto actual, atrapado por el grip, retorno
			if (CurrentGrab) return;

			// Desactivo la colisión del grip
			SetGripperColliderState(true);
		}
		));

		// Marco para que se evalue el final de la transición
		_evaluateTransitionEnd = true;

		// Ejecuto la acción de desactivación del grip
		if (OnGripDeactivated != null) OnGripDeactivated.Invoke(_hitGripCastGameObject);

		// Ejecuto la operación para soltar el grip
		GripRelease();

		// Cuando finalice la acción
		if (_onActionEnd != null) _onActionEnd.Invoke();
	}

	public void SetGrip(bool value)
	{
		// Defino el valor del grip
		GripHold = value;

		// Defino el evento
		if (GripHold) ActivateGrip();
		else DeactivateGrip();

		// Cuando finalice la acción
		if (_onActionEnd != null) _onActionEnd.Invoke();
	}

	public void ToggleGrip()
	{
		// Si está activado el grip, lo desactivo, si no, viceversa
		if (GripHold) DeactivateGrip();
		else ActivateGrip();

		// Cuando finalice la acción
		if (_onActionEnd != null) _onActionEnd.Invoke();
	}

	public void UpdatePositionCurrentGrab(Vector3 targetPosition)
	{
		// Si tengo un objeto
		if (CurrentGrab != null)
		{
			CurrentGrab.PickedUpLocation = targetPosition;
			// Lo ubico en el punto de agarre
			CurrentGrab.MoveObject();
		}
	}

	private Vector3 ConvertToLocalPosition(Vector3 worldPosition)
	{
		return _localPositionConverter?.InverseTransformPoint(worldPosition) ?? Vector3.zero;
	}

	private Vector3 ConvertToWorldPosition(Vector3 localPosition)
	{
		return _localPositionConverter?.TransformPoint(localPosition) ?? Vector3.zero;
	}

}
