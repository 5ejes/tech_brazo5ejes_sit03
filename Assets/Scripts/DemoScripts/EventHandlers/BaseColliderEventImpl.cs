﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseColliderEventImpl : MonoBehaviour
{
	[Header("Filters")]
	[SerializeField] protected string _name = string.Empty;
	[SerializeField] protected string _tag = string.Empty;
	[SerializeField] protected LayerMask _layer = Physics.AllLayers;


	/// <summary>
	/// Comprueba si cumple con los filtros definidos en la instancia
	/// </summary>
	/// <param name="target">Objeto del que se evaluarán los filtros</param>
	/// <returns>'true' si coinciden todos los filtros dados</returns>
	protected bool CheckFilters(GameObject target)
	{
		// Creo las variables de comprobación de los objetos
		bool nameCheck = false;
		bool tagCheck = false;
		bool layerCheck = false;

		// Si el filtro de nombre está vacío o nulo
		if (string.IsNullOrEmpty(_name))
		{
			// Se ignora si no hay un atributo nombre definido
			// Marco la validación del nombre como satisfactoria
			nameCheck = true;
		}
		else
		{
			// Si no está vacío

			// Compruebo que el nombre coincida y defino el valor resultante de esta comparación
			nameCheck = target.name.Equals(_name);
		}

		// Si el filtro de tag está vacío o nulo
		if (string.IsNullOrEmpty(_tag))
		{
			// Se ignora si no hay un atributo tag definido
			// Marco la validación del tag como satisfactoria
			tagCheck = true;
		}
		else
		{
			// Si no es nulo o vacío

			// Compruebo que el tag coincida y defino el valor resultante de esta comparación
			tagCheck = target.CompareTag(_tag);
		}

		// Si la capa es igual a todas las capas
		if (_layer == Physics.AllLayers)
		{
			// Si la capa de filtro corresponde a todas las capas
			// Marco la comprobación como válida
			layerCheck = true;
		}
		else
		{
			// Si no son todas las capas como filtro

			// Compruebo el layer del objeto
			layerCheck = target.layer.Equals(_layer);
		}

		// Retorno el conjunto de todos los valores
		return nameCheck && tagCheck && layerCheck;
	}
}
