﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemoLogCollider : MonoBehaviour
{
	public void LogCollider(Collider other)
	{
		Debug.Log(other);
	}

	public void LogCollision(Collision other)
	{
		Debug.Log(other);
	}
}
