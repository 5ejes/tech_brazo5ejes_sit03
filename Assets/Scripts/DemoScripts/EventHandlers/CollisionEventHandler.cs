﻿using System.Collections;
using System.Collections.Generic;
using NSUtilities;
using UnityEngine;

public class CollisionEventHandler : BaseColliderEventImpl
{

	[Header("Events")]

	[SerializeField] private Modular.CollisionEvent _onCollisionEnterEvent;
	[SerializeField] private Modular.CollisionEvent _onCollisionStayEvent;
	[SerializeField] private Modular.CollisionEvent _onCollisionExitEvent;

	public Modular.CollisionEvent OnCollisionEnterEvent => _onCollisionEnterEvent;
	public Modular.CollisionEvent OnCollisionStayEvent => _onCollisionStayEvent;
	public Modular.CollisionEvent OnCollisionExitEvent => _onCollisionExitEvent;

	public void OnCollisionEnter(Collision other)
	{
		// Compruebo los filtros, si el objeto no supera los filtros, retorno
		if (!CheckFilters(other.gameObject)) return;
		
		// Si el evento no es nulo
		_onCollisionEnterEvent.WrapInvoke(other);
	}

	public void OnCollisionStay(Collision other)
	{
		// Compruebo los filtros, si el objeto no supera los filtros, retorno
		if (!CheckFilters(other.gameObject)) return;

		// Si el evento no es nulo
		_onCollisionStayEvent.WrapInvoke(other);
	}

	public void OnCollisionExit(Collision other)
	{
		// Compruebo los filtros, si el objeto no supera los filtros, retorno
		if (!CheckFilters(other.gameObject)) return;

		// Si el evento no es nulo
		_onCollisionExitEvent.WrapInvoke(other);
	}
}