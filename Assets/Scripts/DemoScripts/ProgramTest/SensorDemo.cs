﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SensorDemo : MonoBehaviour
{

	[Header("Barrier Sensor")]
	[SerializeField] private bool _hasPiece;

	[Header("Color Sensor")]
	[SerializeField] private Color _sensedColor = Color.blue;

	public bool HasPiece => _hasPiece;
	public Color SensedColor => _sensedColor;
}