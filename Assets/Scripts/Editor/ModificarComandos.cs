﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class ModificarComandos : MonoBehaviour
{

    [MenuItem("Utilities/Remover navigation")]
    static void RemoverNavigation()
    {
        var tmpAllInteractables = Resources.FindObjectsOfTypeAll<Selectable>();
        var tmpNewNavigation = new Navigation();
        tmpNewNavigation.mode = Navigation.Mode.None;
        foreach (var tmpSelectable in tmpAllInteractables)
            tmpSelectable.navigation = tmpNewNavigation;
    }


}
