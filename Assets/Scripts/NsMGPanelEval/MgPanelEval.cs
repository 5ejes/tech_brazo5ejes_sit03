﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NSInterfaz;
using NSBoxMessage;
using NSTraduccionIdiomas;
using UnityEngine.Events;
using NSUtilities;
using System;
using NSScriptableValues;

public class MgPanelEval : MonoBehaviour
{
	private const string ACCEPT_LABEL_KEY = "TextAceptarMayusculas";
	private const string CANCEL_LABEL_KEY = "TextCancelar";
	private const string SUCCESS_EVALUATION_KEY = "textEntrarEvaluacionCalificacionBuena";
	private const string FAILED_EVALUATION_KEY = "textEntrarEvaluacionCalificacionMala";
	private const float SUCCESS_QUALIFY_THRESHOLD = 0.5f;


	private string _successEvaluationString = null;
	public string SuccessEvaluationString
	{
		get
		{
			if (string.IsNullOrEmpty(_successEvaluationString))
				_successEvaluationString = DiccionarioIdiomas.Instance.Traducir(SUCCESS_EVALUATION_KEY);

			return _successEvaluationString;
		}
	}

	private string _failedEvaluationString = null;
	public string FailedEvaluationString
	{
		get
		{
			if (string.IsNullOrEmpty(_failedEvaluationString))
				_failedEvaluationString = DiccionarioIdiomas.Instance.Traducir(FAILED_EVALUATION_KEY);

			return _failedEvaluationString;
		}
	}

	private string _acceptLabel;
	public string AcceptLabel
	{
		get
		{
			if (string.IsNullOrEmpty(_acceptLabel)) _acceptLabel = DiccionarioIdiomas.Instance.Traducir(ACCEPT_LABEL_KEY);
			return _acceptLabel;
		}
	}

	private string _cancelLabel;
	public string CancelLabel
	{
		get
		{
			if (string.IsNullOrEmpty(_cancelLabel)) _cancelLabel = DiccionarioIdiomas.Instance.Traducir(CANCEL_LABEL_KEY);
			return _cancelLabel;
		}
	}


	[Header("Reference")]
	[SerializeField] private PanelEvaluacion panelEval;
	[SerializeField] private ValueFloat _qualificationAsset;

	[Header("Events")]
	[SerializeField] private UnityEvent _onEntrarDecision = new UnityEvent();
	[Space]
	[SerializeField] private UnityEvent _onEvaluationSucceded = new UnityEvent();
	[SerializeField] private UnityEvent _onEvaluationFailed = new UnityEvent();

	private string GetStringFromEvaluation(bool evaluationSucceded)
	{
		return evaluationSucceded ? SuccessEvaluationString : FailedEvaluationString;
	}

	private static bool IsEvaluationSuccessful(float evaluationValue)
	{
		return evaluationValue >= SUCCESS_QUALIFY_THRESHOLD;
	}

	/// <summary>
	/// activa el panel evaluacion
	/// </summary>
	private void entrar()
	{
		panelEval.ShowPanel(true);
	}

	private IEnumerator WaitFor(float seconds, Action callback)
	{
		if (callback == null) yield break;

		if (seconds > 0)
		{
			yield return new WaitForSeconds(seconds);
		}

		callback?.Invoke();
	}

	private void EvaluateQualificationAsset()
	{
		// Obtengo el valor de evaluación
		float evaluationValue = _qualificationAsset?.Value ?? 1f;

		// Si el asset de calificación es nulo, retorno un mensaje
		if (_qualificationAsset.Equals(null))
			Debug.LogError($"Undefined Qualification Asset, qualification will always be {evaluationValue}");

		// Realizo la evaluación según el valor de calificación, y obtengo un texto de feedback
		bool evaluationSucceded = IsEvaluationSuccessful(evaluationValue);
		string text = GetStringFromEvaluation(evaluationSucceded);

		// Muestro el texto generado por la evaluación
		BoxMessageManager.Instance.CreateBoxMessageDecision(
			text,
			CancelLabel,
			AcceptLabel,
			() => { entrar(); _onEntrarDecision.WrapInvoke(); }, null);

		// Invoco un evento, según el resultado de la evaluación
		if (evaluationSucceded)
		{
			_onEvaluationSucceded?.Invoke();
		}
		else
		{
			_onEvaluationFailed?.Invoke();
		}
	}

	public void ActivarEvaluacion()
	{
		// Ejecuto un delay, a la acción de calificación
		StartCoroutine(
			WaitFor(0.1f, EvaluateQualificationAsset)
		);
	}
}
