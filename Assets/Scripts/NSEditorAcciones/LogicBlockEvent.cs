﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSEditorAcciones
{
	public class LogicBlockEventArgs : System.EventArgs
	{
		private LogicBlock _contextBlock;
		public LogicBlock ContextBlock => _contextBlock;

		public LogicBlockEventArgs(LogicBlock argContextBlock)
		{
			// Defino el bloque del contexto
			_contextBlock = argContextBlock;
		}

	}

	public delegate void LogicBlockEventHandler(object sender, LogicBlockEventArgs eventArgs);
}