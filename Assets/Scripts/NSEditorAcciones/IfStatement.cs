﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSEditorAcciones
{
	public class IfStatement : Conditional
	{

		public IfStatement(Condition argCondition)
		{
			BlockID = IDTable.IF_STATEMENT_ID;
			// Defino la condición del if
			CurrentCondition = argCondition;
		}

		protected LogicBlock[] _ifExecutionBlocks;

		public LogicBlock[] IfExecutionBlocks
		{
			get => _ifExecutionBlocks;
			set => _ifExecutionBlocks = value;
		}

		public void SetIgnore(LogicBlock[] argBlocks, bool argIgnoreValue)
		{
			// Ignora la ejecución de los bloques especificados
			foreach (var block in argBlocks)
			{
				// Defino al valor especificado como parametro
				block.IgnoreExecution = argIgnoreValue;
			}
		}

		public int IfSkipIndex
		{
			get
			{
				// Retorno el orden actual más la longitud de bloques anidados
				// +1 para referenciar el índice del argumento después del IF
				return ExecutionOrder + IfExecutionBlocks.Length + 1;
			}
		}
	}
}