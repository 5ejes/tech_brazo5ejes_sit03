﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSEditorAcciones
{
	public class ColorSensor : LogicSensor
	{

		// TODO: Crear campo de color que refleje varios colores para comparar
		private Color _evaluationColor = Color.red;

		public ColorSensor(SensorDemo argSensor) : base(argSensor)
		{
			// Comunico el sensor al constructor superclase

			// Defino el ID
			SensorID = IDTable.COLOR_LSENSOR_ID;
		}

		public Color GetColorRead()
		{
			// Obtengo el color del sensor

			// TOOD: Reemplazar por sensor físico
			return Sensor.SensedColor;
		}

		public bool GetColorAndCompare(Color argMatchColor)
		{
			// Obtengo los datos del sensor
			var color = GetColorRead();

			// Si el color del sensor es idéntico al color del argumento
			return color.Equals(argMatchColor);

		}

		public override bool Evaluate()
		{
			// Comparo

			// TODO: Usar color seleccionado desde la interfaz
			return GetColorAndCompare(_evaluationColor);
		}
	}
}