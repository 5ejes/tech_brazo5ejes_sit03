﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSEditorAcciones
{
	public class MoveOperation : Operation
	{

		public MoveOperation()
		{
			BlockID = IDTable.MOVE_OPERATION_ID;
		}

		#region Private Fields

		private static Vector3Int _minLimit = new Vector3Int(0, 0, 0);
		private static Vector3Int _maxLimit = new Vector3Int(100, 100, 100);

		private Vector3Int _target;

		#endregion

		#region Public Properties

		public Vector3Int Target
		{
			get => _target;
			set
			{
				_target = value;
				ClampAxes(_target);
			}
		}

		#endregion

		#region Public Methods

		public static Vector3Int ClampAxes(Vector3Int argValue)
		{
			// Restrinjo los valores a los límites establecidos
			argValue.Clamp(_minLimit, _maxLimit);

			return argValue;
		}

		#endregion
	}
}