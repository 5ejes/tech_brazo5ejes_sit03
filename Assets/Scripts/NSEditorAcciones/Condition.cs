﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSEditorAcciones
{
	public abstract class Condition
	{
		public abstract bool Evaluate();
	}
}