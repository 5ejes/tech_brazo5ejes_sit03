﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSEditorAcciones
{

	public abstract class LogicSensor : Condition
	{

		private SensorDemo _sensor;
		public SensorDemo Sensor => _sensor;

		public LogicSensor(SensorDemo argSensor)
		{
			// Defino el sensor físico del objeto
			_sensor = argSensor;
		}

		private string _sensorID;
		public string SensorID
		{
			get => _sensorID;
			set => _sensorID = value;
		}
	}

}