﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSEditorAcciones
{
	public class BarrierSensor : LogicSensor
	{

		public BarrierSensor(SensorDemo argSensor) : base(argSensor)
		{
			// Comunico el sensor al constructor superclase

			// Defino el ID
			SensorID = IDTable.BARRIER_LSENSOR_ID;
		}

		public override bool Evaluate()
		{
			// Evaluo la condición de la barrera
			return HasPiece();
		}

		public bool HasPiece()
		{
			// TODO: Crear sensor físico que me provea de este dato
			return Sensor.HasPiece;
		}
	}
}