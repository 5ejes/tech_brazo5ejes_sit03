﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSEditorAcciones
{
	public abstract class LogicBlock
	{

		private string _blockID;
		private int _executionOrder;
		private bool _ignoreExecution;
		private Conditional _parent;

		public int ExecutionOrder { get => _executionOrder; set => _executionOrder = value; }
		public string BlockID { get => _blockID; set => _blockID = value; }
		public bool IgnoreExecution { get => _ignoreExecution; set => _ignoreExecution = value; }
		public Conditional Parent { get => _parent; set => _parent = value; }

		public event LogicBlockEventHandler OnStartExecute;
		public event LogicBlockEventHandler OnEndExecute;
	}
}