﻿#pragma warning disable 0649
using System;
using UnityEngine;

namespace NSCalificacionSituacion
{
    /// <summary>
    /// Clase que contiene el sprite del enunciado, texto del enunciado, preguntas con respecto al enunciado, 
    /// respuestas para cada pregunta y respuestas falsas, esto para una evaluacion tipo PISA
    /// </summary>
    [CreateAssetMenu(menuName = "NSCalificacionSituacion/Preguntas Situacion")]
    public class SOPreguntasSituacion : ScriptableObject
    {
        #region members

        /// <summary>
        /// Imagen del enunciado de la evaluacion
        /// </summary>
        [SerializeField] private Sprite imagenEnunciado;

        /// <summary>
        /// Texto del enunciado de la evaluacion 
        /// </summary>
        [SerializeField] private string textoEnunciado;

        /// <summary>
        /// Preguntas con respecto al enunciado, cada indice representa una pregunta diferente
        /// </summary>
        [SerializeField] private string[] preguntas;

        /// <summary>
        /// Respuestas con respecto a cada pregunta, cada indice corresponde directamente al array preguntas
        /// </summary>
        [SerializeField] private string[] respuestas;

        /// <summary>
        /// Respuestas imagen con respecto a cada pregunta, cada indice corresponde directamente al array preguntas
        /// </summary>
        [SerializeField] private Sprite[] respuestasImagen;

        /// <summary>
        /// Array de la clase que dentro tiene otro array que contiene respuestas falsas, cada indice de este array corresponde directamente al array preguntas
        /// </summary>
        [SerializeField] private ContendorRespuestasFalsas[] contenedorRespuestasFalsas;

        #endregion

        #region accesors

        public Sprite ImagenEnunciado => imagenEnunciado;

        public string TextoEnunciado => textoEnunciado;

        public string[] Preguntas => preguntas;

        public string[] Respuestas => respuestas;

        public Sprite[] RespuestasImagen => respuestasImagen;

        public ContendorRespuestasFalsas[] ContenedorRespuestasFalsas => contenedorRespuestasFalsas;

        #endregion
    }

    [Serializable]
    public class ContendorRespuestasFalsas
    {
        #region members

        [SerializeField] private string[] respuestasFalsas;

        [SerializeField] private Sprite[] respuestasFalsasImagen;

        #endregion

        #region accesors

        public string[] _respuestasFalsas => respuestasFalsas;

        public Sprite[] _respuestasFalsasImagen => respuestasFalsasImagen;

        #endregion
    }
}