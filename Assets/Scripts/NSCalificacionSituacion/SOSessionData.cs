﻿using UnityEngine;

namespace NSCalificacionSituacion
{
    [CreateAssetMenu(fileName = "SOSessionData", menuName = "NSCalificacionSituacion/SOSessionData", order = 0)]
    public class SOSessionData : ScriptableObject
    {
        public byte quantityAttempts;

        public float timeSituationFloat;

        public string TimeSituationString
        {
            get
            {
                var tmpSegundos = Mathf.Floor(timeSituationFloat % 60);
                var tmpMinutos = Mathf.Floor(timeSituationFloat / 60f);
                return (tmpMinutos < 10f ? "0" + tmpMinutos : tmpMinutos.ToString()) + ":" + (tmpSegundos < 10f ? "0" + tmpSegundos : tmpSegundos.ToString());
            }
        }
    }
}