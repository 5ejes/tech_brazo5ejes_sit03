﻿using System.Collections;
using System.Collections.Generic;
using Modular;
using NSScriptableValues;
using NSUtilities;
using UnityEngine;

namespace NSScriptableValueReader
{
	public class ScriptableValueBoolReader : AbstractScriptableValueReader
	{
		[SerializeField] private ValueBool _valueBool;
		[SerializeField] private BoolEvent _readingResult;

		public override void Read()
		{
			_readingResult.WrapInvoke(_valueBool);
		}
	}
}
