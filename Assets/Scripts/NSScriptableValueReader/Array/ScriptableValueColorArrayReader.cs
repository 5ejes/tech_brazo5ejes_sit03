﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NSScriptableValues;
using static NSScriptableEvent.ScriptableEventColor;
using NSUtilities;

namespace NSScriptableValueReader.Array
{
	public class ScriptableValueColorArrayReader : GenericValueArrayReader<Color>
	{
		[SerializeField] private ValueColorArray _arrayValue;
		[Header("Events")]
		[SerializeField] private ColorEvent _onIndexReading = new ColorEvent();

		private void Awake()
		{
			// Inicializo el elemento
			Initialize();
		}

		public void Initialize()
		{
			// Defino el evento de inicialización
			ResponseEvent = _onIndexReading;
		}

		public override void Read(int index)
		{
			// Si el elemento Scriptable Value Array está definido
			if (Initialized)
			{
				if (_arrayValue != null)
				{
					// Si el índice es válido
					int length = _arrayValue.Value.Length;
					if (index >= 0 && index < length)
					{
						// Leo el valor del arreglo dado
						var indexReading = _arrayValue.Value[index];

						// Ejecuto el evento, retorno el valor
						ResponseEvent.WrapInvoke(indexReading);
					}
					else
					{
						// El índice es inválido

						// Lanzo un error
						Debug.LogError($"The provided index \"{index}\" is not in bounds; Length is {length} : {gameObject}", this);
						return;
					}
				}
				else
				{
					// El elemento Scriptable Value Array no está definido

					// Lanzo un error
					Debug.LogError($"The value to read from, is null : {gameObject}");
					return;
				}
			}
			else
			{
				// Si no está inicializado
				// Lanzo un error
				Debug.LogError($"The array reader is not initialized, please use Initialize() somewhere before reading : {gameObject}");
				return;
			}
		}
	}
}
