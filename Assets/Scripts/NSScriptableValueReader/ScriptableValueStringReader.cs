﻿using System.Collections;
using System.Collections.Generic;
using Modular;
using NSScriptableValues;
using NSUtilities;
using UnityEngine;
using static NSScriptableEvent.ScriptableEventString;

namespace NSScriptableValueReader
{
	public class ScriptableValueStringReader : AbstractScriptableValueReader
	{
		[SerializeField] private ValueString _valueString;
		[SerializeField] private StringEvent _readingResult;

		public override void Read()
		{
			_readingResult.WrapInvoke(_valueString);
		}
	}
}
