﻿using System.Collections;
using UnityEngine;

public class RoboticViewerFunctionAccessor : MonoBehaviour
{

	private static readonly uint DEFAULT_VALUE = 255;

	[Header("Robotic Viewer")]
	[SerializeField] private RoboticViewer _viewer;

	private IEnumerator Start()
	{
		// Si el viewer no ha sido definido
		if (_viewer == null)
		{
			// Logueo un error en consola y rompo la secuencia
			Debug.LogError($"The RoboticViewer hasn't been defined", this);
			yield break;
		}

		// Espero 0.1 segundos para asegurarme que el RoboticViewer ya se halla inicializado
		yield return new WaitForSeconds(0.1f);

		// Defino los valores por defecto
		SetDefaultSceneValues();
	}

	public void SetDefaultSceneValues()
	{
		// Defino los valores de posición por defecto
		_viewer.memoryData[8] = DEFAULT_VALUE;  // Valor de X
		_viewer.memoryData[9] = DEFAULT_VALUE;  // Valor de Y
		_viewer.memoryData[10] = DEFAULT_VALUE; // Valor de Z

		_viewer.memoryData[14] = DEFAULT_VALUE; // Valor del Color ID
	}

	public void SetViewerStartInput(bool value)
	{
		// Si el viewer es nulo
		if (_viewer == null)
		{
			// Lanzo el error
			Debug.LogError("Viewer is not defined", this);
			return;
		}

		// Modifico el valor de start input
		_viewer.digitalInputs[0] = value;
	}

	public void SetViewerGripperInput(bool gripState)
	{
		// Si el viewer es nulo
		if (_viewer == null)
		{
			// Lanzo el error
			Debug.LogError("Viewer is not defined", this);
			return;
		}

		// Modifico el valor de gripper input
		_viewer.digitalInputs[2] = gripState;
	}
}
