﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSScriptableEvent.GenericListeners
{
	public class ScriptableEventListenerBool : GenericScriptableEventListener<bool>
	{
		[SerializeField] private ScriptableEventBool _scriptableEvent;
		[SerializeField] private ScriptableEventBool.BoolEvent _boolEvent;

		protected override void OnEnable() => _scriptableEvent.Subscribe(_boolEvent.Invoke);
		protected override void OnDisable() => _scriptableEvent.Unsubscribe(_boolEvent.Invoke);
	}
}
