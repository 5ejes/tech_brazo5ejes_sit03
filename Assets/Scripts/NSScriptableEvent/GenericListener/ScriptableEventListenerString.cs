﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSScriptableEvent.GenericListeners
{
	public class ScriptableEventListenerString : GenericScriptableEventListener<string>
	{
		[SerializeField] private ScriptableEventString _scriptableEvent;
		[SerializeField] private ScriptableEventString.StringEvent _StringEvent;

		protected override void OnEnable() => _scriptableEvent.Subscribe(_StringEvent.Invoke);
		protected override void OnDisable() => _scriptableEvent.Unsubscribe(_StringEvent.Invoke);
	}
}
