﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSScriptableEvent.GenericListeners
{
	public class ScriptableEventListenerErrorDefinition : GenericScriptableEventListener<ErrorDefinition>
	{
		[SerializeField] private ScriptableEventErrorDefinition _scriptableEvent;
		[SerializeField] private ScriptableEventErrorDefinition.ErrorDefinitionEvent _collisionEvent;

		protected override void OnEnable() => _scriptableEvent.Subscribe(_collisionEvent.Invoke);
		protected override void OnDisable() => _scriptableEvent.Unsubscribe(_collisionEvent.Invoke);
	}
}
