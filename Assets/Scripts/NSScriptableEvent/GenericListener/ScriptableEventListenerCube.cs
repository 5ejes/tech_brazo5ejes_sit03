﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSScriptableEvent.GenericListeners
{
	public class ScriptableEventListenerCube : GenericScriptableEventListener<Cube>
	{
		[SerializeField] private ScriptableEventCube _scriptableEvent;
		[SerializeField] private ScriptableEventCube.CubeEvent _collisionEvent;

		protected override void OnEnable() => _scriptableEvent.Subscribe(_collisionEvent.Invoke);
		protected override void OnDisable() => _scriptableEvent.Unsubscribe(_collisionEvent.Invoke);
	}
}
