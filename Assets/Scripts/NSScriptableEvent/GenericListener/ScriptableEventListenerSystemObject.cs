﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSScriptableEvent.GenericListeners
{
	public class ScriptableEventListenerSystemObject : GenericScriptableEventListener<object>
	{
		[SerializeField] private ScriptableEventSystemObject _scriptableEvent;
		[SerializeField] private ScriptableEventSystemObject.SystemObjectEvent _systemObjectEvent;

		protected override void OnEnable() => _scriptableEvent.Subscribe(_systemObjectEvent.Invoke);
		protected override void OnDisable() => _scriptableEvent.Unsubscribe(_systemObjectEvent.Invoke);
	}
}
