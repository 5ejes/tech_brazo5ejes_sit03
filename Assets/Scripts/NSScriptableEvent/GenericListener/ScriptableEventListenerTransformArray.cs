﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSScriptableEvent.GenericListeners
{
	public class ScriptableEventListenerTransformArray : GenericScriptableEventListener<Transform[]>
	{
		[SerializeField] private ScriptableEventTransformArray _scriptableEvent;
		[SerializeField] private ScriptableEventTransformArray.TransformArrayEvent _collisionEvent;

		protected override void OnEnable() => _scriptableEvent.Subscribe(_collisionEvent.Invoke);
		protected override void OnDisable() => _scriptableEvent.Unsubscribe(_collisionEvent.Invoke);
	}
}
