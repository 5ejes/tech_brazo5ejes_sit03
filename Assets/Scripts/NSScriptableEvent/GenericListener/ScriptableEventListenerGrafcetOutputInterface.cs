﻿using System.Collections;
using System.Collections.Generic;
using NSInterfazControlPLC;
using UnityEngine;

namespace NSScriptableEvent.GenericListeners
{
	public class ScriptableEventListenerGrafcetOutputInterface : GenericScriptableEventListener<grafcetOutputInterface>
	{
		[SerializeField] private ScriptableEventGrafcetOutputInterface _scriptableEvent;
		[SerializeField] private ScriptableEventGrafcetOutputInterface.GrafcetOutputInterfaceEvent _grafcetOutputInterfaceEvent;

		protected override void OnEnable() => _scriptableEvent.Subscribe(_grafcetOutputInterfaceEvent.Invoke);
		protected override void OnDisable() => _scriptableEvent.Unsubscribe(_grafcetOutputInterfaceEvent.Invoke);
	}
}
