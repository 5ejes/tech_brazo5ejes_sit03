﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSScriptableEvent.GenericListeners
{
	public class ScriptableEventListenerColor : GenericScriptableEventListener<Color>
	{
		[SerializeField] private ScriptableEventColor _scriptableEvent;
		[SerializeField] private ScriptableEventColor.ColorEvent _collisionEvent;

		protected override void OnEnable() => _scriptableEvent.Subscribe(_collisionEvent.Invoke);
		protected override void OnDisable() => _scriptableEvent.Unsubscribe(_collisionEvent.Invoke);
	}
}
