﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSScriptableEvent.GenericListeners
{
	public class ScriptableEventListenerTransform : GenericScriptableEventListener<Transform>
	{
		[SerializeField] private ScriptableEventTransform _scriptableEvent;
		[SerializeField] private ScriptableEventTransform.TransformEvent _collisionEvent;

		protected override void OnEnable() => _scriptableEvent.Subscribe(_collisionEvent.Invoke);
		protected override void OnDisable() => _scriptableEvent.Unsubscribe(_collisionEvent.Invoke);
	}
}
