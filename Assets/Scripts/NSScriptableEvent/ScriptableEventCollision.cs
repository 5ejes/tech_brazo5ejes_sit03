using UnityEngine;
using UnityEngine.Events;

namespace NSScriptableEvent
{
	[CreateAssetMenu(fileName = "Event Collision", menuName = "NSScriptableEvent/Collision", order = 0)]
	public class ScriptableEventCollision : AbstractScriptableEvent<Collision>
	{
		[System.Serializable]
		public class CollisionEvent : UnityEvent<Collision> { }
	}
}