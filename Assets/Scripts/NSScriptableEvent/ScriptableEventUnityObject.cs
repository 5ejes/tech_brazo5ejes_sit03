using UnityEngine;
using UnityEngine.Events;

namespace NSScriptableEvent
{
	[CreateAssetMenu(fileName = "Event UnityObject", menuName = "NSScriptableEvent/UnityObject", order = 0)]
	public class ScriptableEventUnityObject : AbstractScriptableEvent<Object>
	{
		[System.Serializable]
		public class UnityObjectEvent : UnityEvent<Object> { }
	}
}