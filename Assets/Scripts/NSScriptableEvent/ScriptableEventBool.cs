using UnityEngine;
using UnityEngine.Events;

namespace NSScriptableEvent
{
	[CreateAssetMenu(fileName = "Event Bool", menuName = "NSScriptableEvent/Bool", order = 0)]
	public class ScriptableEventBool : AbstractScriptableEvent<bool>
	{
		[System.Serializable] public class BoolEvent : UnityEvent<bool> { }
	}
}