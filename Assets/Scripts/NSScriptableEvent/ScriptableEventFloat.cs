using UnityEngine;
using UnityEngine.Events;

namespace NSScriptableEvent
{
	[CreateAssetMenu(fileName = "Event float", menuName = "NSScriptableEvent/Float", order = 0)]
	public class ScriptableEventFloat : AbstractScriptableEvent<float>
	{
		[System.Serializable] public class FloatEvent : UnityEvent<float> { }
	}
}