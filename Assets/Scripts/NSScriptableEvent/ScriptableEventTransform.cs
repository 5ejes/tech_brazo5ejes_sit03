using UnityEngine;
using UnityEngine.Events;

namespace NSScriptableEvent
{
    [CreateAssetMenu(fileName = "Event Transform", menuName = "NSScriptableEvent/Transform", order = 0)]
    public class ScriptableEventTransform : AbstractScriptableEvent<Transform>
    {
        [System.Serializable] public class TransformEvent : UnityEvent<Transform> {  }
    }
}