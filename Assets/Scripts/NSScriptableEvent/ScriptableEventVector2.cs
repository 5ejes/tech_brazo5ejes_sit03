using UnityEngine;

namespace NSScriptableEvent
{
    [CreateAssetMenu(fileName = "Event Vector2", menuName = "NSScriptableEvent/Vector2", order = 0)]
    public class ScriptableEventVector2 : AbstractScriptableEvent<Vector2>
    {
        
    }
}