﻿using NSInterfazControlPLC;
using UnityEngine;
using UnityEngine.Events;

namespace NSScriptableEvent
{
	[CreateAssetMenu(fileName = "Event grafcetController", menuName = "NSScriptableEvent/grafcetController", order = 0)]
	public class ScriptableEventGrafcetController : AbstractScriptableEvent<grafcetController>
	{
		[System.Serializable]
		public class GrafcetControllerEvent : UnityEvent<grafcetController> { }
	}
}