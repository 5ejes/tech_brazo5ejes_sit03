#pragma warning disable 0649
using NSUtilities;
using UnityEngine;
using UnityEngine.Events;

namespace NSScriptableEvent
{
	public class ScriptableEventListener : MonoBehaviour
	{
		[SerializeField] private ScriptableEvent scriptableEvent;

		public UnityEvent unityEvent;

		private void OnEnable()
		{
			if (scriptableEvent)
			{
				scriptableEvent.Subscribe(unityEvent.Invoke);
			}
		}

		private void OnDisable()
		{
			if (scriptableEvent)
			{
				scriptableEvent.Unsubscribe(unityEvent.Invoke);
			}
		}

		[ContextMenu("Execute listener")]
		public void ExecuteListener()
		{
			unityEvent.WrapInvoke();
		}
	}
}