﻿using System.Collections;
using System.Collections.Generic;
using Modular;
using UnityEngine;

namespace NSInspectorValue
{
	public class IntInspectorValue : GenericInspectorValue<int>
	{
		[Header("Events")]
		[SerializeField] private IntEvent _onSetValue = new IntEvent();
		[SerializeField] private IntEvent _onGetValue = new IntEvent();


		private void Awake()
		{
			// Inicializo el script
			Initialize();
		}

		public override void Initialize()
		{
			// Defino los eventos de respuesta
			GetResponse = _onGetValue;
			SetResponse = _onSetValue;
		}
	}
}
