﻿using Modular;
using NSInterfazControlPLC;
using NSUtilities;
using UnityEngine;

namespace NSBrazo5Ejes.NSTransportBelt
{
	public class TransportBeltGrafcetReader : MonoBehaviour
	{
		[Header("Events")]
		[SerializeField] private BoolEvent _onTransportBeltMotorStateChange;
		public grafcetOutputInterface GrafcetOutput { get; set; }

		public bool? BeltMotorState
		{
			get
			{
				if (GrafcetOutput == null || !GrafcetOutput.HasController) return null;
				else return GrafcetOutput.GrafcetController.SalidasBool[1];
			}
		}

		private bool _lastBeltMotorState = false;

		public void Update()
		{
			if (GrafcetOutput == null) return;

			var logString = string.Empty;

			// Si tiene un valor
			if (BeltMotorState.HasValue)
			{
				// Obtengo el valor
				var value = BeltMotorState.Value;

				// Si el valor es diferente al último valor almacenado
				if (!_lastBeltMotorState.Equals(value))
				{
					// Logueo el string del motor
					logString = $"El motor de la banda cambió de {_lastBeltMotorState} a {value}";

					// Actualizo el ultimo valor
					_lastBeltMotorState = value;

					// Comunico el evento
					_onTransportBeltMotorStateChange.WrapInvoke(value);
				}
			}
		}

		public void RemoveControllerReference()
		{
			GrafcetOutput = null;
		}
	}
}
