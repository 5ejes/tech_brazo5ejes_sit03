﻿using System.Text.RegularExpressions;
using NSTraduccionIdiomas;
using NSUtilities;
using UnityEngine;
using static NSScriptableEvent.ScriptableEventString;

namespace NSBrazo5Ejes.NSTransportBelt
{
	public class TransportBeltPositionCommunicator : MonoBehaviour
	{
		[Header("Belt Sensor Transport")]
		[SerializeField] private Transform _beltSensorTransform = null;
		[Header("Local Position Transform")]
		[SerializeField] private Transform _localPositionConverter = null;
		[Header("Events")]
		[SerializeField]private StringEvent _onCommunicatedBeltPosition = null;

		private static readonly string DESCRIPTION_SITUACION_2_KEY = "TextDescripcionSituacionP2";
		private static readonly string REGEX_PATTERN = @"\(([^)]*)\)";

		public string DescriptionSituacion2
		{
			get
			{
				if (string.IsNullOrEmpty(_descriptionSituation)) _descriptionSituation = DiccionarioIdiomas.Instance.Traducir(DESCRIPTION_SITUACION_2_KEY);
				return _descriptionSituation;
			}
		}

		private string _descriptionSituation = null;

		public void UpdateInfoWithCurrentPosition()
		{
			// Si las variables no están definidas, retorno
			if (_beltSensorTransform == null) return;
			if (_localPositionConverter == null) return;

			// Obtengo la descripción de la situación
			string info = DescriptionSituacion2;

			// Hallo los paréntesis según el Regex de la operación
			MatchCollection splitted = Regex.Matches(info, REGEX_PATTERN);

			int count = splitted.Count;
			// Si la cuenta es menor a uno, retorno
			if (count < 1) return;
			
			// Obtengo el valor
			var parenthesisMatch = splitted[0].Value;

			// Transformo la posición y escribo las variables de posición
			int x, z;
			Vector3 localPosition = _localPositionConverter.InverseTransformPoint(_beltSensorTransform.position).ToAbsolute();
			x = Mathf.RoundToInt(localPosition.x * 100f);
			z = Mathf.RoundToInt(localPosition.z * 100f);

			// Reemplazo las variables por los valores
			parenthesisMatch = parenthesisMatch.Replace("a", x.ToString());
			parenthesisMatch = parenthesisMatch.Replace("b", z.ToString());

			// Reemplazo la información en el texto, con los valores dados en el match
			info = info.Replace(splitted[0].Value, parenthesisMatch);

			// Invoco el evento
			_onCommunicatedBeltPosition?.Invoke(info);
		}
	}
}
