﻿using System.Collections;
using System.Collections.Generic;
using NSBrazo5Ejes.NSDraggingBehaviour;
using UnityEngine;

public class DragSpecificVectorMoveTest : MonoBehaviour
{
	[SerializeField] private Vector3 _rotation;
	[SerializeField] private Transform _positionMarker;
	[SerializeField] private Transform[] _bounds;
	[SerializeField] private bool _evalInsideBounds = true;
	[Header("Reference"), SerializeField] private DragMoveComponent _moveComponent;
	[SerializeField] private DraggableComponent _draggable;
	private TransformBound[] _convertedBounds;

	private void Start()
	{
		List<TransformBound> tempBounds = new List<TransformBound>();

		for (int i = 0; i < _bounds.Length; i++)
		{
			tempBounds.Add(_bounds[i]);
		}

		_convertedBounds = tempBounds.ToArray();
	}

	public void Update()
	{
		// Si existen los componentes
		if (_draggable && _moveComponent)
		{

			// Muevo el draggable a la posición
			if (_moveComponent.TryMoveDraggable(_draggable, _positionMarker.position, Vector3.zero ,Quaternion.Euler(_rotation), _convertedBounds, _evalInsideBounds))
			{
				// Aplico la rotación a draggable
				_draggable.transform.rotation = Quaternion.Euler(_rotation);
			}
		}
	}

}
