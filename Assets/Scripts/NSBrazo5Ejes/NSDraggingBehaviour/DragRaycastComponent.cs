﻿using System;
using System.Collections;
using System.Collections.Generic;
using Modular.LerpUpdate;
using NSUtilities;
using UnityEngine;

namespace NSBrazo5Ejes.NSDraggingBehaviour
{
  public class DragRaycastComponent : MonoBehaviour
  {
    [Header("Global References")]
    [SerializeField] private Camera _camera = null;
    [SerializeField] DragMoveComponent _dragMove;
    [Header("References")]
    [SerializeField] private Transform _planeHeightReference = null;
    [Header("Free drag limits")]

    [SerializeField] private Transform _maxLimit = null;
    [SerializeField] private Transform _minLimit = null;

    [Header("Settings")]
    [SerializeField] private LayerMask _checkDraggableLayer = Physics.AllLayers;
    public DraggableComponent CurrentDraggable { get; set; } = null;
    public DragMoveComponent MoveComponent { get => _dragMove; set => _dragMove = value; }

    /// <value></value>
    private Plane _dragPlane;
    private Ray _cameraRay;
    private Vector3 planePosition;
    private Vector3 contactPoint;

    private bool _currentHoldValidMove = false;
    private bool _markToFloat = false;

    private readonly float _heightOffset = 0.025f;
    private float _storedGroundPos = 0f;
    private float _currentHeightOffset = 0f;

		private static readonly float _invalidMoveOffsetHeight = 0.075f;
		private static readonly float _invalidOffsetIncrementSpeed = 0.6f;
		private float _invalidCurrentHeightOffset = 0f;
    public delegate void DragRaycastComponentEventHandler(DragRaycastComponent sender, DraggableComponent draggable, bool ValidMove, Vector3 dragPosition);

    public event DragRaycastComponentEventHandler MouseDown;
    public event DragRaycastComponentEventHandler MouseHold;
    public event DragRaycastComponentEventHandler MouseUp;

    public event DragRaycastComponentEventHandler PlanePositionDragEvent;

    private void Update()
    {

      if (_markToFloat)
      {
        if (_currentHeightOffset < _heightOffset)
        {
          _currentHeightOffset += Time.deltaTime * 0.15f;
        }
        else
        {
          _currentHeightOffset = _heightOffset;
        }
      }

      if (Input.GetMouseButtonDown(0))
      {
        MouseDownHandler();
      }

      if (Input.GetMouseButton(0))
      {
        MouseHoldHandler();
      }

      if (Input.GetMouseButtonUp(0))
      {
        MouseUpHandler();
      }

      // var parsedPosition = _pivot.InverseTransformPoint(_test.position);
      // _test.position = SnapVector(_test.position, snap);
    }

    private void MouseDownHandler()
    {
      // Si la cámara está definida
      if (_camera != null)
      {
        // Defino el rayo de la cámara
        _cameraRay = _camera.ScreenPointToRay(Input.mousePosition);

        // Obtengo la información de todos los hits
        RaycastHit hitInfo;
        bool hitSomething = Physics.Raycast(_cameraRay, out hitInfo, 10f, _checkDraggableLayer, QueryTriggerInteraction.Collide);

        // Evalúo cada resultado
        if (hitSomething)
        {
          // Obtengo el componente de Dragging
          var draggingComponent = hitInfo.transform.GetComponent<DraggableComponent>();
          if (draggingComponent == null) draggingComponent = hitInfo.transform.GetComponentInParent<DraggableComponent>();

          // Si el componente de dragging no es nulo
          if (draggingComponent != null)
          {
            // Defino el draggable actual
            CurrentDraggable = draggingComponent;
            contactPoint = hitInfo.point;

            // Marco el componente como flotante
            _markToFloat = true;

            // Deshabilito el Rigidbody
            DisableDraggableRigidbody(CurrentDraggable);
          }
        }

      }

      // Comunico el evento

      // Marco el movimiento del click down como movimiento inválido ya que no se realiza movimiento alguno
      MouseDown?.Invoke(this, CurrentDraggable, false, MoveComponent.LastValidMovePosition);
    }

    private void MouseHoldHandler()
    {

      // Si hay un draggable actual
      if (CurrentDraggable)
      {
        // Creo un plano sobre el punto de contacto
        _dragPlane = new Plane(Vector3.up, Vector3.up * contactPoint.y);

        // Defino el rayo de la cámara
        _cameraRay = _camera.ScreenPointToRay(Input.mousePosition);

        // Creo una variable para la distancia
        float distance = 0f;

        // Genero un rayo que atraviese el plano
        if (_dragPlane.Raycast(_cameraRay, out distance))
        {
          // A través del rayo generado, obtengo un punto dada la distancia
          planePosition = _cameraRay.GetPoint(distance);

          // Defino la altura del punto a la posición referencia
          planePosition.y = _planeHeightReference.position.y;

          // Muevo el draggable a la posición espacial
          var validMove = MoveComponent.TryMoveDraggable(CurrentDraggable, planePosition, Vector3.up * _currentHeightOffset, CurrentDraggable.Target.rotation);

          // Obtengo una variable para comprobar si está dentro de los límites registrado como movido
          bool onDraggedComponentList = MoveComponent.IsOnDraggedComponentsList(CurrentDraggable);

          // Si el movimiento actual no se ha registrado como válido, lo evalúo
          if (!_currentHoldValidMove)
            // Registro el movimiento actual 
            _currentHoldValidMove = validMove;

          // Si el movimiento fué válido
          if (validMove)
            //// Unicamente flota en areas donde se puede mover
            _markToFloat = true;
          else
          {
            // Si no fue válido el movimiento

            // Compruebo que no haya entrado en la zona de arrastre
            if (!onDraggedComponentList)
            {
							// Actualizo los valores para la animación del offset
							_invalidCurrentHeightOffset += Time.deltaTime * _invalidOffsetIncrementSpeed;
							if(_invalidCurrentHeightOffset > _invalidMoveOffsetHeight) _invalidCurrentHeightOffset = _invalidMoveOffsetHeight;

              // Defino la posición del plano como arrastre
              CurrentDraggable.Target.position = planePosition + Vector3.up * _invalidCurrentHeightOffset;
            }
          }

          // Comunico el evento de arrastre del plano
          bool isLastMoveValid = validMove && onDraggedComponentList;
          Vector3 lastValidMovePosition = MoveComponent.LastValidMovePosition;

          PlanePositionDragEvent?.Invoke(this, CurrentDraggable, isLastMoveValid, isLastMoveValid ? lastValidMovePosition : planePosition);

          if (_minLimit && _maxLimit)
          {
            var position = CurrentDraggable.transform.position;

            position.x = Mathf.Clamp(position.x, _minLimit.position.x, _maxLimit.position.x);
            position.z = Mathf.Clamp(position.z, _minLimit.position.z, _maxLimit.position.z);

            CurrentDraggable.transform.position = position;
          }

          //if (_storedGroundPos < float.Epsilon)
          //{
          //	_storedGroundPos = CurrentDraggable.transform.position.y;
          //}

          //var position = CurrentDraggable.transform.position;
          //position.y = _storedGroundPos + _offsetUpdater.LerpValue.y;
          //CurrentDraggable.transform.position = position;
        }
      }

      // Invoco el evento
      MouseHold?.Invoke(this, CurrentDraggable, MoveComponent.IsLastMoveValid, MoveComponent.LastValidMovePosition);
    }

    private void MouseUpHandler()
    {

      // Marco el componente para que deje de flotar
      _markToFloat = false;
      _currentHeightOffset = 0f;
			_invalidCurrentHeightOffset = 0f;

      if (_currentHoldValidMove)
      {
        // Inicio la corutina para descender el objeto al suelo
        StartCoroutine(GroundFloatingObject(CurrentDraggable));
      }
      else
      {
        if (MoveComponent.IsOnDraggedComponentsList(CurrentDraggable))
        {
          EnableDraggableRigidbody(CurrentDraggable);
        }
        // Elimino la referencia al objeto arrastrado
        CurrentDraggable = null;
      }

      _currentHoldValidMove = false;

      // Comunico el evento 
      MouseUp?.Invoke(this, CurrentDraggable, MoveComponent.IsLastMoveValid, MoveComponent.LastValidMovePosition);
    }

    private IEnumerator GroundFloatingObject(DraggableComponent target)
    {

      if (target == null) yield break;
      float targetHeight = _planeHeightReference.position.y;

      Transform targetTransform = target.Target;
      var targetPosition = targetTransform.position;
      float offsetToMax = Mathf.Abs(target.GetOffsetToMax().y);
      targetPosition.y = targetHeight + offsetToMax;

      Vector3 currentPosition = targetTransform.position;
      targetTransform.position = targetPosition;

      while ((currentPosition - targetPosition).sqrMagnitude > float.Epsilon)
      {
        targetTransform.position = Vector3.MoveTowards(currentPosition, targetPosition, Time.deltaTime * 0.5f);
        currentPosition = target.transform.position;
        yield return null;
      }

      targetTransform.position = targetPosition;

      // Habilito el rigidbody
      EnableDraggableRigidbody(CurrentDraggable);

      // Elimino la referencia al arrastrable actual
      CurrentDraggable = null;
    }

    private void EnableDraggableRigidbody(DraggableComponent draggable)
    {
      if (draggable == null)
      {
        Debug.LogWarning($"Not a draggable selected : {this}", this);
        return;
      }

      Rigidbody draggableRigidbody = draggable.Rigidbody;
      SetRigidbodyActive(draggableRigidbody, true);
    }
    private void DisableDraggableRigidbody(DraggableComponent draggable)
    {
      if (draggable == null)
      {
        Debug.LogError($"Not a draggable selected : {this}", this);
        return;
      }

      Rigidbody draggableRigidbody = draggable.Rigidbody;
      SetRigidbodyActive(draggableRigidbody, false);
    }

    private static void SetRigidbodyActive(Rigidbody draggableRigidbody, bool active)
    {
      if (draggableRigidbody == null) return;

      draggableRigidbody.isKinematic = !active;
      draggableRigidbody.useGravity = active;
    }
  }
}