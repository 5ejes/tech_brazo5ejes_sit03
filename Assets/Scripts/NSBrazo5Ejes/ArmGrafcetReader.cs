﻿using System;
using System.Collections;
using System.Collections.Generic;
using NSInterfazControlPLC;
using NSUtilities;
using UnityEngine;
using UnityEngine.Events;

namespace NSBrazo5Ejes
{
	public class ArmGrafcetReader : MonoBehaviour
	{

		public class ArmReadingData
		{
			public Vector3? Position { get; set; }
			public bool? GripState { get; set; }
			public bool? HorizontalMode { get; set; }

			public void ClearData()
			{
				HorizontalMode = null;
				Position = null;
				GripState = null;
			}

			public bool HasValuesStored()
			{
				return Position.HasValue || GripState.HasValue || HorizontalMode.HasValue;
			}
		}

		[Header("Grafcet Controller")]
		[SerializeField] private grafcetOutputInterface _refGrafcetOutput;


		[Header("Settings")]
		[SerializeField] private float _orderDelay = 0.025f;
		[Header("Events")]
		[SerializeField] private Modular.Vector3Event _onPositionChange;
		[SerializeField] private Modular.BoolEvent _onGripStateChange;
		[Space]
		[SerializeField] private Modular.BoolEvent _onHorizontalModeStateChange;
		[Header("Error Events")]
		[SerializeField] private UnityEvent _onWrongCoordinatePassed = new UnityEvent();
		[Header("Debug")]
		[SerializeField] private bool _showDebug;

		private ArmReadingData _data = new ArmReadingData();
		private bool _firstCompileCheck = true;
		private int _historyIndex = 0;
		private Vector3[] _historyArray;
		private int _historySize = 5;

		private Coroutine _sendDataCoroutine;

		public grafcetOutputInterface GrafcetOutput { get => _refGrafcetOutput; set => _refGrafcetOutput = value; }

		public Vector3? ArmPosition
		{
			get
			{
				// Si el grafcet no existe, retorno null
				if (GrafcetOutput == null || !GrafcetOutput.HasController) return null;
				else return new Vector3(GrafcetOutput.PositionX / 100f, GrafcetOutput.PositionZ / 100f, GrafcetOutput.PositionY / 100f);
			}
		}
		private Vector3 _lastArmPosition = -Vector3.one;

		public bool? GripState
		{
			get
			{
				// Si el grafcet no existe, retorno null
				if (GrafcetOutput == null || !GrafcetOutput.HasController) return null;
				else return GrafcetOutput.GripState;
			}
		}
		private bool _lastGripState = false;

		public bool? Eje5
		{
			get
			{
				if (GrafcetOutput == null || !GrafcetOutput.HasController) return null;
				else return GrafcetOutput.Eje5;
			}
		}

		private bool _lastEje5 = false;


		private void Update()
		{
			// Si el grafcet output no ha sido definido
			if (GrafcetOutput == null) return;

			var logString = string.Empty;

			// Compruebo si existe un valor para la velocidad del motor 1
			if (Eje5.HasValue)
			{
				// Guardo el valor para evitar cambios en mitad de frame
				bool value = Eje5.Value;

				// Si existe un cambio en el valor
				if (!_lastEje5.Equals(value))
				{
					// Logueo en consola
					logString = $"El estado del eje 5 cambio de {_lastEje5} a {value}";

					// Invoco el evento de cambio
					_data.HorizontalMode = value;
					// _onHorizontalModeStateChange.WrapInvoke(value != 0);


					// Actualizo el valor anterior
					_lastEje5 = value;

					// Si hay un valor para hacer log
					LogString(logString);
				}

			}

			// Compruebo si existe un valor para la posición del brazo
			if (ArmPosition.HasValue)
			{

				// Obtengo el número de pasos
				int stepsNumber = GrafcetOutput.GrafcetController._steps.Length;

				// Si el grafcet controller no existe, retorno
				if (GrafcetOutput.GrafcetController == null) return;

				// Si no hay pasos en el programa, retorno
				if (stepsNumber == 0) return;

				// Obtengo el valor de la posición
				var value = ArmPosition.Value;

				// El valor no es un valor por defecto
				if ((value - Vector3.one * 2.55f).sqrMagnitude > 0.00025f)
				{


					// Si se realiza una comprobación inicial en el historial
					if (_firstCompileCheck)
					{

						// Si el indice supera el tamaño del historial
						if (_historyIndex < _historySize)
						{
							// Si el historial es nulo, lo creo
							if (_historyArray == null) _historyArray = new Vector3[_historySize];

							// Registro los primeros valores del historial para la comprobación
							_historyArray[_historyIndex] = value;
							_historyIndex++;

							return;
						}
						else
						{
							// Obtengo el valor basado en el historial
							value = GetHistoryValue();

							// Marco la primera comprobación como falsa
							_firstCompileCheck = false;
						}
					}

					// Compruebo si existe un cambio o si el valor no es nulo
					if (!_lastArmPosition.Equals(value) && value != null)
					{
						// Compruebo que la posición sea válida
						var isPositionValid = IsValid(value);

						if (!isPositionValid)
							// Ejecuto el error de coordenada inválida
							_onWrongCoordinatePassed.WrapInvoke();

						// Existe un cambio en la posición
						logString = $"La posición cambió de {_lastArmPosition.ToString("0.00")} a {value.ToString("0.00")}";

						// Actualizo los cambios
						_lastArmPosition = value;

						// Ejecuto el evento, si no evalua a nulo
						// if (_onPositionChange != null) _onPositionChange.Invoke(value);
						_data.Position = value;

						// Si hay un valor para hacer log
						LogString(logString);
					}
				}
			}

			// Compruebo si existe un valor para el estado del grip
			if (GripState.HasValue)
			{
				// Obtengo el valor del gripState
				bool value = GripState.Value;

				// Compruebo si existe un cambio
				if (!_lastGripState.Equals(value))
				{
					// Existe un cambio en el agarre del grip
					logString = $"El grip cambió de {_lastGripState} a {value}";

					// Ejecuto el evento, si no evalua a nulo
					// if (_onGripStateChange != null) _onGripStateChange.Invoke(value);
					_data.GripState = value;

					// Actualizo los cambios
					_lastGripState = value;

					// Si hay un valor para hacer log
					LogString(logString);
				}

			}

			// Si hay valores a ser enviados
			if (_data.HasValuesStored())
				// Envío los datos recolectados
				SendData(_orderDelay, _data);
		}

		private void SendData(float delay, ArmReadingData data)
		{
			if (data == null) return;
			// Creo la rutina de transmisión de datos
			IEnumerator routine = SendDataCo(delay, data);

			if (_sendDataCoroutine != null)
				// Detengo una coroutine previa si existe
				StopCoroutine(_sendDataCoroutine);

			// Inicializo la actual
			_sendDataCoroutine = StartCoroutine(routine);
		}

		private IEnumerator SendDataCo(float delay, ArmReadingData data)
		{
			// Creo la espera para la transferencia de los datos leidos por el grafcet
			WaitForSeconds dataTransferDelay = new WaitForSeconds(delay);

			// Respaldo los datos antes de borrarlos
			bool? horizontalMode = data.HorizontalMode ?? null;
			bool? gripState = data.GripState ?? null;
			Vector3? position = data.Position ?? null;

			// Limpio los datos al finalizar
			data.ClearData();

			// Transmito los datos
			if (horizontalMode.HasValue)
			{
				// Obtengo el valor del modo horizontal
				bool value = horizontalMode.Value;

				_onHorizontalModeStateChange.WrapInvoke(value);

				// Realizo la espera habitual
				yield return dataTransferDelay;
			}

			if (gripState.HasValue)
			{
				_onGripStateChange.WrapInvoke(gripState.Value);
				yield return dataTransferDelay;
			}

			// Si el valor de posición existe y no ha sido emitido
			if (position.HasValue)
			{
				_onPositionChange.WrapInvoke(position.Value);
				yield return dataTransferDelay;
			}

		}

		private void LogString(string logString)
		{
			if (!_showDebug) return;
			if (!string.IsNullOrEmpty(logString)) Debug.Log($"{logString} : {Time.frameCount}", this);
		}

		private Vector3 GetHistoryValue()
		{
			Vector3 output = -Vector3.one;

			for (int i = 1; i < _historySize; i++)
			{
				var firstHistoryValue = _historyArray[0];
				var currentHistoryValue = _historyArray[i - 1];

				if (firstHistoryValue != currentHistoryValue)
				{
					output = currentHistoryValue;
					return output;
				}
				else
				{
					output = firstHistoryValue;
				}
			}

			return output;
		}

		public void IgnoreFirstCompile()
		{
			_firstCompileCheck = true;
			_lastArmPosition = -Vector3.one;
			_lastEje5 = false;

			_historyArray = new Vector3[_historySize];
			_historyIndex = 0;
		}

		public void ResetStoredReaderValues()
		{
			_lastArmPosition = -Vector3.one;
			_lastGripState = false;
			_lastEje5 = false;
		}

		private bool IsValid(Vector3 target)
		{

			// Defino los steps (Número entre el que se va a mover)
			var steps = 0.05f;

			// Hago un ajuste hacia el número más cercano multiplo de steps
			target.x = Mathf.Round(target.x / steps) * steps;
			target.y = Mathf.Round(target.y / steps) * steps;
			target.z = Mathf.Round(target.z / steps) * steps;

			// Compruebo los límites de cada dimensión
			bool wrongX = target.x < ArmPositionHandler.MIN_LIMITS.x || target.x > ArmPositionHandler.MAX_LIMITS.x;
			bool wrongY = target.y < ArmPositionHandler.MIN_LIMITS.y || target.y > ArmPositionHandler.MAX_LIMITS.y;
			bool wrongZ = target.z < ArmPositionHandler.MIN_LIMITS.z || target.z > ArmPositionHandler.MAX_LIMITS.z;

			// Si alguno de los ejes no está en el valor predeterminado
			bool targetHasInvalidLocalPositionValues = wrongX || wrongY || wrongZ;

			// Retorno el valor
			return !targetHasInvalidLocalPositionValues;
		}

		public void RemoveControllerReference()
		{
			GrafcetOutput = null;
		}
	}

}