﻿using System.Collections;
using UnityEngine;

namespace NSBrazo5Ejes
{
	public class CameraTextureRender : MonoBehaviour
	{
		[Header("Settings")]
		[SerializeField] private RenderTexture _renderTexture;
		[SerializeField] private Camera _targetRenderCamera;
		[Space]
		[SerializeField] private Transform _override;

		private Transform _transform;

		/// <summary>
		/// Si no hay un override, usa el transform como Target
		/// </summary>
		public Transform Target => Override != null ? Override : _transform;
		private static readonly WaitForEndOfFrame _waitEndOfFrame = new WaitForEndOfFrame();

		private static Camera _staticRenderCamera;

		public Camera TargetRenderCamera
		{
			get
			{
				// Uso la cámara estática si está definida
				if (_staticRenderCamera != null) _targetRenderCamera = _staticRenderCamera;
				if (_targetRenderCamera == null)
				{
					// Creo una cámara y la defino como estática de renderizado
					_targetRenderCamera = SetupNewCamera();
					_staticRenderCamera = _targetRenderCamera;
				}
				return _targetRenderCamera;
			}
			set => _staticRenderCamera = _targetRenderCamera = value;

		}

		public RenderTexture RenderTexture { get => _renderTexture; set => _renderTexture = value; }
		/// <summary>
		/// Define el Override para posicionar la cámara
		/// </summary>
		/// <value></value>
		public Transform Override { get => _override; set => _override = value; }

		private void Awake()
		{
			// Defino el transform actual en caché
			_transform = transform;
		}

		private Camera SetupNewCamera()
		{
			// Creo una nueva cámara
			var output = new GameObject("ScriptRenderCamera").AddComponent<Camera>();

			output.fieldOfView = 21f;

			// Retorno la cámara
			return output;
		}
		public void Render()
		{
			if (TargetRenderCamera == null) return;
			if (RenderTexture == null) return;

			// Renderizo la textura
			RenderTextureFromCamera(TargetRenderCamera, RenderTexture, Target);
		}

		/// <summary>
		/// Renderiza un rendertexture usando la cámara especificada
		/// </summary>
		/// <param name="camera">Cámara para renderizar la textura</param>
		/// <param name="renderTexture">Textura a renderizar</param>
		/// <param name="copyTransform">Transform al que se va transladar la cámara</param>
		public void RenderTextureFromCamera(Camera camera, RenderTexture renderTexture, Transform copyTransform)
		{
			Coroutine coroutine = StartCoroutine(RenderTextureFromCameraCoroutine(camera, renderTexture, copyTransform));
		}
		private static IEnumerator RenderTextureFromCameraCoroutine(Camera camera, RenderTexture renderTexture, Transform copyTransform)
		{
			// Almaceno la posición original de la cámara
			Vector3 cameraPosition = camera.transform.position;
			Quaternion cameraRotation = camera.transform.rotation;

			camera.transform.SetPositionAndRotation(copyTransform.position, copyTransform.rotation);

			// Asigno el render texture
			camera.targetTexture = renderTexture;
			renderTexture.anisoLevel = 8;

			// Espero al final del frame para renderizar la imagen
			yield return _waitEndOfFrame;

			// Renderizo
			camera.Render();

			// Restablezco la cámara
			camera.targetTexture = null;
			camera.transform.SetPositionAndRotation(cameraPosition, cameraRotation);
		}

		[ContextMenu("Set Camera Debug View")]
		private void SetViewCamera()
		{
			TargetRenderCamera.transform.position = Target.position;
			TargetRenderCamera.transform.rotation = Target.rotation;
			TargetRenderCamera.enabled = true;
		}
	}
}

