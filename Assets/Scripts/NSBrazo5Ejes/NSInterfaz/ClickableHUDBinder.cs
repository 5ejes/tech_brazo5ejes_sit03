﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace NSBrazo5Ejes.NSInterfaz
{

	public class ClickableHUDBinder : MonoBehaviour
	{
		[Header("Reference")]
		[SerializeField] private HUDDynamicCoordinates _hudCoordinatesPrefab;
		[SerializeField] private Canvas _parent = null;
		[SerializeField] private Camera _camera = null;
		[Header("Settings")]
		[SerializeField] private float _length = 3.5f;
		[SerializeField] private LayerMask _layerMask = Physics.AllLayers;

		private Transform _currentActive;

		public Camera Camera
		{
			get
			{
				if (!_camera) _camera = Camera.main;
				return _camera;
			}
			set => _camera = value;
		}

		private void Start()
		{
			// Verifico que el objeto prefab del HUDCoordinates esté disponible
			if (!_hudCoordinatesPrefab)
			{
				Debug.LogError($"The HUD Coordinates prefab is not defined.", this);
				return;
			}
		}

		private void Update()
		{
			// Si la cámara del ray no está definida
			if (!Camera)
			{
				Debug.Log($"Raycast Camera is not defined. : {this}", this);
				return;
			}

			if (Input.GetMouseButtonDown(0))
			{
				// Si doy click con el botón del mouse
				RaycastHit hit;
				if (GetRaycastResult(out hit))
				{
					// Si existe el transform
					if (hit.transform)
						ClickablePointerPress(hit.transform);
				}
			}

			if (Input.GetMouseButtonUp(0))
			{
				// Si suelto el botón del mouse
				ClickablePointerRelease();
			}
		}

		private bool GetRaycastResult(out RaycastHit hit)
		{
			return GetRaycastResult(Input.mousePosition, Camera, _length, _layerMask, out hit);
		}
		private bool GetRaycastResult(Vector3 mousePosition, Camera camera, float length, LayerMask layerMask, out RaycastHit hit)
		{
			Ray cameraRay = camera.ScreenPointToRay(mousePosition);
			return Physics.Raycast(cameraRay, out hit, length, layerMask, QueryTriggerInteraction.UseGlobal);
		}

		public void ClickablePointerPress(Transform target)
		{
			_currentActive = target;

			if (!_hudCoordinatesPrefab) return;

			_hudCoordinatesPrefab.HUDWorldPosition = target.position;
			_hudCoordinatesPrefab.Activate();
		}

		public void ClickablePointerRelease()
		{
			_currentActive = null;

			if (!_hudCoordinatesPrefab) return;

			_hudCoordinatesPrefab.Deactivate();
		}
	}
}
