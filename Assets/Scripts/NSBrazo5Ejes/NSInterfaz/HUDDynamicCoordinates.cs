﻿using System;
using System.Collections;
using NSUtilities;
using TMPro;
using UnityEngine;

namespace NSBrazo5Ejes.NSInterfaz
{

	public class HUDDynamicCoordinates : MonoBehaviour
	{
		public static readonly float MOVE_SMOOTH_THRESHOLD = 0.0025f;

		[Header("Settings")]
		[SerializeField] private Camera _canvasCamera;
		[SerializeField] private GameObject _displayGameObject;
		[SerializeField] private Transform _positionConverter;
		[Space]
		[SerializeField] private float _autoDeactivationTimeout = 1f;
		[Header("Animation Settings")]
		[SerializeField] private float _animationTime = 1f;

		[Header("Text Settings")]
		[SerializeField] private TextMeshProUGUI _xCoordinateText;
		[SerializeField] private TextMeshProUGUI _yCoordinateText;
		public bool Active => _displayGameObject.activeSelf;

		private RectTransform _transform;
		private float _storedAutoDeactivationTime = -1f;

		private Animator _animator;
		public Animator HUDAnimator
		{
			get
			{
				if (_animator == null) _animator = GetComponent<Animator>();
				return _animator;
			}
		}

		private Vector3 _hudWorldPosition;
		private Vector2 _screenSize = Vector2.zero;
		private WaitForSeconds _animationWaitTime;
		private Coroutine _deactivationCoroutine;
		private Coroutine _repositionCoroutine;
		private static readonly string ANIMATION_OPEN = "open";
		private static readonly string CLOSE_TRIGGER = "close";

		private Vector3 _lastHUDPosition = new Vector3(0f, 0f, 0f);

		private Vector3 _targetPosition;
		private Vector3 _bugLockedPosition = Vector3.zero;
		private float _bugLogElapsedTime = 0f;
		private float _bugLockLimitTimeout = 0.5f;
		private bool _co_reposition_to_target;

		private bool _initialized = false;

		public Vector3 HUDWorldPosition
		{
			set
			{
				if (_transform == null) _transform = transform as RectTransform;
				Initialize();

				if (_positionConverter)
				{
					var localPosition = _positionConverter.InverseTransformPoint(value);
					localPosition = localPosition.ToAbsolute();

					// localPosition = localPosition.SnapVector(0.05f);
					SetXCoordinateText(Mathf.RoundToInt(localPosition.x * 100));
					SetYCoordinateText(Mathf.RoundToInt(localPosition.z * 100));
				}

				_hudWorldPosition = GetScreenSpacePosition(value);
				_targetPosition = _hudWorldPosition;

				StartCoroutine(RepositionTarget(_targetPosition));
			}
			get => _hudWorldPosition;
		}

		private void Start()
		{
			Initialize();

			// Defino el tamaño de la pantalla
			_screenSize = new Vector2(Screen.width, Screen.height);
		}

		private void Initialize()
		{
			if (_initialized) return;
			_initialized = true;

			// Inicializo las variables iniciales
			HUDAnimator.SetFloat("playbackSpeed", 1 / _animationTime);

			// Si el animation time no existe o el valor de autodesactivación ha cambiado
			if (_animationWaitTime == null || !_autoDeactivationTimeout.Equals(_storedAutoDeactivationTime))
			{
				// Almaceno el nuevo valor de desactivación
				_storedAutoDeactivationTime = _autoDeactivationTimeout;

				// Creo un nuevo objeto de delay para desactivar automáticamente
				_animationWaitTime = new WaitForSeconds(_autoDeactivationTimeout);
			}

			// Si el objeto que controla el estado de activación no está definido
			if (!_displayGameObject)
			{
				// Defino el objeto
				_displayGameObject = transform.Find("DisplayBar").gameObject;
			}
		}

		private Vector3 GetScreenSpacePosition(Vector3 worldPosition)
		{
			if (_canvasCamera == null) return Vector3.zero;

			Vector3 screenPoint = _canvasCamera.WorldToScreenPoint(worldPosition);

			// Limito las coordenadas para que no se salgan de la pantalla
			screenPoint.x = Mathf.Clamp(screenPoint.x, 0f, _screenSize.x);
			screenPoint.y = Mathf.Clamp(screenPoint.y, 0f, _screenSize.y);

			// Aplico el offset para que la posición quede directamente sobre la flecha
			screenPoint.y += 25f;
			screenPoint.z = 1f; //distance of the plane from the camera
			return _canvasCamera.ScreenToWorldPoint(screenPoint);
		}

		public void SetXCoordinateText(int coordinate)
		{
			_xCoordinateText?.SetText(coordinate.ToString());
		}
		public void SetYCoordinateText(int coordinate)
		{
			_yCoordinateText?.SetText(coordinate.ToString());
		}

		public void Activate()
		{
			Initialize();
			InterruptDeactivateDelay();
			HUDAnimator.ResetTrigger(CLOSE_TRIGGER);
			HUDAnimator.Play(ANIMATION_OPEN);
		}

		private void InterruptDeactivateDelay()
		{
			if (_deactivationCoroutine != null)
			{
				// Detengo el proceso de desactivación
				StopCoroutine(_deactivationCoroutine);
			}
		}

		public void Deactivate()
		{
			InterruptDeactivateDelay();
			_deactivationCoroutine = StartCoroutine(DeactivateAfter());
		}

		private IEnumerator DeactivateAfter()
		{
			yield return _animationWaitTime;
			HUDAnimator.SetTrigger(CLOSE_TRIGGER);
		}

		private IEnumerator RepositionTarget(Vector3 targetPosition)
		{

			// Si la corutina está ejecutándose, salgo de ella
			if (_co_reposition_to_target) yield break;
			// La marco para que se ejecute
			_co_reposition_to_target = true;

			// La distancia al objetivo sea menor al threshold definido			
			while ((targetPosition - _transform.position).sqrMagnitude > Mathf.Pow(Mathf.Epsilon, 2) && Active)
			{
				// Posiciono el objeto un poco más cerca de la posición objetivo
				_transform.position = Vector3.MoveTowards(_transform.position, targetPosition, 3f * Time.deltaTime);
				yield return null;
			}

			// Defino el objeto en la posición objetivo
			_transform.position = _targetPosition;

			// Cancelo la ejecución de la reposición
			_co_reposition_to_target = false;
		}

	}
}