﻿using Modular;
using NSUtilities;
using System.Collections.Generic;
using UnityEngine;

public class PointCollisionValidator : MonoBehaviour
{
	[Header("Point References")]
	[SerializeField] private Transform[] _pointReferences;

	[Header("Layer and Detection settings")]
	[SerializeField] private LayerMask _detectionLayer;
	[SerializeField] private QueryTriggerInteraction _interaction = QueryTriggerInteraction.Collide;
	[SerializeField] private int _updateFrame = 0;
	[SerializeField] private ScriptableEnumID _enumID;
	[SerializeField] private List<GameObject> _ignoreGameObjects = new List<GameObject>();
	[SerializeField] private bool _notifyOnce = true;

	[Header("Events")]
	[SerializeField] private TransformEvent _onPointCollisionDetected;
	[Header("Debug")]
	[SerializeField] private bool _showDebug = false;

	private Transform _lastNotifiedValue = null;

	private readonly Collider[] _results = new Collider[8];
	private void FixedUpdate()
	{
		// Si el frame de actualización es igual a 0 ó coincide con el frame de actualización del frameCount
		if (_updateFrame == 0 || Time.frameCount % _updateFrame == 0)
			HandlePointCollision();
	}

	private void HandlePointCollision()
	{

		int length = _pointReferences.Length;
		for (int i = 0; i < length; i++)
		{
			var currentPoint = _pointReferences[i];

			// Si el punto no está activo, lo omito
			if (!currentPoint.gameObject.activeSelf) continue;

			// Obtengo la cuenta de elementos físicos, dadas las transformaciones
			int count = Physics.OverlapBoxNonAlloc(currentPoint.position, currentPoint.localScale / 2f, _results, currentPoint.rotation, _detectionLayer, _interaction);

			for (int pointIndex = 0; pointIndex < count; pointIndex++)
			{
				var currentCollider = _results[pointIndex];
				var enumIdentificator = currentCollider.GetComponent<EnumIdentificator>();

				var validEnumID = true;

				// Si el enum ID local está definido
				if (_enumID != null)
				{
					// Si el colisionado tiene un enum identificator
					if (enumIdentificator != null)
					{
						// Defino la validez del identificador según su equalidad con la referencia
						validEnumID = enumIdentificator.EnumID == _enumID;
					}
					else
					{
						// No hay un identificador en el objeto
						validEnumID = false;
					}
				}

				// Si la comprobación de enum es válida
				if (validEnumID)
				{
					// Busco el objeto en la lista de objetos por ignrar
					var ignoreObject = _ignoreGameObjects.Find(go => go.Equals(currentCollider.gameObject)) != null;

					if (!ignoreObject)
					{
						if (!_notifyOnce || (_notifyOnce && _lastNotifiedValue != currentPoint))
						{
							if (_showDebug)
								Debug.Log($"Point collision detected at : {currentPoint} => {currentCollider}", currentCollider);

							_lastNotifiedValue = currentPoint;
							_onPointCollisionDetected.WrapInvoke(currentPoint);
						}
					}
				}
			}
		}
	}
	public void ResetLastNotifiedValue() => _lastNotifiedValue = null;

	public void AddToIgnoreList(GameObject GO)
	{
		// Compruebo que el objeto esté referenciado
		if (!GO) return;

		// Si la lista contiene el objeto a incluir
		if (_ignoreGameObjects.Contains(GO)) return;

		_ignoreGameObjects.Add(GO);
	}

	public void ClearIgnoreList()
	{
		_ignoreGameObjects.Clear();
	}

	public void RemoveFromIgnoreList(GameObject GO)
	{
		// Si el objeto no está referenciado
		if (!GO) return;

		// Si está contenido en la lista de objetos
		if (_ignoreGameObjects.Contains(GO))
		{
			// Remuevo el objeto
			_ignoreGameObjects.Remove(GO);
		}
	}

	private void OnDrawGizmos()
	{
		if (!_showDebug) return;
		if (_pointReferences == null) return;

		int length = _pointReferences.Length;
		for (int i = 0; i < length; i++)
		{
			var currentPoint = _pointReferences[i];

			// Si el objeto no está activo, no lo dibujo
			if (!currentPoint.gameObject.activeSelf) continue;

			// Aplico matrices y rotaciones por cada punto dibujado
			Color green = Color.green;
			green.a = 0.2f;
			Gizmos.color = green;

			Gizmos.matrix = Matrix4x4.TRS(currentPoint.position, currentPoint.rotation, currentPoint.localScale);
			Gizmos.DrawCube(Vector3.zero, Vector3.one);

			green.a = 0.8f;
			Gizmos.color = green;
			Gizmos.DrawWireCube(Vector3.zero, Vector3.one);
		}
	}
}
