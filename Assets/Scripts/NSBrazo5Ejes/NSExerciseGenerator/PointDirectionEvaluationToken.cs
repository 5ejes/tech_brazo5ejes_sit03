﻿using System;
using UnityEngine;

namespace NSBrazo5Ejes.NSExerciseGenerator
{
	public class PointDirectionEvaluationToken : PointEvaluationToken
	{
		private static readonly float unitValue = 1f - float.Epsilon;

		public enum EvaluationAxis
		{
			Right, Up, Forward
		}

		public enum AxisOption
		{
			Possitive, Negative, Both
		}

		public EvaluationAxis Axis { get; }
		public AxisOption Option { get; }
		public Vector3 DirectionPoint { get; }
		public float MarginError { get; } = 0.1f;
		public PointDirectionEvaluationToken(Vector3 worldPosition, Vector3 directionRequired, EvaluationAxis targetEvaluationDir, AxisOption option) : base(worldPosition)
		{
			DirectionPoint = directionRequired.normalized;
			Axis = targetEvaluationDir;
			this.Option = option;
		}

		public override void Evaluate()
		{
			// Evalua la posición del token dado cierto radio en busca de un objeto determinado

			// Obtengo los resultados de la prueba

			// Si existe un valor para la dirección límite

			var results = Physics.OverlapSphere(WorldPosition, Radius, CheckLayerMask, QueryTriggerInteraction.Ignore);

			// Si hay más de un resultado
			if (results.Length > 0)
			{
				// Si hay un target ID
				if (TargetEnumID != null)
				{
					// Genero una copia de la variable para usar en el lambda
					var targetEnumIDResult = TargetEnumID;

					// Por cada resultado, compruebo si existe un elemento que tenga el identificador de la propiedad
					var foundResult = System.Array.Find(results, result =>
					{
						// Obtengo el identificador de enumeración
						EnumIdentificator enumIdentificator = result.gameObject.GetComponent<EnumIdentificator>();

						// Si el identificador de enumeración es nulo, retorno falso
						if (enumIdentificator == null) return false;

						// Obtengo el enum ID del identificador
						var enumId = enumIdentificator.EnumID;

						// Retorno el resultado de la comparación filtro
						return enumId.Equals(targetEnumIDResult);
					});

					// Considero la evaluación como erronea
					bool isValid = false;

					if (foundResult != null)
					{
						// Evaluo si el objeto indicado se encuentra en la dirección indicada
						float dot = Vector3.Dot(GetEvaluatedDirection(foundResult.transform, Axis), DirectionPoint);

						// Si la evaluación es en la dirección adecuada
						bool isOnDirection = EvaluateDirection(dot, Option, MarginError);

						// Si se ha encontr ado el resultado y la dirección es válida
						isValid = isOnDirection && IsValidDistanceThreshold(foundResult.transform.position);
					}

					// Retorno el resultado
					if (isValid) InvokeSuccess();
					else InvokeFailure();
				}
				else
				{
					// No existe con qué comparar
					Debug.LogWarning($"El filtro de comparación no está definido {this}");
					InvokeFailure();
				}
			}
			else
			{
				// Si no hay resultados

				// Invoco el fallo
				InvokeFailure();
			}
		}

		private bool EvaluateDirection(float dot, AxisOption option, float marginError)
		{

			// Defino si la opción es para ambas direcciones
			if (option.Equals(AxisOption.Both))
			{
				// Si el valor absoluto del producto punto es mayor o igual a la unidad menos el margen de error
				// La dirección es paralela
				return Mathf.Abs(dot) >= unitValue - marginError;
			}
			else
			{
				// Es o positiva o negativa

				// Si la dirección es positiva, evaluo la dirección a positivo
				if (option.Equals(AxisOption.Possitive))
				{
					return dot >= unitValue - marginError;
				}
				else
				{
					// Si la posición es negativa, evaluo la dirección a negativo
					return dot <= -unitValue + marginError;
				}
			}
		}

		private Vector3 GetEvaluatedDirection(Transform target, EvaluationAxis direction)
		{
			Vector3 directionVector = Vector3.zero;

			// Defino la dirección del vector según la opción
			switch (direction)
			{
				case EvaluationAxis.Right:
					directionVector = Vector3.right;
					break;
				case EvaluationAxis.Up:
					directionVector = Vector3.up;
					break;
				case EvaluationAxis.Forward:
					directionVector = Vector3.forward;
					break;
				default:
					break;
			}

			// Retorno la dirección con la rotación objetivo
			return target.rotation * directionVector;
		}
	}
}
