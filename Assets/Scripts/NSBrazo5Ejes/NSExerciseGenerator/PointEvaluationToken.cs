﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSBrazo5Ejes.NSExerciseGenerator
{
	public class PointEvaluationToken : AbstractEvaluationToken
	{
		public PointEvaluationToken(Vector3 worldPosition)
		{
			// Defino los valores iniciales de la instancia
			WorldPosition = worldPosition;
		}

		/// <summary>
		/// Posición global en la que se evaluará
		/// </summary>
		/// <value>Vector3 con coordenadas globales</value>
		public Vector3 WorldPosition { get; set; }

		/// <summary>
		/// Radio de la evaluación
		/// </summary>
		/// <value>Flotante con el radio de la búsqueda</value>
		public float Radius { get; set; } = 0.1f;

		public float MaxDistance { get; set; } = 0f;

		/// <summary>
		/// Evalua el token con las propiedades especificadas
		/// </summary>
		public override void Evaluate()
		{
			// Evalua la posición del token dado cierto radio en busca de un objeto determinado

			// Obtengo los resultados de la prueba

			// Si existe un valor para la dirección límite

			var results = Physics.OverlapSphere(WorldPosition, Radius, CheckLayerMask, QueryTriggerInteraction.Ignore);

			// Si hay más de un resultado
			if (results.Length > 0)
			{
				// Si hay un target ID
				if (TargetEnumID != null)
				{
					// Genero una copia de la variable para usar en el lambda
					var targetEnumIDResult = TargetEnumID;

					// Por cada resultado, compruebo si existe un elemento que tenga el identificador de la propiedad
					var found = (System.Array.Find(results, result =>
					{
						// Obtengo el identificador de enumeración
						EnumIdentificator enumIdentificator = result.gameObject.GetComponent<EnumIdentificator>();

						// Si el identificador de enumeración es nulo, retorno falso
						if (enumIdentificator == null) return false;

						// Obtengo el enum ID del identificador
						var enumId = enumIdentificator.EnumID;

						// Retorno el resultado de la comparación filtro
						return enumId.Equals(targetEnumIDResult) && IsValidDistanceThreshold(result.transform.position);
					}) != null);

					// Retorno el resultado
					if (found) InvokeSuccess();
					else InvokeFailure();
				}
				else
				{
					// No existe con qué comparar
					Debug.LogWarning($"El filtro de comparación no está definido {this}");
					InvokeFailure();
				}
			}
			else
			{
				// Invoco el fallo
				InvokeFailure();
			}
		}

		protected bool IsValidDistanceThreshold(Vector3 targetPosition)
		{
			// Si el threshold es válido, realice la evaluación de distancia
			// Si el threshold no es válido, ignore esta evaluación y considerese correcta
			bool validThreshold = MaxDistance > 0f && MaxDistance < Radius;
			return validThreshold ? (targetPosition - WorldPosition).magnitude <= MaxDistance : true;
		}
	}
}
