﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSBrazo5Ejes.NSExerciseGenerator
{
	public class PileExerciseCheckDrawer : MonoBehaviour
	{
		public PileExerciseRoutine DrawingRoutine { get; set; }

		private void OnDrawGizmos()
		{
			// Defino el color de los tokens
			Gizmos.color = Color.white;

			// Si existe una referencia a la rutina
			if (DrawingRoutine != null)
			{

				// Si hay tokens de evaluación definidos
				if (DrawingRoutine.EvaluatorTokens != null)
				{
					// Obtengo la cuenta de los tokens
					int count = DrawingRoutine.EvaluatorTokens.Count;

					// Recorro cada token de evaluación
					for (int i = 0; i < count; i++)
					{
						// Obtengo el token actual
						var currentEvaluator = DrawingRoutine.EvaluatorTokens[i];

						// Dibujo los tokens en pantalla
						Gizmos.DrawWireSphere(center: currentEvaluator.WorldPosition,
																	radius: currentEvaluator.Radius);

					}
				}
			}
		}
	}
}
