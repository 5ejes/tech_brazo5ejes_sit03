﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Interfaz de evaluación
/// </summary>
public interface IEvaluator
{
	void Evaluate();
}
