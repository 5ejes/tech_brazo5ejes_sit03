﻿using System.Collections;
using System.Collections.Generic;
using NSUtilities;
using UnityEngine;

namespace NSBrazo5Ejes.NSExerciseGenerator
{
	public class CubeAreaEvaluationToken : AreaEvaluationToken
	{
		/// <summary>
		/// Define el ID requerido por los cubos como filtro de evaluación
		/// </summary>
		/// <value></value>
		public int RequiredColorID { get; set; } = -1;

		private Collider[] _castResults = new Collider[4];

		public override void Evaluate()
		{
			// Creo un area para hacer la comprobación

			// Obtengo los cuadrados resultantes del chequeo

			// TODO: POR UN MALFUNCIONAMIENTO DEL OVERLAPBOX NO SE PUEDE USAR EN LA COMPROBACIÓN DEL STAND, POR FAVOR TRATAR DE CORREGIR ESTE BUG

			// Reinicio los elementos encontrados
			CheckedElements = 0;

			Vector3 halfScale = (PositionInfo.localScale / 2f).ToAbsolute();
			var count = Physics.OverlapBoxNonAlloc(PositionInfo.position, halfScale, _castResults, PositionInfo.rotation, CheckLayerMask, QueryTriggerInteraction.Collide);

			Vector3 max = halfScale + PositionInfo.position;
			Vector3 min = PositionInfo.position - halfScale;

			Vector3 maxB = max;
			Vector3 minB = min;

			maxB.z = min.z;
			minB.z = max.z;

			Debug.DrawRay(PositionInfo.position, Vector3.one.normalized * 0.2f, Color.white, 2f);
			Debug.DrawLine(min, max, Color.green, 2f);
			Debug.DrawLine(minB, maxB, Color.green, 2f);

			// Si hay al menos un resultado
			if (count > 0)
			{

				// Si existe un CheckEnumID
				if (TargetEnumID != null)
				{
					// Inicio la comprobación de elementos

					// Creo una lista de resultados temporales
					var tempResults = new List<Transform>();

					// Recorro cada resultado
					for (int i = 0; i < count; i++)
					{
						var result = _castResults[i];

						Debug.DrawRay(result.transform.position, Vector3.one.normalized * 0.05f, Color.cyan, 2f);
						// Obtengo el identificador de enumeración
						var enumIdentificator = result.transform.GetComponent<EnumIdentificator>();

						// Si el identificador no es nulo
						if (enumIdentificator != null)
						{
							// Si la enumeración del identificador y la enumeración objetivo coinciden
							if (enumIdentificator.EnumID.Equals(TargetEnumID))
							{
								// Obtengo el componente cubo del resultado
								var cube = result.transform.GetComponent<Cube>();

								// Si existe el componente cubo
								if (cube)
								{

									// Defino una variable de control
									var isValid = false;

									// Si el Color ID requerido es válido
									if (RequiredColorID >= 0)
									{
										// Evaluo el ColorID requerido y del cubo
										if (cube.ColorID.Equals(RequiredColorID))
										{
											// Marco como válido
											isValid = true;
										}
									}
									else
									{
										// El color ID no está definido

										// Marco como válido
										isValid = true;
									}

									// Si es válido
									if (isValid)
										// Agrego la entrada a los resultados
										tempResults.Add(result.transform);
								}
							}
						}
					}

					// Defino el resultado de los elementos encontrados
					CheckedElements = tempResults.Count;
				}
				else
				{
					// Si no existe

					// Defino el número de elementos comprobados a los elementos actuales
					CheckedElements = count;
				}
			}

			// Si se cumple el número objetivo de elementos a detectar
			if (CheckedElements > CheckedElementGoal)
			{
				// Invoco la función de exito
				InvokeSuccess();
			}
			else
			{
				// Invoco la función de fallo
				InvokeFailure();
			}
		}
	}
}
