﻿using System;
using System.Collections;
using System.Collections.Generic;
using NSUtilities;
using UnityEngine;

namespace NSBrazo5Ejes.NSExerciseGenerator
{
	public class RadialAreaEvaluationToken : AreaEvaluationToken
	{
		/// <summary>
		/// Define el ID requerido por los cubos como filtro de evaluación
		/// </summary>
		/// <value></value>
		public int RequiredColorID { get; set; } = -1;

		public float CheckRadius { get; set; } = 0f;

		public Func<Cube, bool> SpecialRequeriments { get; set; } = null;

		private Collider[] _castResults = new Collider[4];

		public override void Evaluate()
		{
			// Creo un area para hacer la comprobación

			if (CheckRadius < 0)
			{
				Debug.LogError("Invalid Radius on evaluation token");
				return;
			}

			// Obtengo los cuadrados resultantes del chequeo

			// Reinicio los elementos de comprobación
			CheckedElements = 0;

			// Compruebo los elementos que interactuan con el token de evaluación
			int count = Physics.OverlapSphereNonAlloc(PositionInfo.position, CheckRadius, _castResults, CheckLayerMask, QueryTriggerInteraction.Collide);

			// Si hay al menos un resultado
			if (count > 0)
			{

				// Si existe un CheckEnumID
				if (TargetEnumID != null)
				{
					// Inicio la comprobación de elementos

					// Creo una lista de resultados temporales
					var tempResults = new List<Transform>();

					// Recorro cada resultado
					for (int i = 0; i < count; i++)
					{
						var result = _castResults[i];

						Debug.DrawRay(result.transform.position, Vector3.one.normalized * 0.05f, Color.cyan, 2f);
						// Obtengo el identificador de enumeración
						var enumIdentificator = result.transform.GetComponent<EnumIdentificator>();

						// Si el identificador no es nulo
						if (enumIdentificator != null)
						{
							// Si la enumeración del identificador y la enumeración objetivo coinciden
							if (enumIdentificator.EnumID.Equals(TargetEnumID))
							{
								// Obtengo el componente cubo del resultado
								var cube = result.transform.GetComponent<Cube>();

								// Si existe el componente cubo
								if (cube)
								{

									// Defino una variable de control
									var isValid = false;

									// Si el Color ID requerido es válido
									if (RequiredColorID >= 0)
									{
										// Evaluo el ColorID requerido y del cubo
										if (cube.ColorID.Equals(RequiredColorID))
										{

											// Si los requerimientos especiales están definidos
											if (SpecialRequeriments != null)
											{
												// Evalúo los requerimientos
												isValid = SpecialRequeriments.Invoke(cube);
											}
											else
											{
												// Marco como válido
												isValid = true;
											}
										}
									}
									else
									{
										// El color ID no está definido

										// Si los requerimientos especiales están definidos
										if (SpecialRequeriments != null)
										{
											// Evalúo los requerimientos
											isValid = SpecialRequeriments.Invoke(cube);
										}
										else
										{
											// Marco como válido
											isValid = true;
										}
									}

									// Si es válido
									if (isValid)
										// Agrego la entrada a los resultados
										tempResults.Add(result.transform);
								}
							}
						}
					}

					// Defino el resultado de los elementos encontrados
					CheckedElements = tempResults.Count;
				}
				else
				{
					// Si no existe

					// Defino el número de elementos comprobados a los elementos actuales
					CheckedElements = count;
				}
			}

			// Si se cumple el número objetivo de elementos a detectar
			if (CheckedElements > CheckedElementGoal)
			{
				// Invoco la función de exito
				InvokeSuccess();
			}
			else
			{
				// Invoco la función de fallo
				InvokeFailure();
			}
		}
	}
}
