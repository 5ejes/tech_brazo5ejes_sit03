﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace NSBrazo5Ejes.NSExerciseGenerator
{
	[System.Serializable]
	public class PileExerciseEvent : UnityEvent<PileExerciseRoutine> { }
}