﻿using Modular;
using NSBrazo5Ejes;
using NSUtilities;
using UnityEngine;
using UnityEngine.Events;

public class ArmHorizontalModeValidationChecker : MonoBehaviour
{
	[Header("References")]
	[SerializeField] private ArmPositionHandler _positionHandler;

	[Header("Events")]
	[SerializeField] private BoolEvent _onValidTransition;

	private static readonly float _limitHeight = 0.3f;

	public void ValidateHorizontalTransition(bool transitionValue)
	{
		if (_positionHandler == null)
		{
			Debug.LogError($"The ArmPositionHandler Reference is null : {this}", this);
			return;
		}

		// Todas las posiciones ahora son válidas ya que el brazo puede llegar a toda la mesa en horizontal
		_onValidTransition?.Invoke(transitionValue);

	}
}
