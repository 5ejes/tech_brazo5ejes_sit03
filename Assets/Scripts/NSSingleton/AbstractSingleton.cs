﻿#pragma warning disable 0649
using System.Collections.Generic;
using UnityEngine;

namespace NSSingleton
{
    public abstract class AbstractSingleton<T> : MonoBehaviour where T : AbstractSingleton<T>
    {
        protected static T instance;

        public static T Instance
        {
            get
            {
                if (instance == null)
                {
                    var tmpObjectsOfType = FindInstanceInScene();

                    if (tmpObjectsOfType != null)
                        instance = tmpObjectsOfType;
                    else
                        instance = (new GameObject(typeof(T).ToString())).AddComponent<T>();
                }

                return instance;
            }
        }

        private static T FindInstanceInScene()
        {
            var tmpObjectsInScene = new List<T>();

            foreach (var tmpObjectFinded in Resources.FindObjectsOfTypeAll<T>())
            {
                if (tmpObjectFinded.hideFlags == HideFlags.NotEditable || tmpObjectFinded.hideFlags == HideFlags.HideAndDontSave)
                    continue;

                tmpObjectsInScene.Add(tmpObjectFinded.GetComponent<T>());
            }

            return tmpObjectsInScene.Count > 0 ? tmpObjectsInScene[0] : null;
        }
        
        public static void DestroySingleton()
        {
            Destroy(instance.gameObject);
        }

        public static void CreateInstance()
        {
            DontDestroyOnLoad(Instance.gameObject);            
        }
    }
}