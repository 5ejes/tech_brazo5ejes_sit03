﻿#pragma warning disable 0649
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace NSBoxMessage
{
    #region delegates

    public delegate void delegateOnBoxMessageButtonAccept();

    public delegate void delegateOnBoxMessageButtonCancel();

    #endregion

    public abstract class AbstractBoxMessage : MonoBehaviour
    {
        #region members

        private TextMeshProUGUI textMessage;

        protected Button buttonAccept;

        protected Button buttonCancel;

        private float timeForDestroy;

        private TextMeshProUGUI textButtonAccept;

        private TextMeshProUGUI textButtonCancel;

        [SerializeField, Header("Panel fondo oscuro")]
        private GameObject prefabPanelImagenFondoVentanaObscura;

        private GameObject panelImagenFondoActivo;

        #endregion

        #region couInstances

        private IEnumerator couInstanceDestroy;

        #endregion

        #region accesors

        public float _timeForDestroy
        {
            set
            {
                timeForDestroy = value;

                if (timeForDestroy == -1)
                    return;

                couInstanceDestroy = couDestroy(timeForDestroy);
                StartCoroutine(couInstanceDestroy);
            }
        }

        public string _textMessage
        {
            set
            {
                if (textMessage == null)
                    textMessage = transform.Find("ImageFondoTexto").GetChild(0).GetComponent<TextMeshProUGUI>();

                textMessage.text = value;
            }
        }

        public string _textButtonAccept
        {
            set { textButtonAccept.text = value; }
        }

        public string _textButtonCancel
        {
            set { textButtonCancel.text = value; }
        }

        #endregion

        #region delegatesInstance

        public delegateOnBoxMessageButtonAccept dltOnButtonAccept;

        public delegateOnBoxMessageButtonCancel dltOnButtonCancel;

        #endregion

        #region methods

        protected void Awake()
        {
            var tempButtonAccept = transform.Find("ButtonAccept");

            if (tempButtonAccept != null)
            {
                buttonAccept = tempButtonAccept.GetComponent<Button>();
                buttonAccept.onClick.AddListener(OnButtonAccept);
                textButtonAccept = tempButtonAccept.GetChild(0).GetComponent<TextMeshProUGUI>();
            }

            var tempButtonCancel = transform.Find("ButtonCancel");

            if (tempButtonCancel != null)
            {
                buttonCancel = tempButtonCancel.GetComponent<Button>();
                buttonCancel.onClick.AddListener(OnButtonCancel);
                textButtonCancel = tempButtonCancel.GetChild(0).GetComponent<TextMeshProUGUI>();
            }

            panelImagenFondoActivo = Instantiate(prefabPanelImagenFondoVentanaObscura, transform.parent);
            panelImagenFondoActivo.GetComponent<Transform>().SetSiblingIndex(transform.GetSiblingIndex());
        }

        private void OnButtonAccept()
        {
            if (dltOnButtonAccept != null)
                dltOnButtonAccept();

            Destroy(gameObject);

            if (panelImagenFondoActivo)
                Destroy(panelImagenFondoActivo);
        }

        private void OnButtonCancel()
        {
            if (dltOnButtonCancel != null)
                dltOnButtonCancel();

            Destroy(gameObject);

            if (panelImagenFondoActivo)
                Destroy(panelImagenFondoActivo);
        }

        /// <summary>
        /// Add a method that is executed when button Accept is pressed
        /// </summary>
        /// <param name="argDelegate">function signature</param>
        public void mtdAddDelegateButtonAccept(delegateOnBoxMessageButtonAccept argDelegate)
        {
            dltOnButtonAccept += argDelegate;
        }

        /// <summary>
        /// Add a method that is Execute when button Cancel is pressed
        /// </summary>
        /// <param name="argDelegate">function signature</param>
        public void mtdAddDelegateButtonCancel(delegateOnBoxMessageButtonCancel argDelegate)
        {
            dltOnButtonCancel += argDelegate;
        }

        /// <summary>
        /// Cuando se preciona el boton cerrar
        /// </summary>
        public void OnButtonCerrar()
        {
            Destroy(gameObject);

            if (panelImagenFondoActivo)
                Destroy(panelImagenFondoActivo);
        }

        #endregion

        #region courutines

        private IEnumerator couDestroy(float argTimeForDestroy)
        {
            yield return new WaitForSeconds(argTimeForDestroy);
            Destroy(gameObject);
        }

        #endregion
    }
}