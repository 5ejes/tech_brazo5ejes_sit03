﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace NSUtilities
{
	public abstract class GenericEditorTargetBase<T> : Editor where T : Object
	{
		private T _editorTarget;

		protected T EditorTarget
		{
			get
			{
				if (_editorTarget == null) _editorTarget = (T)target;
				return _editorTarget;
			}
			set => _editorTarget = value;
		}
	}
}
