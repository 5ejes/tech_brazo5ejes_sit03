﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NSInterfaz;

public class ActivarBienvenida : MonoBehaviour
{
    [SerializeField]
    private PanelBienvenida panel;

    // Start is called before the first frame update
    void Start()
    {
        panel.ShowPanel(true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
