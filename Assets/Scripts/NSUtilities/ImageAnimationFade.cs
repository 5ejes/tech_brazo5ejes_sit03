﻿#pragma warning disable 0649
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace NSUtilities
{
    public class ImageAnimationFade : MonoBehaviour
    {
        #region members

        private Image imageAnimation;

        [SerializeField] private float animationVelocity;

        [SerializeField] private float animationUmbral;

        #endregion

        private void Awake()
        {
            imageAnimation = GetComponent<Image>();
        }

        private void OnEnable()
        {
            PlayAnimation();
        }

        public void PlayAnimation(bool argPlayAnimation = true)
        {
            if (argPlayAnimation)
                StartCoroutine(CouButtonAnimation());
            else
                StopAllCoroutines();
        }

        private IEnumerator CouButtonAnimation()
        {
            var tmpAngle = 0f;

            while (true)
            {
                tmpAngle += 0.5f * animationVelocity;
                tmpAngle %= 180f;
                imageAnimation.color = new Color(1, 1, 1, Mathf.Sin(tmpAngle * Mathf.Deg2Rad) * animationUmbral);
                yield return null;
            }
        }
    }
}