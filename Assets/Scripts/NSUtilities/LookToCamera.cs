﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSUtilities
{
    
    public class LookToCamera : MonoBehaviour
    {
				[Header("Camera Reference")]
        [SerializeField] private Camera refMainCamera;

        // Update is called once per frame
        void Update()
        {
            LookToCameraExecute();
        }

        private void LookToCameraExecute()
        {
            if (!refMainCamera)
                refMainCamera = Camera.main;

            var tmpDirection = refMainCamera.transform.position - transform.position;
            tmpDirection[1] = 0;
            transform.rotation = Quaternion.LookRotation(tmpDirection);
        }
    }
}

