﻿#pragma warning disable 0649
using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace NSUtilities
{
    public class DraggableObjectUI : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        #region members

        private RectTransform rectTransform;

        private RectTransform rectTransformPuntoMedicionVoltajeActual;

        private Vector3 posicionPuntero;

        private Vector3 posicionInicial;

        [SerializeField] private string tagPuntoAnclaje;

        [SerializeField] private float minDistanceToPoint = 10;

        private GameObject[] puntosMedicionCerca;

        public Action<GameObject> DelegatePuntoAnclajeCerca = delegate(GameObject argPuntoAnclajeCerca) { };

        public Action DelegateDesanclajePunto = delegate() { };

        public Action DelegateOnDragFinish = delegate() { };

        private bool notificarNuevoPuntoAnclaje = true;

        private bool canBeDragged = true;

        [SerializeField] private GameObject[] zonasMedicion;

        [Header("Events")] public UnityEvent OnBeginDragExecute;

        public UnityEvent OnDragExecute;

        public UnityEvent OnEndDragExecute;

        #endregion

        #region accesors

        public bool CanBeDragged
        {
            set { canBeDragged = value; }
            get { return canBeDragged; }
        }

        #endregion

        private void Awake()
        {
            rectTransform = gameObject.GetComponent<RectTransform>();
            posicionInicial = rectTransform.position;
            Debug.Log("posicionInicial : " + posicionInicial);
            posicionPuntero = posicionInicial;
        }

        private void Update()
        {
            MoverseHaciaPuntoMedicionVoltaje();
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            OnBeginDragExecute.Invoke();

            if (CanBeDragged)
            {
                ActivarZonasMedicion();
                puntosMedicionCerca = GameObject.FindGameObjectsWithTag(tagPuntoAnclaje);
            }
        }

        public void OnDrag(PointerEventData eventData)
        {
            OnDragExecute.Invoke();

            if (CanBeDragged)
            {
                rectTransformPuntoMedicionVoltajeActual = RevisarPuntoCerca(eventData.position);
                posicionPuntero = eventData.position;
            }
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            OnEndDragExecute.Invoke();

            if (CanBeDragged)
            {
                posicionPuntero = posicionInicial;
                DelegateOnDragFinish();
                ActivarZonasMedicion(false);
            }
        }

        public void ResetPositionToInit()
        {
            posicionPuntero = posicionInicial;
            rectTransform.position = posicionPuntero;
            rectTransformPuntoMedicionVoltajeActual = null;
            DelegateOnDragFinish();
            ActivarZonasMedicion(false);
        }

        private void MoverseHaciaPuntoMedicionVoltaje()
        {
            if (rectTransformPuntoMedicionVoltajeActual)
            {
                rectTransform.position = Vector2.Lerp(rectTransform.position, rectTransformPuntoMedicionVoltajeActual.position, 0.2f);

                if (notificarNuevoPuntoAnclaje)
                {
                    DelegatePuntoAnclajeCerca(rectTransformPuntoMedicionVoltajeActual.gameObject);
                    notificarNuevoPuntoAnclaje = false;
                }

                Debug.Log("MoverseHaciaPuntoMedicionVoltaje 1");
            }
            else
            {
                rectTransform.position = Vector2.Lerp(rectTransform.position, posicionPuntero, 0.5f);

                if (!notificarNuevoPuntoAnclaje)
                {
                    DelegateDesanclajePunto();
                    notificarNuevoPuntoAnclaje = true;
                }

                Debug.Log("MoverseHaciaPuntoMedicionVoltaje 2 :");
            }
        }

        private RectTransform RevisarPuntoCerca(Vector2 argPosition)
        {
            foreach (var tmpPoint in puntosMedicionCerca)
            {
                var tmpPointRectTransform = tmpPoint.GetComponent<RectTransform>();

                if (Vector2.Distance(argPosition, tmpPointRectTransform.position) <= minDistanceToPoint)
                    return tmpPointRectTransform;
            }

            return null;
        }

        private void ActivarZonasMedicion(bool argActivar = true)
        {
            foreach (var tmpZonaMedicion in zonasMedicion)
                if (tmpZonaMedicion)
                    tmpZonaMedicion.SetActive(argActivar);
        }
    }
}