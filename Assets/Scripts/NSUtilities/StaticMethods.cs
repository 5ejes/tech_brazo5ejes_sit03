﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSUtilities
{
	public static class StaticMethods
	{

		/// <summary>
		/// Obtiene el índice del valor máximo sobre un arreglo de flotantes
		/// </summary>
		/// <param name="args">Arreglo de flotantes a evaluar</param>
		/// <returns>Entero con el índice del valor máximo</returns>
		public static int GetMaxIndex(params float[] args)
		{
			// Creo un índice de salida
			int output = -1;

			// Creo la variable de valor máximo
			var maxValue = float.MinValue;

			int length = args.Length;

			// Por cada elemento
			for (int argIndex = 0; argIndex < length; argIndex++)
			{
				// Obtengo el valor actual
				var currentValue = args[argIndex];

				// Si el valor actual es mayor al mayor almacenado
				if (currentValue > maxValue)
				{
					// Actualizo el índice de salida y el valor máximo
					output = argIndex;
					maxValue = currentValue;
				}
			}

			// Retorno el índice de salida
			return output;
		}

		/// <summary>
		/// Obtiene el índice del valor mínimo sobre un arreglo de flotantes
		/// </summary>
		/// <param name="args">Arreglo de flotantes a evaluar</param>
		/// <returns>Entero con el índice del valor mínimo</returns>
		public static int GetMinIndex(params float[] args)
		{
			// Creo un índice de salida
			int output = -1;

			// Creo la variable de valor máximo
			var maxValue = float.MinValue;

			int length = args.Length;

			// Por cada elemento
			for (int argIndex = 0; argIndex < length; argIndex++)
			{
				// Obtengo el valor actual
				var currentValue = args[argIndex];

				// Si el valor actual es mayor al mayor almacenado
				if (currentValue > maxValue)
				{
					// Actualizo el índice de salida y el valor máximo
					output = argIndex;
					maxValue = currentValue;
				}
			}

			// Retorno el índice de salida
			return output;
		}

		/// <summary>
		/// Rota un punto a través de un pivote
		/// </summary>
		/// <param name="point">Punto a rotar</param>
		/// <param name="pivot">Pivote desde el cual rotar</param>
		/// <param name="angles">Grados los cuales rotar</param>
		/// <returns>El punto inicial aplicada la rotación a través del pivote</returns>
		public static Vector3 RotatePointAroundPivot(Vector3 point, Vector3 pivot, Vector3 angles)
		{
			return RotatePointAroundPivot(point, pivot, Quaternion.Euler(angles));
		}

		/// <summary>
		/// Rota un punto a través de un pivote
		/// </summary>
		/// <param name="point">Punto a rotar</param>
		/// <param name="pivot">Pivote desde el cual rotar</param>
		/// <param name="angles">Grados los cuales rotar</param>
		/// <returns>El punto inicial aplicada la rotación a través del pivote</returns>
		public static Vector3 RotatePointAroundPivot(Vector3 point, Vector3 pivot, Quaternion rotation)
		{
			return rotation * (point - pivot) + pivot;
		}
	}
}
