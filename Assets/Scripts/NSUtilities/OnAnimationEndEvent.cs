﻿#pragma warning disable 0649
using UnityEngine;
using UnityEngine.Events;

namespace NSUtilities
{
    public class OnAnimationEndEvent : MonoBehaviour
    {
        public UnityEvent OnAnimationEnd;

        public void TriggerAnimationEnd()
        {
            OnAnimationEnd.Invoke();
        }
    } 
}
