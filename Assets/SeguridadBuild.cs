﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SeguridadBuild : MonoBehaviour
{
    [SerializeField] public NSSeguridad.SOSecurityConfiguration laseguridad;

    public NSTraduccionIdiomas.IdiomsList idiomActual_init;
    public NSInitSimulator.InitSimulator init_Link;


    // Start is called before the first frame update
    void Start()
    {
        init_Link = GameObject.Find("InitSimulador").GetComponent<NSInitSimulator.InitSimulator>();
        init_Link.actualIdiom_init = idiomActual_init;
    }

    // Update is called once per frame
    void Update()
    {

    }
}

