﻿using System;
using NSArm3Axis;
using UnityEngine;

public class RotationBehaviourTest : MonoBehaviour
{
  [SerializeField] private Transform _gripTransform;
  [SerializeField] private Transform _cube;
  [SerializeField] private bool _gripSimulation;
  [Space]
  [SerializeField] private float _snap = 90f;
  [SerializeField] private float _threshold = 45f;
  [Space]
  [SerializeField] private float _cubeAngleOffset;
  private float _targetOffset;

  private AbstractCollectableObject CurrentGrab { get; set; }
  private Quaternion _initialGripRotation;
  private Quaternion _cubeInitialRotation;

  private bool _previousGripValue;

  private void Start()
  {
    CurrentGrab = _cube.GetComponent<Cube>();
  }

  private void LateUpdate()
  {

    // Obtengo el Transform objetivo
    Transform target = CurrentGrab.Target;

    // Esto se realiza al iniciar el agarre, en este caso es si cambia la garra de estado
    if (_previousGripValue != _gripSimulation)
    {
      // Defino el valor de control de simulación
      _previousGripValue = _gripSimulation;
      if (_gripSimulation)
      {
        // NOTA: Esto se ejecuta al iniciar el agarre con la grip
        // El script emula este comportamiento
        #region At Grab Start

        // Reinicio el valor anterior de offset
        _cubeAngleOffset = 0f;

        // Defino las rotaciones iniciales
        _initialGripRotation = _gripTransform.rotation;
        _cubeInitialRotation = _cube.rotation;

        _targetOffset = GetTargetOffsetAngle(target, _gripTransform);

        #endregion

        // Debug.DrawRay(_gripTransform.position, targetEulerDir, Color.white);
        // Debug.DrawRay(_gripTransform.position, cubeCurrentDir, Color.red);
      }
    }

    // NOTA: Esta sección del script se ejecuta en el LateUpdate, es ejecución normal de la rutina
    if (!_gripSimulation) return;

    #region Late Update Processing

    // Muevo el offset hacia el target según el delta dado
    _cubeAngleOffset = Mathf.MoveTowards(_cubeAngleOffset, _targetOffset, Time.deltaTime * 45f);

    // Aplico el offset creando un quaternion del eje y con los ángulos procesados
    Quaternion offset = Quaternion.identity;
    offset = Quaternion.AngleAxis(_cubeAngleOffset, Vector3.up);

    // INICIO DE APLICACIÖN DE LAS ROTACIONES

    // Calculo la diferencia de movimiento del grip para simular la rotación del grip
    var difference = _gripTransform.rotation * Quaternion.Inverse(_initialGripRotation);

    // Aplico todas las rotaciones en este orden
    // Rotación del grip
    // Rotación del ajuste
    // Y la rotación inicial del cubo
    target.rotation = difference * offset * _cubeInitialRotation;

    #endregion
  }

  private float GetTargetOffsetAngle(Transform target, Transform grip)
  {
    // Aplico el offset según la rotación de la grip
    float offsetAngle = grip.eulerAngles.y;

    // Obtengo los angulos en euler
    Vector3 targetEulerAngles = target.eulerAngles;

    // Calculo la dirección del objeto con el Vector forward
    var cubeCurrentDir = GetForwardFlattenDir(targetEulerAngles);

    // Hallo el snap y le aplico el offset
    float roundWeight = _threshold / _snap;
    targetEulerAngles.y = SnapTo(targetEulerAngles.y - offsetAngle, _snap, roundWeight) + offsetAngle;

    // Hago una copia del ángulo del cubo
    var targetEulerDir = GetForwardFlattenDir(targetEulerAngles);

    // Calculo el ángulo entre el ángulo actual del target y el ángulo objetivo
    // Con el Vector up
    float cubeAngle = GetGrabAutoAdjustmentAngle(targetEulerDir, cubeCurrentDir);

    // Aplico el offset dando el ángulo negativo
    return -cubeAngle;
  }

  private Vector3 GetForwardFlattenDir(Vector3 targetEulerAngles)
  {
    Vector3 output = Quaternion.Euler(targetEulerAngles) * Vector3.forward;
    // Aplano en el eje y
    output.y = 0f;
    output.Normalize();

    return output;
  }
  private float SnapTo(float value, float snap, float roundWeight)
  {
    return WeightedRound(value / snap, roundWeight) * snap;
  }

  private float WeightedRound(float value, float weight)
  {
    float prevValue = Mathf.Floor(value);
    float nextValue = prevValue + 1;

    float weightValue = Mathf.Lerp(prevValue, nextValue, 1 - weight);
    return value <= weightValue ? prevValue : nextValue;
  }

  private float GetGrabAutoAdjustmentAngle(Vector3 adjustmentTargetDir, Vector3 cubeDir)
  {
    return Vector3.SignedAngle(adjustmentTargetDir, cubeDir, Vector3.up);
  }

  private bool IsHorizontal(Transform target)
  {
    return Mathf.Abs(Vector3.Dot(target.rotation * Vector3.up, Vector3.up)) < 0.05f;
  }

}
