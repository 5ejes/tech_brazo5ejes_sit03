﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawPointsInspector : MonoBehaviour
{
	[SerializeField] private List<Vector3> _pointList = new List<Vector3>();

	public void AddPoints(params Vector3[] points)
	{
		// Agrego los puntos a la lista
		_pointList.AddRange(points);
	}

	public void RemoveClosestPoint(Vector3 point, float minDistance, bool removeAllOccurrencies = false)
	{
		// Si existe la lista
		if (_pointList != null)
		{
			// Si hay más de un punto
			if (_pointList.Count > 0)
			{
				// Por cada punto en la lista
				for (int i = 0; i < _pointList.Count; i++)
				{
					// Obtengo el punto de la lista
					var currentPoint = _pointList[i];

					// Obtengo la distancia entre los dos puntos
					float distance = Vector3.Distance(currentPoint, point);

					// Si la distancia es menor a la distancia mínima definida
					if (distance < minDistance)
					{
						// Remuevo el índice
						_pointList.RemoveAt(i);

						// Si remuevo todas las ocurrencias
						if (!removeAllOccurrencies)
							// Salgo de la función
							return;
					}
				}
			}
		}
	}

	public void ClearPointList()
	{
		// Limpio la lista de puntos
		_pointList.Clear();
	}

	public void OnDrawGizmosSelected()
	{
		// Por cada punto de la lista
		if (_pointList != null)
		{
			// Si hay al menos un elemento
			if (_pointList.Count > 0)
			{
				int length = _pointList.Count;
				// Por cada punto
				for (int i = 0; i < length; i++)
				{
					// Obtengo cada punto
					var currentPosition = _pointList[i];

					// Dibujo cada punto
					Gizmos.DrawSphere(currentPosition, 0.02f);
				}
			}
		}
	}
}
