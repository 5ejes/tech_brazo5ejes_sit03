﻿using NSArm3Axis;
using UnityEngine;

public class ArmGripRotationTest : MonoBehaviour
{
	[SerializeField] private ArmGripController _gripController;
	[SerializeField] private AbstractCollectableObject _collectable;

	public void OnEnable()
	{
		if(_gripController == null) return;
		if(_collectable == null) return;

		_gripController.CurrentGrab = _collectable;
	}
}
