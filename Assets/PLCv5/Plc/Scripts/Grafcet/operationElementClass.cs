﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class operationElementClass : MonoBehaviour {

	public GameObject _situation;

	public Text _title;
	public Button _editBt;
	public Button _deleteBt;

	public int _type = 0; //0: value, 1: operator, 2:(, 3:)
	public int _id = 0;

	public int _editorType = 2; //2:value, 4:operator, 5:(, 6:)
	public int _typeValue = 0; //1:inputs, 2:outputs, 3:timers, 4:counters, 5:comparator
	public int _index = 0;
	public string _value = "";
	public int _index2 = 0;
	public string _value2 = "";
	public int _index3 = 0;
	public string _value3 = "";
	public bool _isInverse = false;
	public bool _constant1 = false;
	public bool _constant2 = false;

	private bool _menuActive = false;
	// Use this for initialization
	void Start () {
		_title.font = (Font)Resources.Load ("Fonts/ArialBold28");

		_situation = GameObject.Find ("Canvas/Grafcet");

		_editBt.transform.localScale = new Vector3 (0,0,0);
		_deleteBt.transform.localScale = new Vector3 (0,0,0);

		gameObject.GetComponent<Button> ().onClick.AddListener (toggleButtonsAction);

		_editBt.onClick.AddListener (editAction);
		_deleteBt.onClick.AddListener (deleteAction);
	}

	private void toggleButtonsAction(){
		if (_menuActive == false) {
			_menuActive = true;
			iTween.ScaleTo (_deleteBt.gameObject, new Vector3 (0.6f, 0.6f, 0.6f), 0.3f);
			iTween.ScaleTo (_editBt.gameObject, new Vector3 (0.6f, 0.6f, 0.6f), 0.3f);
		} else {
			closeEditElements ();
		}
	}

	private void editAction(){
		
		_situation.GetComponent<GrafcetMainClass> ()._editorPanel.transform.SetParent (GameObject.Find ("Canvas").transform, true);
		_situation.GetComponent<GrafcetMainClass> ()._editorPanel.GetComponent<editorPanelClass> ().configureEditor (gameObject, "transition", "operation");
		_situation.GetComponent<GrafcetMainClass> ()._editorTransPanel.GetComponent<editorTransPanel> ().showOverElement ();

		iTween.MoveTo(
			_situation.GetComponent<GrafcetMainClass> ()._editorPanel,
			iTween.Hash(
				"position", new Vector3(0,0,0),
				"looktarget", Camera.main,
				"easeType", iTween.EaseType.easeOutExpo,
				"time", 1f,
				"islocal",true
			)
		);
	}

	public void openEditPostAction(){
		Invoke ("editAction", 0.2f);
	}

	private void deleteAction(){
		for(int i = _id + 1; i < _situation.GetComponent<GrafcetMainClass> ()._editorTransPanel.GetComponent<editorTransPanel> ().operatorsCount; i++ ){
			GameObject _element = GameObject.Find ("operator_" + i);
			if (_element.GetComponent<operationElementClass> () != null) {
				//is operation
				_element.GetComponent<operationElementClass> ()._id--;
				_element.name = "operator_" + _element.GetComponent<operationElementClass> ()._id;
			} else if(_element.GetComponent<conditionElementClass> () != null){
				//is condition
				_element.GetComponent<conditionElementClass> ()._id--;
				_element.name = "operator_" + _element.GetComponent<conditionElementClass> ()._id;
			} else if(_element.GetComponent<groupElementClass> () != null){
				//is group
				_element.GetComponent<groupElementClass> ()._id--;
				_element.name = "operator_" + _element.GetComponent<groupElementClass> ()._id;
			}
		}

		_situation.GetComponent<GrafcetMainClass> ()._editorTransPanel.GetComponent<editorTransPanel> ().operatorsCount--;

		Destroy (gameObject);
	}

	public void closeEditElements(){
		iTween.ScaleTo (_deleteBt.gameObject, new Vector3 (0f, 0f, 0f), 0.3f);
		iTween.ScaleTo (_editBt.gameObject, new Vector3 (0f, 0f, 0f), 0.3f);
		_menuActive = false;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
