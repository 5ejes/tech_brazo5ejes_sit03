﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ConvergenceClass : MonoBehaviour {

	public GameObject _situation;

	public bool _active = false;
	public int _id = 0;
	public bool _onSim = false;
	public string _type = "AND"; //AND, OR
	public int _editorType = 3;
	public int _branches = 2;
	public int countConnected = 0;
	public GameObject _ANDBg;
	public GameObject _ORBg;

	//public Image _topConnector;
	public Image _bottomConnector;
	public Button _deleteBt;
	public Button _editBt;
	public Button _deleteBottomBt;

	public List<GameObject> _topConnectors;
	public List<float> _widthTopConnectors;
	public List<float> _yTopConnectors;

	public bool dragEnabled = true;
	private bool objectDragged;
	private Vector3 initialPosition;
	private Vector3 startPosition;
	private int initialIndex;
	private int startIndex;

	private EventTrigger.Entry entry_0;
	private EventTrigger.Entry entry_1;
	private EventTrigger.Entry entry_2;
	private EventTrigger.Entry entry_3;

	public bool _menuActive = false;

	private float x_offset = 0;
	private float y_offset = 0;

	private bool _mouseOver = false;

	// Use this for initialization
	void Start () {
		_situation = GameObject.Find ("Canvas/Grafcet");

		initialPosition = gameObject.transform.position;

		_bottomConnector.enabled = false;

		_deleteBt.gameObject.transform.localScale = new Vector3 (0, 0, 0);
		_editBt.gameObject.transform.localScale = new Vector3 (0, 0, 0);
		_deleteBottomBt.gameObject.transform.localScale = new Vector3 (0, 0, 0);

		_deleteBt.onClick.AddListener (deleteAction);
		_editBt.onClick.AddListener (editAction);
		_deleteBottomBt.onClick.AddListener (deleteBottomAction);

		gameObject.AddComponent<EventTrigger> ();

		EventTrigger trigger_Object = gameObject.GetComponent<EventTrigger>();

		entry_0 = new EventTrigger.Entry();
		entry_0.eventID = EventTriggerType.BeginDrag;
		entry_0.callback.AddListener((data) => { BeginDrag_((PointerEventData)data); });

		entry_1 = new EventTrigger.Entry();
		entry_1.eventID = EventTriggerType.Drag;
		entry_1.callback.AddListener((data) => { ObjectDrag_((PointerEventData)data); });

		entry_2 = new EventTrigger.Entry();
		entry_2.eventID = EventTriggerType.EndDrag;
		entry_2.callback.AddListener((data) => { ObjectDrop_((PointerEventData)data); });

		EventTrigger.Entry entry_4 = new EventTrigger.Entry();
		entry_4.eventID = EventTriggerType.PointerEnter;
		entry_4.callback.AddListener((data) => { ObjectMouseOver_((PointerEventData)data); });

		EventTrigger.Entry entry_5 = new EventTrigger.Entry();
		entry_5.eventID = EventTriggerType.PointerExit;
		entry_5.callback.AddListener((data) => { ObjectMouseOut_((PointerEventData)data); });

		trigger_Object.triggers.Add(entry_0);
		trigger_Object.triggers.Add(entry_1);
		trigger_Object.triggers.Add(entry_2);
		trigger_Object.triggers.Add(entry_4);
		trigger_Object.triggers.Add(entry_5);

		gameObject.name = "convergence_base";
	}

	public void deleteAction(){
		for(int i = this._id + 1; i < _situation.GetComponent<GrafcetMainClass> ().convergenceModules; i++){
			GameObject _element = GameObject.Find ("convergence_" + i.ToString());
			_element.GetComponent<ConvergenceClass> ().updateNameAndId (i - 1);
		}
		_situation.GetComponent<GrafcetMainClass> ().convergenceModules--;

		for(int i = 0; i < _topConnectors.Count; i++){
			deleteTopAction (_topConnectors [i]);
		}
		Destroy (gameObject);
	}

	private void editAction(){
		_situation.GetComponent<GrafcetMainClass> ().activateBackground ();
		_situation.GetComponent<GrafcetMainClass> ()._editorPanel.transform.SetParent (GameObject.Find ("Canvas").transform, true);
		_situation.GetComponent<GrafcetMainClass> ()._editorPanel.GetComponent<editorPanelClass> ().configureEditor (gameObject, "convergence");

		iTween.MoveTo(
			_situation.GetComponent<GrafcetMainClass> ()._editorPanel,
			iTween.Hash(
				"position", new Vector3(0,0,0),
				"looktarget", Camera.main,
				"easeType", iTween.EaseType.easeOutExpo,
				"time", 1f,
				"islocal",true
			)
		);
	}

	private void deleteTopAction(GameObject _elementClicked){
		if(_elementClicked.GetComponentInChildren<connModuleClass>()._line){
			GameObject _remoteStartElement = _elementClicked.GetComponentInChildren<connModuleClass> ()._line.GetComponent<connLineClass> ().startConnector;
			GameObject _remoteEndElement = _elementClicked.GetComponentInChildren<connModuleClass> ()._line.GetComponent<connLineClass> ().endConnector;

			if (_elementClicked.GetComponentInChildren<connModuleClass>().gameObject.name == _remoteStartElement.name) {
				Destroy (_remoteEndElement.GetComponent<connModuleClass> ()._line);
				_remoteEndElement.GetComponent<connModuleClass> ()._line = null;

			} else {
				Destroy (_remoteStartElement.GetComponent<connModuleClass> ()._line);
				_remoteStartElement.GetComponent<connModuleClass> ()._line = null;
			}

			Destroy (_elementClicked.GetComponentInChildren<connModuleClass>()._line);
			_elementClicked.GetComponentInChildren<connModuleClass> ()._line = null;
			iTween.ScaleTo (_elementClicked.GetComponentInChildren<Button>().gameObject, new Vector3 (0f, 0f, 0f), 0.3f);
		}
	}
		
	private void deleteBottomAction(){
		if(_bottomConnector.GetComponent<connModuleClass>()._line){
			GameObject _remoteStartElement = _bottomConnector.GetComponent<connModuleClass> ()._line.GetComponent<connLineClass> ().startConnector;
			GameObject _remoteEndElement = _bottomConnector.GetComponent<connModuleClass> ()._line.GetComponent<connLineClass> ().endConnector;

			if(_bottomConnector.name == _remoteStartElement.name){
				if(_remoteEndElement.GetComponent<connModuleClass> ()._parentElement.name.IndexOf("step") == 0){
					_remoteEndElement.GetComponent<connModuleClass> ()._parentElement.GetComponent<StepClass> ().dragEnabled = true;
				}
				//TODO: others elements
				Destroy (_remoteEndElement.GetComponent<connModuleClass>()._line);
				_remoteEndElement.GetComponent<connModuleClass> ()._line = null;
			} else {
				if(_remoteStartElement.GetComponent<connModuleClass> ()._parentElement.name.IndexOf("step") == 0){
					_remoteStartElement.GetComponent<connModuleClass> ()._parentElement.GetComponent<StepClass> ().dragEnabled = true;
				}
				//TODO: others elements
				Destroy (_remoteStartElement.GetComponent<connModuleClass> ()._line);
				_remoteStartElement.GetComponent<connModuleClass> ()._line = null;
			}

			Destroy (_bottomConnector.GetComponent<connModuleClass>()._line);
			_bottomConnector.GetComponent<connModuleClass> ()._line = null;
			iTween.ScaleTo (_deleteBottomBt.gameObject, new Vector3 (0f, 0f, 0f), 0.3f);
		}
	}

	public void updateNameAndId(int _id){
		this._id = _id;
		this.name = "convergence_" + this._id.ToString ();
	}

	private void BeginDrag_(PointerEventData data)
	{
		if (dragEnabled == true) {
			objectDragged = true;

			if (_active == false) {

				gameObject.transform.SetParent (GameObject.Find ("Canvas/Grafcet").transform, true);

				startPosition = gameObject.transform.position;
				startIndex = gameObject.GetComponent<RectTransform> ().GetSiblingIndex ();

				gameObject.GetComponent<RectTransform> ().SetAsLastSibling ();

				_situation.GetComponent<GrafcetMainClass> ().activateWorkbenchCollider ();

				x_offset = 0;
				y_offset = 0;
			} else {
				Vector3 mousePos = Camera.main.ScreenToWorldPoint (Input.mousePosition);
				x_offset = gameObject.GetComponent<RectTransform> ().position.x - mousePos.x;
				y_offset = gameObject.GetComponent<RectTransform> ().position.y - mousePos.y;
			}
		}
	}

	private void ObjectDrag_(PointerEventData data)
	{
		if (objectDragged == true) {
			Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			gameObject.GetComponent<RectTransform> ().position = new Vector3 (mousePos.x + x_offset, mousePos.y + y_offset, 0);
		}
	}

	private void ObjectDrop_(PointerEventData data)
	{
		if (objectDragged == true) {
			objectDragged = false;

			_situation.GetComponent<GrafcetMainClass> ().deactivateWorkbenchCollider ();

			if (_active == false) {
				if (data != null && data.pointerEnter != null && data.pointerEnter.name == "workbenchCollider") {
					gameObject.transform.SetParent (_situation.GetComponent<GrafcetMainClass> ()._workbenchArea.transform, true);

					cloneElement ();

					initializeElement ();
				} else {
					iTween.MoveTo(
						gameObject,
						iTween.Hash(
							"position", startPosition,
							"looktarget", Camera.main,
							"easeType", iTween.EaseType.easeOutExpo,
							"time", 0.2f,
							"islocal",false
						)
					);

					Invoke ("goToInitialState", 0.2f);
				}
			}
		}
	}

	private void ObjectMouseOver_(PointerEventData data){
		if(_active == false){
			_mouseOver = true;
			_situation.GetComponent<GrafcetMainClass> ().showInfoText (Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='convergence']").InnerText);
		}
	}

	private void ObjectMouseOut_(PointerEventData data){
		if(_mouseOver == true){
			_mouseOver = false;
			_situation.GetComponent<GrafcetMainClass> ().hideInfoText ();
		}
	}

	private void goToInitialState(){
		//inScenario = false;
		gameObject.GetComponent<RectTransform> ().SetSiblingIndex (initialIndex);
		gameObject.transform.SetParent(GameObject.Find("space6").transform, true);
	}

	public void cloneElement(){
		GameObject _clone = Instantiate(_situation.GetComponent<GrafcetMainClass>().convergencePrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
		_clone.transform.SetParent (GameObject.Find("Canvas/Grafcet/leftMenu/containerElements/Viewport/Content/space6").transform, false);
		_clone.transform.localPosition = new Vector3 (0,0,0);
	}

	public void initializeElement(bool fromOpenAction = false){
		
		this._id = _situation.GetComponent<GrafcetMainClass> ().convergenceModules;
		_situation.GetComponent<GrafcetMainClass> ().convergenceModules++;
		this.name = "convergence_" + this._id.ToString ();

		if (fromOpenAction == true) {
			Vector2 _size = gameObject.GetComponent<RectTransform> ().sizeDelta;
			_size = new Vector2 (200f, _size.y);
			gameObject.GetComponent<RectTransform> ().sizeDelta = _size;
			initialAction ();
		} else {
			StartCoroutine (increaseWidth ());
		}

		entry_3 = new EventTrigger.Entry();
		entry_3.eventID = EventTriggerType.PointerClick;
		entry_3.callback.AddListener((data) => { ObjectClicked_((PointerEventData)data); });
		EventTrigger trigger_Object = gameObject.GetComponent<EventTrigger>();
		trigger_Object.triggers.Add(entry_3);
	}

	public void ObjectClicked_(PointerEventData data){
		if (objectDragged == false && _active == true) {
			if (_menuActive == false) {
				_situation.GetComponent<GrafcetMainClass> ().deactivateEditWithoutElement (gameObject.name);
				_menuActive = true;
				dragEnabled = false;
				iTween.ScaleTo (_deleteBt.gameObject, new Vector3 (0.6f, 0.6f, 0.6f), 0.3f);
				iTween.ScaleTo (_editBt.gameObject, new Vector3 (0.6f, 0.6f, 0.6f), 0.3f);
				if(_bottomConnector.GetComponent<connModuleClass>().lineIsBusy()){
					iTween.ScaleTo (_deleteBottomBt.gameObject, new Vector3 (0.4f, 0.4f, 0.4f), 0.3f);
				}

				for(int i = 0; i < _topConnectors.Count; i++){
					if(_topConnectors[i].GetComponentInChildren<connModuleClass>().lineIsBusy()){
						iTween.ScaleTo (_topConnectors[i].GetComponentInChildren<Button> ().gameObject, new Vector3 (0.4f, 0.4f, 0.4f), 0.3f);
					}
				}
			} else {
				closeEditElements ();
			}
		}
	}

	public void closeEditElements(){
		iTween.ScaleTo (_deleteBt.gameObject, new Vector3 (0f, 0f, 0f), 0.3f);
		iTween.ScaleTo (_editBt.gameObject, new Vector3 (0f, 0f, 0f), 0.3f);
		iTween.ScaleTo (_deleteBottomBt.gameObject, new Vector3 (0f, 0f, 0f), 0.3f);
		for (int i = 0; i < _topConnectors.Count; i++) {
			iTween.ScaleTo (_topConnectors[i].GetComponentInChildren<Button> ().gameObject, new Vector3 (0f, 0f, 0f), 0.3f);
		}
		_menuActive = false;
		dragEnabled = true;
	}

	public void initialAction(){
		//TODO: check if element is used;
		this._active = true;
		_bottomConnector.enabled = true;

		_topConnectors = new List<GameObject> ();

		for(int i = 0; i < _branches; i++){
			GameObject _topElement = Instantiate(_situation.GetComponent<GrafcetMainClass>().connTopDivPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
			_topElement.transform.SetParent (gameObject.transform, false);
			float _width = gameObject.GetComponent<RectTransform> ().rect.width;
			float _x = -_width / 2f + (float)i * _width / ((float)_branches - 1f);
			_topElement.transform.localPosition = new Vector3 (_x, 24.5f, 0);

			_topElement.GetComponentInChildren<connModuleClass> ()._parentElement = gameObject;
			_topElement.GetComponentInChildren<connModuleClass> ()._id = i;
			_topElement.name = "conn_" + i.ToString();

			_topElement.GetComponentInChildren<Button> ().onClick.AddListener(() => deleteTopAction(_topElement));
			_topElement.GetComponentInChildren<Button> ().gameObject.transform.localScale = new Vector3 (0, 0, 0);

			_topConnectors.Add (_topElement);
			_widthTopConnectors.Add (0);
			_yTopConnectors.Add (0);
		}

		if (_type == "AND") {
			_ANDBg.SetActive (true);
			_ORBg.SetActive (false);
		} else {
			_ANDBg.SetActive (false);
			_ORBg.SetActive (true);
		}
	}

	public void updateConnections(){
		if (_type == "AND") {
			_ANDBg.SetActive (true);
			_ORBg.SetActive (false);
		} else {
			_ANDBg.SetActive (false);
			_ORBg.SetActive (true);
		}

		if(_branches != _topConnectors.Count){
			if (_topConnectors.Count < _branches) {
				//is required add elements
				for(int i = _topConnectors.Count; i < _branches; i++){
					GameObject _topElement = Instantiate(_situation.GetComponent<GrafcetMainClass>().connTopDivPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
					_topElement.transform.SetParent (gameObject.transform, false);

					_topElement.GetComponentInChildren<connModuleClass> ()._parentElement = gameObject;
					_topElement.GetComponentInChildren<connModuleClass> ()._id = i;
					_topElement.name = "conn_" + (i).ToString ();

					_topElement.GetComponentInChildren<Button> ().onClick.AddListener(() => deleteTopAction(_topElement));
					_topElement.GetComponentInChildren<Button> ().gameObject.transform.localScale = new Vector3 (0, 0, 0);

					_topConnectors.Add (_topElement);
					_widthTopConnectors.Add (0);
					_yTopConnectors.Add (0);
				}

				updateTopPositions ();
			} else {
				for(int i = (_topConnectors.Count-1); i >= _branches; i--){
					deleteTopAction (_topConnectors [i]);
					Destroy (_topConnectors[i]);
					_topConnectors.RemoveAt (i);
					_widthTopConnectors.RemoveAt (i);
					_yTopConnectors.RemoveAt (i);
				}
				updateTopPositions ();
			}
		}

        //update connections
        if (_type == "AND")
        {
            //Bottom -> transition
            if (_bottomConnector.GetComponent<connModuleClass>()._line)
            {
                if (_bottomConnector.GetComponent<connModuleClass>()._line.GetComponent<connLineClass>().endConnector.GetComponent<connModuleClass>()._parentElement.name.IndexOf("transition") == -1)
                {
                    deleteBottomAction();
                }
            }

            //find jumps and delete
            for (int j = 0; j < _situation.GetComponent<GrafcetMainClass>().jumpModules; j++)
            {
                GameObject _element = GameObject.Find("jump_" + j.ToString());
                if (_element.GetComponent<JumpClass>().startObject.name == gameObject.name)
                {
                    _element.GetComponent<JumpClass>().deleteAction();
                }
            }

            //TOP -> STEP
            for (int i = 0; i < _topConnectors.Count; i++)
            {
                if (_topConnectors[i].GetComponentInChildren<connModuleClass>().lineIsBusy())
                {
                    if (_topConnectors[i].GetComponentInChildren<connModuleClass>()._line.GetComponent<connLineClass>().endConnector.GetComponent<connModuleClass>()._parentElement.name.IndexOf("step") == -1)
                    {
                        deleteTopAction(_topConnectors[i]);
                    }
                }
            }
        }
        else
        {
            //bottom -> step
            if (_bottomConnector.GetComponent<connModuleClass>()._line)
            {
                if (_bottomConnector.GetComponent<connModuleClass>()._line.GetComponent<connLineClass>().endConnector.GetComponent<connModuleClass>()._parentElement.name.IndexOf("step") == -1)
                {
                    deleteBottomAction();
                }
            }

            //top -> transition
            for (int i = 0; i < _topConnectors.Count; i++)
            {
                if (_topConnectors[i].GetComponentInChildren<connModuleClass>().lineIsBusy())
                {
                    if (_topConnectors[i].GetComponentInChildren<connModuleClass>()._line.GetComponent<connLineClass>().endConnector.GetComponent<connModuleClass>()._parentElement.name.IndexOf("transition") == -1)
                    {
                        deleteTopAction(_topConnectors[i]);
                    }
                }
            }
        }
    }

	private void updateTopPositions(){
		float _width = 0;

		int countConnectorsUsed = 0;
		for(int i = 0; i < _topConnectors.Count; i++){
			if(_widthTopConnectors[i] > 0){
				countConnectorsUsed++;
			}
		}

		if (countConnectorsUsed == 0) {
			//without connections
			_width = 200f * (float)(_topConnectors.Count - 1);
			gameObject.GetComponent<RectTransform> ().sizeDelta = new Vector2 (_width, gameObject.GetComponent<RectTransform> ().rect.height);

			for (int i =0; i <  _topConnectors.Count; i++) {
				float _x = -_width / 2f + (float)i * _width / ((float)_branches - 1f);
				_topConnectors [i].transform.localPosition = new Vector3 (_x, 24.5f, 0);
			}
		} else {
			//with connections
			float[] _newXLocalPositions = new float[_topConnectors.Count];
			int countPrevWithoutConnection = 0;
			float maxY = 0;
			int maxYIndex = 0;
			for (int i = 0; i < _topConnectors.Count; i++) {
				if (_widthTopConnectors [i] > 0) {
					for(int j = (i - countPrevWithoutConnection); j < i; j++){
						_widthTopConnectors [j] = _widthTopConnectors [i] - 200f * (float)(i - j);
					}
					countPrevWithoutConnection = 0;

					if( _yTopConnectors[i] < maxY){
						maxY = _yTopConnectors [i];
						maxYIndex = i;
					}
				} else {
					countPrevWithoutConnection++;
				}
			}
			if(countPrevWithoutConnection > 0){
				for(int i = (_topConnectors.Count - countPrevWithoutConnection); i < _topConnectors.Count; i++){
					_widthTopConnectors [i] = _widthTopConnectors [i - 1] + 200f;
				}
			}

            float minPoint = _widthTopConnectors[0];
            float maxPoint = _widthTopConnectors[_widthTopConnectors.Count - 1];
            for (int i = 0; i < _widthTopConnectors.Count; i++)
            {
                if (_widthTopConnectors[i] < minPoint)
                {
                    minPoint = _widthTopConnectors[i];
                }

                if (_widthTopConnectors[i] > maxPoint)
                {
                    maxPoint = _widthTopConnectors[i];
                }
            }

            //_width = _widthTopConnectors [_widthTopConnectors.Count - 1] - _widthTopConnectors [0];
            _width = maxPoint - minPoint;
            gameObject.GetComponent<RectTransform> ().sizeDelta = new Vector2 (Mathf.Abs(_width), gameObject.GetComponent<RectTransform> ().rect.height);

			Vector3 _newLocalPosition = gameObject.transform.localPosition;
            //_newLocalPosition.x = _widthTopConnectors [0] + (_widthTopConnectors [_widthTopConnectors.Count - 1] - _widthTopConnectors [0]) / 2f;
            _newLocalPosition.x = minPoint + (maxPoint - minPoint) / 2f;


            GameObject _parentElement = _topConnectors [maxYIndex].GetComponentInChildren<connModuleClass> ()._line.GetComponent<connLineClass> ().endConnector.GetComponent<connModuleClass> ()._parentElement;
			if (_parentElement.name.IndexOf ("step") == 0) {
				_newLocalPosition.y = maxY - 115f;
			} else {
				_newLocalPosition.y = maxY - 85f;
			}

			gameObject.transform.localPosition = _newLocalPosition;
            /*
			for (int i = 0; i < _topConnectors.Count; i++) {
				float _x = 0;
				if (i == 0) {
					_x = -_width / 2f;
				} else if (i == (_topConnectors.Count - 1)) {
					_x = _width / 2f;
				} else {
                    _x = _topConnectors[i - 1].transform.localPosition.x + (_widthTopConnectors[i] - _widthTopConnectors[i - 1]);
                }
					
				_topConnectors [i].transform.localPosition = new Vector3 (_x, 24.5f, 0);
			}
            */
            for (int i = 0; i < _topConnectors.Count; i++)
            {
                float _x = 0;
                if (_topConnectors[i].GetComponentInChildren<connModuleClass>()._line != null)
                {
                    GameObject _parentElementTop = _topConnectors[i].GetComponentInChildren<connModuleClass>()._line.GetComponent<connLineClass>().endConnector.GetComponent<connModuleClass>()._parentElement;
                    _x = _parentElementTop.gameObject.GetComponent<RectTransform>().localPosition.x - gameObject.GetComponent<RectTransform>().localPosition.x;
                }
                else
                {
                    if (i == 0)
                    {
                        _x = -_width / 2f;
                    }
                    else if (i == (_topConnectors.Count - 1))
                    {
                        _x = _width / 2f;
                    }
                    else
                    {
                        _x = _topConnectors[i - 1].transform.localPosition.x + (_widthTopConnectors[i] - _widthTopConnectors[i - 1]);
                    }
                }

                _topConnectors[i].transform.localPosition = new Vector3(_x, 24.5f, 0);
            }
        }
	}

	private IEnumerator increaseWidth(){
		Vector2 _size = gameObject.GetComponent<RectTransform> ().sizeDelta;
		_size = new Vector2 (_size.x + 50f, _size.y);
		gameObject.GetComponent<RectTransform> ().sizeDelta = _size;
		yield return new WaitForSeconds (1f / 36f);
		if (gameObject.GetComponent<RectTransform> ().rect.width < 200) {
			StartCoroutine (increaseWidth ());
		} else {
			initialAction ();
		}
	}

    public void activateJumpListenerAction()
    {
        if (this.name != "convergence_base")
        {
            if (_type == "OR")
            {
                _bottomConnector.enabled = true;
            }
        }
    }

    public void deactivateJumpListenerAction()
    {
        if (_type == "OR")
        {
            if (_bottomConnector.GetComponent<connModuleClass>()._line && _bottomConnector.GetComponent<connModuleClass>()._line.GetComponent<connLineClass>().startConnector && _bottomConnector.GetComponent<connModuleClass>()._line.GetComponent<connLineClass>().endConnector)
            {
                _bottomConnector.enabled = false;
            }
        }
    }

    public convergenceStructure getJSONData(){
		convergenceStructure _result = new convergenceStructure ();
		_result.x = gameObject.transform.localPosition.x;
		_result.y = gameObject.transform.localPosition.y;
		_result._id = this._id;
		_result.name = this.name;
		_result._type = this._type;
		_result._editorType = this._editorType;
		_result._branches = this._branches;
		_result._d_conn = _bottomConnector.GetComponent<connModuleClass> ().getJSONData ();
		if(_topConnectors.Count > 0){
			connModuleStructure[] _conns = new connModuleStructure[_topConnectors.Count];
			for(int i = 0; i < _topConnectors.Count; i++){
				_conns [i] = _topConnectors [i].GetComponentInChildren<connModuleClass> ().getJSONData ();
			}
			_result.conns = _conns;
		}
		return _result;
	}

	// Update is called once per frame
	void Update () {
		if(_active == true){
			int _countConnected = 0;
			float[] _newXPositions = new float[_topConnectors.Count];
			float[] _newXLocalPositions = new float[_topConnectors.Count];
			float[] _parentYPositions = new float[_topConnectors.Count];
			float[] _parentYLocalPositions = new float[_topConnectors.Count];
			bool[] _connectorsBusy = new bool[_topConnectors.Count];

			for(int i = 0; i < _topConnectors.Count; i++){
				_widthTopConnectors [i] = 0;
				_yTopConnectors [i] = 0;

				if (_topConnectors [i].GetComponentInChildren<connModuleClass> ().lineIsBusy ()) {
					_countConnected++;
					GameObject _parentElement = _topConnectors [i].GetComponentInChildren<connModuleClass> ()._line.GetComponent<connLineClass> ().endConnector.GetComponent<connModuleClass> ()._parentElement;
					_newXPositions [i] = _parentElement.transform.position.x;
					_newXLocalPositions [i] = _parentElement.transform.localPosition.x;
					_parentYPositions[i] = _parentElement.transform.position.y;
					_parentYLocalPositions[i] = _parentElement.transform.localPosition.y;

					_widthTopConnectors [i] = _parentElement.transform.localPosition.x;
					_yTopConnectors [i] = _parentElement.transform.localPosition.y;
					_connectorsBusy [i] = true;
				} else {
					_connectorsBusy [i] = false;
				}
			}

			if(_countConnected == 0){
				
			} else if (_countConnected > 0) {
				dragEnabled = false;
			}

			countConnected = _countConnected;

			updateTopPositions ();

		}
	}
}
