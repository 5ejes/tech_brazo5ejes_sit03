﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class ActionClass : MonoBehaviour {

	public GameObject _situation;

	public bool _active = false;
	public int _id = 0;
	public bool _onSim = false;

	public Text _titleElement;
	public Text _textElement;
	public Image _leftConnector;
	public Image _rightConnector;
	public Button _editBt;
	public Button _deleteBt;
	public Button _deleteLeftBt;
	public Button _deleteRightBt;
	public Image _bgElement;

	private float x_offset = 0;
	private float y_offset = 0;

	public bool dragEnabled = true;
	private bool objectDragged;
	private Vector3 initialPosition;
	private Vector3 startPosition;
	private int initialIndex;
	private int startIndex;

	private EventTrigger.Entry entry_0;
	private EventTrigger.Entry entry_1;
	private EventTrigger.Entry entry_2;
	private EventTrigger.Entry entry_3;

	public bool _menuActive = false;

	//ELEMENT DATA
	public int _type = 0; //0: none, 1: outputs, 2: memory, 3: timers, 4: counters, 5: aritmetic op, 6: logic op
    public int _typeRobotic = 0; //0: none, 1: gripper, 2: band, 3: motor A, 4, motor B, 5: motor C, 6: motor D, 7: coordenadas rect, 8: coord polar
	public string _value = "";
	public int _index = 0;
	public string _value2 = "";
	public int _index2 = 0;
	public string _value3 = "";
	public int _index3 = 0;
	public string _value4 = "0";
	public int _index4 = 0;
	public int _scale = 0;
	public bool _constant1 = false;
	public bool _constant2 = false;
	public string _options = "";
	public int _editorType = 1;
	public bool _isInverse = false;
	public bool _isReset = false;

    public ActionClass[] childActions;
    public string childName = "";
    public string nextAction = "";

    private bool _mouseOver = false;

	// Use this for initialization
	void Start () {
		_situation = GameObject.Find ("Canvas/Grafcet");

		initialPosition = gameObject.transform.position;

		_titleElement.text = "";
		_textElement.text = "";
		_leftConnector.enabled = false;
		_rightConnector.enabled = false;

		_editBt.gameObject.transform.localScale = new Vector3 (0, 0, 0);
		_deleteBt.gameObject.transform.localScale = new Vector3 (0, 0, 0);

		_deleteLeftBt.gameObject.transform.localScale = new Vector3 (0, 0, 0);
		_deleteRightBt.gameObject.transform.localScale = new Vector3 (0, 0, 0);

		_editBt.onClick.AddListener (editAction);
		_deleteBt.onClick.AddListener (deleteAction);

		_deleteRightBt.onClick.AddListener (deleteRightAction);
		_deleteLeftBt.onClick.AddListener (deleteLeftAction);

		gameObject.AddComponent<EventTrigger> ();

		EventTrigger trigger_Object = gameObject.GetComponent<EventTrigger>();

		entry_0 = new EventTrigger.Entry();
		entry_0.eventID = EventTriggerType.BeginDrag;
		entry_0.callback.AddListener((data) => { BeginDrag_((PointerEventData)data); });

		entry_1 = new EventTrigger.Entry();
		entry_1.eventID = EventTriggerType.Drag;
		entry_1.callback.AddListener((data) => { ObjectDrag_((PointerEventData)data); });

		entry_2 = new EventTrigger.Entry();
		entry_2.eventID = EventTriggerType.EndDrag;
		entry_2.callback.AddListener((data) => { ObjectDrop_((PointerEventData)data); });

		EventTrigger.Entry entry_4 = new EventTrigger.Entry();
		entry_4.eventID = EventTriggerType.PointerEnter;
		entry_4.callback.AddListener((data) => { ObjectMouseOver_((PointerEventData)data); });

		EventTrigger.Entry entry_5 = new EventTrigger.Entry();
		entry_5.eventID = EventTriggerType.PointerExit;
		entry_5.callback.AddListener((data) => { ObjectMouseOut_((PointerEventData)data); });

		trigger_Object.triggers.Add(entry_0);
		trigger_Object.triggers.Add(entry_1);
		trigger_Object.triggers.Add(entry_2);
		trigger_Object.triggers.Add(entry_4);
		trigger_Object.triggers.Add(entry_5);

		gameObject.name = "action_base";
	}

	private void BeginDrag_(PointerEventData data)
	{
		if (dragEnabled == true) {
			objectDragged = true;

			if (_active == false) {

				gameObject.transform.SetParent (GameObject.Find ("Canvas/Grafcet").transform, true);

				startPosition = gameObject.transform.position;
				startIndex = gameObject.GetComponent<RectTransform> ().GetSiblingIndex ();

				gameObject.GetComponent<RectTransform> ().SetAsLastSibling ();

				_situation.GetComponent<GrafcetMainClass> ().activateWorkbenchCollider ();

				x_offset = 0;
				y_offset = 0;
			} else {
				Vector3 mousePos = Camera.main.ScreenToWorldPoint (Input.mousePosition);
				x_offset = gameObject.GetComponent<RectTransform> ().position.x - mousePos.x;
				y_offset = gameObject.GetComponent<RectTransform> ().position.y - mousePos.y;
			}
		}
	}

	private void ObjectDrag_(PointerEventData data)
	{
		if (objectDragged == true) {
			Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			gameObject.GetComponent<RectTransform> ().position = new Vector3 (mousePos.x + x_offset, mousePos.y + y_offset, 0);
		}
	}

	private void ObjectDrop_(PointerEventData data)
	{
		if (objectDragged == true) {
			objectDragged = false;

			_situation.GetComponent<GrafcetMainClass> ().deactivateWorkbenchCollider ();

			if (_active == false) {
				if (data != null && data.pointerEnter != null && data.pointerEnter.name == "workbenchCollider") {
					gameObject.transform.SetParent ( _situation.GetComponent<GrafcetMainClass>()._workbenchArea.transform , true);
					cloneElement ();
					initializeElement ();
				} else {
					iTween.MoveTo(
						gameObject,
						iTween.Hash(
							"position", startPosition,
							"looktarget", Camera.main,
							"easeType", iTween.EaseType.easeOutExpo,
							"time", 0.2f,
							"islocal",false
						)
					);

					Invoke ("goToInitialState", 0.2f);
				}
			}
		}
	}

	private void ObjectMouseOver_(PointerEventData data){
		if(_active == false){
			_mouseOver = true;
			_situation.GetComponent<GrafcetMainClass> ().showInfoText (Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='action']").InnerText);
		}
	}

	private void ObjectMouseOut_(PointerEventData data){
		if(_mouseOver == true){
			_mouseOver = false;
			_situation.GetComponent<GrafcetMainClass> ().hideInfoText ();
		}
	}

	private void goToInitialState(){
		//inScenario = false;
		gameObject.GetComponent<RectTransform> ().SetSiblingIndex (initialIndex);
		gameObject.transform.SetParent(GameObject.Find("space3").transform, true);
	}

	public void cloneElement(){
		GameObject _clone = Instantiate(_situation.GetComponent<GrafcetMainClass>().actionPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
		_clone.transform.SetParent (GameObject.Find("Canvas/Grafcet/leftMenu/containerElements/Viewport/Content/space3").transform, false);
		_clone.transform.localPosition = new Vector3 (0f,0f,0f);
	}

	public void initializeElement(){
		this._active = true;
		this._id = _situation.GetComponent<GrafcetMainClass> ().actionModules;
		_situation.GetComponent<GrafcetMainClass> ().actionModules++;
		this.name = "action_" + this._id.ToString ();
		_titleElement.text = "?";
		_textElement.text = "";

		StartCoroutine (increaseWidth ());

		entry_3 = new EventTrigger.Entry();
		entry_3.eventID = EventTriggerType.PointerClick;
		entry_3.callback.AddListener((data) => { ObjectClicked_((PointerEventData)data); });
		EventTrigger trigger_Object = gameObject.GetComponent<EventTrigger>();
		trigger_Object.triggers.Add(entry_3);

		initialAction();
	}

	public void updateNameAndId(int _id){
		this._id = _id;
		this.name = "action_" + this._id.ToString ();
        if(childActions.Length > 0)
        {
            for (int i = 0; i < childActions.Length; i++)
            {
                childActions[i].childName = this.name + ChildId(i);
            }
        }
    }

    private string ChildId(int id)
    {
        string result = "";
        switch (id)
        {
            case 0:
                result = "";
                break;
            case 1:
                result = "_A";
                break;
            case 2:
                result = "_B";
                break;
            case 3:
                result = "_C";
                break;
            case 4:
                result = "_D";
                break;
            case 5:
                result = "_E";
                break;
            case 6:
                result = "_F";
                break;
            case 7:
                result = "_G";
                break;
            case 8:
                result = "_H";
                break;
            case 9:
                result = "_I";
                break;
            case 10:
                result = "_J";
                break;
            case 11:
                result = "_K";
                break;
            case 12:
                result = "_L";
                break;
            case 13:
                result = "_M";
                break;
            case 14:
                result = "_N";
                break;
            case 15:
                result = "_O";
                break;
            case 16:
                result = "_P";
                break;
            case 17:
                result = "_Q";
                break;
            case 18:
                result = "_R";
                break;
            case 19:
                result = "_S";
                break;
            case 20:
                result = "_T";
                break;
            case 21:
                result = "_U";
                break;
            case 22:
                result = "_V";
                break;
            case 23:
                result = "_W";
                break;
            case 24:
                result = "_Z";
                break;
            case 25:
                result = "_Y";
                break;
            case 26:
                result = "_Z";
                break;
            default:
                result = "";
                break;
        }

        return result;
    }

    public void deleteAction(){
		for(int i = this._id + 1; i < _situation.GetComponent<GrafcetMainClass> ().actionModules; i++){
			GameObject _element = GameObject.Find ("action_" + i.ToString());
			_element.GetComponent<ActionClass> ().updateNameAndId (i - 1);
		}
		_situation.GetComponent<GrafcetMainClass> ().actionModules--;

		deleteRightAction ();
		Destroy (gameObject);
	}

	private void deleteRightAction(){
		if(_rightConnector.GetComponent<connModuleClass>()._line){
			GameObject _remoteStartElement = _rightConnector.GetComponent<connModuleClass> ()._line.GetComponent<connLineClass> ().startConnector;
			GameObject _remoteEndElement = _rightConnector.GetComponent<connModuleClass> ()._line.GetComponent<connLineClass> ().endConnector;

			if (_rightConnector.name == _remoteStartElement.name) {
				_remoteEndElement.GetComponent<connModuleClass> ()._parentElement.GetComponent<ActionClass> ().dragEnabled = true;
				Destroy (_remoteEndElement.GetComponent<connModuleClass> ()._line);
				_remoteEndElement.GetComponent<connModuleClass> ()._line = null;

			} else {
				_remoteStartElement.GetComponent<connModuleClass> ()._parentElement.GetComponent<ActionClass> ().dragEnabled = true;
				Destroy (_remoteStartElement.GetComponent<connModuleClass> ()._line);
				_remoteStartElement.GetComponent<connModuleClass> ()._line = null;
			}

			Destroy (_rightConnector.GetComponent<connModuleClass>()._line);
			_rightConnector.GetComponent<connModuleClass> ()._line = null;
			iTween.ScaleTo (_deleteRightBt.gameObject, new Vector3 (0f, 0f, 0f), 0.3f);
		}
	}

	private void deleteLeftAction(){
		if(_leftConnector.GetComponent<connModuleClass>()._line){
			GameObject _remoteStartElement = _leftConnector.GetComponent<connModuleClass> ()._line.GetComponent<connLineClass> ().startConnector;
			GameObject _remoteEndElement = _leftConnector.GetComponent<connModuleClass> ()._line.GetComponent<connLineClass> ().endConnector;

			if(_leftConnector.name == _remoteStartElement.name){
				Destroy (_remoteEndElement.GetComponent<connModuleClass>()._line);
				_remoteEndElement.GetComponent<connModuleClass> ()._line = null;
			} else {
				Destroy (_remoteStartElement.GetComponent<connModuleClass> ()._line);
				_remoteStartElement.GetComponent<connModuleClass> ()._line = null;
			}

			Destroy (_leftConnector.GetComponent<connModuleClass>()._line);
			_leftConnector.GetComponent<connModuleClass> ()._line = null;
			iTween.ScaleTo (_deleteLeftBt.gameObject, new Vector3 (0f, 0f, 0f), 0.3f);
		}
	}

	private void editAction(){
		_situation.GetComponent<GrafcetMainClass> ().activateBackground ();
		_situation.GetComponent<GrafcetMainClass> ()._editorPanel.transform.SetParent (GameObject.Find ("Canvas").transform, true);
		_situation.GetComponent<GrafcetMainClass> ()._editorPanel.GetComponent<editorPanelClass> ().configureEditor (gameObject, "action");

		iTween.MoveTo(
			_situation.GetComponent<GrafcetMainClass> ()._editorPanel,
			iTween.Hash(
				"position", new Vector3(0,0,0),
				"looktarget", Camera.main,
				"easeType", iTween.EaseType.easeOutExpo,
				"time", 1f,
				"islocal",true
			)
		);
	}

	public void initialAction(){
		//TODO: check if element is used;
		_rightConnector.enabled = true;
		//TODO: check if element is used;
		_leftConnector.enabled = true;

	}

	public void ObjectClicked_(PointerEventData data){
		if (objectDragged == false && _active == true) {
			if (_menuActive == false) {
				_situation.GetComponent<GrafcetMainClass> ().deactivateEditWithoutElement (gameObject.name);
				_menuActive = true;
				dragEnabled = false;
				iTween.ScaleTo (_deleteBt.gameObject, new Vector3 (0.6f, 0.6f, 0.6f), 0.3f);
				iTween.ScaleTo (_editBt.gameObject, new Vector3 (0.6f, 0.6f, 0.6f), 0.3f);
				if(_rightConnector.GetComponent<connModuleClass>().lineIsBusy()){
					iTween.ScaleTo (_deleteRightBt.gameObject, new Vector3 (0.4f, 0.4f, 0.4f), 0.3f);
				}
				if(_leftConnector.GetComponent<connModuleClass>().lineIsBusy()){
					iTween.ScaleTo (_deleteLeftBt.gameObject, new Vector3 (0.4f, 0.4f, 0.4f), 0.3f);
				}

			} else {
				closeEditElements ();
			}
		}

	}

	public void closeEditElements(){
		iTween.ScaleTo (_deleteBt.gameObject, new Vector3 (0f, 0f, 0f), 0.3f);
		iTween.ScaleTo (_editBt.gameObject, new Vector3 (0f, 0f, 0f), 0.3f);
		iTween.ScaleTo (_deleteRightBt.gameObject, new Vector3 (0f, 0f, 0f), 0.3f);
		iTween.ScaleTo (_deleteLeftBt.gameObject, new Vector3 (0f, 0f, 0f), 0.3f);
		_menuActive = false;
		dragEnabled = true;
	}

	private IEnumerator increaseWidth(){
		Vector2 _size = gameObject.GetComponent<RectTransform> ().sizeDelta;
		_size = new Vector2 (_size.x + 26f, _size.y);
		gameObject.GetComponent<RectTransform> ().sizeDelta = _size;
		yield return new WaitForSeconds (1f / 36f);
		if(gameObject.GetComponent<RectTransform> ().rect.width < 208){
			StartCoroutine (increaseWidth());
		}
	}

    public void SetLastConnection(string parentName)
    {
        if (Manager.Instance.globalRoboticsMode == true)
        {
            if (this.childActions != null && this.childActions.Length >= 1)
            {
                this.childActions[this.childActions.Length - 1].nextAction = parentName;
            }
        }
    }


    public actionStructure getJSONData(){
		actionStructure _result = new actionStructure ();
        try
        {
            _result.x = gameObject.transform.localPosition.x;
            _result.y = gameObject.transform.localPosition.y;

            _result._title = _titleElement.text;
            _result._msg = this._textElement.text;

            _result._r_conn = _rightConnector.GetComponent<connModuleClass>().getJSONData();
            _result._l_conn = _leftConnector.GetComponent<connModuleClass>().getJSONData();
        }
        catch (Exception e)
        {
            Debug.Log(e);
        }
        _result._id = this._id;

        if (childName == "")
        {
            _result.name = this.name;
        }
        else
        {
            _result.name = this.childName;

            if (nextAction != "")
            {
                _result._r_conn._line.endConnectorParent = nextAction;
            }
            else
            {
                _result._r_conn._id = -1;
            }
        }

        _result._type = this._type;
		_result._value = this._value;
		_result._index = this._index;
		_result._value2 = this._value2;
		_result._index2 = this._index2;
		_result._value3 = this._value3;
		_result._index3 = this._index3;
		_result._value4 = this._value4;
		_result._index4 = this._index4;
		_result._scale = this._scale;
		_result._constant1 = this._constant1;
		_result._constant2 = this._constant2;
		_result._options = this._options;
		_result._editorType = this._editorType;
		_result._isInverse = this._isInverse;
		_result._isReset = this._isReset;
        _result._typeRobotic = this._typeRobotic;

        if (this.childActions != null && this.childActions.Length > 0)
        {
            _result._childActions = new actionStructure[this.childActions.Length];
            for (int i = 0; i < this.childActions.Length; i++)
            {
                _result._childActions[i] = this.childActions[i].getJSONData();
            }
        }
        //_result._isRobotic = false;
		return _result;
	}
	// Update is called once per frame
	void Update () {
		if(_leftConnector.GetComponent<connModuleClass>()._line){
			if(_leftConnector.GetComponent<connModuleClass>()._line.GetComponent<connLineClass>().startConnector && _leftConnector.GetComponent<connModuleClass>()._line.GetComponent<connLineClass>().endConnector){
				dragEnabled = false;

				Vector3 _newPosition = new Vector3 (0,0,0);
				GameObject _parentElement = _leftConnector.GetComponent<connModuleClass>()._line.GetComponent<connLineClass>().endConnector.GetComponent<connModuleClass>()._parentElement;

				if(_parentElement.name.IndexOf("step") != -1){
					_newPosition.x = _parentElement.transform.localPosition.x + 195f;
					_newPosition.y = _parentElement.transform.localPosition.y;
				} else if(_parentElement.name.IndexOf("action") != -1){
					_newPosition.x = _parentElement.transform.localPosition.x + 245f;
					_newPosition.y = _parentElement.transform.localPosition.y;

                    _parentElement.GetComponent<ActionClass>().SetLastConnection(this.name);

                }

				gameObject.transform.localPosition = _newPosition;


			}
		}

        if (_rightConnector.GetComponent<connModuleClass>().getJSONData()._line._id == -1)
        {
            this.SetLastConnection("");
        }
    }
}
