﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class JumpClass : MonoBehaviour {

	public GameObject _situation;

	public bool _active = false;
	public int _id = 0;

	public Image _bgImage;

	public Button _deleteBt;

	public Image _line1;
	public Image _line2;
	public Image _line3;
	public Image _line4;
	public Image _arrow;

	public Image _overA1;
	public Image _overA2;
	public Image _overA3;

	public GameObject startObject;
	public GameObject endObject;
	public GameObject objectToFollow;

	public bool dragEnabled = true;
	private bool objectDragged;
	private Vector3 initialPosition;
	private Vector3 startPosition;
	private int initialIndex;
	private int startIndex;

	private EventTrigger.Entry entry_0;
	private EventTrigger.Entry entry_1;
	private EventTrigger.Entry entry_2;
	private EventTrigger.Entry entry_3;

	public bool _menuActive = false;
	private bool _arrowBuilding = false;

	private float x_offset = 0;
	private float y_offset = 0;

	private float lineWidth = 4;
	private float lineWidthOffset = 100;

	private float moveScroll = 0;
	private float moveScrollY = 0;

	private bool lookingForNewLeft = false;

	private bool _mouseOver = false;

	// Use this for initialization
	void Start () {
		this.name = "jump_base";
		//TODO: x button
		_line1.enabled = false;
		_line2.enabled = false;
		_line3.enabled = false;
		_line4.enabled = false;
		_arrow.enabled = false;

		_overA1.enabled = false;
		_overA2.enabled = false;
		_overA3.enabled = false;

		_situation = GameObject.Find ("Canvas/Grafcet");
		initialPosition = gameObject.transform.position;

		_deleteBt.gameObject.transform.localScale = new Vector3 (0, 0, 0);
		_deleteBt.onClick.AddListener (deleteAction);

		gameObject.AddComponent<EventTrigger> ();

		EventTrigger trigger_Object = gameObject.GetComponent<EventTrigger>();

		entry_0 = new EventTrigger.Entry();
		entry_0.eventID = EventTriggerType.BeginDrag;
		entry_0.callback.AddListener ((data) => {
			BeginDrag_ ((PointerEventData)data);
		});

		entry_1 = new EventTrigger.Entry();
		entry_1.eventID = EventTriggerType.Drag;
		entry_1.callback.AddListener ((data) => {
			ObjectDrag_ ((PointerEventData)data);
		});

		entry_2 = new EventTrigger.Entry();
		entry_2.eventID = EventTriggerType.EndDrag;
		entry_2.callback.AddListener ((data) => {
			ObjectDrop_ ((PointerEventData)data);
		});

		entry_3 = new EventTrigger.Entry ();
		entry_3.eventID = EventTriggerType.PointerClick;
		entry_3.callback.AddListener ((data) => {
			ObjectClick_((PointerEventData)data);
		});

		EventTrigger.Entry entry_4 = new EventTrigger.Entry();
		entry_4.eventID = EventTriggerType.PointerEnter;
		entry_4.callback.AddListener((data) => { ObjectMouseOver_((PointerEventData)data); });

		EventTrigger.Entry entry_5 = new EventTrigger.Entry();
		entry_5.eventID = EventTriggerType.PointerExit;
		entry_5.callback.AddListener((data) => { ObjectMouseOut_((PointerEventData)data); });

		trigger_Object.triggers.Add(entry_0);
		trigger_Object.triggers.Add(entry_1);
		trigger_Object.triggers.Add(entry_2);
		trigger_Object.triggers.Add(entry_3);
		trigger_Object.triggers.Add(entry_4);
		trigger_Object.triggers.Add(entry_5);
	}

	private void BeginDrag_(PointerEventData data)
	{
		if (dragEnabled == true) {

			if (_active == false) {

				objectDragged = true;

				gameObject.transform.SetParent (GameObject.Find ("Canvas/Grafcet").transform, true);

				startPosition = gameObject.transform.position;
				startIndex = gameObject.GetComponent<RectTransform> ().GetSiblingIndex ();

				iTween.RotateTo (gameObject, new Vector3 (0, 0, 90), 0.2f);
				_bgImage.transform.localPosition = new Vector3 (0, 35, 0);

				gameObject.GetComponent<RectTransform> ().SetAsLastSibling ();

				_situation.GetComponent<GrafcetMainClass> ().activateJumpListenersAction ();

				x_offset = 0;
				y_offset = 0;
			} else {
				if(endObject == null){

					_situation.GetComponent<GrafcetMainClass> ().jump_connecting = true;

					Quaternion _quaternion = new Quaternion ();
					_quaternion.eulerAngles = new Vector3 (0,0,-90);
					_arrow.transform.rotation = _quaternion;

					_line2.enabled = true;
					_line3.enabled = true;

					_arrowBuilding = true;
				}
			}
		}
	}

	private void ObjectDrag_(PointerEventData data)
	{
		if (objectDragged == true) {
			Vector3 mousePos = Camera.main.ScreenToWorldPoint (Input.mousePosition);
			gameObject.GetComponent<RectTransform> ().position = new Vector3 (mousePos.x + x_offset, mousePos.y + y_offset, 0);
		} else if (_arrowBuilding == true) {
			Vector3 mousePos = Camera.main.ScreenToWorldPoint (Input.mousePosition);
			_arrow.transform.position = new Vector3 (mousePos.x, mousePos.y, 0);
			_arrow.transform.localPosition = new Vector3 (_arrow.transform.localPosition.x - 20, _arrow.transform.localPosition.y, _arrow.transform.localPosition.z);


			float _widthBase = (_bgImage.transform.localPosition.x - _arrow.transform.localPosition.x);
			if (_widthBase < 0) {
				_widthBase = 0;
			}

			float _line1Width = _widthBase + lineWidthOffset;
			_line1.GetComponent<RectTransform> ().sizeDelta = new Vector2 (_line1Width, lineWidth);

			_line2.transform.localPosition = new Vector3 (_bgImage.transform.localPosition.x - _line1Width + 2, _line1.transform.localPosition.y);

			float _line2Width = _arrow.transform.localPosition.y - _line1.transform.localPosition.y;
			Quaternion _quaternion = new Quaternion ();
			if (_line2Width < 0) {
				_line2Width = _line2Width * (-1f);
				_quaternion.eulerAngles = new Vector3 (0, 0, 180);
			} else {
				_quaternion.eulerAngles = new Vector3 (0, 0, 0);
			}
			_line2.transform.rotation = _quaternion;
			_line2.GetComponent<RectTransform> ().sizeDelta = new Vector2 (lineWidth, _line2Width);

			_line3.transform.localPosition = new Vector3 (_bgImage.transform.localPosition.x - _line1Width, _arrow.transform.localPosition.y, 0);
			float _line3Width = Mathf.Abs ((_bgImage.transform.localPosition.x - _line1Width) - _arrow.transform.localPosition.x);
			_line3.GetComponent<RectTransform> ().sizeDelta = new Vector2 (_line3Width, lineWidth);


			//
			float _aspectRatio = 0;
			switch(Manager.Instance.globalGraphicInterface){
			case baseSystem.graphicInterfaces.electrical_bench:
				_aspectRatio = (float)_situation.GetComponent<GrafcetMainClass> ()._situation.GetComponent<ElectricalTestBenchMainClass> ()._cam1.pixelWidth / (float)_situation.GetComponent<GrafcetMainClass> ()._situation.GetComponent<ElectricalTestBenchMainClass> ()._cam1.pixelHeight;
				break;
			case baseSystem.graphicInterfaces.pneumatic_bench:
				_aspectRatio = (float)_situation.GetComponent<GrafcetMainClass> ()._situation.GetComponent<PnematicTestBenchClass> ()._cam1.pixelWidth / (float)_situation.GetComponent<GrafcetMainClass> ()._situation.GetComponent<PnematicTestBenchClass> ()._cam1.pixelHeight;
				break;
            case baseSystem.graphicInterfaces.roboticInterface:
                _aspectRatio = (float)_situation.GetComponent<GrafcetMainClass>()._situation.GetComponent<onlyControllerMainClass>()._cam1.pixelWidth / (float)_situation.GetComponent<GrafcetMainClass>()._situation.GetComponent<onlyControllerMainClass>()._cam1.pixelHeight;
                break;
            }

			float _heightTotal = 1080f * ((1920f / 1080f) / _aspectRatio);
			float scaleFactor = 0;
			switch(Manager.Instance.globalGraphicInterface){
			case baseSystem.graphicInterfaces.electrical_bench:
				scaleFactor = 1920f / (float)_situation.GetComponent<GrafcetMainClass> ()._situation.GetComponent<ElectricalTestBenchMainClass> ()._cam1.pixelWidth;
				break;
			case baseSystem.graphicInterfaces.pneumatic_bench:
				scaleFactor = 1920f / (float)_situation.GetComponent<GrafcetMainClass> ()._situation.GetComponent<PnematicTestBenchClass> ()._cam1.pixelWidth;
				break;
            case baseSystem.graphicInterfaces.roboticInterface:
                scaleFactor = 1920f / (float)_situation.GetComponent<GrafcetMainClass>()._situation.GetComponent<onlyControllerMainClass>()._cam1.pixelWidth;
                break;
            }

			float widthNormalized = Input.mousePosition.x * scaleFactor;
			float heightNormalized = Input.mousePosition.y;
			float yLimit = 0;
			switch (Manager.Instance.globalGraphicInterface) {
			case baseSystem.graphicInterfaces.electrical_bench:
				yLimit = (_heightTotal - 114f) * ((float)_situation.GetComponent<GrafcetMainClass> ()._situation.GetComponent<ElectricalTestBenchMainClass> ()._cam1.pixelHeight / _heightTotal);
				break;
			case baseSystem.graphicInterfaces.pneumatic_bench:
				yLimit = (_heightTotal - 114f) * ((float)_situation.GetComponent<GrafcetMainClass> ()._situation.GetComponent<PnematicTestBenchClass> ()._cam1.pixelHeight / _heightTotal);
				break;
            case baseSystem.graphicInterfaces.roboticInterface:
                yLimit = (_heightTotal - 114f) * ((float)_situation.GetComponent<GrafcetMainClass>()._situation.GetComponent<onlyControllerMainClass>()._cam1.pixelHeight / _heightTotal);
                break;
            }


			//x scroll analysis:
			moveScroll = 0;
			if (widthNormalized > (1920f - 20f)) {
				if(_situation.GetComponent<GrafcetMainClass> ()._workbenchContainer.GetComponent<ScrollRect> ().horizontalNormalizedPosition < 1){
					moveScroll = 0.02f;
				}
			} else {
				if (_situation.GetComponent<GrafcetMainClass> ().leftMenuHidden == true) {
					if (widthNormalized < (30f)) {
						if(_situation.GetComponent<GrafcetMainClass> ()._workbenchContainer.GetComponent<ScrollRect> ().horizontalNormalizedPosition > 0){
							moveScroll = -0.02f;
						}
					}
				} else {
					if (widthNormalized < (200f)) {
						if(_situation.GetComponent<GrafcetMainClass> ()._workbenchContainer.GetComponent<ScrollRect> ().horizontalNormalizedPosition > 0){
							moveScroll = -0.02f;
						}
					}
				}
			}
			// end x scroll analysis

            

            //y scroll analysis:
            moveScrollY = 0;
			if (Input.mousePosition.y < 0) {
				moveScrollY = -0.02f;
			} else {
				if (Input.mousePosition.y > yLimit) {
					moveScrollY = 0.02f;
				}
			}


        }
    }

	private void ObjectDrop_(PointerEventData data)
	{
		if (objectDragged == true) {
			objectDragged = false;

			_situation.GetComponent<GrafcetMainClass> ().deactivateJumpListenersAction ();

			if (_active == false) {

				if (data != null && data.pointerEnter != null && data.pointerEnter.name == "bottomGreenConnector") {
					if (data.pointerEnter.GetComponent<connModuleClass> ()._parentElement.name.IndexOf ("transition") != -1 || (data.pointerEnter.GetComponent<connModuleClass>()._parentElement.name.IndexOf("convergence") != -1) && data.pointerEnter.GetComponent<connModuleClass>()._parentElement.GetComponent<ConvergenceClass>()._type == "OR") {

						startObject = data.pointerEnter.GetComponent<connModuleClass> ()._parentElement;
						objectToFollow = data.pointerEnter;

						gameObject.transform.SetParent ( _situation.GetComponent<GrafcetMainClass>()._workbenchArea.transform , true);

						gameObject.transform.position = data.pointerEnter.transform.position;

						data.pointerEnter.GetComponent<connModuleClass>().deactivateIfAvailable();

						cloneElement ();

						initializeElement ();

						_line1.transform.position = new Vector3(data.pointerEnter.transform.position.x, data.pointerEnter.transform.position.y, data.pointerEnter.transform.position.z);
						_line1.transform.localPosition = new Vector3 (2,0,0);
						_line1.GetComponent<RectTransform> ().sizeDelta = new Vector2 (100f, lineWidth);

						_overA1.transform.localPosition = _line1.transform.localPosition;
						_overA1.GetComponent<RectTransform> ().sizeDelta = new Vector2 (100f, 30f);

						_deleteBt.transform.localPosition = new Vector3 (-100, 0, 0);

					} else {
						returnAction ();
					}
				} else {
					returnAction ();
				}
			}
		}
		else if(_arrowBuilding == true){
			_arrowBuilding = false;
			_situation.GetComponent<GrafcetMainClass> ().jump_connecting = false;

			if(data.pointerEnter != null && data.pointerEnter.name == "topGreenConnector" && data.pointerEnter.GetComponent<connModuleClass>()._parentElement.name.IndexOf("step_") == 0) {
				endObject = data.pointerEnter.GetComponent<connModuleClass> ()._parentElement;

				setlookingForNewLeft ();

				/*

				if (endObject.transform.GetSiblingIndex () < startObject.transform.GetSiblingIndex ()) {
					//gameObject.transform.SetSiblingIndex (endObject.transform.GetSiblingIndex () - 1);
				} else {
					//gameObject.transform.SetSiblingIndex (startObject.transform.GetSiblingIndex () - 1);
				}
				*/
			}
			else {
				_bgImage.enabled = false;
				_line1.enabled = true;
				_line2.enabled = false;
				_line3.enabled = false;
				_arrow.enabled = true;

				Quaternion _quaternion = new Quaternion ();
				_quaternion.eulerAngles = new Vector3 (0,0,0);
				gameObject.transform.rotation = _quaternion;

				_line1.GetComponent<RectTransform> ().sizeDelta = new Vector2 (100f, lineWidth);

				_quaternion.eulerAngles = new Vector3 (0,0,90);
				_arrow.transform.localPosition = new Vector3 (-100,0,0);
				_arrow.transform.rotation = _quaternion;
			}
		}
	}

	private void ObjectMouseOver_(PointerEventData data){
		if(_active == false){
			_mouseOver = true;
			_situation.GetComponent<GrafcetMainClass> ().showInfoText (Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='jump']").InnerText);
		}
	}

	private void ObjectMouseOut_(PointerEventData data){
		if(_mouseOver == true){
			_mouseOver = false;
			_situation.GetComponent<GrafcetMainClass> ().hideInfoText ();
		}
	}

	public void setlookingForNewLeft(){
		if(endObject != null){
			GameObject _nextElement = _situation.GetComponent<GrafcetMainClass> ().getNextElement (endObject);

			while(_nextElement != null && _nextElement.name != startObject.name){
				_nextElement = _situation.GetComponent<GrafcetMainClass> ().getNextElement (_nextElement);
			}

			if (_nextElement != null) {
				//same line, so, not change the left line;
				Debug.Log (_nextElement.name);
				lookingForNewLeft = false;
			} else {
				//search the most left element.
				lookingForNewLeft = true;
			}	
		}
	}

	public void ObjectClick_(PointerEventData data){
		if(_active == true){
			if (objectDragged == false && _active == true) {
				if (_menuActive == false) {
					_situation.GetComponent<GrafcetMainClass> ().deactivateEditWithoutElement (gameObject.name);
					_menuActive = true;
					iTween.ScaleTo (_deleteBt.gameObject, new Vector3 (0.6f, 0.6f, 0.6f), 0.3f);
					if(endObject == null){
						_overA1.enabled = true;
					}
				} else {
					iTween.ScaleTo (_deleteBt.gameObject, new Vector3 (0f, 0f, 0f), 0.3f);
					_menuActive = false;
					if(endObject == null){
						_overA1.enabled = false;
					}
				}
			}
		}
	}

	public void cloneElement(){
		GameObject _clone = Instantiate(_situation.GetComponent<GrafcetMainClass>().jumpPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
		_clone.transform.SetParent (GameObject.Find("Canvas/Grafcet/leftMenu/containerElements/Viewport/Content/space5").transform, false);
		_clone.transform.localPosition = new Vector3 (0,0,0);
	}

	public void initializeElement(){
		_active = true;
		this._id = _situation.GetComponent<GrafcetMainClass> ().jumpModules;
		_situation.GetComponent<GrafcetMainClass> ().jumpModules++;
		this.name = "jump_" + this._id.ToString();

		Quaternion _quaternion = new Quaternion ();
		_quaternion.eulerAngles = new Vector3 (0,0,0);
		gameObject.transform.rotation = _quaternion;

		_bgImage.enabled = false;
		_line1.enabled = true;
		_arrow.enabled = true;

		_quaternion.eulerAngles = new Vector3 (0,0,90);
		_arrow.transform.localPosition = new Vector3 (-100,0,0);
		_arrow.transform.rotation = _quaternion;
	}

	private void returnAction(){
		iTween.RotateTo (gameObject, new Vector3 (0, 0, 0), 0.2f);

		iTween.MoveTo(
			gameObject,
			iTween.Hash(
				"position", startPosition,
				"looktarget", Camera.main,
				"easeType", iTween.EaseType.easeOutExpo,
				"time", 0.2f,
				"islocal",false
			)
		);

		Invoke ("goToInitialState", 0.2f);
	}

	private void goToInitialState(){
		//inScenario = false;
		gameObject.GetComponent<RectTransform> ().SetSiblingIndex (initialIndex);
		gameObject.transform.SetParent (GameObject.Find ("space5").transform, true);
	}

	public void deleteAction(){
		for(int i = this._id + 1; i < _situation.GetComponent<GrafcetMainClass> ().jumpModules; i++){
			GameObject _element = GameObject.Find ("jump_" + i.ToString());
			_element.GetComponent<JumpClass> ().updateNameAndId (i - 1);
		}

		_situation.GetComponent<GrafcetMainClass> ().jumpModules--;

		Destroy (gameObject);
	}

	public void activateLines(){
		_line1.transform.position = new Vector3(objectToFollow.transform.position.x,objectToFollow.transform.position.y, objectToFollow.transform.position.z);
		_line1.transform.localPosition = new Vector3 (2,0,0);

		if (endObject != null) {
			_line1.enabled = true;
			_line2.enabled = true;
			_line3.enabled = true;
			_arrow.enabled = true;

			Quaternion _quaternion = new Quaternion ();
			_quaternion.eulerAngles = new Vector3 (0, 0, -90);
			_arrow.transform.rotation = _quaternion;	
		} else {
			_line1.enabled = true;
			_arrow.enabled = true;
			_line1.GetComponent<RectTransform> ().sizeDelta = new Vector2 (100f, lineWidth);

			_overA1.transform.localPosition = _line1.transform.localPosition;
			_overA1.GetComponent<RectTransform> ().sizeDelta = new Vector2 (100f, 30f);
			_deleteBt.transform.localPosition = new Vector3 (-100, 0, 0);
		}
	}

	public void updateNameAndId(int _id){
		this._id = _id;
		this.name = "jump_" + this._id.ToString ();
	}

	public jumpStructure getJSONData(){
		jumpStructure _result = new jumpStructure ();
		_result.x = gameObject.transform.localPosition.x;
		_result.y = gameObject.transform.localPosition.y;
		_result._id = this._id;
		_result.name = this.name;
		if (startObject != null) {
			_result.startObject = startObject.name;
		} else {
			_result.startObject = "";
		}
		if (endObject != null) {
			_result.endObject = endObject.name;
		} else {
			_result.endObject = "";
		}
		return _result;
	}

	// Update is called once per frame
	void Update () {
		if (endObject == null && objectToFollow != null) {
			gameObject.transform.position = objectToFollow.transform.position;
		}
		else if(endObject != null && objectToFollow != null){
			gameObject.transform.position = objectToFollow.transform.position;

			_arrow.transform.position = new Vector3 (endObject.transform.position.x, endObject.transform.position.y, 0);
			_arrow.transform.localPosition = new Vector3 (_arrow.transform.localPosition.x - 20f, _arrow.transform.localPosition.y + 73f, _arrow.transform.localPosition.z);

			float _widthBase = (_bgImage.transform.localPosition.x - _arrow.transform.localPosition.x);

			float min_x = 0;

			if(lookingForNewLeft == true){
				//looking for the most left element
				min_x = float.MaxValue;

				for(int i = 0; i < _situation.GetComponent<GrafcetMainClass>().stepsModules; i++){
					GameObject _element = GameObject.Find ("step_" + i.ToString());
					if(_element.transform.localPosition.x < min_x){
						min_x = _element.transform.localPosition.x;
					}
				}

				for(int i = 0; i < _situation.GetComponent<GrafcetMainClass>().transitionModules; i++){
					GameObject _element = GameObject.Find ("transition_" + i.ToString());
					if(_element.transform.localPosition.x < min_x){
						min_x = _element.transform.localPosition.x;
					}
				}

				_widthBase = gameObject.transform.localPosition.x - min_x;

			}

			if(_widthBase < 0){
				_widthBase = 0;
			}

			_widthBase = _widthBase + 60f * _id;

			float _line1Width = _widthBase + lineWidthOffset;
			_line1.GetComponent<RectTransform> ().sizeDelta = new Vector2 (_line1Width, lineWidth);

			_line2.transform.localPosition = new Vector3 (_bgImage.transform.localPosition.x - _line1Width + 2, _line1.transform.localPosition.y);

			float _line2Width = _arrow.transform.localPosition.y - _line1.transform.localPosition.y;
			Quaternion _quaternion = new Quaternion();
			if (_line2Width < 0) {
				_line2Width = _line2Width * (-1f);
				_quaternion.eulerAngles = new Vector3 (0, 0, 180);
			} else {
				_quaternion.eulerAngles = new Vector3 (0, 0, 0);
			}
			_line2.transform.rotation = _quaternion;
			_line2.GetComponent<RectTransform> ().sizeDelta = new Vector2 (lineWidth, _line2Width);

			_line3.transform.localPosition = new Vector3 (_bgImage.transform.localPosition.x - _line1Width, _arrow.transform.localPosition.y, 0);
			float _line3Width = Mathf.Abs ((_bgImage.transform.localPosition.x - _line1Width) - _arrow.transform.localPosition.x);
			_line3.GetComponent<RectTransform> ().sizeDelta = new Vector2 (_line3Width, lineWidth);

			_overA1.transform.localPosition = _line1.transform.localPosition;
			_overA1.GetComponent<RectTransform> ().sizeDelta = new Vector2 (_line1Width, 30);
			_overA1.enabled = true;

			_overA2.transform.localPosition = _line2.transform.localPosition;
			_overA2.transform.rotation = _line2.transform.rotation;
			_overA2.GetComponent<RectTransform> ().sizeDelta = new Vector2 (30, _line2Width);
			_overA2.enabled = true;

			_overA3.transform.localPosition = _line3.transform.localPosition;
			_overA3.GetComponent<RectTransform> ().sizeDelta = new Vector2 (_line3Width, 30);
			_overA3.enabled = true;
			if (_line3.transform.localPosition.y >= 0) {
				_deleteBt.transform.localPosition = new Vector3 (_line2.transform.localPosition.x, _line2.GetComponent<RectTransform> ().rect.height / 2f, 0);
			} else {
				_deleteBt.transform.localPosition = new Vector3 (_line2.transform.localPosition.x, -_line2.GetComponent<RectTransform> ().rect.height / 2f, 0);
			}


			if(endObject.name == "step_0"){
				_line4.enabled = true;
				_line4.transform.localPosition = new Vector3 (_arrow.transform.localPosition.x + 20f, _arrow.transform.localPosition.y, 0);
				_line4.GetComponent<RectTransform> ().sizeDelta = new Vector2 (lineWidth, 21);
			}
		}

		if(moveScroll != 0){
			_situation.GetComponent<GrafcetMainClass> ()._workbenchContainer.GetComponent<ScrollRect> ().horizontalNormalizedPosition += moveScroll;
		}
		if (moveScrollY != 0) {
			if (moveScrollY > 0) {
				if(_situation.GetComponent<GrafcetMainClass> ()._workbenchContainer.GetComponent<ScrollRect> ().verticalNormalizedPosition < 1){
					_situation.GetComponent<GrafcetMainClass> ()._workbenchContainer.GetComponent<ScrollRect> ().verticalNormalizedPosition += moveScrollY;
				}
			} else {
				if (_situation.GetComponent<GrafcetMainClass> ()._workbenchContainer.GetComponent<ScrollRect> ().verticalNormalizedPosition > 0) {
					_situation.GetComponent<GrafcetMainClass> ()._workbenchContainer.GetComponent<ScrollRect> ().verticalNormalizedPosition += moveScrollY;
				}
			}
		}
	}
}
