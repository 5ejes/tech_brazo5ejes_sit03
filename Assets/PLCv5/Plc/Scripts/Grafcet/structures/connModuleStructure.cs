﻿using System;

[Serializable]
public struct connModuleStructure
{
	public string name;
	public string _location;
	public int _id;
	public connLineStructure _line;
}