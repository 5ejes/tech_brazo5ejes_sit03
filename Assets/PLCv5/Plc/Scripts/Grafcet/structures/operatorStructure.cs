﻿using System;

[Serializable]
public struct operatorStructure
{
	public string name;
	public string title;
	public int type;
	public int id;
	public int editorType;
	public int typeValue;
	public int index;
	public string value;
	public int index2;
	public string value2;
	public int index3;
	public string value3;
	public bool isInverse;
	public bool constant1;
	public bool constant2;
    public bool isRobotic;
    public operatorStructure[] childOperators;
    public int typeRobotics;
}

