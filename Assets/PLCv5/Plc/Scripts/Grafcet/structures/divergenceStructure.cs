﻿using System;

[Serializable]
public struct divergenceStructure
{
	public float x;
	public float y;
	public int _id;
	public string name;
	public string _type;
	public int _editorType;
	public int _branches;
	public connModuleStructure _u_conn;
	public connModuleStructure[] conns;
}