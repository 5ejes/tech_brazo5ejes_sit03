﻿using System;

[Serializable]
public struct jumpStructure
{
	public float x;
	public float y;
	public int _id;
	public string name;
	public string startObject;
	public string endObject;
}