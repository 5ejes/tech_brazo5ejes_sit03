﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class InitialStepClass : MonoBehaviour {

	public GameObject _situation;

	public bool _active = false;
	public int _id = 0;
	public bool _onSim = false;
	public float _totalWidth = 0;

	public Text _textElement;
	public Image _topConnector;
	public Image _rightConnector;
	public Image _bottomConnector;
	public Button _deleteBt;
	public Button _deleteRightBt;
	public Button _deleteBottomBt;
	public Image _bgElement;

	private float x_offset = 0;
	private float y_offset = 0;

	public bool dragEnabled = true;
	private bool objectDragged;
	private Vector3 initialPosition;
	private Vector3 startPosition;
	private int initialIndex;
	private int startIndex;

	private EventTrigger.Entry entry_3;

	public bool _menuActive = false;

	private bool _mouseOver = false;

	// Use this for initialization
	void Start () {

		_situation = GameObject.Find ("Canvas/Grafcet");

		initialPosition = gameObject.transform.position;

		_textElement.text = "";
		_topConnector.enabled = false;
		_rightConnector.enabled = false;
		_bottomConnector.enabled = false;

		_deleteBt.gameObject.transform.localScale = new Vector3 (0, 0, 0);
		_deleteRightBt.gameObject.transform.localScale = new Vector3 (0, 0, 0);
		_deleteBottomBt.gameObject.transform.localScale = new Vector3 (0, 0, 0);

		_deleteBt.onClick.AddListener (deleteAction);
		_deleteRightBt.onClick.AddListener (deleteRightAction);
		_deleteBottomBt.onClick.AddListener (deleteBottomAction);

		gameObject.AddComponent<EventTrigger> ();

		EventTrigger trigger_Object = gameObject.GetComponent<EventTrigger>();

		EventTrigger.Entry entry_0 = new EventTrigger.Entry();
		entry_0.eventID = EventTriggerType.BeginDrag;
		entry_0.callback.AddListener((data) => { BeginDrag_((PointerEventData)data); });

		EventTrigger.Entry entry_1 = new EventTrigger.Entry();
		entry_1.eventID = EventTriggerType.Drag;
		entry_1.callback.AddListener((data) => { ObjectDrag_((PointerEventData)data); });

		EventTrigger.Entry entry_2 = new EventTrigger.Entry();
		entry_2.eventID = EventTriggerType.EndDrag;
		entry_2.callback.AddListener((data) => { ObjectDrop_((PointerEventData)data); });

		EventTrigger.Entry entry_4 = new EventTrigger.Entry();
		entry_4.eventID = EventTriggerType.PointerEnter;
		entry_4.callback.AddListener((data) => { ObjectMouseOver_((PointerEventData)data); });

		EventTrigger.Entry entry_5 = new EventTrigger.Entry();
		entry_5.eventID = EventTriggerType.PointerExit;
		entry_5.callback.AddListener((data) => { ObjectMouseOut_((PointerEventData)data); });

		trigger_Object.triggers.Add(entry_0);
		trigger_Object.triggers.Add(entry_1);
		trigger_Object.triggers.Add(entry_2);
		trigger_Object.triggers.Add(entry_4);
		trigger_Object.triggers.Add(entry_5);

		gameObject.name = "initial_step_base";

	}

	private void BeginDrag_(PointerEventData data)
	{
		if (dragEnabled == true) {
			objectDragged = true;

			if (_active == false) {

				gameObject.transform.SetParent (GameObject.Find ("Canvas/Grafcet").transform, true);

				startPosition = gameObject.transform.position;
				startIndex = gameObject.GetComponent<RectTransform> ().GetSiblingIndex ();

				gameObject.GetComponent<RectTransform> ().SetAsLastSibling ();

				_situation.GetComponent<GrafcetMainClass> ().activateWorkbenchCollider ();

				x_offset = 0;
				y_offset = 0;
			} else {
				_situation.GetComponent<GrafcetMainClass> ().deactivateEditWithoutElement (gameObject.name);
				Vector3 mousePos = Camera.main.ScreenToWorldPoint (Input.mousePosition);
				x_offset = gameObject.GetComponent<RectTransform> ().position.x - mousePos.x;
				y_offset = gameObject.GetComponent<RectTransform> ().position.y - mousePos.y;
			}
		}
	}

	private void ObjectDrag_(PointerEventData data)
	{
		if (objectDragged == true) {
			Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			gameObject.GetComponent<RectTransform> ().position = new Vector3 (mousePos.x + x_offset, mousePos.y + y_offset, 0);
        }
	}

	private void ObjectDrop_(PointerEventData data)
	{
		if (objectDragged == true) {

			_situation.GetComponent<GrafcetMainClass> ().deactivateWorkbenchCollider ();

			if (_active == false) {
				if (data != null && data.pointerEnter != null && data.pointerEnter.name == "workbenchCollider") {

					gameObject.transform.SetParent ( _situation.GetComponent<GrafcetMainClass>()._workbenchArea.transform , true);

					initializeElement ();

				} else {
					iTween.MoveTo(
						gameObject,
						iTween.Hash(
							"position", startPosition,
							"looktarget", Camera.main,
							"easeType", iTween.EaseType.easeOutExpo,
							"time", 0.2f,
							"islocal",false
						)
					);

					Invoke ("goToInitialState", 0.2f);
				}
			} else {
				if (gameObject.transform.localPosition.x < 52) {
					gameObject.transform.localPosition = new Vector3 (52, gameObject.transform.localPosition.y, gameObject.transform.localPosition.z);
				}
				if (gameObject.transform.localPosition.y > -52) {
					gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x, -52, gameObject.transform.localPosition.z);
				}
			}

			objectDragged = false;
		}
	}

	private void ObjectMouseOver_(PointerEventData data){
		if(_active == false){
			_mouseOver = true;
			_situation.GetComponent<GrafcetMainClass> ().showInfoText (Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='first_step']").InnerText);
		}
	}

	private void ObjectMouseOut_(PointerEventData data){
		if(_mouseOver == true){
			_mouseOver = false;
			_situation.GetComponent<GrafcetMainClass> ().hideInfoText ();
		}
	}

	private void goToInitialState(){
		//inScenario = false;
		gameObject.GetComponent<RectTransform> ().SetSiblingIndex (initialIndex);
		gameObject.transform.SetParent(GameObject.Find("space1").transform, true);
	}

	public void initializeElement(){
		this._active = true;
		this._id = _situation.GetComponent<GrafcetMainClass> ().stepsModules;
		_situation.GetComponent<GrafcetMainClass> ().stepsModules++;
		this.name = "step_" + this._id.ToString ();

		_textElement.text = this._id.ToString ();

		entry_3 = new EventTrigger.Entry();
		entry_3.eventID = EventTriggerType.PointerClick;
		entry_3.callback.AddListener((data) => { ObjectClicked_((PointerEventData)data); });
		EventTrigger trigger_Object = gameObject.GetComponent<EventTrigger>();
		trigger_Object.triggers.Add(entry_3);

		initialAction();
	}

	public void ObjectClicked_(PointerEventData data){
		if (objectDragged == false && _active == true) {
			if (_menuActive == false) {
				_situation.GetComponent<GrafcetMainClass> ().deactivateEditWithoutElement (gameObject.name);
				_menuActive = true;
				dragEnabled = false;
				iTween.ScaleTo (_deleteBt.gameObject, new Vector3 (0.6f, 0.6f, 0.6f), 0.3f);
				if(_rightConnector.GetComponent<connModuleClass>().lineIsBusy()){
					iTween.ScaleTo (_deleteRightBt.gameObject, new Vector3 (0.4f, 0.4f, 0.4f), 0.3f);
				}
				if(_bottomConnector.GetComponent<connModuleClass>().lineIsBusy()){
					iTween.ScaleTo (_deleteBottomBt.gameObject, new Vector3 (0.4f, 0.4f, 0.4f), 0.3f);
				}
			} else {
				iTween.ScaleTo (_deleteBt.gameObject, new Vector3 (0f, 0f, 0f), 0.3f);
				iTween.ScaleTo (_deleteRightBt.gameObject, new Vector3 (0f, 0f, 0f), 0.3f);
				iTween.ScaleTo (_deleteBottomBt.gameObject, new Vector3 (0f, 0f, 0f), 0.3f);
				_menuActive = false;
				dragEnabled = true;
			}
		}

	}

	public void deleteAction(){
		if (_situation.GetComponent<GrafcetMainClass> ().stepsModules > 1) {
			iTween.ScaleTo (_deleteBt.gameObject, new Vector3 (0f, 0f, 0f), 0.3f);
			iTween.ScaleTo (_deleteRightBt.gameObject, new Vector3 (0f, 0f, 0f), 0.3f);
			iTween.ScaleTo (_deleteBottomBt.gameObject, new Vector3 (0f, 0f, 0f), 0.3f);
			_menuActive = false;
			dragEnabled = true;
			_situation.GetComponent<GrafcetMainClass> ().showInfoAlert (Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='message_first_step_cant_delete']").InnerText);
		} else {
			for (int i = 0; i < _situation.GetComponent<GrafcetMainClass> ().jumpModules; i++) {
				GameObject _elementJump = GameObject.Find ("jump_" + i.ToString ());
				if (_elementJump.GetComponent<JumpClass> ().endObject.GetInstanceID() == gameObject.GetInstanceID()) {
					_elementJump.GetComponent<JumpClass> ().deleteAction ();
					break;
				}
			}
			_situation.GetComponent<GrafcetMainClass> ().stepsModules--;
			cloneElement ();
			deleteBottomAction ();
			deleteRightAction ();
			Destroy (gameObject);
		}
	}

	private void deleteRightAction(){
		if(_rightConnector.GetComponent<connModuleClass>()._line){
			GameObject _remoteStartElement = _rightConnector.GetComponent<connModuleClass> ()._line.GetComponent<connLineClass> ().startConnector;
			GameObject _remoteEndElement = _rightConnector.GetComponent<connModuleClass> ()._line.GetComponent<connLineClass> ().endConnector;

			if (_rightConnector.name == _remoteStartElement.name) {
				_remoteEndElement.GetComponent<connModuleClass> ()._parentElement.GetComponent<ActionClass> ().dragEnabled = true;
				Destroy (_remoteEndElement.GetComponent<connModuleClass> ()._line);
				_remoteEndElement.GetComponent<connModuleClass> ()._line = null;

			} else {
				_remoteStartElement.GetComponent<connModuleClass> ()._parentElement.GetComponent<ActionClass> ().dragEnabled = true;
				Destroy (_remoteStartElement.GetComponent<connModuleClass> ()._line);
				_remoteStartElement.GetComponent<connModuleClass> ()._line = null;
			}

			Destroy (_rightConnector.GetComponent<connModuleClass>()._line);
			_rightConnector.GetComponent<connModuleClass> ()._line = null;
			iTween.ScaleTo (_deleteRightBt.gameObject, new Vector3 (0f, 0f, 0f), 0.3f);
		}
	}

	private void deleteBottomAction(){
		if(_bottomConnector.GetComponent<connModuleClass>()._line){
			GameObject _remoteStartElement = _bottomConnector.GetComponent<connModuleClass> ()._line.GetComponent<connLineClass> ().startConnector;
			GameObject _remoteEndElement = _bottomConnector.GetComponent<connModuleClass> ()._line.GetComponent<connLineClass> ().endConnector;

			if(_bottomConnector.name == _remoteStartElement.name){
				if(_remoteEndElement.GetComponent<connModuleClass> ()._parentElement.name.IndexOf("transition") == 0){
					_remoteEndElement.GetComponent<connModuleClass> ()._parentElement.GetComponent<TransitionClass> ().dragEnabled = true;
				} else if(_remoteEndElement.GetComponent<connModuleClass> ()._parentElement.name.IndexOf("divergence") == 0){
					_remoteEndElement.GetComponent<connModuleClass> ()._parentElement.GetComponent<DivergenceClass> ().dragEnabled = true;
				} else if(_remoteEndElement.GetComponent<connModuleClass> ()._parentElement.name.IndexOf("convergence") == 0){
					if(_remoteEndElement.GetComponent<connModuleClass> ()._parentElement.GetComponent<ConvergenceClass> ().countConnected == 1){
						_remoteEndElement.GetComponent<connModuleClass> ()._parentElement.GetComponent<ConvergenceClass> ().dragEnabled = true;
					}
				}
				Destroy (_remoteEndElement.GetComponent<connModuleClass>()._line);
				_remoteEndElement.GetComponent<connModuleClass> ()._line = null;
			} else {
				if(_remoteStartElement.GetComponent<connModuleClass> ()._parentElement.name.IndexOf("transition") == 0){
					_remoteStartElement.GetComponent<connModuleClass> ()._parentElement.GetComponent<TransitionClass> ().dragEnabled = true;
				} else if(_remoteStartElement.GetComponent<connModuleClass> ()._parentElement.name.IndexOf("divergence") == 0){
					_remoteStartElement.GetComponent<connModuleClass> ()._parentElement.GetComponent<DivergenceClass> ().dragEnabled = true;
				} else if(_remoteStartElement.GetComponent<connModuleClass> ()._parentElement.name.IndexOf("convergence") == 0){
					if(_remoteStartElement.GetComponent<connModuleClass> ()._parentElement.GetComponent<ConvergenceClass> ().countConnected == 1){
						_remoteStartElement.GetComponent<connModuleClass> ()._parentElement.GetComponent<ConvergenceClass> ().dragEnabled = true;
					}
				}
				Destroy (_remoteStartElement.GetComponent<connModuleClass> ()._line);
				_remoteStartElement.GetComponent<connModuleClass> ()._line = null;
			}

			Destroy (_bottomConnector.GetComponent<connModuleClass>()._line);
			_bottomConnector.GetComponent<connModuleClass> ()._line = null;
			iTween.ScaleTo (_deleteBottomBt.gameObject, new Vector3 (0f, 0f, 0f), 0.3f);
		}
	}

	private void cloneElement(){
		GameObject _clone = Instantiate(_situation.GetComponent<GrafcetMainClass>().initialStepPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
		_clone.transform.SetParent (GameObject.Find("Canvas/Grafcet/leftMenu/containerElements/Viewport/Content/space1").transform, false);
		_clone.transform.localPosition = new Vector3 (0,0,0);
	}

	public void initialAction(){
		//TODO: check if element is used;
		_rightConnector.enabled = true;
		_topConnector.enabled = false;
		//TODO: check if element is used;
		_bottomConnector.enabled = true;

	}

	public stepStructure getJSONData(){
		stepStructure _result = new stepStructure ();
		_result.x = gameObject.transform.localPosition.x;
		_result.y = gameObject.transform.localPosition.y;
		_result._id = this._id;
		_result.name = this.name;
		_result._title = this._textElement.text;
		_result._onSim = this._onSim;
		_result._r_conn = _rightConnector.GetComponent<connModuleClass> ().getJSONData ();
		_result._u_conn = _topConnector.GetComponent<connModuleClass> ().getJSONData ();
		_result._d_conn = _bottomConnector.GetComponent<connModuleClass> ().getJSONData ();
		return _result;
	}

	// Update is called once per frame
	void Update () {
		if (_active == true) {

			if(objectDragged == false){
				if (gameObject.transform.localPosition.x < 52) {
					gameObject.transform.localPosition = new Vector3 (52, gameObject.transform.localPosition.y, gameObject.transform.localPosition.z);
				}
				if (gameObject.transform.localPosition.y > -52) {
					gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x, -52, gameObject.transform.localPosition.z);
				}
			}

			bool _outputActive = false;
			if(_situation.GetComponent<GrafcetMainClass>().grafcetEngine != null && _situation.GetComponent<GrafcetMainClass>().simulationRunning == true){
				SimpleJSON.JSONNode _jsonData = _situation.GetComponent<GrafcetMainClass> ().grafcetEngine.getStepObjectPerName (this.name);
				if (_jsonData != null && _jsonData ["_onSim"].AsBool == true) {
					_outputActive = true;
					_bgElement.color = new Color (0, 113f / 255f, 183f / 255f, 90f / 255f);
				} else {
					_outputActive = false;
					_bgElement.color = new Color (1, 1, 1, 1);
				}
			}

			float lineWidth = 40f;
			_totalWidth = 52f;

			connModuleClass _rightSequence = _rightConnector.GetComponent<connModuleClass> ();
			while(_rightSequence.lineIsBusy()){
				_totalWidth = _totalWidth + lineWidth + 208f;
				_rightSequence = _rightSequence._line.GetComponent<connLineClass> ().endConnector.GetComponent<connModuleClass> ()._parentElement.GetComponent<ActionClass> ()._rightConnector.GetComponent<connModuleClass> ();
				if(_rightSequence._parentElement.GetComponent<ActionClass>()){
					if (_outputActive == true) {
						_rightSequence._parentElement.GetComponent<ActionClass> ()._bgElement.color = new Color (0, 113f / 255f, 183f / 255f, 90f / 255f);
					} else {
						_rightSequence._parentElement.GetComponent<ActionClass> ()._bgElement.color = new Color (1, 1, 1, 1);
					}
				}
			}
		}
	}
}
