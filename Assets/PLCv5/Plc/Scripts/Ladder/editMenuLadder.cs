﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class editMenuLadder : MonoBehaviour {

	public Button addBt;
	public Button editBt;
	public Button deleteBt;
	public Button increaseBranch;

	private GameObject elementToEdit;
	private GameObject _situation;

	private Vector3 deleteInitialPosition;
	private Vector3 addRightInitialPosition;
	// Use this for initialization
	void Start () {

		_situation = GameObject.Find ("Canvas/Ladder");

		addBt.transform.localScale = new Vector3 (0,0,0);
		editBt.transform.localScale = new Vector3 (0,0,0);
		deleteBt.transform.localScale = new Vector3 (0,0,0);
		increaseBranch.transform.localScale = new Vector3 (0,0,0);

		addBt.onClick.AddListener (addBranchAction);
		editBt.onClick.AddListener (editAction);
		deleteBt.onClick.AddListener (deleteAction);
		increaseBranch.onClick.AddListener (addBranchRightAction);

		deleteInitialPosition = deleteBt.transform.localPosition;
		addRightInitialPosition = increaseBranch.transform.localPosition;
	}

	public void configureButtonsAndShow(GameObject elementToEditReceived){
		elementToEdit = elementToEditReceived;

		float xOffset = 27f;
		float yOffset = 20f;

		deleteBt.transform.localPosition = deleteInitialPosition;
		increaseBranch.transform.localPosition = addRightInitialPosition;

		if (elementToEdit.GetComponent<lineObject> ().lineNumber == 0) {
			if (elementToEdit.GetComponent<lineObject> ().typeElement != dragElementLadder._type_elements.none) {
				addBt.transform.localScale = new Vector3 (0,0,0);
				iTween.ScaleTo (editBt.gameObject, new Vector3 (1, 1, 1), 0.2f);
				iTween.ScaleTo (deleteBt.gameObject, new Vector3 (1, 1, 1), 0.2f);
				increaseBranch.transform.localScale = new Vector3 (0,0,0);
				xOffset = xOffset - 72f;
			} else {
				addBt.transform.localScale = new Vector3 (0,0,0);
				editBt.transform.localScale = new Vector3 (0,0,0);
				deleteBt.transform.localScale = new Vector3 (0,0,0);
				increaseBranch.transform.localScale = new Vector3 (0,0,0);
			}
		} else if (elementToEdit.GetComponent<lineObject> ().lineNumber == 12) {
			if (elementToEdit.GetComponent<lineObject> ().typeElement != dragElementLadder._type_elements.none) {
				if (elementToEdit.GetComponent<lineObject> ().branchElement == null) {
					iTween.ScaleTo (addBt.gameObject, new Vector3 (1, 1, 1), 0.2f);
				} else {
					addBt.transform.localScale = new Vector3 (0,0,0);
				}
				iTween.ScaleTo (editBt.gameObject, new Vector3 (1, 1, 1), 0.2f);
				iTween.ScaleTo (deleteBt.gameObject, new Vector3 (1, 1, 1), 0.2f);
				increaseBranch.transform.localScale = new Vector3 (0,0,0);
				xOffset = -44f;
			} else {
				iTween.ScaleTo (addBt.gameObject, new Vector3 (1, 1, 1), 0.2f);
				if (elementToEdit.GetComponent<lineObject> ().branchLevel > 0) {
					if (elementToEdit.GetComponent<lineObject> ().branchElement == null) {
						iTween.ScaleTo (deleteBt.gameObject, new Vector3 (1, 1, 1), 0.2f);
					} else {
						deleteBt.transform.localScale = new Vector3 (0,0,0);
					}
				} else {
					deleteBt.transform.localScale = new Vector3 (0,0,0);
				}
				editBt.transform.localScale = new Vector3 (0,0,0);
				increaseBranch.transform.localScale = new Vector3 (0,0,0);

				deleteBt.transform.localPosition = new Vector3 (deleteBt.transform.localPosition.x - 72f, deleteBt.transform.localPosition.y, deleteBt.transform.localPosition.z);
			}
		} else {
			if (elementToEdit.GetComponent<lineObject> ().typeElement != dragElementLadder._type_elements.none) {
				if (elementToEdit.GetComponent<lineObject> ().branchElement == null) {
					iTween.ScaleTo (addBt.gameObject, new Vector3 (1, 1, 1), 0.2f);
				} else {
					addBt.transform.localScale = new Vector3 (0,0,0);
					xOffset = xOffset - 72f;
				}
				iTween.ScaleTo (editBt.gameObject, new Vector3 (1, 1, 1), 0.2f);
				iTween.ScaleTo (deleteBt.gameObject, new Vector3 (1, 1, 1), 0.2f);
				if (elementToEdit.GetComponent<lineObject> ().branchLevel > 0 && elementToEdit.GetComponent<lineObject> ().rightBranchElement == null && elementToEdit.GetComponent<lineObject> ().lineNumber < 11) {
					iTween.ScaleTo (increaseBranch.gameObject, new Vector3 (1, 1, 1), 0.2f);
				} else {
					increaseBranch.transform.localScale = new Vector3 (0,0,0);
				}
			} else {
				float increaseOffset = 0;
				if (elementToEdit.GetComponent<lineObject> ().branchElement == null) {
					iTween.ScaleTo (addBt.gameObject, new Vector3 (1, 1, 1), 0.2f);
				} else {
					addBt.transform.localScale = new Vector3 (0,0,0);
					increaseOffset = increaseOffset + 72;
				}
				if (elementToEdit.GetComponent<lineObject> ().branchLevel > 0) {
					if (elementToEdit.GetComponent<lineObject> ().branchElement == null) {
						iTween.ScaleTo (deleteBt.gameObject, new Vector3 (1, 1, 1), 0.2f);
					} else {
						deleteBt.transform.localScale = new Vector3 (0,0,0);
						increaseOffset = increaseOffset + 72;
					}
					if (elementToEdit.GetComponent<lineObject> ().rightBranchElement == null && elementToEdit.GetComponent<lineObject> ().lineNumber < 11) {
						if (elementToEdit.GetComponent<lineObject> ().parentLineGroup.GetComponent<lineGroupObject> ().getElementPerLineAndLevel (elementToEdit.GetComponent<lineObject> ().lineNumber + 1, elementToEdit.GetComponent<lineObject> ().branchLevel - 1) != null) {
							iTween.ScaleTo (increaseBranch.gameObject, new Vector3 (1, 1, 1), 0.2f);	
						} else {
							increaseBranch.transform.localScale = new Vector3 (0,0,0);
						}
					} else {
						increaseBranch.transform.localScale = new Vector3 (0,0,0);
					}
				} else {
					deleteBt.transform.localScale = new Vector3 (0,0,0);
				}
				deleteBt.transform.localPosition = new Vector3 (deleteBt.transform.localPosition.x - 72f, deleteBt.transform.localPosition.y, deleteBt.transform.localPosition.z);
				increaseBranch.transform.localPosition = new Vector3 (increaseBranch.transform.localPosition.x - 72f - increaseOffset, increaseBranch.transform.localPosition.y, increaseBranch.transform.localPosition.z);
				editBt.transform.localScale = new Vector3 (0,0,0);
			}
		}

		gameObject.transform.SetAsLastSibling ();
		gameObject.transform.position = elementToEdit.transform.position;
		gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x + xOffset, gameObject.transform.localPosition.y + yOffset, gameObject.transform.localPosition.z);
		//Debug.Log ( gameObject.GetComponent<RectTransform>().anchoredPosition );
		//Debug.Log (_situation.GetComponent<LadderMainClass> ()._workbenchContainer.GetComponent<RectTransform> ().rect.height);
		if ((Mathf.Abs (gameObject.GetComponent<RectTransform>().anchoredPosition.y) - 132) > (_situation.GetComponent<LadderMainClass> ()._workbenchContainer.GetComponent<RectTransform> ().rect.height - 30f)) {
			gameObject.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (gameObject.GetComponent<RectTransform> ().anchoredPosition.x, -(_situation.GetComponent<LadderMainClass> ()._workbenchContainer.GetComponent<RectTransform> ().rect.height - 30f + 132f));
		}
	}

	public void hideButtons(){
		iTween.ScaleTo (addBt.gameObject, new Vector3 (0, 0, 0), 0.2f);
		iTween.ScaleTo (editBt.gameObject, new Vector3 (0, 0, 0), 0.2f);
		iTween.ScaleTo (deleteBt.gameObject, new Vector3 (0, 0, 0), 0.2f);
		iTween.ScaleTo (increaseBranch.gameObject, new Vector3 (0, 0, 0), 0.2f);
		Invoke ("goToOutside", 0.21f);
	}

	private void goToOutside(){
		gameObject.transform.position = new Vector3 (-100,0,0);
	}

	private void addBranchAction(){
		elementToEdit.GetComponent<lineObject> ().addBranch ();
		hideButtons ();
		_situation.GetComponent<LadderMainClass> ().deactivateBackground ();
	}

	private void editAction(){
		hideButtons ();
		_situation.GetComponent<LadderMainClass> ().editorBox.GetComponent<editorBox> ().configureBoxAndShow (elementToEdit);
	}

	private void deleteAction(){
		elementToEdit.GetComponent<lineObject> ().resetObject ();
	}

	private void addBranchRightAction(){
		elementToEdit.GetComponent<lineObject> ().addRightBranch ();
		hideButtons ();
		_situation.GetComponent<LadderMainClass> ().deactivateBackground ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
