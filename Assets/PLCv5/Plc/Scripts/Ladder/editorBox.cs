﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class editorBox : MonoBehaviour {

	public GameObject _situation;

	public GameObject basicBox;
	public GameObject complexBox;
	public GameObject PIDBox;

	public Text basicTitle;
	public Dropdown basicDdl1;

	public Text complexTitle;
	public Image complexBgDdl1;
	public Image complexBgConstant1;
	public Dropdown complexDdl1;
	public Text label1;
	public InputField inputText1;

	public Image complexBgDdl2;
	public Image complexBgConstant2;
	public Dropdown complexDdl2;
	public Text label2;
	public InputField inputText2;

	public Image complexBgDdl3;
	public Dropdown complexDdl3;
	public Text label3;

	public Toggle constantToggle1;
	public Toggle constantToggle2;

	public Text PIDTitle;
	public Text labelPID1;
	public Text labelPID2;
	public Text labelPID3;
	public Text labelPID4;
	public Text labelPID5;
	public Text labelPID6;
	public Dropdown PIDDdl1;
	public Dropdown PIDDdl2;
	public InputField inputTextPID1;
	public InputField inputTextPID2;
	public InputField inputTextPID3;
	public InputField inputTextPID4;

	public bool editorActive = false;
	private GameObject objectToEdit;
	// Use this for initialization
	void Start () {

		_situation = GameObject.Find ("Canvas/Ladder");

		basicBox.SetActive (false);
		complexBox.SetActive (false);
		PIDBox.SetActive (false);
	}

	public void configureBoxAndShow(GameObject objectToEditReceived){
		objectToEdit = objectToEditReceived;

		float xOffset = 0;
		float yOffset = 0;

		switch(objectToEdit.GetComponent<lineObject>().typeElement){
		case dragElementLadder._type_elements.open_contact:
		case dragElementLadder._type_elements.close_contact:
			basicBox.SetActive (true);
			complexBox.SetActive (false);
			PIDBox.SetActive (false);
			basicTitle.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='ladder_input']").InnerText;

			List<Dropdown.OptionData> ddl1List = new List<Dropdown.OptionData> ();

			for (int i = 0; i < _situation.GetComponent<LadderMainClass> ().digitalInputs; i++) {
				Dropdown.OptionData ddl1Object = new Dropdown.OptionData ();
				ddl1Object.text = "I" + i.ToString ();
				ddl1List.Add (ddl1Object);
			}

			for (int i = 0; i < _situation.GetComponent<LadderMainClass> ().digitalOutputs; i++) {
				Dropdown.OptionData ddl1Object = new Dropdown.OptionData ();
				ddl1Object.text = "O" + i.ToString ();
				ddl1List.Add (ddl1Object);
			}

			for (int i = 0; i < _situation.GetComponent<LadderMainClass> ().counterElements; i++) {
				Dropdown.OptionData ddl1Object = new Dropdown.OptionData ();
				ddl1Object.text = "C" + i.ToString ();
				ddl1List.Add (ddl1Object);
			}

			for (int i = 0; i < _situation.GetComponent<LadderMainClass> ().timerElements; i++) {
				Dropdown.OptionData ddl1Object = new Dropdown.OptionData ();
				ddl1Object.text = "T" + i.ToString ();
				ddl1List.Add (ddl1Object);
			}

			for (int i = 0; i < _situation.GetComponent<LadderMainClass> ().memoryElements; i++) {
				for (int j = 0; j < 8; j++) {
					Dropdown.OptionData ddl1Object = new Dropdown.OptionData ();
					ddl1Object.text = "M" + i.ToString () + "," + j.ToString ();
					ddl1List.Add (ddl1Object);	
				}
			}

			basicDdl1.options = ddl1List;

			if (objectToEdit.GetComponent<lineObject> ().value1 != "") {
				if (objectToEdit.GetComponent<lineObject> ().index1 == -1) {
					basicDdl1.value = basicDdl1.options.FindIndex (option => option.text == objectToEdit.GetComponent<lineObject> ().value1);
				} else {
					basicDdl1.value = objectToEdit.GetComponent<lineObject> ().index1;
				}
			} else {
				basicDdl1.value = 0;
			}

			xOffset = 63f;
			yOffset = -58f;
			break;
		case dragElementLadder._type_elements.open_coil:
		case dragElementLadder._type_elements.close_coil:
		case dragElementLadder._type_elements.reset_coil:
		case dragElementLadder._type_elements.set_coil:
			basicBox.SetActive (true);
			complexBox.SetActive (false);
			PIDBox.SetActive (false);
			basicTitle.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='ladder_output']").InnerText;

			List<Dropdown.OptionData> ddl2List = new List<Dropdown.OptionData> ();

			for (int i = 0; i < _situation.GetComponent<LadderMainClass> ().digitalOutputs; i++) {
				Dropdown.OptionData ddl1Object = new Dropdown.OptionData ();
				ddl1Object.text = "O" + i.ToString ();
				ddl2List.Add (ddl1Object);
			}

			for (int i = 0; i < _situation.GetComponent<LadderMainClass> ().memoryElements; i++) {
				for (int j = 0; j < 8; j++) {
					Dropdown.OptionData ddl1Object = new Dropdown.OptionData ();
					ddl1Object.text = "M" + i.ToString () + "," + j.ToString ();
					ddl2List.Add (ddl1Object);	
				}
			}

			basicDdl1.options = ddl2List;

			if (objectToEdit.GetComponent<lineObject> ().value1 != "") {
				if (objectToEdit.GetComponent<lineObject> ().index1 == -1) {
					basicDdl1.value = basicDdl1.options.FindIndex (option => option.text == objectToEdit.GetComponent<lineObject> ().value1);
				} else {
					basicDdl1.value = objectToEdit.GetComponent<lineObject> ().index1;
				}
			} else {
				basicDdl1.value = 0;
			}

			xOffset = 63f;
			yOffset = -58f;
			break;
		case dragElementLadder._type_elements.reset:
			basicBox.SetActive (true);
			complexBox.SetActive (false);
			PIDBox.SetActive (false);
			basicTitle.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='ladder_output']").InnerText;

			List<Dropdown.OptionData> ddl7List = new List<Dropdown.OptionData> ();

			for (int i = 0; i < _situation.GetComponent<LadderMainClass> ().counterElements; i++) {
				Dropdown.OptionData ddl1Object = new Dropdown.OptionData ();
				ddl1Object.text = "C" + i.ToString ();
				ddl7List.Add (ddl1Object);
			}

			for (int i = 0; i < _situation.GetComponent<LadderMainClass> ().timerElements; i++) {
				Dropdown.OptionData ddl1Object = new Dropdown.OptionData ();
				ddl1Object.text = "T" + i.ToString ();
				ddl7List.Add (ddl1Object);
			}

			basicDdl1.options = ddl7List;

			if (objectToEdit.GetComponent<lineObject> ().value1 != "") {
				if (objectToEdit.GetComponent<lineObject> ().index1 == -1) {
					basicDdl1.value = basicDdl1.options.FindIndex (option => option.text == objectToEdit.GetComponent<lineObject> ().value1);
				} else {
					basicDdl1.value = objectToEdit.GetComponent<lineObject> ().index1;
				}
			} else {
				basicDdl1.value = 0;
			}

			xOffset = 63f;
			yOffset = -58f;
			break;
		case dragElementLadder._type_elements.add:
		case dragElementLadder._type_elements.subs:
		case dragElementLadder._type_elements.prod:
		case dragElementLadder._type_elements.division:
		case dragElementLadder._type_elements.and:
		case dragElementLadder._type_elements.or:
		case dragElementLadder._type_elements.xor:
			basicBox.SetActive (false);
			complexBox.SetActive (true);
			PIDBox.SetActive (false);
			constantToggle1.gameObject.SetActive (true);
			constantToggle2.gameObject.SetActive (true);

			if (objectToEdit.GetComponent<lineObject> ().typeElement == dragElementLadder._type_elements.add) {
				complexTitle.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='ladder_add']").InnerText.ToUpper ();	
			} else if (objectToEdit.GetComponent<lineObject> ().typeElement == dragElementLadder._type_elements.subs) {
				complexTitle.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='ladder_subs']").InnerText.ToUpper ();	
			} else if (objectToEdit.GetComponent<lineObject> ().typeElement == dragElementLadder._type_elements.prod) {
				complexTitle.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='ladder_prod']").InnerText.ToUpper ();	
			} else if (objectToEdit.GetComponent<lineObject> ().typeElement == dragElementLadder._type_elements.division) {
				complexTitle.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='ladder_division']").InnerText.ToUpper ();	
			} else if (objectToEdit.GetComponent<lineObject> ().typeElement == dragElementLadder._type_elements.and) {
				complexTitle.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='ladder_options_and']").InnerText.ToUpper ();	
			} else if (objectToEdit.GetComponent<lineObject> ().typeElement == dragElementLadder._type_elements.or) {
				complexTitle.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='ladder_options_or']").InnerText.ToUpper ();	
			} else if (objectToEdit.GetComponent<lineObject> ().typeElement == dragElementLadder._type_elements.xor) {
				complexTitle.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='ladder_options_xor']").InnerText.ToUpper ();	
			}

			List<Dropdown.OptionData> ddl3List = new List<Dropdown.OptionData> ();
			List<Dropdown.OptionData> ddl3AList = new List<Dropdown.OptionData> ();

			for (int i = 0; i < _situation.GetComponent<LadderMainClass> ().memoryElements; i++) {
				Dropdown.OptionData ddl1Object = new Dropdown.OptionData ();
				ddl1Object.text = "M" + i.ToString ();
				ddl3List.Add (ddl1Object);
			}

			for (int i = 0; i < _situation.GetComponent<LadderMainClass> ().memoryNElements; i++) {
				Dropdown.OptionData ddl1Object = new Dropdown.OptionData ();
				ddl1Object.text = "N" + i.ToString ();
				ddl3List.Add (ddl1Object);
			}

			for (int i = 0; i < _situation.GetComponent<LadderMainClass> ().counterElements; i++) {
				Dropdown.OptionData ddl1Object = new Dropdown.OptionData ();
				ddl1Object.text = "C" + i.ToString ();
				ddl3List.Add (ddl1Object);
			}

			for (int i = 0; i < _situation.GetComponent<LadderMainClass> ().timerElements; i++) {
				Dropdown.OptionData ddl1Object = new Dropdown.OptionData ();
				ddl1Object.text = "T" + i.ToString ();
				ddl3List.Add (ddl1Object);
			}

			for (int i = 0; i < _situation.GetComponent<LadderMainClass> ().analogInputs; i++) {
				Dropdown.OptionData ddl1Object = new Dropdown.OptionData ();
				ddl1Object.text = "AI" + i.ToString ();
				ddl3List.Add (ddl1Object);
			}

			complexDdl1.options = ddl3List;
			complexDdl2.options = ddl3List;

			for (int i = 0; i < _situation.GetComponent<LadderMainClass> ().memoryElements; i++) {
				Dropdown.OptionData ddl1Object = new Dropdown.OptionData ();
				ddl1Object.text = "M" + i.ToString ();
				ddl3AList.Add (ddl1Object);
			}

			for (int i = 0; i < _situation.GetComponent<LadderMainClass> ().memoryNElements; i++) {
				Dropdown.OptionData ddl1Object = new Dropdown.OptionData ();
				ddl1Object.text = "N" + i.ToString ();
				ddl3AList.Add (ddl1Object);
			}

			for (int i = 0; i < _situation.GetComponent<LadderMainClass> ().analogOutputs; i++) {
				Dropdown.OptionData ddl1Object = new Dropdown.OptionData ();
				ddl1Object.text = "AO" + i.ToString ();
				ddl3AList.Add (ddl1Object);
			}

			complexDdl3.options = ddl3AList;

			label1.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='ladder_complex_source_1']").InnerText;
			label2.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='ladder_complex_source_2']").InnerText;
			label3.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='ladder_complex_target']").InnerText;

			constantToggle1.GetComponentInChildren<Text> ().text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='constant']").InnerText;
			constantToggle2.GetComponentInChildren<Text> ().text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='constant']").InnerText;

			if (objectToEdit.GetComponent<lineObject> ().constant1 == true) {
				constantToggle1.isOn = true;
				inputText1.text = objectToEdit.GetComponent<lineObject> ().value1;

				complexDdl1.gameObject.SetActive (false);
				complexBgConstant1.enabled = true;
				inputText1.gameObject.SetActive (true);
			} else {
				constantToggle1.isOn = false;
				complexDdl1.gameObject.SetActive (true);
				complexBgConstant1.enabled = false;
				inputText1.gameObject.SetActive (false);

				if (objectToEdit.GetComponent<lineObject> ().value1 != "") {
					if (objectToEdit.GetComponent<lineObject> ().index1 == -1) {
						complexDdl1.value = complexDdl1.options.FindIndex (option => option.text == objectToEdit.GetComponent<lineObject> ().value1);
					} else {
						complexDdl1.value = objectToEdit.GetComponent<lineObject> ().index1;
					}
				} else {
					complexDdl1.value = 0;
				}	
			}

			if (objectToEdit.GetComponent<lineObject> ().constant2 == true) {
				constantToggle2.isOn = true;
				inputText2.text = objectToEdit.GetComponent<lineObject> ().value2;

				complexDdl2.gameObject.SetActive (false);
				complexBgDdl2.enabled = false;
				complexBgConstant2.enabled = true;
				inputText2.gameObject.SetActive (true);
			} else {
				constantToggle2.isOn = false;
				complexDdl2.gameObject.SetActive (true);
				complexBgDdl2.enabled = true;
				complexBgConstant2.enabled = false;
				inputText2.gameObject.SetActive (false);

				if (objectToEdit.GetComponent<lineObject> ().value2 != "") {
					if (objectToEdit.GetComponent<lineObject> ().index2 == -1) {
						complexDdl2.value = complexDdl2.options.FindIndex (option => option.text == objectToEdit.GetComponent<lineObject> ().value2);
					} else {
						complexDdl2.value = objectToEdit.GetComponent<lineObject> ().index2;
					}
				} else {
					complexDdl2.value = 0;
				}	
			}

			if (objectToEdit.GetComponent<lineObject> ().value3 != "") {
				if (objectToEdit.GetComponent<lineObject> ().index3 == -1) {
					complexDdl3.value = complexDdl3.options.FindIndex (option => option.text == objectToEdit.GetComponent<lineObject> ().value3);
				} else {
					complexDdl3.value = objectToEdit.GetComponent<lineObject> ().index3;
				}
			} else {
				complexDdl3.value = 0;
			}

			inputText1.GetComponent<InputField> ().characterValidation = InputField.CharacterValidation.Integer;
			inputText2.GetComponent<InputField> ().characterValidation = InputField.CharacterValidation.Integer;

			complexBgDdl3.enabled = true;
			complexDdl3.gameObject.SetActive (true);

			xOffset = 43f;
			yOffset = -58f;

			break;
		case dragElementLadder._type_elements.equal:
		case dragElementLadder._type_elements.different:
		case dragElementLadder._type_elements.great:
		case dragElementLadder._type_elements.great_equal:
		case dragElementLadder._type_elements.less:
		case dragElementLadder._type_elements.less_equal:
			basicBox.SetActive (false);
			complexBox.SetActive (true);
			PIDBox.SetActive (false);

			constantToggle1.gameObject.SetActive (true);
			constantToggle2.gameObject.SetActive (true);

			complexTitle.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='ladder_options']").InnerText.ToUpper ();	

			List<Dropdown.OptionData> ddl4List = new List<Dropdown.OptionData> ();

			for (int i = 0; i < _situation.GetComponent<LadderMainClass> ().memoryElements; i++) {
				Dropdown.OptionData ddl1Object = new Dropdown.OptionData ();
				ddl1Object.text = "M" + i.ToString ();
				ddl4List.Add (ddl1Object);
			}

			for (int i = 0; i < _situation.GetComponent<LadderMainClass> ().memoryNElements; i++) {
				Dropdown.OptionData ddl1Object = new Dropdown.OptionData ();
				ddl1Object.text = "N" + i.ToString ();
				ddl4List.Add (ddl1Object);
			}

			for (int i = 0; i < _situation.GetComponent<LadderMainClass> ().counterElements; i++) {
				Dropdown.OptionData ddl1Object = new Dropdown.OptionData ();
				ddl1Object.text = "C" + i.ToString ();
				ddl4List.Add (ddl1Object);
			}

			for (int i = 0; i < _situation.GetComponent<LadderMainClass> ().timerElements; i++) {
				Dropdown.OptionData ddl1Object = new Dropdown.OptionData ();
				ddl1Object.text = "T" + i.ToString ();
				ddl4List.Add (ddl1Object);
			}

			for (int i = 0; i < _situation.GetComponent<LadderMainClass> ().analogInputs; i++) {
				Dropdown.OptionData ddl1Object = new Dropdown.OptionData ();
				ddl1Object.text = "AI" + i.ToString ();
				ddl4List.Add (ddl1Object);
			}

			complexDdl1.options = ddl4List;
			complexDdl2.options = ddl4List;

			label1.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='ladder_complex_source_1']").InnerText;
			label2.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='ladder_complex_source_2']").InnerText;
			label3.text = "";

			constantToggle1.GetComponentInChildren<Text> ().text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='constant']").InnerText;
			constantToggle2.GetComponentInChildren<Text> ().text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='constant']").InnerText;

			if (objectToEdit.GetComponent<lineObject> ().constant1 == true) {
				constantToggle1.isOn = true;
				inputText1.text = objectToEdit.GetComponent<lineObject> ().value1;

				complexDdl1.gameObject.SetActive (false);
				complexBgConstant1.enabled = true;
				inputText1.gameObject.SetActive (true);
			} else {
				constantToggle1.isOn = false;
				complexDdl1.gameObject.SetActive (true);
				complexBgConstant1.enabled = false;
				inputText1.gameObject.SetActive (false);

				if (objectToEdit.GetComponent<lineObject> ().value1 != "") {
					if (objectToEdit.GetComponent<lineObject> ().index1 == -1) {
						complexDdl1.value = complexDdl1.options.FindIndex (option => option.text == objectToEdit.GetComponent<lineObject> ().value1);
					} else {
						complexDdl1.value = objectToEdit.GetComponent<lineObject> ().index1;
					}
				} else {
					complexDdl1.value = 0;
				}	
			}

			if (objectToEdit.GetComponent<lineObject> ().constant2 == true) {
				constantToggle2.isOn = true;
				inputText2.text = objectToEdit.GetComponent<lineObject> ().value2;

				complexDdl2.gameObject.SetActive (false);
				complexBgDdl2.enabled = false;
				complexBgConstant2.enabled = true;
				inputText2.gameObject.SetActive (true);
			} else {
				constantToggle2.isOn = false;
				complexDdl2.gameObject.SetActive (true);
				complexBgDdl2.enabled = true;
				complexBgConstant2.enabled = false;
				inputText2.gameObject.SetActive (false);

				if (objectToEdit.GetComponent<lineObject> ().value2 != "") {
					if (objectToEdit.GetComponent<lineObject> ().index2 == -1) {
						complexDdl2.value = complexDdl2.options.FindIndex (option => option.text == objectToEdit.GetComponent<lineObject> ().value2);
					} else {
						complexDdl2.value = objectToEdit.GetComponent<lineObject> ().index2;
					}
				} else {
					complexDdl2.value = 0;
				}	
			}

			inputText1.GetComponent<InputField> ().characterValidation = InputField.CharacterValidation.Integer;
			inputText2.GetComponent<InputField> ().characterValidation = InputField.CharacterValidation.Integer;

			xOffset = 63f;
			yOffset = -58f;

			complexBgDdl3.enabled = false;
			complexDdl3.gameObject.SetActive (false);
			break;
		case dragElementLadder._type_elements.not:
		case dragElementLadder._type_elements.mov:
			basicBox.SetActive (false);
			complexBox.SetActive (true);
			PIDBox.SetActive (false);

			constantToggle1.gameObject.SetActive (true);

			complexTitle.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='ladder_options']").InnerText.ToUpper ();	

			List<Dropdown.OptionData> ddl5List = new List<Dropdown.OptionData> ();
			List<Dropdown.OptionData> ddl5AList = new List<Dropdown.OptionData> ();

			for (int i = 0; i < _situation.GetComponent<LadderMainClass> ().memoryElements; i++) {
				Dropdown.OptionData ddl1Object = new Dropdown.OptionData ();
				ddl1Object.text = "M" + i.ToString ();
				ddl5List.Add (ddl1Object);
			}

			for (int i = 0; i < _situation.GetComponent<LadderMainClass> ().memoryNElements; i++) {
				Dropdown.OptionData ddl1Object = new Dropdown.OptionData ();
				ddl1Object.text = "N" + i.ToString ();
				ddl5List.Add (ddl1Object);
			}

			for (int i = 0; i < _situation.GetComponent<LadderMainClass> ().counterElements; i++) {
				Dropdown.OptionData ddl1Object = new Dropdown.OptionData ();
				ddl1Object.text = "C" + i.ToString ();
				ddl5List.Add (ddl1Object);
			}

			for (int i = 0; i < _situation.GetComponent<LadderMainClass> ().timerElements; i++) {
				Dropdown.OptionData ddl1Object = new Dropdown.OptionData ();
				ddl1Object.text = "T" + i.ToString ();
				ddl5List.Add (ddl1Object);
			}

			for (int i = 0; i < _situation.GetComponent<LadderMainClass> ().analogInputs; i++) {
				Dropdown.OptionData ddl1Object = new Dropdown.OptionData ();
				ddl1Object.text = "AI" + i.ToString ();
				ddl5List.Add (ddl1Object);
			}

			complexDdl1.options = ddl5List;

			for (int i = 0; i < _situation.GetComponent<LadderMainClass> ().memoryElements; i++) {
				Dropdown.OptionData ddl1Object = new Dropdown.OptionData ();
				ddl1Object.text = "M" + i.ToString ();
				ddl5AList.Add (ddl1Object);
			}

			for (int i = 0; i < _situation.GetComponent<LadderMainClass> ().memoryNElements; i++) {
				Dropdown.OptionData ddl1Object = new Dropdown.OptionData ();
				ddl1Object.text = "N" + i.ToString ();
				ddl5AList.Add (ddl1Object);
			}

			for (int i = 0; i < _situation.GetComponent<LadderMainClass> ().analogOutputs; i++) {
				Dropdown.OptionData ddl1Object = new Dropdown.OptionData ();
				ddl1Object.text = "AO" + i.ToString ();
				ddl5AList.Add (ddl1Object);
			}

			complexDdl3.options = ddl5AList;

			label1.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='ladder_complex_source_1']").InnerText;
			label2.text = "";
			label3.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='ladder_complex_target']").InnerText;

			constantToggle1.GetComponentInChildren<Text> ().text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='constant']").InnerText;

			if (objectToEdit.GetComponent<lineObject> ().constant1 == true) {
				constantToggle1.isOn = true;
				inputText1.text = objectToEdit.GetComponent<lineObject> ().value1;

				complexDdl1.gameObject.SetActive (false);
				complexBgConstant1.enabled = true;
				inputText1.gameObject.SetActive (true);
			} else {
				constantToggle1.isOn = false;
				complexDdl1.gameObject.SetActive (true);
				complexBgConstant1.enabled = false;
				inputText1.gameObject.SetActive (false);

				if (objectToEdit.GetComponent<lineObject> ().value1 != "") {
					if (objectToEdit.GetComponent<lineObject> ().index1 == -1) {
						complexDdl1.value = complexDdl1.options.FindIndex (option => option.text == objectToEdit.GetComponent<lineObject> ().value1);
					} else {
						complexDdl1.value = objectToEdit.GetComponent<lineObject> ().index1;
					}
				} else {
					complexDdl1.value = 0;
				}	
			}

			if (objectToEdit.GetComponent<lineObject> ().value3 != "") {
				if (objectToEdit.GetComponent<lineObject> ().index3 == -1) {
					complexDdl3.value = complexDdl3.options.FindIndex (option => option.text == objectToEdit.GetComponent<lineObject> ().value3);
				} else {
					complexDdl3.value = objectToEdit.GetComponent<lineObject> ().index3;
				}
			} else {
				complexDdl3.value = 0;
			}

			inputText1.GetComponent<InputField> ().characterValidation = InputField.CharacterValidation.Integer;

			complexBgDdl2.enabled = false;
			complexBgConstant2.enabled = false;
			complexDdl2.gameObject.SetActive (false);
			constantToggle2.gameObject.SetActive (false);
			inputText2.gameObject.SetActive (false);

			complexBgDdl3.enabled = true;
			complexDdl3.gameObject.SetActive (true);

			xOffset = 43f;
			yOffset = -58f;
			break;
		case dragElementLadder._type_elements.ctu:
		case dragElementLadder._type_elements.ctd:
			basicBox.SetActive (false);
			complexBox.SetActive (true);
			PIDBox.SetActive (false);

			complexDdl1.gameObject.SetActive (true);
			complexBgDdl1.enabled = true;
			complexBgConstant1.enabled = false;
			inputText1.gameObject.SetActive (false);

			constantToggle1.gameObject.SetActive (false);
			constantToggle2.gameObject.SetActive (false);

			complexDdl2.gameObject.SetActive (false);
			complexBgDdl2.enabled = false;

			complexBgConstant2.enabled = true;
			inputText2.gameObject.SetActive (true);

			inputText2.characterValidation = InputField.CharacterValidation.Integer;

			complexBgDdl3.enabled = true;
			complexDdl3.gameObject.SetActive (true);

			complexTitle.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='ladder_options']").InnerText.ToUpper ();	

			List<Dropdown.OptionData> ddl6List = new List<Dropdown.OptionData> ();
			List<Dropdown.OptionData> ddl6AList = new List<Dropdown.OptionData> ();

			for (int i = 0; i < _situation.GetComponent<LadderMainClass> ().counterElements; i++) {
				Dropdown.OptionData ddl1Object = new Dropdown.OptionData ();
				ddl1Object.text = "C" + i.ToString ();
				ddl6List.Add (ddl1Object);
			}

			complexDdl1.options = ddl6List;

			if (objectToEdit.GetComponent<lineObject> ().value2 != "") {
				inputText2.text = objectToEdit.GetComponent<lineObject> ().value2;
			} else {
				inputText2.text = "0";
			}

			for (int i = 0; i < _situation.GetComponent<LadderMainClass> ().memoryElements; i++) {
				Dropdown.OptionData ddl1Object = new Dropdown.OptionData ();
				ddl1Object.text = "M" + i.ToString ();
				ddl6AList.Add (ddl1Object);
			}

			for (int i = 0; i < _situation.GetComponent<LadderMainClass> ().memoryNElements; i++) {
				Dropdown.OptionData ddl1Object = new Dropdown.OptionData ();
				ddl1Object.text = "N" + i.ToString ();
				ddl6AList.Add (ddl1Object);
			}

			for (int i = 0; i < _situation.GetComponent<LadderMainClass> ().analogOutputs; i++) {
				Dropdown.OptionData ddl1Object = new Dropdown.OptionData ();
				ddl1Object.text = "AO" + i.ToString ();
				ddl6AList.Add (ddl1Object);
			}

			complexDdl3.options = ddl6AList;

			label1.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='name']").InnerText.ToUpper ();
			label2.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='pv']").InnerText.ToUpper ();
			label3.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='cv']").InnerText.ToUpper ();

			if (objectToEdit.GetComponent<lineObject> ().value1 != "") {
				if (objectToEdit.GetComponent<lineObject> ().index1 == -1) {
					complexDdl1.value = complexDdl1.options.FindIndex (option => option.text == objectToEdit.GetComponent<lineObject> ().value1);
				} else {
					complexDdl1.value = objectToEdit.GetComponent<lineObject> ().index1;
				}
			} else {
				complexDdl1.value = 0;
			}

			if (objectToEdit.GetComponent<lineObject> ().value3 != "") {
				if (objectToEdit.GetComponent<lineObject> ().index3 == -1) {
					complexDdl3.value = complexDdl3.options.FindIndex (option => option.text == objectToEdit.GetComponent<lineObject> ().value3);
				} else {
					complexDdl3.value = objectToEdit.GetComponent<lineObject> ().index3;
				}
			} else {
				complexDdl3.value = 0;
			}

			xOffset = 43f;
			yOffset = -58f;
			break;
		case dragElementLadder._type_elements.ton:
		case dragElementLadder._type_elements.toff:
		case dragElementLadder._type_elements.tp:
			basicBox.SetActive (false);
			complexBox.SetActive (true);
			PIDBox.SetActive (false);

			complexDdl1.gameObject.SetActive (true);
			complexBgDdl1.enabled = true;
			complexBgConstant1.enabled = false;
			inputText1.gameObject.SetActive (false);

			constantToggle1.gameObject.SetActive (false);
			constantToggle2.gameObject.SetActive (false);

			complexDdl2.gameObject.SetActive (false);
			complexBgDdl2.enabled = false;

			complexBgConstant2.enabled = true;
			inputText2.gameObject.SetActive (true);

			inputText2.characterValidation = InputField.CharacterValidation.Decimal;

			complexBgDdl3.enabled = true;
			complexDdl3.gameObject.SetActive (true);

			complexTitle.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='ladder_options']").InnerText.ToUpper ();	

			List<Dropdown.OptionData> ddl8List = new List<Dropdown.OptionData> ();
			List<Dropdown.OptionData> ddl8AList = new List<Dropdown.OptionData> ();

			for (int i = 0; i < _situation.GetComponent<LadderMainClass> ().timerElements; i++) {
				Dropdown.OptionData ddl1Object = new Dropdown.OptionData ();
				ddl1Object.text = "T" + i.ToString ();
				ddl8List.Add (ddl1Object);
			}

			complexDdl1.options = ddl8List;

			if (objectToEdit.GetComponent<lineObject> ().value2 != "") {
				float numericValue1 = 0;
				if (float.TryParse (inputText2.text, out numericValue1)) {
					if (objectToEdit.GetComponent<lineObject>().scaleTP == 0) {
						label2.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='TP']").InnerText + " (" + Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='mseg_scale']").InnerText + ")";
						if (float.Parse (inputText2.text) >= 1000) {
							objectToEdit.GetComponent<lineObject>().scaleTP = 1;
							inputText2.text = (float.Parse (inputText2.text) / 1000f).ToString ();
							label2.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='TP']").InnerText + " (" + Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='seg_scale']").InnerText + ")";
						}
					} else if (objectToEdit.GetComponent<lineObject>().scaleTP == 1) {
						label2.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='TP']").InnerText + " (" + Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='seg_scale']").InnerText + ")";
						if (float.Parse (inputText2.text) >= 60) {
							objectToEdit.GetComponent<lineObject>().scaleTP = 2;
							inputText2.text = (Mathf.RoundToInt ((float.Parse (inputText2.text) / 60f) * 1000f) / 1000f).ToString ();
							label2.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='TP']").InnerText + " (" + Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='min_scale']").InnerText + ")";
						}
					} else if (objectToEdit.GetComponent<lineObject>().scaleTP == 2) {
						label2.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='TP']").InnerText + " (" + Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='min_scale']").InnerText + ")";
						if (float.Parse (inputText2.text) >= 60) {
							objectToEdit.GetComponent<lineObject>().scaleTP = 3;
							inputText2.text = (Mathf.RoundToInt ((float.Parse (inputText2.text) / 60f) * 1000f) / 1000f).ToString ();
							label2.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='TP']").InnerText + " (" + Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='h_scale']").InnerText + ")";
						}
					} else if(objectToEdit.GetComponent<lineObject>().scaleTP == 3){
						label2.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='TP']").InnerText + " (" + Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='h_scale']").InnerText + ")";
					}
				} else {
					inputText2.text = "0";
				}
			} else {
				inputText2.text = "0";
			}

			for (int i = 0; i < _situation.GetComponent<LadderMainClass> ().memoryElements; i++) {
				Dropdown.OptionData ddl1Object = new Dropdown.OptionData ();
				ddl1Object.text = "M" + i.ToString ();
				ddl8AList.Add (ddl1Object);
			}

			for (int i = 0; i < _situation.GetComponent<LadderMainClass> ().memoryNElements; i++) {
				Dropdown.OptionData ddl1Object = new Dropdown.OptionData ();
				ddl1Object.text = "N" + i.ToString ();
				ddl8AList.Add (ddl1Object);
			}

			for (int i = 0; i < _situation.GetComponent<LadderMainClass> ().analogOutputs; i++) {
				Dropdown.OptionData ddl1Object = new Dropdown.OptionData ();
				ddl1Object.text = "AO" + i.ToString ();
				ddl8AList.Add (ddl1Object);
			}

			complexDdl3.options = ddl8AList;

			label1.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='name']").InnerText.ToUpper ();
			label3.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='ET']").InnerText.ToUpper ();

			if (objectToEdit.GetComponent<lineObject> ().value1 != "") {
				if (objectToEdit.GetComponent<lineObject> ().index1 == -1) {
					complexDdl1.value = complexDdl1.options.FindIndex (option => option.text == objectToEdit.GetComponent<lineObject> ().value1);
				} else {
					complexDdl1.value = objectToEdit.GetComponent<lineObject> ().index1;
				}
			} else {
				complexDdl1.value = 0;
			}

			if (objectToEdit.GetComponent<lineObject> ().value3 != "") {
				if (objectToEdit.GetComponent<lineObject> ().index3 == -1) {
					complexDdl3.value = complexDdl3.options.FindIndex (option => option.text == objectToEdit.GetComponent<lineObject> ().value3);
				} else {
					complexDdl3.value = objectToEdit.GetComponent<lineObject> ().index3;
				}
			} else {
				complexDdl3.value = 0;
			}

			xOffset = 43f;
			yOffset = -58f;
			break;
		case dragElementLadder._type_elements.pid:
			basicBox.SetActive (false);
			complexBox.SetActive (false);
			PIDBox.SetActive (true);

			inputTextPID1.characterValidation = InputField.CharacterValidation.Integer;
			inputTextPID2.characterValidation = InputField.CharacterValidation.Decimal;
			inputTextPID3.characterValidation = InputField.CharacterValidation.Decimal;
			inputTextPID4.characterValidation = InputField.CharacterValidation.Decimal;

			PIDTitle.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='ladder_pid']").InnerText.ToUpper ();	

			labelPID1.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='input']").InnerText;
			labelPID2.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='output']").InnerText;
			labelPID3.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='ladder_setpoint']").InnerText;
			labelPID4.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='ladder_kp']").InnerText;
			labelPID5.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='ladder_kd']").InnerText;
			labelPID6.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='ladder_ki']").InnerText;

			List<Dropdown.OptionData> ddlPIDAList = new List<Dropdown.OptionData> ();
			List<Dropdown.OptionData> ddlPIDBList = new List<Dropdown.OptionData> ();

			for (int i = 0; i < _situation.GetComponent<LadderMainClass> ().analogInputs; i++) {
				Dropdown.OptionData ddl1Object = new Dropdown.OptionData ();
				ddl1Object.text = "AI" + i.ToString ();
				ddlPIDAList.Add (ddl1Object);
			}

			PIDDdl1.options = ddlPIDAList;

			for (int i = 0; i < _situation.GetComponent<LadderMainClass> ().analogOutputs; i++) {
				Dropdown.OptionData ddl1Object = new Dropdown.OptionData ();
				ddl1Object.text = "AO" + i.ToString ();
				ddlPIDBList.Add (ddl1Object);
			}

			PIDDdl2.options = ddlPIDBList;

			if (objectToEdit.GetComponent<lineObject> ().value1 != "") {
				if (objectToEdit.GetComponent<lineObject> ().index1 == -1) {
					PIDDdl1.value = PIDDdl1.options.FindIndex (option => option.text == objectToEdit.GetComponent<lineObject> ().value1);
				} else {
					PIDDdl1.value = objectToEdit.GetComponent<lineObject> ().index1;
				}
			} else {
				PIDDdl1.value = 0;
			}

			if (objectToEdit.GetComponent<lineObject> ().value2 != "") {
				if (objectToEdit.GetComponent<lineObject> ().index2 == -1) {
					PIDDdl2.value = PIDDdl2.options.FindIndex (option => option.text == objectToEdit.GetComponent<lineObject> ().value2);
				} else {
					PIDDdl2.value = objectToEdit.GetComponent<lineObject> ().index2;
				}
			} else {
				PIDDdl2.value = 0;
			}

			if (objectToEdit.GetComponent<lineObject> ().value3 != "") {
				inputTextPID1.text = objectToEdit.GetComponent<lineObject> ().value3;
			} else {
				inputTextPID1.text = "0";
			}

			if (objectToEdit.GetComponent<lineObject> ().value4 != "") {
				inputTextPID2.text = objectToEdit.GetComponent<lineObject> ().value4;
			} else {
				inputTextPID2.text = "0";
			}

			if (objectToEdit.GetComponent<lineObject> ().value5 != "") {
				inputTextPID3.text = objectToEdit.GetComponent<lineObject> ().value5;
			} else {
				inputTextPID3.text = "0";
			}

			if (objectToEdit.GetComponent<lineObject> ().value6 != "") {
				inputTextPID4.text = objectToEdit.GetComponent<lineObject> ().value6;
			} else {
				inputTextPID4.text = "0";
			}

			break;
		}

		editorActive = true;
		gameObject.transform.position = objectToEdit.transform.position;
		gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x + xOffset, gameObject.transform.localPosition.y + yOffset, gameObject.transform.localPosition.z);
		if (basicBox.activeInHierarchy == true) {
			if ((Mathf.Abs (gameObject.GetComponent<RectTransform>().anchoredPosition.y) - 210) > (_situation.GetComponent<LadderMainClass> ()._workbenchContainer.GetComponent<RectTransform> ().rect.height - 80f)) {
				gameObject.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (gameObject.GetComponent<RectTransform> ().anchoredPosition.x, -(_situation.GetComponent<LadderMainClass> ()._workbenchContainer.GetComponent<RectTransform> ().rect.height - 80f + 210f));
			}
		} else if (complexBox.activeInHierarchy == true) {
			if ((Mathf.Abs (gameObject.GetComponent<RectTransform>().anchoredPosition.y) - 210) > (_situation.GetComponent<LadderMainClass> ()._workbenchContainer.GetComponent<RectTransform> ().rect.height - 230f)) {
				gameObject.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (gameObject.GetComponent<RectTransform> ().anchoredPosition.x, -(_situation.GetComponent<LadderMainClass> ()._workbenchContainer.GetComponent<RectTransform> ().rect.height - 230f + 210f));
			}
		} else if (PIDBox.activeInHierarchy == true) {
			if ((Mathf.Abs (gameObject.GetComponent<RectTransform>().anchoredPosition.y) - 210) > (_situation.GetComponent<LadderMainClass> ()._workbenchContainer.GetComponent<RectTransform> ().rect.height - 310f)) {
				gameObject.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (gameObject.GetComponent<RectTransform> ().anchoredPosition.x, -(_situation.GetComponent<LadderMainClass> ()._workbenchContainer.GetComponent<RectTransform> ().rect.height - 310f + 210f));
			}
		}
			
		gameObject.transform.SetAsLastSibling ();
	}

	public void hideEditor(bool willSave = true){
		if(willSave == true){
			_situation.GetComponent<LadderMainClass>().debugMode = false;

			switch(objectToEdit.GetComponent<lineObject>().typeElement){
			case dragElementLadder._type_elements.open_contact:
			case dragElementLadder._type_elements.close_contact:
			case dragElementLadder._type_elements.open_coil:
			case dragElementLadder._type_elements.close_coil:
			case dragElementLadder._type_elements.reset_coil:
			case dragElementLadder._type_elements.set_coil:
			case dragElementLadder._type_elements.reset:
				objectToEdit.GetComponent<lineObject> ().value1 = basicDdl1.options [basicDdl1.value].text;
				objectToEdit.GetComponent<lineObject> ().index1 = basicDdl1.value;
				break;
			case dragElementLadder._type_elements.add:
			case dragElementLadder._type_elements.subs:
			case dragElementLadder._type_elements.prod:
			case dragElementLadder._type_elements.division:
			case dragElementLadder._type_elements.and:
			case dragElementLadder._type_elements.or:
			case dragElementLadder._type_elements.xor:
				if (constantToggle1.isOn == true) {
					objectToEdit.GetComponent<lineObject> ().constant1 = true;
					objectToEdit.GetComponent<lineObject> ().value1 = inputText1.text;
					objectToEdit.GetComponent<lineObject> ().index1 = 0;
				} else {
					objectToEdit.GetComponent<lineObject> ().constant1 = false;
					objectToEdit.GetComponent<lineObject> ().value1 = complexDdl1.options [complexDdl1.value].text;
					objectToEdit.GetComponent<lineObject> ().index1 = complexDdl1.value;
				}

				if (constantToggle2.isOn == true) {
					objectToEdit.GetComponent<lineObject> ().constant2 = true;
					objectToEdit.GetComponent<lineObject> ().value2 = inputText2.text;
					objectToEdit.GetComponent<lineObject> ().index2 = 0;
				} else {
					objectToEdit.GetComponent<lineObject> ().constant2 = false;
					objectToEdit.GetComponent<lineObject> ().value2 = complexDdl2.options [complexDdl2.value].text;
					objectToEdit.GetComponent<lineObject> ().index2 = complexDdl2.value;
				}

				objectToEdit.GetComponent<lineObject> ().value3 = complexDdl3.options [complexDdl3.value].text;
				objectToEdit.GetComponent<lineObject> ().index3 = complexDdl3.value;
				break;
			case dragElementLadder._type_elements.equal:
			case dragElementLadder._type_elements.different:
			case dragElementLadder._type_elements.great:
			case dragElementLadder._type_elements.great_equal:
			case dragElementLadder._type_elements.less:
			case dragElementLadder._type_elements.less_equal:
				if (constantToggle1.isOn == true) {
					objectToEdit.GetComponent<lineObject> ().constant1 = true;
					objectToEdit.GetComponent<lineObject> ().value1 = inputText1.text;
					objectToEdit.GetComponent<lineObject> ().index1 = 0;
				} else {
					objectToEdit.GetComponent<lineObject> ().constant1 = false;
					objectToEdit.GetComponent<lineObject> ().value1 = complexDdl1.options [complexDdl1.value].text;
					objectToEdit.GetComponent<lineObject> ().index1 = complexDdl1.value;
				}

				if (constantToggle2.isOn == true) {
					objectToEdit.GetComponent<lineObject> ().constant2 = true;
					objectToEdit.GetComponent<lineObject> ().value2 = inputText2.text;
					objectToEdit.GetComponent<lineObject> ().index2 = 0;
				} else {
					objectToEdit.GetComponent<lineObject> ().constant2 = false;
					objectToEdit.GetComponent<lineObject> ().value2 = complexDdl2.options [complexDdl2.value].text;
					objectToEdit.GetComponent<lineObject> ().index2 = complexDdl2.value;
				}
				break;
			case dragElementLadder._type_elements.not:
			case dragElementLadder._type_elements.mov:
				if (constantToggle1.isOn == true) {
					objectToEdit.GetComponent<lineObject> ().constant1 = true;
					objectToEdit.GetComponent<lineObject> ().value1 = inputText1.text;
					objectToEdit.GetComponent<lineObject> ().index1 = 0;
				} else {
					objectToEdit.GetComponent<lineObject> ().constant1 = false;
					objectToEdit.GetComponent<lineObject> ().value1 = complexDdl1.options [complexDdl1.value].text;
					objectToEdit.GetComponent<lineObject> ().index1 = complexDdl1.value;
				}
				objectToEdit.GetComponent<lineObject> ().value3 = complexDdl3.options [complexDdl3.value].text;
				objectToEdit.GetComponent<lineObject> ().index3 = complexDdl3.value;
				break;
			case dragElementLadder._type_elements.ctu:
			case dragElementLadder._type_elements.ctd:
				//if (_situation.GetComponent<LadderMainClass> ().countersUsed.IndexOf (complexDdl1.value) == -1) {
					objectToEdit.GetComponent<lineObject> ().constant1 = false;
					objectToEdit.GetComponent<lineObject> ().value1 = complexDdl1.options [complexDdl1.value].text;
					objectToEdit.GetComponent<lineObject> ().index1 = complexDdl1.value;

					objectToEdit.GetComponent<lineObject> ().constant2 = true;
					objectToEdit.GetComponent<lineObject> ().value2 = inputText2.text;
					objectToEdit.GetComponent<lineObject> ().index2 = 0;

					objectToEdit.GetComponent<lineObject> ().value3 = complexDdl3.options [complexDdl3.value].text;
					objectToEdit.GetComponent<lineObject> ().index3 = complexDdl3.value;

					_situation.GetComponent<LadderMainClass> ().countersUsed.Add (complexDdl1.value);
				//} else {
				//	_situation.GetComponent<LadderMainClass> ().showInfoText ("jejeje");
				//}
				break;
			case dragElementLadder._type_elements.ton:
			case dragElementLadder._type_elements.toff:
			case dragElementLadder._type_elements.tp:
				//if (_situation.GetComponent<LadderMainClass> ().tempsUsed.IndexOf (complexDdl1.value) == -1) {
					objectToEdit.GetComponent<lineObject> ().constant1 = false;
					objectToEdit.GetComponent<lineObject> ().value1 = complexDdl1.options [complexDdl1.value].text;
					objectToEdit.GetComponent<lineObject> ().index1 = complexDdl1.value;

					objectToEdit.GetComponent<lineObject> ().constant2 = true;
					objectToEdit.GetComponent<lineObject> ().value2 = inputText2.text;
					objectToEdit.GetComponent<lineObject> ().index2 = 0;

					objectToEdit.GetComponent<lineObject> ().value3 = complexDdl3.options [complexDdl3.value].text;
					objectToEdit.GetComponent<lineObject> ().index3 = complexDdl3.value;

					_situation.GetComponent<LadderMainClass> ().tempsUsed.Add (complexDdl1.value);
				//} else {
				//	//not possible save
				//}
				break;
			case dragElementLadder._type_elements.pid:
				objectToEdit.GetComponent<lineObject> ().value1 = PIDDdl1.options [PIDDdl1.value].text;
				objectToEdit.GetComponent<lineObject> ().index1 = PIDDdl1.value;

				objectToEdit.GetComponent<lineObject> ().value2 = PIDDdl2.options [PIDDdl2.value].text;
				objectToEdit.GetComponent<lineObject> ().index2 = PIDDdl2.value;

				objectToEdit.GetComponent<lineObject> ().value3 = inputTextPID1.text;
				objectToEdit.GetComponent<lineObject> ().index3 = 0;

				objectToEdit.GetComponent<lineObject> ().value4 = inputTextPID2.text;
				objectToEdit.GetComponent<lineObject> ().value5 = inputTextPID3.text;
				objectToEdit.GetComponent<lineObject> ().value6 = inputTextPID4.text;
				break;
			}
		}
		basicBox.SetActive (false);
		complexBox.SetActive (false);
		PIDBox.SetActive (false);
	}

	public void changeConstant1Action(){
		if (constantToggle1.isOn == true) {
			inputText1.text = objectToEdit.GetComponent<lineObject> ().value1;
			complexDdl1.gameObject.SetActive (false);
			complexBgConstant1.enabled = true;
			inputText1.gameObject.SetActive (true);
		} else {
			complexDdl1.gameObject.SetActive (true);
			complexBgConstant1.enabled = false;
			inputText1.gameObject.SetActive (false);

			if (objectToEdit.GetComponent<lineObject> ().value1 != "") {
				complexDdl1.value = objectToEdit.GetComponent<lineObject> ().index1;
			} else {
				complexDdl1.value = 0;
			}	
		}
	}

	public void changeConstant2Action(){
		if (constantToggle2.isOn == true) {
			inputText2.text = objectToEdit.GetComponent<lineObject> ().value2;
			complexDdl2.gameObject.SetActive (false);
			complexBgConstant2.enabled = true;
			inputText2.gameObject.SetActive (true);
		} else {
			complexDdl2.gameObject.SetActive (true);
			complexBgConstant2.enabled = false;
			inputText2.gameObject.SetActive (false);

			if (objectToEdit.GetComponent<lineObject> ().value2 != "") {
				complexDdl2.value = objectToEdit.GetComponent<lineObject> ().index2;
			} else {
				complexDdl2.value = 0;
			}	
		}
	}

	public void changeInputField1Action(){
		switch(objectToEdit.GetComponent<lineObject>().typeElement){
		case dragElementLadder._type_elements.add:
		case dragElementLadder._type_elements.subs:
		case dragElementLadder._type_elements.prod:
		case dragElementLadder._type_elements.division:
		case dragElementLadder._type_elements.equal:
		case dragElementLadder._type_elements.different:
		case dragElementLadder._type_elements.great:
		case dragElementLadder._type_elements.great_equal:
		case dragElementLadder._type_elements.less:
		case dragElementLadder._type_elements.less_equal:
		case dragElementLadder._type_elements.and:
		case dragElementLadder._type_elements.or:
		case dragElementLadder._type_elements.xor:
		case dragElementLadder._type_elements.not:
		case dragElementLadder._type_elements.mov:
			int numericValue = 0;
			if (int.TryParse (inputText1.text, out numericValue)) {
				if (numericValue < 0) {
					inputText1.text = "0";
				} else if (numericValue > 255) {
					inputText1.text = "255";
				} else {
					inputText1.text = numericValue.ToString ();
				}
			} else {
				inputText1.text = "0";
			}
			break;
		case dragElementLadder._type_elements.pid:
			int numericValuePID = 0;
			if (int.TryParse (inputTextPID1.text, out numericValuePID)) {
				if (numericValuePID < 0) {
					inputTextPID1.text = "0";
				} else if (numericValuePID > 255) {
					inputTextPID1.text = "255";
				} else {
					inputTextPID1.text = numericValuePID.ToString ();
				}
			} else {
				inputTextPID1.text = "0";
			}
			break;
		}
	}

	public void changeInputField2Action(){
		switch(objectToEdit.GetComponent<lineObject>().typeElement){
		case dragElementLadder._type_elements.add:
		case dragElementLadder._type_elements.subs:
		case dragElementLadder._type_elements.prod:
		case dragElementLadder._type_elements.division:
		case dragElementLadder._type_elements.equal:
		case dragElementLadder._type_elements.different:
		case dragElementLadder._type_elements.great:
		case dragElementLadder._type_elements.great_equal:
		case dragElementLadder._type_elements.less:
		case dragElementLadder._type_elements.less_equal:
		case dragElementLadder._type_elements.and:
		case dragElementLadder._type_elements.or:
		case dragElementLadder._type_elements.xor:
			int numericValue = 0;
			if (int.TryParse (inputText2.text, out numericValue)) {
				if(numericValue < 0){
					inputText2.text = "0";
				}
				else if(numericValue > 255){
					inputText2.text = "255";
				} else {
					inputText2.text = numericValue.ToString ();
				}
			} else {
				inputText2.text = "0";
			}
			break;
		case dragElementLadder._type_elements.ctu:
		case dragElementLadder._type_elements.ctd:
			int numericValue2 = 0;
			if (int.TryParse (inputText2.text, out numericValue2)) {
				if(numericValue2 < 0){
					inputText2.text = "0";
				}
				else {
					inputText2.text = numericValue2.ToString ();
				}
			} else {
				inputText2.text = "0";
			}
			break;
		case dragElementLadder._type_elements.ton:
		case dragElementLadder._type_elements.toff:
		case dragElementLadder._type_elements.tp:
			float numericValue1 = 0;
			if (float.TryParse (inputText2.text, out numericValue1)) {
				if (numericValue1 >= 0) {
					if (objectToEdit.GetComponent<lineObject>().scaleTP == 0) {
						label2.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='TP']").InnerText + " (" + Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='mseg_scale']").InnerText + ")";
						if (numericValue1 >= 1000) {
							objectToEdit.GetComponent<lineObject>().scaleTP = 1;
							inputText2.text = (numericValue1 / 1000f).ToString ();
							label2.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='TP']").InnerText + " (" + Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='seg_scale']").InnerText + ")";
						}
					} else if (objectToEdit.GetComponent<lineObject>().scaleTP == 1) {
						label2.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='TP']").InnerText + " (" + Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='seg_scale']").InnerText + ")";
						if (numericValue1 >= 60) {
							objectToEdit.GetComponent<lineObject>().scaleTP = 2;
							inputText2.text = (Mathf.RoundToInt ((numericValue1 / 60f) * 1000f) / 1000f).ToString ();
							label2.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='TP']").InnerText + " (" + Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='min_scale']").InnerText + ")";
						}
					} else if (objectToEdit.GetComponent<lineObject>().scaleTP == 2) {
						label2.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='TP']").InnerText + " (" + Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='min_scale']").InnerText + ")";
						if (numericValue1 >= 60) {
							objectToEdit.GetComponent<lineObject>().scaleTP = 3;
							inputText2.text = (Mathf.RoundToInt ((numericValue1 / 60f) * 1000f) / 1000f).ToString ();
							label2.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='TP']").InnerText + " (" + Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='h_scale']").InnerText + ")";
						}
					} else if(objectToEdit.GetComponent<lineObject>().scaleTP == 3){
						label2.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='TP']").InnerText + " (" + Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='h_scale']").InnerText + ")";
					}
				} else {
					inputText2.text = "0";
					objectToEdit.GetComponent<lineObject>().scaleTP = 0;
					label2.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='TP']").InnerText + " (" + Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='mseg_scale']").InnerText + ")";
				}
			} else {
				inputText2.text = "0";
				objectToEdit.GetComponent<lineObject>().scaleTP = 0;
				label2.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='TP']").InnerText + " (" + Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='mseg_scale']").InnerText + ")";
			}
			break;
		case dragElementLadder._type_elements.pid:
			float numericPIDValue = 0;
			if (float.TryParse (inputTextPID2.text, out numericPIDValue)) {
				if (numericPIDValue < 0) {
					inputTextPID2.text = "0";
				} else {
					inputTextPID2.text = numericPIDValue.ToString ();
				}
			} else {
				inputTextPID2.text = "0";
			}
			break;
		}
	}

	public void changeInputField3Action(){
		switch(objectToEdit.GetComponent<lineObject>().typeElement){
		case dragElementLadder._type_elements.pid:
			float numericPIDValue = 0;
			if (float.TryParse (inputTextPID3.text, out numericPIDValue)) {
				if (numericPIDValue < 0) {
					inputTextPID3.text = "0";
				} else {
					inputTextPID3.text = numericPIDValue.ToString ();
				}
			} else {
				inputTextPID3.text = "0";
			}
			break;
		}
	}

	public void changeInputField4Action(){
		switch(objectToEdit.GetComponent<lineObject>().typeElement){
		case dragElementLadder._type_elements.pid:
			float numericPIDValue = 0;
			if (float.TryParse (inputTextPID4.text, out numericPIDValue)) {
				if (numericPIDValue < 0) {
					inputTextPID4.text = "0";
				} else {
					inputTextPID4.text = numericPIDValue.ToString ();
				}
			} else {
				inputTextPID4.text = "0";
			}
			break;
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
