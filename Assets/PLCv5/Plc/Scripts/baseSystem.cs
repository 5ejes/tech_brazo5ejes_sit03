﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class baseSystem : BaseSimulator {

	public enum Controllers
	{
		Grafcet,
		Ladder
	}

	public enum graphicInterfaces
	{
		electrical_bench,
		pneumatic_bench,
		none,
        roboticInterface
	}

    [Header("PLC SETTINGS")]
	public Controllers Controller;
	public graphicInterfaces graphicInterface;
    public bool RoboticsMode;

    [Header("PLC ROBOTICS SETTINGS")]
    public int engineMaxVel;

	[Header("AULA SETTINGS")]
	public int grafcet_electric_practices;
	public int grafcet_pneumatic_practices;
	public int ladder_electric_practices;
	public int ladder_pneumatic_practices;

	[Header("PLC GAMEOBJECTS")]
	public GameObject _electricalBench;
	public GameObject _pneumaticBench;
	public GameObject _noneBench;
	public GameObject _grafcet;
	public GameObject _ladder;
    public GameObject _roboticInterface;

	[HideInInspector]
	public Image blackWindow;

	//[HideInInspector]
	//public bool onlyLadder;

	private bool afterLoggedActionDone = false;

	// Use this for initialization
	void Start ()
    {
        /*switch (Manager.Instance.globalLanguage)
        {
            case BaseSimulator.LanguageList.Spanish:
                Manager.Instance.globalLanguage = BaseSimulator.LanguageList.Spanish;
                GameObject.Find("SystemController").GetComponent<baseSystem>().Language = BaseSimulator.LanguageList.Spanish;
                break;

            case BaseSimulator.LanguageList.English:
                Manager.Instance.globalLanguage = BaseSimulator.LanguageList.English;
                GameObject.Find("SystemController").GetComponent<baseSystem>().Language = BaseSimulator.LanguageList.English;
                break;

            case BaseSimulator.LanguageList.Portuguese:
                Manager.Instance.globalLanguage = BaseSimulator.LanguageList.Portuguese;
                GameObject.Find("SystemController").GetComponent<baseSystem>().Language = BaseSimulator.LanguageList.Portuguese;
                break;
        }*/

        blackWindow = GameObject.Find("Fundido").GetComponent<Image>();

		Manager.Instance.globalController = Controller;
		Manager.Instance.globalGraphicInterface = graphicInterface;
        Manager.Instance.globalRoboticsMode = RoboticsMode;
        Manager.Instance.globalEngineMaxVel = engineMaxVel;

		base.startSimulator();

		GameObject saveAsWindow = GameObject.Find ("saveWindow");
		if(saveAsWindow != null){
			saveAsWindow.GetComponent<SaveWindow> ().setTexts ();
		}

		GameObject openWindow = GameObject.Find ("openWindow");
		if(openWindow != null){
			openWindow.GetComponent<OpenWindow> ().setTexts ();
		}

		switch(graphicInterface){
		case graphicInterfaces.electrical_bench:
            _roboticInterface.SetActive(false);
			_electricalBench.GetComponent<ElectricalTestBenchMainClass> ().setTexts ();
			_electricalBench.GetComponent<ElectricalTestBenchMainClass> ().checkLanguageComponent ();
			break;
		case graphicInterfaces.pneumatic_bench:
            _roboticInterface.SetActive(false);
			_pneumaticBench.GetComponent<PnematicTestBenchClass> ().setTexts ();
			_pneumaticBench.GetComponent<PnematicTestBenchClass> ().checkLanguageComponent ();
			break;
		case graphicInterfaces.none:
            _roboticInterface.SetActive(false);
			break;
        case graphicInterfaces.roboticInterface:
            _roboticInterface.SetActive(true);
            _roboticInterface.GetComponent<RoboticViewer>().InitialAction();
            break;
		}

		switch (Controller) {
		case Controllers.Grafcet:
			_grafcet.SetActive (true);
			_grafcet.GetComponent<GrafcetMainClass> ().initialAction ();
			_grafcet.GetComponent<GrafcetMainClass> ().setTexts ();
			break;
		case Controllers.Ladder:
			_grafcet.SetActive (false);
			_ladder.GetComponent<LadderMainClass> ().initialAction ();
			_ladder.GetComponent<LadderMainClass> ().setTexts ();
			break;
		}
			
	}

	public void goToController(){
		switch(Controller){
		case Controllers.Grafcet:
			_grafcet.transform.SetAsLastSibling ();
			//_grafcet.GetComponent<GrafcetMainClass> ().initialAction ();
			_grafcet.GetComponent<GrafcetMainClass> ().setTexts ();
			break;
		case Controllers.Ladder:
			_ladder.transform.SetAsLastSibling ();
			//_ladder.GetComponent<LadderMainClass> ().initialAction ();
			_ladder.GetComponent<LadderMainClass> ().setTexts ();
			break;
		}

		//GameObject.Find ("upperIndicator").transform.SetAsLastSibling();
		blackWindow.transform.SetAsLastSibling ();
		blackWindow.CrossFadeAlpha(0, 0.5f, false);
		blackWindow.raycastTarget = true;

		Invoke ("reactivateBlackWindow", 1f);
	}

	public void goToBench(){
		switch (graphicInterface) {
		case graphicInterfaces.electrical_bench:
			_electricalBench.transform.SetAsLastSibling ();
			break;
		case graphicInterfaces.pneumatic_bench:
			_pneumaticBench.transform.SetAsLastSibling ();
			break;
        case graphicInterfaces.roboticInterface:
            _roboticInterface.transform.SetAsLastSibling();
            break;
		}
        if(graphicInterface != graphicInterfaces.roboticInterface){
            GameObject.Find("upperIndicator").transform.SetAsLastSibling();
        }
		
		blackWindow.transform.SetAsLastSibling ();
		blackWindow.CrossFadeAlpha(0, 0.5f, false);
		blackWindow.raycastTarget = true;

		Invoke ("reactivateBlackWindow", 1f);
	}

	private void reactivateBlackWindow(){
		blackWindow.CrossFadeAlpha(1, 0.00f, false);
		blackWindow.transform.SetAsFirstSibling ();
	}
	
	// Update is called once per frame
	void Update () {
		if(Manager.Instance.logged == true && afterLoggedActionDone == false){
			afterLoggedActionDone = true;

			if (Aula == true) {
				if (graphicInterface == graphicInterfaces.none) {
					switch (Controller) {
					case Controllers.Grafcet:
						_noneBench.GetComponent<onlyControllerMainClass> ().situationTag = "Only_Grafcet";
						break;
					case Controllers.Ladder:
						_noneBench.GetComponent<onlyControllerMainClass> ().situationTag = "Only_Ladder";
						break;
					}
					_noneBench.GetComponent<onlyControllerMainClass> ().loadSituation ();
				} else {
					GameObject aulaSelector = GameObject.Find ("aulaSelectorGObject");
					if (aulaSelector != null) {
						aulaSelector.GetComponent<AulaSelector> ().showSelector ();
					}
				}
			} else {
				switch(graphicInterface){
				case graphicInterfaces.electrical_bench:
					switch(Controller){
					case Controllers.Grafcet:
						_electricalBench.GetComponent<ElectricalTestBenchMainClass> ().situationTag = "Electric_with_Grafcet_" + (grafcet_electric_practices - 1).ToString ();
						break;
					case Controllers.Ladder:
						_electricalBench.GetComponent<ElectricalTestBenchMainClass> ().situationTag = "Electric_with_Ladder_" +(ladder_electric_practices - 1).ToString ();
						break;
					}
					_electricalBench.GetComponent<ElectricalTestBenchMainClass> ().loadSituation ();
					break;
				case graphicInterfaces.pneumatic_bench:
					switch(Controller){
					case Controllers.Grafcet:
						_pneumaticBench.GetComponent<PnematicTestBenchClass> ().situationTag = "Pneumatic_with_Grafcet_" + (grafcet_pneumatic_practices - 1).ToString ();
						break;
					case Controllers.Ladder:
						_pneumaticBench.GetComponent<PnematicTestBenchClass> ().situationTag = "Pneumatic_with_Ladder_" + (ladder_pneumatic_practices - 1).ToString ();
						break;
					}
					_pneumaticBench.GetComponent<PnematicTestBenchClass> ().loadSituation ();
					break;
				case graphicInterfaces.none:
					switch (Controller) {
					case Controllers.Grafcet:
						_noneBench.GetComponent<onlyControllerMainClass> ().situationTag = "Only_Grafcet";
						break;
					case Controllers.Ladder:
						_noneBench.GetComponent<onlyControllerMainClass> ().situationTag = "Only_Ladder";
						break;
					}
					_noneBench.GetComponent<onlyControllerMainClass> ().loadSituation ();
					break;
                case graphicInterfaces.roboticInterface:
                    _noneBench.GetComponent<onlyControllerMainClass>().situationTag = "Only_Robotics";
                    _noneBench.GetComponent<onlyControllerMainClass>().loadSituation();
                    break;
				}
			}
		}
	}
}
