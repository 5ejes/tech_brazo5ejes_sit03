﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class redCable : MonoBehaviour {

	public Image _initialPoint;
	public Image _line1;
	public Image _line2;
	public Image _line3;
	public Image _line4;
	public Image _line5;
	public Image _endPoint;

	public GameObject _parentElement;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void setPosition(int _row, int _column, int _input, bool _isRight = false){

		Quaternion _angleRotation = new Quaternion ();

		float _lineWidth = 0;
		float _offsetLevel = 0;
		float _xInitial = 0;
		float _width = 0;
		float _height = 0;
		float _angleLine2 = 0;
		float _firstLineWidth = 0;
		float _topLine2 = 0;
		float _xOffset = 0;
		float _widthOffset = 0;
		float _heightOffset = 0;
		float _xGeneralOffset = 0;

		_lineWidth = 2f;

		switch (Manager.Instance.globalGraphicInterface) {
		case baseSystem.graphicInterfaces.electrical_bench:
			if(_parentElement){
				switch(_parentElement.GetComponent<dragElement>()._typeComponent){
				case dragElement.typesComponents.limit_switch_sensor:
					_heightOffset = 2.08f;
					break;
				case dragElement.typesComponents.green_button_sensor:
				case dragElement.typesComponents.red_button_sensor:
				case dragElement.typesComponents.switch_sensor:
					_xOffset = 2;
					_widthOffset = -2;
					break;
				}
			}

			switch(_row){
			case 1:
				switch(_column){
				case 1:
					_offsetLevel = 3f;
					_xInitial = 422f + _xOffset;
					_width = 486f + _widthOffset;
					_height = 52.92f + _heightOffset;
					_firstLineWidth = 40f;
					_angleLine2 = -12f;
					_topLine2 = 1.1f;
					break;
				case 2:
					_offsetLevel = 7f;
					_xInitial = 556.64f + _xOffset;
					_width = 351.35f + _widthOffset;
					_height = 56.39f + _heightOffset;
					_firstLineWidth = 38f;
					_angleLine2 = -8.7f;
					_topLine2 = 0.5f;
					break;
				case 3:
					_offsetLevel = 11f;
					_xInitial = 692.5f + _xOffset;
					_width = 215.5f + _widthOffset;
					_height = 61.05f + _heightOffset;
					_firstLineWidth = 41f;
					_angleLine2 = -6.6f;
					_topLine2 = 0.3f;
					break;
				case 4:
					_offsetLevel = 15f;
					_xInitial = 829.2f + _xOffset;
					_width = 78.74f + _widthOffset;
					_height = 65.3f + _heightOffset;
					_firstLineWidth = 36f;
					_angleLine2 = -4f;
					_topLine2 = 0.12f;
					break;
				}
				break;
			case 2:
				switch (_column) {
				case 1:
					_offsetLevel = 2f;
					_xInitial = 405.6f + _xOffset;
					_width = 502.43f + _widthOffset;
					_height = 132f + _heightOffset;
					_firstLineWidth = 39f;
					_angleLine2 = -12f;
					_topLine2 = 1.1f;
					break;
				case 2:
					_offsetLevel = 6f;
					_xInitial = 544.4434f + _xOffset;
					_width = 363.557f + _widthOffset;
					_height = 136f + _heightOffset;
					_firstLineWidth = 37f;
					_angleLine2 = -8.7f;
					_topLine2 = 1.5f;
					break;
				case 3:
					_offsetLevel = 10f;
					_xInitial = 684f + _xOffset;
					_width = 223.91f + _widthOffset;
					_height = 140.28f + _heightOffset;
					_firstLineWidth = 39f;
					_angleLine2 = -6.6f;
					_topLine2 = 0.87f;
					break;
				case 4:
					_offsetLevel = 14f;
					_xInitial = 824.7261f + _xOffset;
					_width = 83.204f + _widthOffset;
					_height = 144.077f + _heightOffset;
					_firstLineWidth = 34f;
					_angleLine2 = -4f;
					_topLine2 = 0.32f;
					break;
				}
				break;
			case 3:
				switch (_column) {
				case 1:
					_offsetLevel = 1f;
					_xInitial = 386.12f + _xOffset;
					_width = 521.9f + _widthOffset;
					_height = 215f + _heightOffset;
					_firstLineWidth = 38f;
					_angleLine2 = -12f;
					_topLine2 = 3.8f;
					break;
				case 2:
					_offsetLevel = 5f;
					_xInitial = 530.22f + _xOffset;
					_width = 377.79f + _widthOffset;
					_height = 220.09f + _heightOffset;
					_firstLineWidth = 36f;
					_angleLine2 = -8.7f;
					_topLine2 = 2.5f;
					break;
				case 3:
					_offsetLevel = 9f;
					_xInitial = 674.8f + _xOffset;
					_width = 233.16f + _widthOffset;
					_height = 223.36f + _heightOffset;
					_firstLineWidth = 37f;
					_angleLine2 = -6.6f;
					_topLine2 = 1.4f;
					break;
				case 4:
					_offsetLevel = 13f;
					_xInitial = 819.32f + _xOffset;
					_width = 88.61f + _widthOffset;
					_height = 227.39f + _heightOffset;
					_firstLineWidth = 32f;
					_angleLine2 = -4f;
					_topLine2 = 0.52f;
					break;
				}
				break;
			case 4:
				switch (_column) {
				case 1:
					_offsetLevel = 0f;
					_xInitial = 365.78f + _xOffset;
					_width = 542.25f + _widthOffset;
					_height = 306.12f + _heightOffset;
					_firstLineWidth = 37f;
					_angleLine2 = -12f;
					_topLine2 = 6.8f;
					break;
				case 2:
					_offsetLevel = 4f;
					_xInitial = 516.62f + _xOffset;
					_width = 391.4f + _widthOffset;
					_height = 311.92f + _heightOffset;
					_firstLineWidth = 35f;
					_angleLine2 = -8.7f;
					_topLine2 = 3.5f;
					break;
				case 3:
					_offsetLevel = 8f;
					_xInitial = 664.745f + _xOffset;
					_width = 243.245f + _widthOffset;
					_height = 315.035f + _heightOffset;
					_firstLineWidth = 35f;
					_angleLine2 = -6.6f;
					_topLine2 = 2f;
					break;
				case 4:
					_offsetLevel = 12f;
					_xInitial = 813.948f + _xOffset;
					_width = 93.983f + _widthOffset;
					_height = 318.752f + _heightOffset;
					_firstLineWidth = 30f;
					_angleLine2 = -4f;
					_topLine2 = 0.75f;
					break;
				}
				break;
			}

			gameObject.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (_xInitial, -10f + _offsetLevel);
			gameObject.GetComponent<RectTransform> ().sizeDelta = new Vector2 (_width, _height);
			_angleRotation.eulerAngles = new Vector3 (0, 0, _angleLine2);
			_initialPoint.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (-3f, 0f);
			_line1.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0f, 0.6f);
			_line1.GetComponent<RectTransform> ().sizeDelta = new Vector2 (_firstLineWidth, _lineWidth);
			_line2.transform.rotation = _angleRotation;
			_line2.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (_line1.GetComponent<RectTransform> ().anchoredPosition.x + _line1.GetComponent<RectTransform> ().rect.width - _lineWidth, 0f);
			_line2.GetComponent<RectTransform> ().offsetMax = new Vector2 (_line2.GetComponent<RectTransform> ().offsetMax.x, _topLine2);
			_line4.GetComponent<RectTransform> ().sizeDelta = new Vector2 (_lineWidth, 0.0963f * (float)_input * (float)_input + 15.672f * (float)_input + 30.99f + _offsetLevel - _lineWidth / 2f);
			_line4.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (((-4.8f / 15f) * (float)_input - 4.5f) - (12f + (float)_input) - (Mathf.Tan (_line4.transform.rotation.eulerAngles.z * Mathf.PI / 180f) * _line4.GetComponent<RectTransform> ().rect.height), 0f);
			_line3.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (_line1.GetComponent<RectTransform> ().anchoredPosition.x + _line1.GetComponent<RectTransform> ().rect.width + gameObject.GetComponent<RectTransform> ().rect.height * Mathf.Abs (Mathf.Tan (_line2.transform.rotation.eulerAngles.z * Mathf.PI / 180f)) - _lineWidth, -_lineWidth / 2f);
			_line3.GetComponent<RectTransform> ().sizeDelta = new Vector2 (gameObject.GetComponent<RectTransform> ().rect.width - _line3.GetComponent<RectTransform> ().anchoredPosition.x + _line4.GetComponent<RectTransform> ().anchoredPosition.x, _lineWidth);
			_line5.GetComponent<RectTransform> ().anchoredPosition = new Vector2 ((-4.8f / 15f) * (float)_input - 4.5f, -0.0963f * (float)_input * (float)_input - 15.672f * (float)_input - 28.99f - _offsetLevel);
			_line5.GetComponent<RectTransform> ().sizeDelta = new Vector2 (12f + (float)_input, _lineWidth);
			_endPoint.GetComponent<RectTransform> ().anchoredPosition = new Vector2 ((-4.8f / 15f) * (float)_input, -0.0963f * (float)_input * (float)_input - 15.672f * (float)_input - 28.99f - _offsetLevel);
			break;
		case baseSystem.graphicInterfaces.pneumatic_bench:

			if (_isRight == false) {
				if (_parentElement) {
					switch (_parentElement.GetComponent<dragElementPneumatic> ()._typeComponent) {
					case dragElementPneumatic.typesComponents.limit_switch_sensor:
						_heightOffset = 2.08f;
						break;
					case dragElementPneumatic.typesComponents.green_button_sensor:
					case dragElementPneumatic.typesComponents.red_button_sensor:
					case dragElementPneumatic.typesComponents.switch_sensor:
						_xOffset = 0;
						_widthOffset = 0;
						break;
					}
				}

				switch (_row) {
				case 1:
					switch (_column) {
					case 1:
						_offsetLevel = 3f;
						_xInitial = 396 + _xOffset;
						_width = 202f + _widthOffset;
						_height = 59.6f + _heightOffset;
						_firstLineWidth = 26f;
						_angleLine2 = -12f;
						_topLine2 = 1.1f;
						break;
					case 2:
						_offsetLevel = 7f;
						_xInitial = 531.5f + _xOffset;
						_width = 66.2f + _widthOffset;
						_height = 63.2f + _heightOffset;
						_firstLineWidth = 22f;
						_angleLine2 = -8.7f;
						_topLine2 = 0.5f;
						break;
					}
					break;
				case 2:
					switch (_column) {
					case 1:
						_offsetLevel = 2f;
						_xInitial = 379.5f + _xOffset;
						_width = 219f + _widthOffset;
						_height = 139.5f + _heightOffset;
						_firstLineWidth = 24f;
						_angleLine2 = -12f;
						_topLine2 = 1.1f;
						break;
					case 2:
						_offsetLevel = 6f;
						_xInitial = 519f + _xOffset;
						_width = 79.1f + _widthOffset;
						_height = 142.8f + _heightOffset;
						_firstLineWidth = 20f;
						_angleLine2 = -8.7f;
						_topLine2 = 1.5f;
						break;
					}
					break;
				case 3:
					switch (_column) {
					case 1:
						_offsetLevel = 1f;
						_xInitial = 361.3f + _xOffset;
						_width = 237f + _widthOffset;
						_height = 222.1f + _heightOffset;
						_firstLineWidth = 22f;
						_angleLine2 = -12f;
						_topLine2 = 3.8f;
						break;
					case 2:
						_offsetLevel = 5f;
						_xInitial = 505.4f + _xOffset;
						_width = 92.8f + _widthOffset;
						_height = 227.1f + _heightOffset;
						_firstLineWidth = 18f;
						_angleLine2 = -8.7f;
						_topLine2 = 2.5f;
						break;
					}
					break;
				case 4:
					switch (_column) {
					case 1:
						_offsetLevel = 0f;
						_xInitial = 340.9f + _xOffset;
						_width = 257.4f + _widthOffset;
						_height = 313.5f + _heightOffset;
						_firstLineWidth = 20f;
						_angleLine2 = -12f;
						_topLine2 = 6.8f;
						break;
					case 2:
						_offsetLevel = 4f;
						_xInitial = 491f + _xOffset;
						_width = 107.7f + _widthOffset;
						_height = 318.9f + _heightOffset;
						_firstLineWidth = 16f;
						_angleLine2 = -8.7f;
						_topLine2 = 3.5f;
						break;
					}
					break;
				}

				float _xEndPosition = -2.0156f * (float)_input + 0.5919f;
				float _yEndPosition = -0.0844f * ((float)_input * (float)_input) - 15.225f * (float)_input - 29.471f;


				gameObject.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (_xInitial, -82f + _offsetLevel);
				gameObject.GetComponent<RectTransform> ().sizeDelta = new Vector2 (_width, _height);
				_angleRotation.eulerAngles = new Vector3 (0, 0, _angleLine2);
				_initialPoint.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (-3f, 0f);
				_line1.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0f, 0.6f);
				_line1.GetComponent<RectTransform> ().sizeDelta = new Vector2 (_firstLineWidth, _lineWidth);
				_line2.transform.rotation = _angleRotation;
				_line2.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (_line1.GetComponent<RectTransform> ().anchoredPosition.x + _line1.GetComponent<RectTransform> ().rect.width - _lineWidth, 0f);
				_line2.GetComponent<RectTransform> ().offsetMax = new Vector2 (_line2.GetComponent<RectTransform> ().offsetMax.x, _topLine2);
				_line4.GetComponent<RectTransform> ().sizeDelta = new Vector2 (_lineWidth, -_yEndPosition + _lineWidth + _offsetLevel - _lineWidth / 2f);
				_line4.GetComponent<RectTransform> ().anchoredPosition = new Vector2 ((_xEndPosition - 4.5f) - (12f + (float)_input) - (Mathf.Tan (_line4.transform.rotation.eulerAngles.z * Mathf.PI / 180f) * _line4.GetComponent<RectTransform> ().rect.height), 0f);
				_line3.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (_line1.GetComponent<RectTransform> ().anchoredPosition.x + _line1.GetComponent<RectTransform> ().rect.width + gameObject.GetComponent<RectTransform> ().rect.height * Mathf.Abs (Mathf.Tan (_line2.transform.rotation.eulerAngles.z * Mathf.PI / 180f)) - _lineWidth, -_lineWidth / 2f);
				_line3.GetComponent<RectTransform> ().sizeDelta = new Vector2 (gameObject.GetComponent<RectTransform> ().rect.width - _line3.GetComponent<RectTransform> ().anchoredPosition.x + _line4.GetComponent<RectTransform> ().anchoredPosition.x, _lineWidth);
				_line5.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (_xEndPosition - 4.5f, _yEndPosition - _offsetLevel);
				_line5.GetComponent<RectTransform> ().sizeDelta = new Vector2 (12f + (float)_input, _lineWidth);
				_endPoint.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (_xEndPosition, _yEndPosition - _offsetLevel);
			} else {
				//for right position
				if (_parentElement) {
					switch (_parentElement.GetComponent<dragElementPneumatic> ()._typeComponent) {
					case dragElementPneumatic.typesComponents.limit_switch_sensor:
						_heightOffset = 0f;
						break;
					case dragElementPneumatic.typesComponents.green_button_sensor:
					case dragElementPneumatic.typesComponents.red_button_sensor:
					case dragElementPneumatic.typesComponents.inductive_sensor:
						_xOffset = 8f; //-2f
						_widthOffset = -8f;//-2f
						_xGeneralOffset = 10f; 
						break;
					}
				}

				switch (_row) {
				case 1:
					switch (_column) {
					case 1:
						_offsetLevel = 3f;
						_xInitial = 877.5f + _xOffset;
						_width = -280f + _widthOffset;
						_height = 85.6f + _heightOffset;
						_firstLineWidth = 26f;
						_angleLine2 = -2f;
						_topLine2 = 0f;
						break;
					case 2:
						_offsetLevel = 7f;
						_xInitial = 1019f + _xOffset;
						_width = -421.4f + _widthOffset;
						_height = 89.6f + _heightOffset;
						_firstLineWidth = 22f;
						_angleLine2 = 1f;
						_topLine2 = 0f;
						break;
					case 3:
						_offsetLevel = 11f;
						_xInitial = 1160.7f + _xOffset;
						_width = -563.7f + _widthOffset;
						_height = 93.5f + _heightOffset;
						_firstLineWidth = 22f;
						_angleLine2 = 4f;
						_topLine2 = 0f;
						break;
					case 4:
						_offsetLevel = 15f;
						_xInitial = 1302f + _xOffset;
						_width = -704.6f + _widthOffset;
						_height = 97.6f + _heightOffset;
						_firstLineWidth = 22f;
						_angleLine2 = 6.6f;
						_topLine2 = 0f;
						break;
					case 5:
						_offsetLevel = 19f;
						_xInitial = 1441.9f + _xOffset;
						_width = -845f + _widthOffset;
						_height = 101.4f + _heightOffset;
						_firstLineWidth = 22f;
						_angleLine2 = 9f;
						_topLine2 = 0f;
						break;
					case 6:
						_offsetLevel = 23f;
						_xInitial = 1582.25f + _xOffset;
						_width = -984.85f + _widthOffset;
						_height = 105f + _heightOffset;
						_firstLineWidth = 22f;
						_angleLine2 = 11f;
						_topLine2 = 1.5707f;
						break;
					}
					break;
				case 2:
					switch (_column) {
					case 1:
						_offsetLevel = 2f;
						_xInitial = 874f + _xOffset;
						_width = -275.6f + _widthOffset;
						_height = 167f + _heightOffset;
						_firstLineWidth = 24f;
						_angleLine2 = -2f;
						_topLine2 = 0f;
						break;
					case 2:
						_offsetLevel = 6f;
						_xInitial = 1019.7f + _xOffset;
						_width = -421.8f + _widthOffset;
						_height = 171f + _heightOffset;
						_firstLineWidth = 20f;
						_angleLine2 = 1f;
						_topLine2 = 0f;
						break;
					case 3:
						_offsetLevel = 10f;
						_xInitial = 1164.6f + _xOffset;
						_width = -566.85f + _widthOffset;
						_height = 175.1f + _heightOffset;
						_firstLineWidth = 20f;
						_angleLine2 = 4f;
						_topLine2 = 0f;
						break;
					case 4:
						_offsetLevel = 14f;
						_xInitial = 1310f + _xOffset;
						_width = -712.25f + _widthOffset;
						_height = 179.1f + _heightOffset;
						_firstLineWidth = 20f;
						_angleLine2 = 6.6f;
						_topLine2 = 0f;
						break;
					case 5:
						_offsetLevel = 18f;
						_xInitial = 1453.25f + _xOffset;
						_width = -855.46f + _widthOffset;
						_height = 183.13f + _heightOffset;
						_firstLineWidth = 20f;
						_angleLine2 = 9f;
						_topLine2 = 0f;
						break;
					case 6:
						_offsetLevel = 22f;
						_xInitial = 1597.65f + _xOffset;
						_width = -999.95f + _widthOffset;
						_height = 187.17f + _heightOffset;
						_firstLineWidth = 20f;
						_angleLine2 = 11f;
						_topLine2 = 3.11f;
						break;
					}
					break;
				case 3:
					switch (_column) {
					case 1:
						_offsetLevel = 1f;
						_xInitial = 870f + _xOffset;
						_width = -271.9f + _widthOffset;
						_height = 252.9f + _heightOffset;
						_firstLineWidth = 22f;
						_angleLine2 = -2f;
						_topLine2 = 0f;
						break;
					case 2:
						_offsetLevel = 5f;
						_xInitial = 1020f + _xOffset;
						_width = -422f + _widthOffset;
						_height = 256.96f + _heightOffset;
						_firstLineWidth = 18f;
						_angleLine2 = 1f;
						_topLine2 = 0f;
						break;
					case 3:
						_offsetLevel = 9f;
						_xInitial = 1169.86f + _xOffset;
						_width = -571.87f + _widthOffset;
						_height = 260.78f + _heightOffset;
						_firstLineWidth = 18f;
						_angleLine2 = 4f;
						_topLine2 = 0f;
						break;
					case 4:
						_offsetLevel = 13f;
						_xInitial = 1318.69f + _xOffset;
						_width = -720.8f + _widthOffset;
						_height = 264.83f + _heightOffset;
						_firstLineWidth = 18f;
						_angleLine2 = 6.6f;
						_topLine2 = 0f;
						break;
					case 5:
						_offsetLevel = 17f;
						_xInitial = 1466.2f + _xOffset;
						_width = -868.406f + _widthOffset;
						_height = 268.857f + _heightOffset;
						_firstLineWidth = 18f;
						_angleLine2 = 9f;
						_topLine2 = 3.031f;
						break;
					case 6:
						_offsetLevel = 21f;
						_xInitial = 1615.241f + _xOffset;
						_width = -1017.44f + _widthOffset;
						_height = 272.81f + _heightOffset;
						_firstLineWidth = 18f;
						_angleLine2 = 11f;
						_topLine2 = 4.7106f;
						break;
					}
					break;
				case 4:
					switch (_column) {
					case 1:
						_offsetLevel = 0f;
						_xInitial = 866.14f + _xOffset;
						_width = -268f + _widthOffset;
						_height = 343.75f + _heightOffset;
						_firstLineWidth = 20f;
						_angleLine2 = -2f;
						_topLine2 = 0f;
						break;
					case 2:
						_offsetLevel = 4f;
						_xInitial = 1020.85f + _xOffset;
						_width = -422.3f + _widthOffset;
						_height = 347.9f + _heightOffset;
						_firstLineWidth = 16f;
						_angleLine2 = 1f;
						_topLine2 = 0f;
						break;
					case 3:
						_offsetLevel = 8f;
						_xInitial = 1174.699f + _xOffset;
						_width = -576.2f + _widthOffset;
						_height = 351.98f + _heightOffset;
						_firstLineWidth = 16f;
						_angleLine2 = 4f;
						_topLine2 = 0f;
						break;
					case 4:
						_offsetLevel = 12f;
						_xInitial = 1328.28f + _xOffset;
						_width = -729.88f + _widthOffset;
						_height = 355.89f + _heightOffset;
						_firstLineWidth = 16f;
						_angleLine2 = 6.6f;
						_topLine2 = 2.1415f;
						break;
					case 5:
						_offsetLevel = 16f;
						_xInitial = 1480.55f + _xOffset;
						_width = -882.15f + _widthOffset;
						_height = 359.93f + _heightOffset;
						_firstLineWidth = 16f;
						_angleLine2 = 9f;
						_topLine2 = 4.1661f;
						break;
					case 6:
						_offsetLevel = 20f;
						_xInitial = 1634.44f + _xOffset;
						_width = -1036.143f + _widthOffset;
						_height = 363.98f + _heightOffset;
						_firstLineWidth = 16f;
						_angleLine2 = 11f;
						_topLine2 = 6.4169f;
						break;
					}
					break;
				}

				float _xEndPosition = -2.0156f * (float)_input + 0.5919f;
				float _yEndPosition = -0.0844f * ((float)_input * (float)_input) - 15.225f * (float)_input - 57.2f;


				gameObject.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (_xInitial, -54f + _offsetLevel);
				gameObject.GetComponent<RectTransform> ().sizeDelta = new Vector2 (_width, _height);
				_angleRotation.eulerAngles = new Vector3 (0, 0, _angleLine2);
				_initialPoint.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (-3f, 0f);
				_line1.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0f, 0.6f);
				_line1.GetComponent<RectTransform> ().sizeDelta = new Vector2 (_firstLineWidth, _lineWidth);
				_line2.transform.rotation = _angleRotation;
				_line2.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (_line1.GetComponent<RectTransform> ().anchoredPosition.x + _line1.GetComponent<RectTransform> ().rect.width - _lineWidth, 0f);
				_line2.GetComponent<RectTransform> ().offsetMax = new Vector2 (_line2.GetComponent<RectTransform> ().offsetMax.x, _topLine2);
				_line4.GetComponent<RectTransform> ().sizeDelta = new Vector2 (_lineWidth, -_yEndPosition + _lineWidth + _offsetLevel - _lineWidth / 2f);
				_line4.GetComponent<RectTransform> ().anchoredPosition = new Vector2 ((_xEndPosition - 4.5f) - (12f + (float)_input) - (Mathf.Tan (_line4.transform.rotation.eulerAngles.z * Mathf.PI / 180f) * _line4.GetComponent<RectTransform> ().rect.height), 0f);
				_line3.GetComponent<RectTransform> ().pivot = new Vector2 (1, 0.5f);
				//_line3.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (_line1.GetComponent<RectTransform> ().anchoredPosition.x + _line1.GetComponent<RectTransform> ().rect.width + gameObject.GetComponent<RectTransform> ().rect.height * Mathf.Abs (Mathf.Tan (_line2.transform.rotation.eulerAngles.z * Mathf.PI / 180f)) - _lineWidth, -_lineWidth / 2f);
				_line3.GetComponent<RectTransform> ().anchoredPosition = new Vector2(_line1.GetComponent<RectTransform> ().anchoredPosition.x + _line1.GetComponent<RectTransform> ().rect.width - gameObject.GetComponent<RectTransform> ().rect.height * Mathf.Tan (_line2.transform.rotation.eulerAngles.z * Mathf.PI / 180f), -_lineWidth / 2f);
				_line3.GetComponent<RectTransform> ().sizeDelta = new Vector2 (Mathf.Abs (gameObject.GetComponent<RectTransform> ().rect.width - _line3.GetComponent<RectTransform> ().anchoredPosition.x + _line4.GetComponent<RectTransform> ().anchoredPosition.x), _lineWidth);
				_line5.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (_xEndPosition - 4.5f, _yEndPosition - _offsetLevel);
				_line5.GetComponent<RectTransform> ().sizeDelta = new Vector2 (12f + (float)_input, _lineWidth);
				_endPoint.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (_xEndPosition, _yEndPosition - _offsetLevel);
			}
			break;
		}
	}
}
