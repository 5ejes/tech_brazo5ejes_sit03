﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class whiteCable : MonoBehaviour {

	public Image _initialPoint;
	public Image _line1;
	public Image _line2;
	public Image _line3;
	public Image _line4;
	public Image _line5;
	public Image _endPoint;

	public GameObject _parentElement;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}

	public void setPosition(int _row, int _column, int _input){

		Quaternion _angleRotation = new Quaternion ();

		float _lineWidth = 0;
		float _offsetLevel = 0;
		float _xInitial = 0;
		float _yInitial = 0;
		float _width = 0;
		float _height = 0;
		float _angleLine2 = 0;
		float _firstLineWidth = 0;
		float _topLine2 = 0;
		float _bottomLine2 = 0;
		float _xOffset = 0;
		float _widthOffset = 0;
		float _heightOffset = 0;

		_lineWidth = 2f;

		float _endX = 0;
		float _endY = 0;
		float _heightOffset4 = 0;
		float _lineWidth4 = 0;

		switch (Manager.Instance.globalGraphicInterface) {
		case baseSystem.graphicInterfaces.electrical_bench:
			if(_parentElement){
				switch(_parentElement.GetComponent<dragElement>()._typeComponent){
				case dragElement.typesComponents.limit_switch_sensor:
					//_heightOffset = 2.08f;
					break;
				case dragElement.typesComponents.green_button_sensor:
				case dragElement.typesComponents.red_button_sensor:
				case dragElement.typesComponents.switch_sensor:
					//_xOffset = 2;
					//_widthOffset = -2;
					break;
				}
			}

			switch(_row){
			case 1:
				switch(_column){
				case 1:
					_offsetLevel = 15f;
					_angleLine2 = 12f;
					_xInitial = 1596.541f + _xOffset;
					_yInitial = -58.92f;
					_width = 203.38f + _widthOffset;
					_height = 312.48f + _heightOffset;
					_firstLineWidth = 42f;
					_topLine2 = 1.1f;
					_bottomLine2 = -9f;
					break;
				case 2:
					_offsetLevel = 11f;
					_angleLine2 = 12f;
					_xInitial = 1460.78f + _xOffset;
					_yInitial = -59.39f;
					_width = 338.72f + _widthOffset;
					_height = 312.48f + _heightOffset;
					_firstLineWidth = 33f;
					_topLine2 = 0.5f;
					_bottomLine2 = -8f;
					break;
				case 3:
					_offsetLevel = 7f;
					_angleLine2 = 8.7f;
					_xInitial = 1324.76f + _xOffset;
					_yInitial = -60f;
					_width = 474.56f + _widthOffset;
					_height = 312.48f + _heightOffset;
					_firstLineWidth = 33f;
					_topLine2 = 0.5f;
					_bottomLine2 = -4f;
					break;
				case 4:
					_offsetLevel = 3f;
					_angleLine2 = 6.6f;
					_xInitial = 1187.68f + _xOffset;
					_yInitial = -59.39f;
					_width = 612.7f + _widthOffset;
					_height = 312.48f + _heightOffset;
					_firstLineWidth = 33f;
					_topLine2 = 0.5f;
					_bottomLine2 = -2.5f;
					break;
				}
				break;
			case 2:
				switch (_column) {
				case 1:
					_offsetLevel = 14f;
					_angleLine2 = 12f;
					_firstLineWidth = 38f;
					_xInitial = 1612.6f + _xOffset;
					_yInitial = -140f;
					_width = 184.88f + _widthOffset;
					_height = 232.17f + _heightOffset;
					_topLine2 = 1.1f;
					_bottomLine2 = -8f;
					break;
				case 2:
					_offsetLevel = 10f;
					_angleLine2 = 12f;
					_firstLineWidth = 32f;
					_xInitial = 1472.82f + _xOffset;
					_yInitial = -140f;
					_width = 324.71f + _widthOffset;
					_height = 232.17f + _heightOffset;
					_topLine2 = 1.1f;
					_bottomLine2 = -6f;
					break;
				case 3:
					_offsetLevel = 6f;
					_angleLine2 = 8.7f;
					_firstLineWidth = 34f;
					_xInitial = 1332.7f + _xOffset;
					_yInitial = -140f;
					_width = 464.88f + _widthOffset;
					_height = 232.17f + _heightOffset;
					_topLine2 = 1.1f;
					_bottomLine2 = -3.5f;
					break;
				case 4:
					_offsetLevel = 2f;
					_angleLine2 = 6.6f;
					_firstLineWidth = 34f;
					_xInitial = 1192.22f + _xOffset;
					_yInitial = -140f;
					_width = 605.38f + _widthOffset;
					_height = 232.17f + _heightOffset;
					_topLine2 = 1.1f;
					_bottomLine2 = -2f;
					break;
				}
				break;
			case 3:
				switch (_column) {
				case 1:
					_offsetLevel = 13f;
					_angleLine2 = 12f;
					_firstLineWidth = 34f;
					_xInitial = 1630.49f + _xOffset;
					_yInitial = -224f;
					_width = 164.98f + _widthOffset;
					_height = 148.14f + _heightOffset;
					_topLine2 = 1.1f;
					_bottomLine2 = -5.5f;
					break;
				case 2:
					_offsetLevel = 9f;
					_angleLine2 = 12f;
					_firstLineWidth = 31f;
					_xInitial = 1486.63f + _xOffset;
					_yInitial = -224f;
					_width = 308.9f + _widthOffset;
					_height = 148.14f + _heightOffset;
					_topLine2 = 1.1f;
					_bottomLine2 = -4f;
					break;
				case 3:
					_offsetLevel = 5f;
					_firstLineWidth = 32f;
					_angleLine2 = 8.7f;
					_xInitial = 1342.9f + _xOffset;
					_yInitial = -224f;
					_width = 453.18f + _widthOffset;
					_height = 148.14f + _heightOffset;
					_topLine2 = 1.1f;
					_bottomLine2 = -2.5f;
					break;
				case 4:
					_offsetLevel = 1f;
					_firstLineWidth = 32f;
					_angleLine2 = 6.6f;
					_xInitial = 1197.22f + _xOffset;
					_yInitial = -224f;
					_width = 598.41f + _widthOffset;
					_height = 148.14f + _heightOffset;
					_topLine2 = 1.1f;
					_bottomLine2 = -1.5f;
					break;
				}
				break;
			case 4:
				switch (_column) {
				case 1:
					_offsetLevel = 12f;
					_firstLineWidth = 30f;
					_angleLine2 = 12f;
					_xInitial = 1652.27f + _xOffset;
					_yInitial = -316.12f;
					_width = 140.46f + _widthOffset;
					_height = 56f + _heightOffset;
					_topLine2 = 1.1f;
					_bottomLine2 = -3f;
					break;
				case 2:
					_offsetLevel = 8f;
					_angleLine2 = 12f;
					_firstLineWidth = 30f;
					_xInitial = 1502.88f + _xOffset;
					_yInitial = -316.12f;
					_width = 290.64f + _widthOffset;
					_height = 56f + _heightOffset;
					_topLine2 = 1.1f;
					_bottomLine2 = -2f;
					break;
				case 3:
					_offsetLevel = 4f;
					_firstLineWidth = 30f;
					_angleLine2 = 8.7f;
					_xInitial = 1354.47f + _xOffset;
					_yInitial = -316.12f;
					_width = 439.1f + _widthOffset;
					_height = 56f + _heightOffset;
					_topLine2 = 1.1f;
					_bottomLine2 = -1f;
					break;
				case 4:
					_offsetLevel = 0f;
					_firstLineWidth = 30f;
					_angleLine2 = 6.6f;
					_xInitial = 1204.8f + _xOffset;
					_yInitial = -316.12f;
					_width = 588.82f + _widthOffset;
					_height = 56f + _heightOffset;
					_topLine2 = 1.1f;
					_bottomLine2 = -0.5f;
					break;
				}
				break;
			}

			gameObject.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (_xInitial, _yInitial);
			gameObject.GetComponent<RectTransform> ().sizeDelta = new Vector2 (_width, _height + _offsetLevel);
			_angleRotation.eulerAngles = new Vector3 (0, 0, _angleLine2);
			_initialPoint.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (-3f, 0f);
			_line1.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0f, 0.6f);
			_line1.GetComponent<RectTransform> ().sizeDelta = new Vector2 (_firstLineWidth, _lineWidth);
			_line2.transform.rotation = _angleRotation;
			_line2.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (_line1.GetComponent<RectTransform> ().anchoredPosition.x + _line1.GetComponent<RectTransform> ().rect.width, 0f);
			_line2.GetComponent<RectTransform> ().offsetMax = new Vector2 (_line2.GetComponent<RectTransform> ().offsetMax.x, _topLine2);
			_line2.GetComponent<RectTransform> ().offsetMin = new Vector2 (_line2.GetComponent<RectTransform> ().offsetMin.x, _bottomLine2);

			_endX = 0;
			_endY = 0;
			_heightOffset4 = 0;
			_lineWidth4 = 0;

			switch (_row) {
			case 1:
				_endX = -1.5f;
				_endY = 24f;
				_heightOffset4 = 1;
				_lineWidth4 = 24f;
				break;
			case 2:
				_endX = -25.8f;
				_endY = 128f;
				_heightOffset4 = 3.5f;
				_lineWidth4 = 24f;
				break;
			case 3:
				_endX = -48.5f;
				_endY = 223f;
				_heightOffset4 = 7f;
				_lineWidth4 = 24f;
				break;
			case 4:
				_endX = -70f;
				_endY = 314f;
				_heightOffset4 = 8f;
				_lineWidth4 = 24f;
				break;
			}

			_endPoint.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (_endX, _endY + _offsetLevel);

			_line5.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (_endX - 6.5f, _endY + _offsetLevel);
			_line5.GetComponent<RectTransform> ().sizeDelta = new Vector2 (_lineWidth4 + (float)_input, _lineWidth);

			_line4.GetComponent<RectTransform> ().sizeDelta = new Vector2 (_lineWidth, _endY + _offsetLevel + _heightOffset4);
			_line4.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (-(Mathf.Abs (_line5.GetComponent<RectTransform> ().anchoredPosition.x) + _line5.GetComponent<RectTransform> ().rect.width - Mathf.Abs ((_line4.GetComponent<RectTransform> ().rect.height - _heightOffset4) * Mathf.Tan (_line4.transform.rotation.eulerAngles.z * Mathf.PI / 180f)) - _lineWidth), 0f);

			_line3.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (_line1.GetComponent<RectTransform> ().anchoredPosition.x + _line1.GetComponent<RectTransform> ().rect.width + gameObject.GetComponent<RectTransform> ().rect.height * Mathf.Abs (Mathf.Tan (_line2.transform.rotation.eulerAngles.z * Mathf.PI / 180f)) - _lineWidth, -_lineWidth / 2f);
			_line3.GetComponent<RectTransform> ().sizeDelta = new Vector2 (gameObject.GetComponent<RectTransform> ().rect.width - _line3.GetComponent<RectTransform> ().anchoredPosition.x + _line4.GetComponent<RectTransform> ().anchoredPosition.x, _lineWidth);

			break;
		case baseSystem.graphicInterfaces.pneumatic_bench:
			if(_parentElement){
				switch(_parentElement.GetComponent<dragElementPneumatic>()._typeComponent){
				case dragElementPneumatic.typesComponents.limit_switch_sensor:
					//_heightOffset = 2.08f;
					break;
				case dragElementPneumatic.typesComponents.green_button_sensor:
				case dragElementPneumatic.typesComponents.red_button_sensor:
				case dragElementPneumatic.typesComponents.switch_sensor:
					//_xOffset = 2;
					//_widthOffset = -2;
					break;
				}
			}

			switch(_row){
			case 1:
				switch(_column){
				case 1:
					_offsetLevel = 15f;
					_angleLine2 = -2f;
					_xInitial = 876.8f + _xOffset;
					_yInitial = -134.7f;
					_width = 897.5f + _widthOffset;
					_height = 311.7f + _heightOffset;
					_firstLineWidth = 36f;
					_topLine2 = 1.1f;
					_bottomLine2 = -0f;
					break;
				case 2:
					_offsetLevel = 11f;
					_angleLine2 = 1f;
					_xInitial = 1018.5f + _xOffset;
					_yInitial = -135.33f;
					_width = 756.3f + _widthOffset;
					_height = 312.17f + _heightOffset;
					_firstLineWidth = 33f;
					_topLine2 = 0.5f;
					_bottomLine2 = -0f;
					break;
				case 3:
					_offsetLevel = 7f;
					_angleLine2 = 4f;
					_xInitial = 1160f + _xOffset;
					_yInitial = -135f;
					_width = 614.9f + _widthOffset;
					_height = 311.3f + _heightOffset;
					_firstLineWidth = 33f;
					_topLine2 = 0.5f;
					_bottomLine2 = -0f;
					break;
				case 4:
					_offsetLevel = 3f;
					_angleLine2 = 6.6f;
					_xInitial = 1301f + _xOffset;
					_yInitial = -135.5f;
					_width = 472.9f + _widthOffset;
					_height = 309.8f + _heightOffset;
					_firstLineWidth = 33f;
					_topLine2 = 0.5f;
					_bottomLine2 = -0f;
					break;
				case 5:
					_offsetLevel = 3f;
					_angleLine2 = 9f;
					_xInitial = 1441f + _xOffset;
					_yInitial = -135.5f;
					_width = 332.9f + _widthOffset;
					_height = 309.8f + _heightOffset;
					_firstLineWidth = 33f;
					_topLine2 = 0.5f;
					_bottomLine2 = -0f;
					break;
				case 6:
					_offsetLevel = 3f;
					_angleLine2 = 11f;
					_xInitial = 1581.5f + _xOffset;
					_yInitial = -135.5f;
					_width = 192.2f + _widthOffset;
					_height = 309.8f + _heightOffset;
					_firstLineWidth = 26f;
					_topLine2 = 0.5f;
					_bottomLine2 = -0f;
					break;
				}
				break;
			case 2:
				switch (_column) {
				case 1:
					_offsetLevel = 14f;
					_angleLine2 = -2f;
					_firstLineWidth = 34f;
					_xInitial = 873.3f + _xOffset;
					_yInitial = -217.9f;
					_width = 901f + _widthOffset;
					_height = 231.75f + _heightOffset;
					_topLine2 = 1.1f;
					_bottomLine2 = -0f;
					break;
				case 2:
					_offsetLevel = 10f;
					_angleLine2 = 1f;
					_firstLineWidth = 32f;
					_xInitial = 1019.22f + _xOffset;
					_yInitial = -218.11f;
					_width = 755.2f + _widthOffset;
					_height = 234.42f + _heightOffset;
					_topLine2 = 1.1f;
					_bottomLine2 = -0f;
					break;
				case 3:
					_offsetLevel = 6f;
					_angleLine2 = 4f;
					_firstLineWidth = 34f;
					_xInitial = 1164.21f + _xOffset;
					_yInitial = -217.79f;
					_width = 609.84f + _widthOffset;
					_height = 234.08f + _heightOffset;
					_topLine2 = 1.1f;
					_bottomLine2 = -0f;
					break;
				case 4:
					_offsetLevel = 2f;
					_angleLine2 = 6.6f;
					_firstLineWidth = 34f;
					_xInitial = 1309.38f + _xOffset;
					_yInitial = -217.84f;
					_width = 464.22f + _widthOffset;
					_height = 234.5f + _heightOffset;
					_topLine2 = 1.1f;
					_bottomLine2 = -0f;
					break;
				case 5:
					_offsetLevel = 2f;
					_angleLine2 = 9f;
					_firstLineWidth = 34f;
					_xInitial = 1452.74f + _xOffset;
					_yInitial = -217.84f;
					_width = 320.77f + _widthOffset;
					_height = 234.5f + _heightOffset;
					_topLine2 = 1.1f;
					_bottomLine2 = -0f;
					break;
				case 6:
					_offsetLevel = 2f;
					_angleLine2 = 11f;
					_firstLineWidth = 29f;
					_xInitial = 1596.83f + _xOffset;
					_yInitial = -217.84f;
					_width = 176.63f + _widthOffset;
					_height = 234.5f + _heightOffset;
					_topLine2 = 1.1f;
					_bottomLine2 = -0f;
					break;
				}
				break;
			case 3:
				switch (_column) {
				case 1:
					_offsetLevel = 13f;
					_angleLine2 = -2f;
					_firstLineWidth = 32f;
					_xInitial = 869.12f + _xOffset;
					_yInitial = -304.89f;
					_width = 903.85f + _widthOffset;
					_height = 150.23f + _heightOffset;
					_topLine2 = 1.1f;
					_bottomLine2 = -0f;
					break;
				case 2:
					_offsetLevel = 9f;
					_angleLine2 = 1f;
					_firstLineWidth = 31f;
					_xInitial = 1019.24f + _xOffset;
					_yInitial = -305.07f;
					_width = 753.35f + _widthOffset;
					_height = 150.23f + _heightOffset;
					_topLine2 = 1.1f;
					_bottomLine2 = -0f;
					break;
				case 3:
					_offsetLevel = 5f;
					_firstLineWidth = 32f;
					_angleLine2 = 4f;
					_xInitial = 1169.11f + _xOffset;
					_yInitial = -304.71f;
					_width = 603.55f + _widthOffset;
					_height = 149.75f + _heightOffset;
					_topLine2 = 1.1f;
					_bottomLine2 = -0f;
					break;
				case 4:
					_offsetLevel = 1f;
					_firstLineWidth = 32f;
					_angleLine2 = 6.6f;
					_xInitial = 1318.02f + _xOffset;
					_yInitial = -304.75f;
					_width = 454.85f + _widthOffset;
					_height = 150.06f + _heightOffset;
					_topLine2 = 1.1f;
					_bottomLine2 = -0f;
					break;
				case 5:
					_offsetLevel = 1f;
					_firstLineWidth = 32f;
					_angleLine2 = 9f;
					_xInitial = 1465.85f + _xOffset;
					_yInitial = -304.75f;
					_width = 306.95f + _widthOffset;
					_height = 150.06f + _heightOffset;
					_topLine2 = 1.1f;
					_bottomLine2 = -0f;
					break;
				case 6:
					_offsetLevel = 1f;
					_firstLineWidth = 31f;
					_angleLine2 = 11f;
					_xInitial = 1614.63f + _xOffset;
					_yInitial = -304.75f;
					_width = 158.12f + _widthOffset;
					_height = 150.06f + _heightOffset;
					_topLine2 = 1.1f;
					_bottomLine2 = -0f;
					break;
				}
				break;
			case 4:
				switch (_column) {
				case 1:
					_offsetLevel = 12f;
					_firstLineWidth = 30f;
					_angleLine2 = -2f;
					_xInitial = 865.2f + _xOffset;
					_yInitial = -396.93f;
					_width = 906.24f + _widthOffset;
					_height = 61.6f + _heightOffset;
					_topLine2 = 1.1f;
					_bottomLine2 = -0f;
					break;
				case 2:
					_offsetLevel = 8f;
					_angleLine2 = 1f;
					_firstLineWidth = 30f;
					_xInitial = 1020f + _xOffset;
					_yInitial = -396.82f;
					_width = 751.5f + _widthOffset;
					_height = 62f + _heightOffset;
					_topLine2 = 1.1f;
					_bottomLine2 = -0f;
					break;
				case 3:
					_offsetLevel = 4f;
					_firstLineWidth = 30f;
					_angleLine2 = 4f;
					_xInitial = 1173.55f + _xOffset;
					_yInitial = -396.73f;
					_width = 598.2f + _widthOffset;
					_height = 62.25f + _heightOffset;
					_topLine2 = 1.1f;
					_bottomLine2 = -0f;
					break;
				case 4:
					_offsetLevel = 0f;
					_firstLineWidth = 30f;
					_angleLine2 = 6.6f;
					_xInitial = 1327.84f + _xOffset;
					_yInitial = -396.96f;
					_width = 445f + _widthOffset;
					_height = 62.27f + _heightOffset;
					_topLine2 = 1.1f;
					_bottomLine2 = -0f;
					break;
				case 5:
					_offsetLevel = 0f;
					_firstLineWidth = 30f;
					_angleLine2 = 9f;
					_xInitial = 1479.89f + _xOffset;
					_yInitial = -396.96f;
					_width = 292.91f + _widthOffset;
					_height = 62.27f + _heightOffset;
					_topLine2 = 1.1f;
					_bottomLine2 = -0f;
					break;
				case 6:
					_offsetLevel = 0f;
					_firstLineWidth = 34f;
					_angleLine2 = 11f;
					_xInitial = 1633.84f + _xOffset;
					_yInitial = -396.96f;
					_width = 138.86f + _widthOffset;
					_height = 62.27f + _heightOffset;
					_topLine2 = 1.1f;
					_bottomLine2 = -0f;
					break;
				}
				break;
			}

			gameObject.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (_xInitial, _yInitial);
			gameObject.GetComponent<RectTransform> ().sizeDelta = new Vector2 (_width, _height + _offsetLevel);
			_angleRotation.eulerAngles = new Vector3 (0, 0, _angleLine2);
			_initialPoint.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (-3f, 0f);
			_line1.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0f, 0.6f);
			_line1.GetComponent<RectTransform> ().sizeDelta = new Vector2 (_firstLineWidth, _lineWidth);
			_line2.transform.rotation = _angleRotation;
			_line2.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (_line1.GetComponent<RectTransform> ().anchoredPosition.x + _line1.GetComponent<RectTransform> ().rect.width, 0f);
			_line2.GetComponent<RectTransform> ().offsetMax = new Vector2 (_line2.GetComponent<RectTransform> ().offsetMax.x, _topLine2);
			_line2.GetComponent<RectTransform> ().offsetMin = new Vector2 (_line2.GetComponent<RectTransform> ().offsetMin.x, _bottomLine2);

			_endX = 0;
			_endY = 0;
			_heightOffset4 = 0;
			_lineWidth4 = 0;

			switch (_row) {
			case 1:
				_endX = -1.5f;
				_endY = 38f;
				_heightOffset4 = 1;
				_lineWidth4 = 24f;
				break;
			case 2:
				_endX = -25.8f;
				_endY = 142f;
				_heightOffset4 = 3.5f;
				_lineWidth4 = 24f;
				break;
			case 3:
				_endX = -48.5f;
				_endY = 237f;
				_heightOffset4 = 7f;
				_lineWidth4 = 24f;
				break;
			case 4:
				_endX = -70f;
				_endY = 328f;
				_heightOffset4 = 8f;
				_lineWidth4 = 24f;
				break;
			}

			_endPoint.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (_endX, _endY + _offsetLevel);

			_line5.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (_endX - 6.5f, _endY + _offsetLevel);
			_line5.GetComponent<RectTransform> ().sizeDelta = new Vector2 (_lineWidth4 + (float)_input, _lineWidth);

			_line4.GetComponent<RectTransform> ().sizeDelta = new Vector2 (_lineWidth, _endY + _offsetLevel + _heightOffset4);
			_line4.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (-(Mathf.Abs (_line5.GetComponent<RectTransform> ().anchoredPosition.x) + _line5.GetComponent<RectTransform> ().rect.width - Mathf.Abs ((_line4.GetComponent<RectTransform> ().rect.height - _heightOffset4) * Mathf.Tan (_line4.transform.rotation.eulerAngles.z * Mathf.PI / 180f)) - _lineWidth), 0f);

			_line3.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (_line1.GetComponent<RectTransform> ().anchoredPosition.x + _line1.GetComponent<RectTransform> ().rect.width + gameObject.GetComponent<RectTransform> ().rect.height * (Mathf.Tan (_line2.transform.rotation.eulerAngles.z * Mathf.PI / 180f)) - _lineWidth, -_lineWidth / 2f);
			_line3.GetComponent<RectTransform> ().sizeDelta = new Vector2 (gameObject.GetComponent<RectTransform> ().rect.width - _line3.GetComponent<RectTransform> ().anchoredPosition.x + _line4.GetComponent<RectTransform> ().anchoredPosition.x, _lineWidth);

			break;
		}
	}
}
