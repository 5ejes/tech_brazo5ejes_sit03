﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class blackCable : MonoBehaviour {

	public Image _initialPoint;
	public Image _line1;
	public Image _line2;
	public Image _line3;
	public Image _line4;
	public Image _line5;
	public Image _endPoint;

	public GameObject _parentElement;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}

	public void setPosition(int _row, int _column, int _input, bool _isRight = false){

		Quaternion _angleRotation = new Quaternion ();

		float _lineWidth = 0;
		float _offsetLevel = 0;
		float _xInitial = 0;
		float _yInitial = 0;
		float _width = 0;
		float _height = 0;
		float _angleLine2 = 0;
		float _firstLineWidth = 0;
		float _topLine2 = 0;
		float _bottomLine2 = 0;
		float _xOffset = 0;
		float _widthOffset = 0;
		float _heightOffset = 0;

		_lineWidth = 2f;

		float _endX = 0;
		float _endY = 0;
		float _heightOffset4 = 0;
		float _lineWidth4 = 0;

		switch (Manager.Instance.globalGraphicInterface) {
		case baseSystem.graphicInterfaces.electrical_bench:
			if (_parentElement) {
				switch(_parentElement.GetComponent<dragElement>()._typeComponent){
				case dragElement.typesComponents.limit_switch_sensor:
					//_heightOffset = 2.08f;
					break;
				case dragElement.typesComponents.green_button_sensor:
				case dragElement.typesComponents.red_button_sensor:
				case dragElement.typesComponents.switch_sensor:
					//_xOffset = 2;
					//_widthOffset = -2;
					break;
				}
			}

			switch(_row){
			case 1:
				switch(_column){
				case 1:
					_offsetLevel = 15f;
					_angleLine2 = -12f;
					_xInitial = 374.33f + _xOffset;
					_yInitial = -58.92f;
					_width = 201.52f + _widthOffset;
					_height = 312.48f + _heightOffset;
					_firstLineWidth = 42f;
					_topLine2 = 1.1f;
					_bottomLine2 = -9f;
					break;
				case 2:
					_offsetLevel = 11f;
					_angleLine2 = -12f;
					_xInitial = 510.16f + _xOffset;
					_yInitial = -59.39f;
					_width = 337.5f + _widthOffset;
					_height = 312.48f + _heightOffset;
					_firstLineWidth = 33f;
					_topLine2 = 0.5f;
					_bottomLine2 = -8f;
					break;
				case 3:
					_offsetLevel = 7f;
					_angleLine2 = -8.7f;
					_xInitial = 645.9f + _xOffset;
					_yInitial = -60f;
					_width = 473.32f + _widthOffset;
					_height = 312.48f + _heightOffset;
					_firstLineWidth = 33f;
					_topLine2 = 0.5f;
					_bottomLine2 = -4f;
					break;
				case 4:
					_offsetLevel = 3f;
					_angleLine2 = -6.6f;
					_xInitial = 782.94f + _xOffset;
					_yInitial = -59.39f;
					_width = 610.38f + _widthOffset;
					_height = 312.48f + _heightOffset;
					_firstLineWidth = 33f;
					_topLine2 = 0.5f;
					_bottomLine2 = -2.5f;
					break;
				}
				break;
			case 2:
				switch (_column) {
				case 1:
					_offsetLevel = 14f;
					_angleLine2 = -12f;
					_firstLineWidth = 38f;
					_xInitial = 357.7f + _xOffset;
					_yInitial = -140f;
					_width = 184.88f + _widthOffset;
					_height = 232.17f + _heightOffset;
					_topLine2 = 1.1f;
					_bottomLine2 = -7f;
					break;
				case 2:
					_offsetLevel = 10f;
					_angleLine2 = -12f;
					_firstLineWidth = 32f;
					_xInitial = 497.48f + _xOffset;
					_yInitial = -140f;
					_width = 324.71f + _widthOffset;
					_height = 232.17f + _heightOffset;
					_topLine2 = 1.1f;
					_bottomLine2 = -4.5f;
					break;
				case 3:
					_offsetLevel = 6f;
					_angleLine2 = -8.7f;
					_firstLineWidth = 34f;
					_xInitial = 637.6f + _xOffset;
					_yInitial = -140f;
					_width = 464.88f + _widthOffset;
					_height = 232.17f + _heightOffset;
					_topLine2 = 1.1f;
					_bottomLine2 = -3.5f;
					break;
				case 4:
					_offsetLevel = 2f;
					_angleLine2 = -6.6f;
					_firstLineWidth = 34f;
					_xInitial = 778.08f + _xOffset;
					_yInitial = -140f;
					_width = 605.38f + _widthOffset;
					_height = 232.17f + _heightOffset;
					_topLine2 = 1.1f;
					_bottomLine2 = -2f;
					break;
				}
				break;
			case 3:
				switch (_column) {
				case 1:
					_offsetLevel = 13f;
					_angleLine2 = -12f;
					_firstLineWidth = 34f;
					_xInitial = 339.81f + _xOffset;
					_yInitial = -224f;
					_width = 166.98f + _widthOffset;
					_height = 148.14f + _heightOffset;
					_topLine2 = 1.1f;
					_bottomLine2 = -4f;
					break;
				case 2:
					_offsetLevel = 9f;
					_angleLine2 = -12f;
					_firstLineWidth = 31f;
					_xInitial = 483.67f + _xOffset;
					_yInitial = -224f;
					_width = 310.9f + _widthOffset;
					_height = 148.14f + _heightOffset;
					_topLine2 = 1.1f;
					_bottomLine2 = -4f;
					break;
				case 3:
					_offsetLevel = 5f;
					_firstLineWidth = 32f;
					_angleLine2 = -8.7f;
					_xInitial = 627.9f + _xOffset;
					_yInitial = -224f;
					_width = 455.18f + _widthOffset;
					_height = 148.14f + _heightOffset;
					_topLine2 = 1.1f;
					_bottomLine2 = -2.5f;
					break;
				case 4:
					_offsetLevel = 1f;
					_firstLineWidth = 32f;
					_angleLine2 = -6.6f;
					_xInitial = 773.08f + _xOffset;
					_yInitial = -224f;
					_width = 600.41f + _widthOffset;
					_height = 148.14f + _heightOffset;
					_topLine2 = 1.1f;
					_bottomLine2 = -1.5f;
					break;
				}
				break;
			case 4:
				switch (_column) {
				case 1:
					_offsetLevel = 12f;
					_firstLineWidth = 30f;
					_angleLine2 = -12f;
					_xInitial = 319.41f + _xOffset;
					_yInitial = -316.12f;
					_width = 146.57f + _widthOffset;
					_height = 56f + _heightOffset;
					_topLine2 = 1.1f;
					_bottomLine2 = -3f;
					break;
				case 2:
					_offsetLevel = 8f;
					_angleLine2 = -12f;
					_firstLineWidth = 30f;
					_xInitial = 469.42f + _xOffset;
					_yInitial = -316.12f;
					_width = 296.64f + _widthOffset;
					_height = 56f + _heightOffset;
					_topLine2 = 1.1f;
					_bottomLine2 = -3f;
					break;
				case 3:
					_offsetLevel = 4f;
					_firstLineWidth = 30f;
					_angleLine2 = -8.7f;
					_xInitial = 617.83f + _xOffset;
					_yInitial = -316.12f;
					_width = 445.1f + _widthOffset;
					_height = 56f + _heightOffset;
					_topLine2 = 1.1f;
					_bottomLine2 = -1f;
					break;
				case 4:
					_offsetLevel = 0f;
					_firstLineWidth = 30f;
					_angleLine2 = -6.6f;
					_xInitial = 767.5f + _xOffset;
					_yInitial = -316.12f;
					_width = 594.82f + _widthOffset;
					_height = 56f + _heightOffset;
					_topLine2 = 1.1f;
					_bottomLine2 = -0.5f;
					break;
				}
				break;
			}

			gameObject.transform.Rotate(new Vector3(0,180,0));
			gameObject.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (_xInitial, _yInitial);
			gameObject.GetComponent<RectTransform> ().sizeDelta = new Vector2 (_width, _height + _offsetLevel);
			_angleRotation.eulerAngles = new Vector3 (0, 0, _angleLine2);
			_initialPoint.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (-3f, 0f);
			_line1.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0f, 0.6f);
			_line1.GetComponent<RectTransform> ().sizeDelta = new Vector2 (_firstLineWidth, _lineWidth);
			_line2.transform.rotation = _angleRotation;
			_line2.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (_line1.GetComponent<RectTransform> ().anchoredPosition.x + _line1.GetComponent<RectTransform> ().rect.width - _lineWidth, 0f);
			_line2.GetComponent<RectTransform> ().offsetMax = new Vector2 (_line2.GetComponent<RectTransform> ().offsetMax.x, _topLine2);
			_line2.GetComponent<RectTransform> ().offsetMin = new Vector2 (_line2.GetComponent<RectTransform> ().offsetMin.x, _bottomLine2);

			_endX = 0;
			_endY = 0;
			_heightOffset4 = 0;
			_lineWidth4 = 0;

			switch (_row) {
			case 1:
				_endX = -1.5f;
				_endY = 24f;
				_heightOffset4 = 1;
				_lineWidth4 = 24f;
				break;
			case 2:
				_endX = -25.8f;
				_endY = 128f;
				_heightOffset4 = 3.5f;
				_lineWidth4 = 24f;
				break;
			case 3:
				_endX = -48.5f;
				_endY = 225f;
				_heightOffset4 = 7f;
				_lineWidth4 = 24f;
				break;
			case 4:
				_endX = -70f;
				_endY = 316f;
				_heightOffset4 = 8f;
				_lineWidth4 = 24f;
				break;
			}

			_endPoint.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (_endX, _endY + _offsetLevel);

			_line5.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (_endX - 6.5f, _endY + _offsetLevel);
			_line5.GetComponent<RectTransform> ().sizeDelta = new Vector2 (_lineWidth4 + (float)_input, _lineWidth);

			_line4.GetComponent<RectTransform> ().sizeDelta = new Vector2 (_lineWidth, _endY + _offsetLevel + _heightOffset4);
			_line4.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (-(Mathf.Abs (_line5.GetComponent<RectTransform> ().anchoredPosition.x) + _line5.GetComponent<RectTransform> ().rect.width - Mathf.Abs ((_line4.GetComponent<RectTransform> ().rect.height - _heightOffset4) * Mathf.Tan (_line4.transform.rotation.eulerAngles.z * Mathf.PI / 180f)) - _lineWidth), 0f);

			_line3.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (_line1.GetComponent<RectTransform> ().anchoredPosition.x + _line1.GetComponent<RectTransform> ().rect.width + gameObject.GetComponent<RectTransform> ().rect.height * Mathf.Abs (Mathf.Tan (_line2.transform.rotation.eulerAngles.z * Mathf.PI / 180f)) - _lineWidth, -_lineWidth / 2f);
			_line3.GetComponent<RectTransform> ().sizeDelta = new Vector2 (gameObject.GetComponent<RectTransform> ().rect.width - _line3.GetComponent<RectTransform> ().anchoredPosition.x + _line4.GetComponent<RectTransform> ().anchoredPosition.x, _lineWidth);

			break;
		case baseSystem.graphicInterfaces.pneumatic_bench:

			if (_isRight == false) {
				if (_parentElement) {
					switch(_parentElement.GetComponent<dragElementPneumatic>()._typeComponent){
					case dragElementPneumatic.typesComponents.limit_switch_sensor:
						//_heightOffset = 2.08f;
						break;
					case dragElementPneumatic.typesComponents.green_button_sensor:
					case dragElementPneumatic.typesComponents.red_button_sensor:
					case dragElementPneumatic.typesComponents.switch_sensor:
						//_xOffset = 2;
						//_widthOffset = -2;
						break;
					}
				}

				switch(_row){
				case 1:
					switch(_column){
					case 1:
						_offsetLevel = 15f;
						_angleLine2 = -12f;
						_xInitial = 347f + _xOffset;
						_yInitial = -139.7f;
						_width = 131.9f + _widthOffset;
						_height = 302.8f + _heightOffset;
						_firstLineWidth = 22f;
						_topLine2 = 1.1f;
						_bottomLine2 = -9f;
						break;
					case 2:
						_offsetLevel = 11f;
						_angleLine2 = -12f;
						_xInitial = 483f + _xOffset;
						_yInitial = -138.2f;
						_width = 267.7f + _widthOffset;
						_height = 304f + _heightOffset;
						_firstLineWidth = 22f;
						_topLine2 = 0.5f;
						_bottomLine2 = -8f;
						break;
					}
					break;
				case 2:
					switch (_column) {
					case 1:
						_offsetLevel = 14f;
						_angleLine2 = -12f;
						_firstLineWidth = 20f;
						_xInitial = 330.7f + _xOffset;
						_yInitial = -218.4f;
						_width = 117.3f + _widthOffset;
						_height = 229f + _heightOffset;
						_topLine2 = 1.1f;
						_bottomLine2 = -7f;
						break;
					case 2:
						_offsetLevel = 10f;
						_angleLine2 = -12f;
						_firstLineWidth = 21f;
						_xInitial = 470.5f + _xOffset;
						_yInitial = -218.9f;
						_width = 256.6f + _widthOffset;
						_height = 228.4f + _heightOffset;
						_topLine2 = 1.1f;
						_bottomLine2 = -4.5f;
						break;
					}
					break;
				case 3:
					switch (_column) {
					case 1:
						_offsetLevel = 13f;
						_angleLine2 = -12f;
						_firstLineWidth = 18f;
						_xInitial = 312.6f + _xOffset;
						_yInitial = -303f;
						_width = 100.9f + _widthOffset;
						_height = 146.9f + _heightOffset;
						_topLine2 = 1.1f;
						_bottomLine2 = -4f;
						break;
					case 2:
						_offsetLevel = 9f;
						_angleLine2 = -12f;
						_firstLineWidth = 20f;
						_xInitial = 456.6f + _xOffset;
						_yInitial = -304.6f;
						_width = 244.8f + _widthOffset;
						_height = 146.1f + _heightOffset;
						_topLine2 = 1.1f;
						_bottomLine2 = -4f;
						break;
					}
					break;
				case 4:
					switch (_column) {
					case 1:
						_offsetLevel = 12f;
						_firstLineWidth = 16f;
						_angleLine2 = -12f;
						_xInitial = 292.2f + _xOffset;
						_yInitial = -395.7f;
						_width = 82.1f + _widthOffset;
						_height = 57f + _heightOffset;
						_topLine2 = 1.1f;
						_bottomLine2 = -3f;
						break;
					case 2:
						_offsetLevel = 8f;
						_angleLine2 = -12f;
						_firstLineWidth = 19f;
						_xInitial = 442.5f + _xOffset;
						_yInitial = -396.9f;
						_width = 232.8f + _widthOffset;
						_height = 56.3f + _heightOffset;
						_topLine2 = 1.1f;
						_bottomLine2 = -3f;
						break;
					}
					break;
				}

				gameObject.transform.Rotate(new Vector3(0,180,0));
				gameObject.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (_xInitial, _yInitial);
				gameObject.GetComponent<RectTransform> ().sizeDelta = new Vector2 (_width, _height + _offsetLevel);
				_angleRotation.eulerAngles = new Vector3 (0, 0, _angleLine2);
				_initialPoint.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (-3f, 0f);
				_line1.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0f, 0.6f);
				_line1.GetComponent<RectTransform> ().sizeDelta = new Vector2 (_firstLineWidth, _lineWidth);
				_line2.transform.rotation = _angleRotation;
				_line2.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (_line1.GetComponent<RectTransform> ().anchoredPosition.x + _line1.GetComponent<RectTransform> ().rect.width - _lineWidth, 0f);
				_line2.GetComponent<RectTransform> ().offsetMax = new Vector2 (_line2.GetComponent<RectTransform> ().offsetMax.x, _topLine2);
				_line2.GetComponent<RectTransform> ().offsetMin = new Vector2 (_line2.GetComponent<RectTransform> ().offsetMin.x, _bottomLine2);

				_endX = 0;
				_endY = 0;
				_heightOffset4 = 0;
				_lineWidth4 = 0;

				switch (_row) {
				case 1:
					_endX = -1.5f;
					_endY = 34f;
					_heightOffset4 = 1;
					_lineWidth4 = 24f;
					break;
				case 2:
					_endX = -25.8f;
					_endY = 138f;
					_heightOffset4 = 3.5f;
					_lineWidth4 = 24f;
					break;
				case 3:
					_endX = -48.5f;
					_endY = 235f;
					_heightOffset4 = 7f;
					_lineWidth4 = 24f;
					break;
				case 4:
					_endX = -70f;
					_endY = 326f;
					_heightOffset4 = 8f;
					_lineWidth4 = 24f;
					break;
				}

				_endPoint.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (_endX, _endY + _offsetLevel);

				_line5.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (_endX - 6.5f, _endY + _offsetLevel);
				_line5.GetComponent<RectTransform> ().sizeDelta = new Vector2 (_lineWidth4 + (float)_input, _lineWidth);

				_line4.GetComponent<RectTransform> ().sizeDelta = new Vector2 (_lineWidth, _endY + _offsetLevel + _heightOffset4);
				_line4.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (-(Mathf.Abs (_line5.GetComponent<RectTransform> ().anchoredPosition.x) + _line5.GetComponent<RectTransform> ().rect.width - Mathf.Abs ((_line4.GetComponent<RectTransform> ().rect.height - _heightOffset4) * Mathf.Tan (_line4.transform.rotation.eulerAngles.z * Mathf.PI / 180f)) - _lineWidth), 0f);

				_line3.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (_line1.GetComponent<RectTransform> ().anchoredPosition.x + _line1.GetComponent<RectTransform> ().rect.width + gameObject.GetComponent<RectTransform> ().rect.height * Mathf.Abs (Mathf.Tan (_line2.transform.rotation.eulerAngles.z * Mathf.PI / 180f)) - _lineWidth, -_lineWidth / 2f);
				_line3.GetComponent<RectTransform> ().sizeDelta = new Vector2 (gameObject.GetComponent<RectTransform> ().rect.width - _line3.GetComponent<RectTransform> ().anchoredPosition.x + _line4.GetComponent<RectTransform> ().anchoredPosition.x, _lineWidth);

			} else {
				float initialXOffset = 0;
				float initialYOffset = 0;
				if (_parentElement) {
					switch(_parentElement.GetComponent<dragElementPneumatic>()._typeComponent){
					case dragElementPneumatic.typesComponents.limit_switch_sensor:
						//_heightOffset = 2.08f;
						break;
					case dragElementPneumatic.typesComponents.green_button_sensor:
					case dragElementPneumatic.typesComponents.red_button_sensor:
					case dragElementPneumatic.typesComponents.inductive_sensor:
						initialXOffset = -25.8f;
						//initialYOffset = -18;
						_heightOffset = -18f;
						_xOffset = 10;
						_widthOffset = 10;
						break;
					}
				}

				switch(_row){
				case 1:
					switch(_column){
					case 1:
						_offsetLevel = 23f;
						_angleLine2 = -3f;
						_xInitial = 828.49f + _xOffset;
						_yInitial = -136.6f;
						_width = 612.93f + _widthOffset;
						_height = 305.9f + _heightOffset;
						_firstLineWidth = 22f;
						_topLine2 = 1.1f;
						_bottomLine2 = -2.438f;
						break;
					case 2:
						_offsetLevel = 19f;
						_angleLine2 = -2f;
						_xInitial = 970.0142f + _xOffset;
						_yInitial = -136.6f;
						_width = 754.914f + _widthOffset;
						_height = 305.6f + _heightOffset;
						_firstLineWidth = 22f;
						_topLine2 = 0.5f;
						_bottomLine2 = -2f;
						break;
					case 3:
						_offsetLevel = 15f;
						_angleLine2 = 1f;
						_xInitial = 1111.68f + _xOffset;
						_yInitial = -136.6f;
						_width = 896.68f + _widthOffset;
						_height = 305.6f + _heightOffset;
						_firstLineWidth = 22f;
						_topLine2 = 0.5f;
						_bottomLine2 = -2f;
						break;
					case 4:
						_offsetLevel = 11f;
						_angleLine2 = 4f;
						_xInitial = 1252.825f + _xOffset;
						_yInitial = -136.6f;
						_width = 1037.826f + _widthOffset;
						_height = 305.6f + _heightOffset;
						_firstLineWidth = 22f;
						_topLine2 = 0.5f;
						_bottomLine2 = -2f;
						break;
					case 5:
						_offsetLevel = 7f;
						_angleLine2 = 8f;
						_xInitial = 1392.698f + _xOffset;
						_yInitial = -136.6f;
						_width = 1177.809f + _widthOffset;
						_height = 305.6f + _heightOffset;
						_firstLineWidth = 22f;
						_topLine2 = 0.5f;
						_bottomLine2 = -2f;
						break;
					case 6:
						_offsetLevel = 3f;
						_angleLine2 = 9f;
						_xInitial = 1533.07f + _xOffset;
						_yInitial = -136.6f;
						_width = 1318.18f + _widthOffset;
						_height = 305.6f + _heightOffset;
						_firstLineWidth = 22f;
						_topLine2 = 0.5f;
						_bottomLine2 = -5.522f;
						break;
					}
					break;
				case 2:
					switch (_column) {
					case 1:
						_offsetLevel = 22f;
						_angleLine2 = -3f;
						_firstLineWidth = 20f;
						_xInitial = 824.77f + _xOffset;
						_yInitial = -219f;
						_width = 611.47f + _widthOffset;
						_height = 228.4f + _heightOffset;
						_topLine2 = 1.1f;
						_bottomLine2 = -2f;
						break;
					case 2:
						_offsetLevel = 18f;
						_angleLine2 = -2f;
						_firstLineWidth = 21f;
						_xInitial = 970.89f + _xOffset;
						_yInitial = -218.9f;
						_width = 757.18f + _widthOffset;
						_height = 228.4f + _heightOffset;
						_topLine2 = 1.1f;
						_bottomLine2 = -2f;
						break;
					case 3:
						_offsetLevel = 14f;
						_angleLine2 = 1f;
						_firstLineWidth = 21f;
						_xInitial = 1115.685f + _xOffset;
						_yInitial = -218.9f;
						_width = 902.026f + _widthOffset;
						_height = 228.4f + _heightOffset;
						_topLine2 = 1.1f;
						_bottomLine2 = -2f;
						break;
					case 4:
						_offsetLevel = 10f;
						_angleLine2 = 4f;
						_firstLineWidth = 21f;
						_xInitial = 1261.03f + _xOffset;
						_yInitial = -218.9f;
						_width = 1047.43f + _widthOffset;
						_height = 228.4f + _heightOffset;
						_topLine2 = 1.1f;
						_bottomLine2 = -2f;
						break;
					case 5:
						_offsetLevel = 6f;
						_angleLine2 = 8f;
						_firstLineWidth = 21f;
						_xInitial = 1404.428f + _xOffset;
						_yInitial = -218.9f;
						_width = 1190.928f + _widthOffset;
						_height = 228.4f + _heightOffset;
						_topLine2 = 1.1f;
						_bottomLine2 = -2f;
						break;
					case 6:
						_offsetLevel = 2f;
						_angleLine2 = 9f;
						_firstLineWidth = 21f;
						_xInitial = 1548.533f + _xOffset;
						_yInitial = -218.9f;
						_width = 1335.023f + _widthOffset;
						_height = 228.4f + _heightOffset;
						_topLine2 = 1.1f;
						_bottomLine2 = -2f;
						break;
					}
					break;
				case 3:
					switch (_column) {
					case 1:
						_offsetLevel = 21f;
						_angleLine2 = -3f;
						_firstLineWidth = 18f;
						_xInitial = 820.9f + _xOffset;
						_yInitial = -306f;
						_width = 609.4f + _widthOffset;
						_height = 144.26f + _heightOffset;
						_topLine2 = 1.1f;
						_bottomLine2 = -2f;
						break;
					case 2:
						_offsetLevel = 17f;
						_angleLine2 = -2f;
						_firstLineWidth = 20f;
						_xInitial = 971.11f + _xOffset;
						_yInitial = -305.6f;
						_width = 759.4f + _widthOffset;
						_height = 144.74f + _heightOffset;
						_topLine2 = 1.1f;
						_bottomLine2 = -2f;
						break;
					case 3:
						_offsetLevel = 17f;
						_angleLine2 = 1f;
						_firstLineWidth = 20f;
						_xInitial = 1120.568f + _xOffset;
						_yInitial = -305.6f;
						_width = 908.868f + _widthOffset;
						_height = 144.74f + _heightOffset;
						_topLine2 = 1.1f;
						_bottomLine2 = -2f;
						break;
					case 4:
						_offsetLevel = 17f;
						_angleLine2 = 4f;
						_firstLineWidth = 20f;
						_xInitial = 1269.84f + _xOffset;
						_yInitial = -305.6f;
						_width = 1058.24f + _widthOffset;
						_height = 144.74f + _heightOffset;
						_topLine2 = 1.1f;
						_bottomLine2 = -2f;
						break;
					case 5:
						_offsetLevel = 17f;
						_angleLine2 = 8f;
						_firstLineWidth = 20f;
						_xInitial = 1417.219f + _xOffset;
						_yInitial = -305.6f;
						_width = 1205.72f + _widthOffset;
						_height = 144.74f + _heightOffset;
						_topLine2 = 1.1f;
						_bottomLine2 = -2f;
						break;
					case 6:
						_offsetLevel = 17f;
						_angleLine2 = 9f;
						_firstLineWidth = 20f;
						_xInitial = 1566.271f + _xOffset;
						_yInitial = -305.6f;
						_width = 1354.77f + _widthOffset;
						_height = 144.74f + _heightOffset;
						_topLine2 = 1.1f;
						_bottomLine2 = -2f;
						break;
					}
					break;
				case 4:
					switch (_column) {
					case 1:
						_offsetLevel = 20f;
						_firstLineWidth = 16f;
						_angleLine2 = -3f;
						_xInitial = 817.2f + _xOffset;
						_yInitial = -397.75f;
						_width = 607.3f + _widthOffset;
						_height = 55.692f + _heightOffset;
						_topLine2 = 1.1f;
						_bottomLine2 = -2f;
						break;
					case 2:
						_offsetLevel = 16f;
						_angleLine2 = -2f;
						_firstLineWidth = 19f;
						_xInitial = 971.8085f + _xOffset;
						_yInitial = -397.9f;
						_width = 762.307f + _widthOffset;
						_height = 55.3f + _heightOffset;
						_topLine2 = 1.1f;
						_bottomLine2 = -2f;
						break;
					case 3:
						_offsetLevel = 12f;
						_angleLine2 = 1f;
						_firstLineWidth = 19f;
						_xInitial = 1125.571f + _xOffset;
						_yInitial = -397.9f;
						_width = 916.07f + _widthOffset;
						_height = 55.3f + _heightOffset;
						_topLine2 = 1.1f;
						_bottomLine2 = -2f;
						break;
					case 4:
						_offsetLevel = 8f;
						_angleLine2 = 4f;
						_firstLineWidth = 19f;
						_xInitial = 1279.19f + _xOffset;
						_yInitial = -397.9f;
						_width = 1069.8f + _widthOffset;
						_height = 55.3f + _heightOffset;
						_topLine2 = 1.1f;
						_bottomLine2 = -2f;
						break;
					case 5:
						_offsetLevel = 4f;
						_angleLine2 = 8f;
						_firstLineWidth = 19f;
						_xInitial = 1431.721f + _xOffset;
						_yInitial = -397.9f;
						_width = 1222.32f + _widthOffset;
						_height = 55.3f + _heightOffset;
						_topLine2 = 1.1f;
						_bottomLine2 = -2f;
						break;
					case 6:
						_offsetLevel = 0f;
						_angleLine2 = 9f;
						_firstLineWidth = 19f;
						_xInitial = 1585.552f + _xOffset;
						_yInitial = -397.9f;
						_width = 1376.191f + _widthOffset;
						_height = 55.3f + _heightOffset;
						_topLine2 = 1.1f;
						_bottomLine2 = -2f;
						break;
					}
					break;
				}

				gameObject.transform.Rotate(new Vector3(0,180,0));
				gameObject.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (_xInitial, _yInitial + _heightOffset);
				gameObject.GetComponent<RectTransform> ().sizeDelta = new Vector2 (_width, _height + _offsetLevel);
				_angleRotation.eulerAngles = new Vector3 (0, 0, _angleLine2);
				_initialPoint.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (-3f + initialXOffset, 0f);
				_line1.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0f + initialXOffset, 0.6f);
				_line1.GetComponent<RectTransform> ().sizeDelta = new Vector2 (_firstLineWidth + Mathf.Abs (initialXOffset), _lineWidth);
				_line2.transform.rotation = _angleRotation;
				_line2.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (_line1.GetComponent<RectTransform> ().anchoredPosition.x + _line1.GetComponent<RectTransform> ().rect.width - _lineWidth, 0f);
				_line2.GetComponent<RectTransform> ().offsetMax = new Vector2 (_line2.GetComponent<RectTransform> ().offsetMax.x, _topLine2);
				_line2.GetComponent<RectTransform> ().offsetMin = new Vector2 (_line2.GetComponent<RectTransform> ().offsetMin.x, _bottomLine2);

				_endX = 0;
				_endY = 0;
				_heightOffset4 = 0;
				_lineWidth4 = 0;

				switch (_row) {
				case 1:
					_endX = -1.5f;
					_endY = 34f;
					_heightOffset4 = 1;
					_lineWidth4 = 24f;
					break;
				case 2:
					_endX = -25.8f;
					_endY = 138f;
					_heightOffset4 = 3.5f;
					_lineWidth4 = 24f;
					break;
				case 3:
					_endX = -48.5f;
					_endY = 235f;
					_heightOffset4 = 7f;
					_lineWidth4 = 24f;
					break;
				case 4:
					_endX = -70f;
					_endY = 326f;
					_heightOffset4 = 8f;
					_lineWidth4 = 24f;
					break;
				}

				_endPoint.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (_endX, _endY + _offsetLevel);

				_line5.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (_endX - 6.5f, _endY + _offsetLevel);
				_line5.GetComponent<RectTransform> ().sizeDelta = new Vector2 (_lineWidth4 + (float)_input, _lineWidth);

				_line4.GetComponent<RectTransform> ().sizeDelta = new Vector2 (_lineWidth, _endY + _offsetLevel + _heightOffset4);
				_line4.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (-(Mathf.Abs (_line5.GetComponent<RectTransform> ().anchoredPosition.x) + _line5.GetComponent<RectTransform> ().rect.width - Mathf.Abs ((_line4.GetComponent<RectTransform> ().rect.height - _heightOffset4) * Mathf.Tan (_line4.transform.rotation.eulerAngles.z * Mathf.PI / 180f)) - _lineWidth), 0f);

				_line3.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (_line1.GetComponent<RectTransform> ().anchoredPosition.x + _line1.GetComponent<RectTransform> ().rect.width - gameObject.GetComponent<RectTransform> ().rect.height * (Mathf.Tan (_line2.transform.rotation.eulerAngles.z * Mathf.PI / 180f)) - _lineWidth, -_lineWidth / 2f);
				_line3.GetComponent<RectTransform> ().sizeDelta = new Vector2 (gameObject.GetComponent<RectTransform> ().rect.width - _line3.GetComponent<RectTransform> ().anchoredPosition.x + _line4.GetComponent<RectTransform> ().anchoredPosition.x, _lineWidth);

			}
				
			break;
		}
	}
}
