﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class benchAvailability : MonoBehaviour {

	public enum typesBenchArea
	{
		Input,
		Output
	}

	public typesBenchArea typeBenchArea;

	public bool area_1_1_used = false;
	public bool area_1_2_used = false;
	public bool area_1_3_used = false;
	public bool area_1_4_used = false;

	public bool area_2_1_used = false;
	public bool area_2_2_used = false;
	public bool area_2_3_used = false;
	public bool area_2_4_used = false;

	public bool area_3_1_used = false;
	public bool area_3_2_used = false;
	public bool area_3_3_used = false;
	public bool area_3_4_used = false;

	public bool area_4_1_used = false;
	public bool area_4_2_used = false;
	public bool area_4_3_used = false;
	public bool area_4_4_used = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void setAreaBusy(int _row, int _column){
		switch(_row){
		case 1:
			switch (_column) {
			case 1:
				area_1_1_used = true;
				break;
			case 2:
				area_1_2_used = true;
				break;
			case 3:
				area_1_3_used = true;
				break;
			case 4:
				area_1_4_used = true;
				break;
			}
			break;
		case 2:
			switch (_column) {
			case 1:
				area_2_1_used = true;
				break;
			case 2:
				area_2_2_used = true;
				break;
			case 3:
				area_2_3_used = true;
				break;
			case 4:
				area_2_4_used = true;
				break;
			}
			break;
		case 3:
			switch (_column) {
			case 1:
				area_3_1_used = true;
				break;
			case 2:
				area_3_2_used = true;
				break;
			case 3:
				area_3_3_used = true;
				break;
			case 4:
				area_3_4_used = true;
				break;
			}
			break;
		case 4:
			switch (_column) {
			case 1:
				area_4_1_used = true;
				break;
			case 2:
				area_4_2_used = true;
				break;
			case 3:
				area_4_3_used = true;
				break;
			case 4:
				area_4_4_used = true;
				break;
			}
			break;
		}
	}

	public void setAreaAvailable(int _row, int _column){
		switch(_row){
		case 1:
			switch (_column) {
			case 1:
				area_1_1_used = false;
				break;
			case 2:
				area_1_2_used = false;
				break;
			case 3:
				area_1_3_used = false;
				break;
			case 4:
				area_1_4_used = false;
				break;
			}
			break;
		case 2:
			switch (_column) {
			case 1:
				area_2_1_used = false;
				break;
			case 2:
				area_2_2_used = false;
				break;
			case 3:
				area_2_3_used = false;
				break;
			case 4:
				area_2_4_used = false;
				break;
			}
			break;
		case 3:
			switch (_column) {
			case 1:
				area_3_1_used = false;
				break;
			case 2:
				area_3_2_used = false;
				break;
			case 3:
				area_3_3_used = false;
				break;
			case 4:
				area_3_4_used = false;
				break;
			}
			break;
		case 4:
			switch (_column) {
			case 1:
				area_4_1_used = false;
				break;
			case 2:
				area_4_2_used = false;
				break;
			case 3:
				area_4_3_used = false;
				break;
			case 4:
				area_4_4_used = false;
				break;
			}
			break;
		}
	}

	public bool areaIsAvailable(int _row, int _column){
		bool _result = false;

		switch(_row){
		case 1:
			switch (_column) {
			case 1:
				if (area_1_1_used == false) {
					_result = true;
				}
				break;
			case 2:
				if (area_1_2_used == false) {
					_result = true;
				}
				break;
			case 3:
				if (area_1_3_used == false) {
					_result = true;
				}
				break;
			case 4:
				if (area_1_4_used == false) {
					_result = true;
				}
				break;
			}
			break;
		case 2:
			switch (_column) {
			case 1:
				if (area_2_1_used == false) {
					_result = true;
				}
				break;
			case 2:
				if (area_2_2_used == false) {
					_result = true;
				}
				break;
			case 3:
				if (area_2_3_used == false) {
					_result = true;
				}
				break;
			case 4:
				if (area_2_4_used == false) {
					_result = true;
				}
				break;
			}
			break;
		case 3:
			switch (_column) {
			case 1:
				if (area_3_1_used == false) {
					_result = true;
				}
				break;
			case 2:
				if (area_3_2_used == false) {
					_result = true;
				}
				break;
			case 3:
				if (area_3_3_used == false) {
					_result = true;
				}
				break;
			case 4:
				if (area_3_4_used == false) {
					_result = true;
				}
				break;
			}
			break;
		case 4:
			switch (_column) {
			case 1:
				if (area_4_1_used == false) {
					_result = true;
				}
				break;
			case 2:
				if (area_4_2_used == false) {
					_result = true;
				}
				break;
			case 3:
				if (area_4_3_used == false) {
					_result = true;
				}
				break;
			case 4:
				if (area_4_4_used == false) {
					_result = true;
				}
				break;
			}
			break;
		}
		return _result;
	}
}
