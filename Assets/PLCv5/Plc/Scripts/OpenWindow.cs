﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class OpenWindow : MonoBehaviour {

	public Button ButtonAccept;
	public Button ButtonCancel;
	public Text labelMsg;
	public Dropdown fileOptions;

	private GameObject parentSituation;
	// Use this for initialization
	void Start () {
		ButtonAccept.onClick.AddListener (openAction);
		ButtonCancel.onClick.AddListener (cancelAction);
	}

	public void setTexts(){
		ButtonAccept.GetComponentInChildren<Text> ().text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='accept']").InnerText;
		ButtonCancel.GetComponentInChildren<Text> ().text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='cancel']").InnerText;
		labelMsg.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='open_message']").InnerText;
	}

	public void showWindow(GameObject parentGObject){
		parentSituation = parentGObject;

		List<Dropdown.OptionData> dataNamesList = new List<Dropdown.OptionData> ();
		string path = Manager.Instance.globalAndroidUrl + "CloudLabs/";
		DirectoryInfo dataDir = new DirectoryInfo (path);

		switch (Manager.Instance.globalController) {
		case baseSystem.Controllers.Grafcet:
			parentGObject.GetComponent<GrafcetMainClass> ().activateBackground ();
			try {
				FileInfo[] fileinfo = dataDir.GetFiles ();
				for (int i=0; i<fileinfo.Length; i++) {
					if(fileinfo [i].Extension == ".vgraf"){
						dataNamesList.Add(new Dropdown.OptionData(fileinfo [i].Name));
					}
				}
			} catch (System.Exception e) {
				Debug.Log (e);
			}
			break;
		case baseSystem.Controllers.Ladder:
			parentGObject.GetComponent<LadderMainClass> ().activateBackground ();
			parentGObject.GetComponent<LadderMainClass> ().bgEditorActive = true;
			try {
				FileInfo[] fileinfo = dataDir.GetFiles ();
				for (int i=0; i<fileinfo.Length; i++) {
					if(fileinfo [i].Extension == ".vplc"){
						dataNamesList.Add(new Dropdown.OptionData(fileinfo [i].Name));
					}
				}
			} catch (System.Exception e) {
				Debug.Log (e);
			}
			break;
		}

		fileOptions.options = dataNamesList;

		gameObject.transform.SetAsLastSibling ();

		iTween.MoveTo (
			gameObject,
			iTween.Hash (
				"position", new Vector3 (0, 0, 0),
				"looktarget", Camera.main,
				"easeType", iTween.EaseType.easeOutExpo,
				"time", 1f,
				"islocal", true
			)
		);
	}

	private void openAction(){

		string path = Manager.Instance.globalAndroidUrl + "CloudLabs/";

		switch (Manager.Instance.globalController) {
		case baseSystem.Controllers.Grafcet:
			parentSituation.GetComponent<GrafcetMainClass> ().deactivateBackground ();
			if(fileOptions.options.Count > 0){
				parentSituation.GetComponent<GrafcetMainClass> ().openFromAndroid (path + fileOptions.options [fileOptions.value].text);	
			}
			break;
		case baseSystem.Controllers.Ladder:
			parentSituation.GetComponent<LadderMainClass> ().deactivateBackground ();
			if(fileOptions.options.Count > 0){
				parentSituation.GetComponent<LadderMainClass> ().openFromAndroid (path + fileOptions.options [fileOptions.value].text);
			}
			break;
		}

		iTween.MoveTo (
			gameObject,
			iTween.Hash (
				"position", new Vector3 (0, 1400f, 0),
				"looktarget", Camera.main,
				"easeType", iTween.EaseType.easeOutExpo,
				"time", 1f,
				"islocal", true
			)
		);
	}

	private void cancelAction(){
		switch (Manager.Instance.globalController) {
		case baseSystem.Controllers.Grafcet:
			parentSituation.GetComponent<GrafcetMainClass> ().deactivateBackground ();
			break;
		case baseSystem.Controllers.Ladder:
			parentSituation.GetComponent<LadderMainClass> ().deactivateBackground ();
			break;
		}

		iTween.MoveTo (
			gameObject,
			iTween.Hash (
				"position", new Vector3 (0, 1400f, 0),
				"looktarget", Camera.main,
				"easeType", iTween.EaseType.easeOutExpo,
				"time", 1f,
				"islocal", true
			)
		);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
