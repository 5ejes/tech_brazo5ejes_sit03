﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;
using UnityEngine.UI;

using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Xml.Linq;
using System;
using System.Linq;
#if !UNITY_WEBGL
//using ValidacionMenu;
#endif
public class BaseSimulator : MonoBehaviour {

	public enum TypeScoreList { NUMERIC, ALPHABETIC };
	public enum LanguageList {Spanish, English, Portuguese};
	public enum ConfigureLanguageList {Spanish_English, Spanish_Portuguese, English_Portuguese};
	//[Space(10, order = 1)]

	[Header("GENERAL SIMULATOR SETTINGS", order = 1)]
	public bool SecurityModule = false;
	public bool LanguageEnabled = false;
	public bool LanguageFirstTime = false;
	public bool Aula = false;
	public bool MonoUser = false;
	public bool welcomeMessageOnStart = true;
	public LanguageList Language = LanguageList.Spanish;
	public ConfigureLanguageList LanguageConf = ConfigureLanguageList.Spanish_English;

	[Header("GENERAL SIMULATOR PARAMETERS", order = 2)]
	public string AllowedUrl = "ielicenseserver.herokuapp.com";
    //public string BundleId = "air.com.cloudlabs.mru.v2";
    public string BundleId = "com.cloudlabs.triangulos";
    public string UrlServer = "https://ielicenseserver.herokuapp.com/validacion/verificacion_licencia";
	public string UrlScore = "https://ielicenseserver.herokuapp.com/lti_launcher/setScore";
	public string UrlAula = "";
	public string menuName = "menu_scene";

	[Header("GENERAL SIMULATOR SCORE PARAMETERS", order = 3)]
	public float MinimalSuccessScore = 0.6f;
	public TypeScoreList typeScore;
	public int[] NumericScore = new int[2]{0,10};
	public QualificationElement[] AlphabeticalScore = new QualificationElement[5]
	{
		new QualificationElement(){name = "F", maxValue=0.6f},
		new QualificationElement(){name = "D", maxValue=0.7f},
		new QualificationElement(){name = "C", maxValue=0.8f},
		new QualificationElement(){name = "B", maxValue=0.9f},
		new QualificationElement(){name = "A", maxValue=1.0f}
	};

	[Header("GENERAL SIMULATOR PREFABS", order = 4)]
	public GameObject _alertPrefab;
	public GameObject _loginPanelPrefab;
	public GameObject _languagePanelPrefab;

	[HideInInspector]
	public GameObject _alert;
	[HideInInspector]
	public GameObject _loginPanel;
	[HideInInspector]
	public GameObject _languagePanel;

	[HideInInspector]
	public bool postLanguageSelectorReady = false;

	private XmlDocument xmlTexts;
	private XmlDocument xmlInfo;
	private XmlDocument xmlQuestions;

	private string _urlTexts;
	private string _urlInfo;
	private string _urlAula;

    //seguridad nueva datos
    private string[] cmd;
    private string correoMono;
    private string nombreMono;
    private string instituMono;

    // Use this for initialization
    public void startSimulator(){
		//
		// -- Tasks to do:
		// -- Load XMLs.
		// -- Check security.
		// -- Check language.
		// -- Login.
		if (Manager.Instance.logged == false) {
			Manager.Instance.globalLanguageConf = LanguageConf;
			Manager.Instance.globalMenuName = menuName;
			Manager.Instance.globalAula = Aula;
			Manager.Instance.globalMonoUser = MonoUser;
			Manager.Instance.globalUrlAula = UrlAula;
			Manager.Instance.globalTypeScore = typeScore;
			Manager.Instance.globalNumericScore = NumericScore;
			Manager.Instance.globalAlphabeticalScore = AlphabeticalScore;
			Manager.Instance.globalMinimalSuccessScore = MinimalSuccessScore;
			Manager.Instance.globalUrlScore = UrlScore;
			Manager.Instance.globalLanguageEnabled = LanguageEnabled;

			//PARAMETERS FOR LTI
			#if UNITY_WEBGL
			string[] _parametrosLTI = Application.absoluteURL.Split ('?');

			if (_parametrosLTI.Length > 1) {
			string[] arrayParametros = _parametrosLTI[1].Split('&');

			Manager.Instance.globalUser = WWW.UnEscapeURL(arrayParametros[0]);
			Manager.Instance.globalCourse = WWW.UnEscapeURL(arrayParametros[1]);
			Manager.Instance.globalCourseId = WWW.UnEscapeURL(arrayParametros [2]);
			Manager.Instance.globalInstitution = WWW.UnEscapeURL(arrayParametros[3]);
			Manager.Instance.globalLTIParameters = WWW.UnEscapeURL(arrayParametros[4]);
			}
			#endif

			//EXTERNAL PARAMETERS
			onAppInvoke();
		}
    }

    public static string safeCallStringMethod(AndroidJavaObject javaObject, string methodName, params object[] args)
    {

        if (args == null) args = new object[] { null };
        IntPtr methodID = AndroidJNIHelper.GetMethodID<string>(javaObject.GetRawClass(), methodName, args, false);
        jvalue[] jniArgs = AndroidJNIHelper.CreateJNIArgArray(args);
        try
        {
            IntPtr returnValue = AndroidJNI.CallObjectMethod(javaObject.GetRawObject(), methodID, jniArgs);
            if (IntPtr.Zero != returnValue)
            {
                var val = AndroidJNI.GetStringUTFChars(returnValue);
                AndroidJNI.DeleteLocalRef(returnValue);
                return val;
            }
        }
        finally
        {
            AndroidJNIHelper.DeleteJNIArgArray(args, jniArgs);
        }

        return null;

    }

    private void onAppInvoke()
    {

        //string cmdInfo = "";
        string[] _array_invoke;

#if UNITY_ANDROID
        //using (AndroidJavaClass jc = new AndroidJavaClass("com.cloudlabs.triangulos")){
        //  cmdInfo = jc.CallStatic<string>("getLauncherURL");
        //}
        //if (cmdInfo != "no input data" && cmdInfo != ""){
        if (MonoUser == true)
        {

            //Seguridad 2
            try
            {
                AndroidJavaClass UnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                AndroidJavaObject currentActivity = UnityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
                if (currentActivity != null)
                {
                    AndroidJavaObject intent = currentActivity.Call<AndroidJavaObject>("getIntent");
                    if (intent != null)
                    {
                        /*nombreMono = safeCallStringMethod(intent, "getStringExtra", "nombre");
                        instituMono = safeCallStringMethod(intent, "getStringExtra", "institucion");
                        correoMOno = safeCallStringMethod(intent, "getStringExtra", "correo");*/
                        Manager.Instance.globalMonoUserName = safeCallStringMethod(intent, "getStringExtra", "nombre");
                        Manager.Instance.globalMonoUserInstitution = safeCallStringMethod(intent, "getStringExtra", "institucion");
                        Manager.Instance.globalMonoUserEmail = safeCallStringMethod(intent, "getStringExtra", "correo");
                    }
                }

            }
            catch (Exception e)
            {
                Debug.Log(e.ToString());
            }

        }
#elif (UNITY_EDITOR || UNITY_STANDALONE_OSX || UNITY_STANDALONE_WIN || UNITY_IOS)
        string[] args = Environment.GetCommandLineArgs();
        //if (args.Length > 1)
        //{
        if (MonoUser == true)
        {
            _array_invoke = args[0].Split(',');
            //license_number = _array_invoke[0];

            //Nueva seguridad traida de datos
            cmd = System.Environment.CommandLine.Split(',');
            try
            {
                if (cmd.Length > 5)
                {
                    //Datos del LTI
                    Manager.Instance.globalMonoUserName = nombreMono = cmd[3];
                    Manager.Instance.globalMonoUserInstitution = instituMono = cmd[4];
                    Manager.Instance.globalMonoUserEmail = correoMono = cmd[5];
                }

            }
            catch
            {


            }

        }
        else
        {
            //license_number = args[1];
        }

        //}
#endif

        _urlAula = "";
#if UNITY_IPHONE
		_urlAula = Application.persistentDataPath + "/" + "aula_conf.xml"; 
		loadAulaXML();
#elif UNITY_ANDROID
		try{
			var _dir = "/storage/emulated/0/";
			var _dir0 = "/storage/sdcard/";
			var _dir1 = "/storage/sdcard0/";
			var _dir2 = "/storage/sdcard1/";
			var _dirx = "/sdcard/";
			DirectoryInfo currentDirectory = new DirectoryInfo(_dir);
			DirectoryInfo currentDirectory_0 = new DirectoryInfo(_dir0);
			DirectoryInfo currentDirectory_1 = new DirectoryInfo(_dir1);
			DirectoryInfo currentDirectory_2 = new DirectoryInfo(_dir2);
			DirectoryInfo currentDirectory_x = new DirectoryInfo(_dirx);

			if(currentDirectory.Exists){
				Manager.Instance.globalAndroidUrl = _dir;
				ProcessDirectory(currentDirectory); 
			}else if(currentDirectory_0.Exists){
				Manager.Instance.globalAndroidUrl = _dir0;
				ProcessDirectory(currentDirectory_0); 
			}else if(currentDirectory_1.Exists){
				Manager.Instance.globalAndroidUrl = _dir1;
				ProcessDirectory(currentDirectory_1); 
			}else if(currentDirectory_2.Exists){
				Manager.Instance.globalAndroidUrl = _dir2;
				ProcessDirectory(currentDirectory_2); 
			}else if (currentDirectory_x.Exists){
				Manager.Instance.globalAndroidUrl = _dirx;
				ProcessDirectory(currentDirectory_x);
			}
			else {
				//---
				//Manager.Instance.globalAndroidUrl = "../../";
				//_urlAula = Application.dataPath + "/" + "../../aula_conf.xml";
				//---
				loadAulaXML();
			}
		}
		catch(Exception error){
			loadAulaXML();
		}
#elif UNITY_EDITOR
        _urlAula = Application.dataPath + "/" + "../../aula_conf.xml";
        loadAulaXML();
#elif UNITY_STANDALONE_OSX
		_urlAula = Application.dataPath + "/" + "../../aula_conf.xml"; 
		loadAulaXML();
#elif UNITY_STANDALONE_WIN
		_urlAula = Application.dataPath + "/" + "../../../../aula_conf.xml";
		loadAulaXML();
#elif UNITY_WEBGL
		loadAulaXML();
#endif
    }

    public void loadAulaXML(){

		try
		{
			XmlDocument newXml = new XmlDocument();
			newXml.Load(_urlAula);
			XmlNodeList _list = newXml.ChildNodes[0].ChildNodes;

			for (int i = 0; i < _list.Count; i++)
			{
				if (_list[i].Name.Equals("url_aula"))
				{
					UrlAula = _list[i].InnerText;
					_urlAula = _urlAula + " - " + UrlAula;
					Manager.Instance.globalUrlAula = UrlAula;
				}

				if (_list[i].Name.Equals("aula"))
				{
					if (_list[i].InnerText.Equals("true"))
					{
						Aula = true;
						Manager.Instance.globalAula = Aula;
					}
					else
					{
						Aula = false;
						Manager.Instance.globalAula = Aula;
					}

					_urlAula = _urlAula + " - " + Aula.ToString();
				}
			}

			StartCoroutine(loadLanguageXML (true,true));
		}
		catch (Exception e)
		{
			Debug.Log("file not exist" + e);
			Debug.Log("Hello - Sim-xml catch" + e);
			StartCoroutine(loadLanguageXML (true,true));
		}

	}

	public IEnumerator loadLanguageXML(Boolean _firstTime = false, Boolean _toSecurityModule = false){
		//false, false -- to menu
		//false, true -- not implemented
		//true, false -- to login / welcome
		//true, true -- to check security

		if (_firstTime == true) {
			if (PlayerPrefs.HasKey ("language")) {
				string _language = PlayerPrefs.GetString("language");
				switch (_language) {
				case "Spanish":
					Language = LanguageList.Spanish;
					Manager.Instance.globalLanguage = LanguageList.Spanish;
					break;
				case "English":
					Language = LanguageList.English;
					Manager.Instance.globalLanguage = LanguageList.English;
					break;
				case "Portuguese":
					Language = LanguageList.Portuguese;
					Manager.Instance.globalLanguage = LanguageList.Portuguese;
					break;
				}
			}
		}

		switch (Language) {
		case LanguageList.English:
			_urlTexts = "EnglishTexts";
			_urlInfo = "EnglishInfo";
			break;
		case LanguageList.Spanish:
			_urlTexts = "SpanishTexts";
			_urlInfo = "SpanishInfo";
			break;
		case LanguageList.Portuguese:
			_urlTexts = "PortugueseTexts";
			_urlInfo = "PortugueseInfo";
			break;
		}

		TextAsset _xmlTexts = Resources.Load(_urlTexts) as TextAsset;
		TextAsset _xmlInfo = Resources.Load(_urlInfo) as TextAsset;

		xmlTexts = new XmlDocument ();
		xmlTexts.LoadXml (_xmlTexts.text);
		Manager.Instance.globalTexts = xmlTexts;

		xmlInfo = new XmlDocument();
		xmlInfo.LoadXml(_xmlInfo.text);
		Manager.Instance.globalInfo = xmlInfo;

		if (_firstTime == true) {
			if(_toSecurityModule == true){
				/*if(SecurityModule == true){
					checkLicense();
				}
				else {
					StartCoroutine(checkLanguage());
				}*/
                if(SecurityModule == true){
                    //checkLicense();
                    #if !UNITY_WEBGL
                    /*
                    if (Validacion.Validar())
                    {
                        //TODO: Security module
                        StartCoroutine(checkLanguage());
                    }
                    else
                    {
                        loadAlertClose();
                    }*/
                    #endif
                }else{
                    //loadLogin();
                    yield return new WaitForSeconds (0.01f);
                    //loadWelcome();
                    StartCoroutine(checkLanguage());
                    //checkLanguage();
                }
			}
			else {
                //TODO - Si no va seguridad-webGL-LTI
				loadLogin();
				postLanguageSelectorReady = true;
				//yield return new WaitForSeconds (0.01f);
				//loadWelcome();
			}
		}
		else {
			if(_toSecurityModule == true){
				//TODO: Not implemented
				//return;
				Debug.Log ("in step 1");
			}
			else {
				//TODO: to menu
				Debug.Log ("in step 2");
			}
		}
	}

	private void checkLicense(){

	}

	IEnumerator checkLanguage(){
		if (LanguageEnabled == true) {
			if (LanguageFirstTime == true) {
				if (PlayerPrefs.HasKey ("languageFirstTime")) {
					
					string _language = PlayerPrefs.GetString("language");
					switch (_language) {
					case "Spanish":
						Language = LanguageList.Spanish;
						Manager.Instance.globalLanguage = LanguageList.Spanish;

						loadLogin ();
						yield return new WaitForSeconds (0.01f);
						loadWelcome();
						break;
					case "English":
						Language = LanguageList.English;
						Manager.Instance.globalLanguage = LanguageList.English;

						loadLogin ();
						yield return new WaitForSeconds (0.01f);
						loadWelcome();
						break;
					case "Portuguese":
						Language = LanguageList.Portuguese;
						Manager.Instance.globalLanguage = LanguageList.Portuguese;

						loadLogin ();
						yield return new WaitForSeconds (0.01f);
						loadWelcome();
						break;
					}

				} else {
					_languagePanel = Instantiate(_languagePanelPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
					_languagePanel.transform.SetParent (GameObject.Find("Canvas").transform, false);
					_languagePanel.transform.localPosition = new Vector3(30,0,0);
					_languagePanel.GetComponent<LanguageSelector> ().setPostAction (postLanguageAction);
				}
			} else {
				_languagePanel = Instantiate(_languagePanelPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
				_languagePanel.transform.SetParent (GameObject.Find("Canvas").transform, false);
				_languagePanel.transform.localPosition = new Vector3(30,0,0);
				_languagePanel.GetComponent<LanguageSelector> ().setPostAction (postLanguageAction);
			}
		} else {
			//loadLogin ();
			yield return new WaitForSeconds (0.01f);
			//loadWelcome();
		}
	}

	public void postLanguageAction(){

		Debug.Log ("in postlangue");

		Language = Manager.Instance.globalLanguage;

		if (LanguageFirstTime == true) {
			PlayerPrefs.SetString ("languageFirstTime","true");
		}
		StartCoroutine(loadLanguageXML (true, false));
	}

	private void loadLogin(){
		_loginPanel = Instantiate(_loginPanelPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
		_loginPanel.transform.SetParent (GameObject.Find("Canvas").transform, false);
		_loginPanel.transform.localPosition = new Vector3(0,-10,0);
		_loginPanel.transform.SetSiblingIndex (_loginPanel.transform.GetSiblingIndex()-1);
	}

	private void loadWelcome(){
		if(welcomeMessageOnStart == true){
		_alert = Instantiate(_alertPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
		_alert.transform.SetParent (GameObject.Find("Canvas").transform, false);
		_alert.transform.localPosition = new Vector3(-700,700,0);
		_alert.GetComponent<Alert> ().showAlert (1, "", "", Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='welcome']").InnerText, Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='accept']").InnerText);
		}
		return;
	}
        private void loadAlertClose()
    {
        _alert = Instantiate(_alertPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
        _alert.transform.SetParent(GameObject.Find("Canvas").transform, false);
        _alert.transform.localPosition = new Vector3(-700, 700, 0);
        _alert.GetComponent<Alert>().showAlert(1, "", "", Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='securityclose']").InnerText, Manager.Instance.globalTexts.SelectSingleNode("/data/element[@title='accept']").InnerText, "",closeApp);
        return;
    }

        private void closeApp(){
        Application.Quit();
        }



	#if UNITY_ANDROID
	void ProcessDirectory(DirectoryInfo aDir){
	var files = aDir.GetFiles().Where(f => f.Extension == ".xml").ToArray();
	foreach(var _fileName in files)
	{
	if(_fileName.Name.Equals("aula_conf.xml"))
	{
	_urlAula = _fileName.FullName;
	break;
	}
	}

	if (!_urlAula.Equals ("")) {
		loadAulaXML ();
	} else {
		StartCoroutine(loadLanguageXML (true,true));
	}
	}
	#endif
}
