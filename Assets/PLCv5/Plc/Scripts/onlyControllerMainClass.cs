﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class onlyControllerMainClass : BaseSituation {

	public GameObject _backgroundPref;

	[HideInInspector]
	public GameObject _background;
	// Use this for initialization
	void Start () {
		
	}

	public void loadSituation(){
		_cam1.enabled = true;
		_cam2.enabled = false;

		base.StartBaseSituation();

		switch (Manager.Instance.globalController) {
		case baseSystem.Controllers.Grafcet:
			Manager.Instance.currentPDFName = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='name_report_2']").InnerText;
			break;
		case baseSystem.Controllers.Ladder:
			Manager.Instance.currentPDFName = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='name_report_4']").InnerText;
			break;
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
