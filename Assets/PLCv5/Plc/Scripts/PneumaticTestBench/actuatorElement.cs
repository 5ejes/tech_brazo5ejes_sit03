﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class actuatorElement : MonoBehaviour {

	public enum typesActuators
	{
		DoubleEffect,
		SimpleEffect,
		DoubleEffectWithSensors
	}
	public typesActuators typeElement;
	public string textTag;


	public Image _standPosition;
	public GameObject _benchPosition;
	public Image _status1;
	public Image _status2;
	public Image _status3;
	public Image _status4;
	public Image _status5;
	public Image _status6;
	public Image _status7;
	public Image _status8;
	public Image _status9;
	public Image _status10;

	public Vector2[] rowsColumnsUsed;
	//public int[] rowUsed;
	//public int[] columnUsed;
	public int normalValue = 0;
	public int pressedValue = 0;

	public int actualValue = 0;

	public bool dragEnabled = true;
	public bool inBench = false;

	public GameObject _situation;

	public GameObject _doubleSolenoidPrefab;
	public GameObject _simpleSolenoidPrefab;

	public GameObject _solenoidAttached;
	public GameObject _solenoidAttachedB;

	public GameObject _sensorAttachedA;
	public GameObject _sensorAttachedB;

	public int actualStatus = 1;

	private GameObject _rightOvers;
	private GameObject _rightColliders;

	private Image right_over_1_1;
	private Image right_over_1_2;
	private Image right_over_1_3;
	private Image right_over_1_4;
	private Image right_over_1_5;
	private Image right_over_1_6;

	private Image right_over_2_1;
	private Image right_over_2_2;
	private Image right_over_2_3;
	private Image right_over_2_4;
	private Image right_over_2_5;
	private Image right_over_2_6;

	private Image right_over_3_1;
	private Image right_over_3_2;
	private Image right_over_3_3;
	private Image right_over_3_4;
	private Image right_over_3_5;
	private Image right_over_3_6;

	private Image right_over_4_1;
	private Image right_over_4_2;
	private Image right_over_4_3;
	private Image right_over_4_4;
	private Image right_over_4_5;
	private Image right_over_4_6;

	private Image right_collider_1_1;
	private Image right_collider_1_2;
	private Image right_collider_1_3;
	private Image right_collider_1_4;
	private Image right_collider_1_5;
	private Image right_collider_1_6;

	private Image right_collider_2_1;
	private Image right_collider_2_2;
	private Image right_collider_2_3;
	private Image right_collider_2_4;
	private Image right_collider_2_5;
	private Image right_collider_2_6;

	private Image right_collider_3_1;
	private Image right_collider_3_2;
	private Image right_collider_3_3;
	private Image right_collider_3_4;
	private Image right_collider_3_5;
	private Image right_collider_3_6;

	private Image right_collider_4_1;
	private Image right_collider_4_2;
	private Image right_collider_4_3;
	private Image right_collider_4_4;
	private Image right_collider_4_5;
	private Image right_collider_4_6;

	private bool objectDragged;
	private Vector3 initialPosition;
	private Vector3 startPosition;
	private int initialIndex;
	private int startIndex;

	private bool goForward = false;
	private bool goBack = false;
	private bool animationEnabled = false;

	private bool _mouseOver = false;

	// Use this for initialization
	void Start () {
		actualValue = normalValue;

		initialPosition = gameObject.transform.localPosition;
		initialIndex = gameObject.GetComponent<RectTransform> ().GetSiblingIndex ();

		if (gameObject.GetComponent<EventTrigger> () != null) {
			EventTrigger _prev_trigger_Object = gameObject.GetComponent<EventTrigger> ();
			List<EventTrigger.Entry> entriesToRemove = new List<EventTrigger.Entry>();

			foreach (var entry in _prev_trigger_Object.triggers) {        
				entry.callback.RemoveAllListeners ();
				entriesToRemove.Add (entry);
			}

			foreach(var entry in entriesToRemove)
			{
				_prev_trigger_Object.triggers.Remove(entry);
			}
		} else {
			gameObject.AddComponent<EventTrigger> ();
		}

		EventTrigger trigger_Object = gameObject.GetComponent<EventTrigger>();

		EventTrigger.Entry entry_0 = new EventTrigger.Entry();
		entry_0.eventID = EventTriggerType.BeginDrag;
		entry_0.callback.AddListener((data) => { BeginDrag_((PointerEventData)data); });

		EventTrigger.Entry entry_1 = new EventTrigger.Entry();
		entry_1.eventID = EventTriggerType.Drag;
		entry_1.callback.AddListener((data) => { ObjectDrag_((PointerEventData)data); });

		EventTrigger.Entry entry_2 = new EventTrigger.Entry();
		entry_2.eventID = EventTriggerType.EndDrag;
		entry_2.callback.AddListener((data) => { ObjectDrop_((PointerEventData)data); });

		EventTrigger.Entry entry_5 = new EventTrigger.Entry();
		entry_5.eventID = EventTriggerType.PointerEnter;
		entry_5.callback.AddListener((data) => { ObjectMouseOver_((PointerEventData)data); });

		EventTrigger.Entry entry_6 = new EventTrigger.Entry();
		entry_6.eventID = EventTriggerType.PointerExit;
		entry_6.callback.AddListener((data) => { ObjectMouseOut_((PointerEventData)data); });

		trigger_Object.triggers.Add(entry_0);
		trigger_Object.triggers.Add(entry_1);
		trigger_Object.triggers.Add(entry_2);

		trigger_Object.triggers.Add(entry_5);
		trigger_Object.triggers.Add(entry_6);

		_situation = GameObject.Find ("PneumaticBench");

		_rightOvers = GameObject.Find ("Canvas/PneumaticBench/rightOvers");
		_rightColliders = GameObject.Find ("Canvas/PneumaticBench/rightColliders");

		right_over_1_1 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_1_1").GetComponent<Image>();
		right_over_1_2 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_1_2").GetComponent<Image>();
		right_over_1_3 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_1_3").GetComponent<Image>();
		right_over_1_4 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_1_4").GetComponent<Image>();
		right_over_1_5 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_1_5").GetComponent<Image>();
		right_over_1_6 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_1_6").GetComponent<Image>();

		right_over_2_1 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_2_1").GetComponent<Image>();
		right_over_2_2 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_2_2").GetComponent<Image>();
		right_over_2_3 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_2_3").GetComponent<Image>();
		right_over_2_4 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_2_4").GetComponent<Image>();
		right_over_2_5 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_2_5").GetComponent<Image>();
		right_over_2_6 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_2_6").GetComponent<Image>();

		right_over_3_1 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_3_1").GetComponent<Image>();
		right_over_3_2 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_3_2").GetComponent<Image>();
		right_over_3_3 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_3_3").GetComponent<Image>();
		right_over_3_4 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_3_4").GetComponent<Image>();
		right_over_3_5 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_3_5").GetComponent<Image>();
		right_over_3_6 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_3_6").GetComponent<Image>();

		right_over_4_1 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_4_1").GetComponent<Image>();
		right_over_4_2 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_4_2").GetComponent<Image>();
		right_over_4_3 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_4_3").GetComponent<Image>();
		right_over_4_4 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_4_4").GetComponent<Image>();
		right_over_4_5 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_4_5").GetComponent<Image>();
		right_over_4_6 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_4_6").GetComponent<Image>();

		right_collider_1_1 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_1_1").GetComponent<Image>();
		right_collider_1_2 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_1_2").GetComponent<Image>();
		right_collider_1_3 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_1_3").GetComponent<Image>();
		right_collider_1_4 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_1_4").GetComponent<Image>();
		right_collider_1_5 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_1_5").GetComponent<Image>();
		right_collider_1_6 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_1_6").GetComponent<Image>();

		right_collider_2_1 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_2_1").GetComponent<Image>();
		right_collider_2_2 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_2_2").GetComponent<Image>();
		right_collider_2_3 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_2_3").GetComponent<Image>();
		right_collider_2_4 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_2_4").GetComponent<Image>();
		right_collider_2_5 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_2_5").GetComponent<Image>();
		right_collider_2_6 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_2_6").GetComponent<Image>();

		right_collider_3_1 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_3_1").GetComponent<Image>();
		right_collider_3_2 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_3_2").GetComponent<Image>();
		right_collider_3_3 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_3_3").GetComponent<Image>();
		right_collider_3_4 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_3_4").GetComponent<Image>();
		right_collider_3_5 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_3_5").GetComponent<Image>();
		right_collider_3_6 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_3_6").GetComponent<Image>();

		right_collider_4_1 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_4_1").GetComponent<Image>();
		right_collider_4_2 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_4_2").GetComponent<Image>();
		right_collider_4_3 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_4_3").GetComponent<Image>();
		right_collider_4_4 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_4_4").GetComponent<Image>();
		right_collider_4_5 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_4_5").GetComponent<Image>();
		right_collider_4_6 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_4_6").GetComponent<Image>();

		right_over_1_1.CrossFadeAlpha(0, 0.01f, false);
		right_over_1_2.CrossFadeAlpha(0, 0.01f, false);
		right_over_1_3.CrossFadeAlpha(0, 0.01f, false);
		right_over_1_4.CrossFadeAlpha(0, 0.01f, false);
		right_over_1_5.CrossFadeAlpha(0, 0.01f, false);
		right_over_1_6.CrossFadeAlpha(0, 0.01f, false);

		right_over_2_1.CrossFadeAlpha(0, 0.01f, false);
		right_over_2_2.CrossFadeAlpha(0, 0.01f, false);
		right_over_2_3.CrossFadeAlpha(0, 0.01f, false);
		right_over_2_4.CrossFadeAlpha(0, 0.01f, false);
		right_over_2_5.CrossFadeAlpha(0, 0.01f, false);
		right_over_2_6.CrossFadeAlpha(0, 0.01f, false);

		right_over_3_1.CrossFadeAlpha(0, 0.01f, false);
		right_over_3_2.CrossFadeAlpha(0, 0.01f, false);
		right_over_3_3.CrossFadeAlpha(0, 0.01f, false);
		right_over_3_4.CrossFadeAlpha(0, 0.01f, false);
		right_over_3_5.CrossFadeAlpha(0, 0.01f, false);
		right_over_3_6.CrossFadeAlpha(0, 0.01f, false);

		right_over_4_1.CrossFadeAlpha(0, 0.01f, false);
		right_over_4_2.CrossFadeAlpha(0, 0.01f, false);
		right_over_4_3.CrossFadeAlpha(0, 0.01f, false);
		right_over_4_4.CrossFadeAlpha(0, 0.01f, false);
		right_over_4_5.CrossFadeAlpha(0, 0.01f, false);
		right_over_4_6.CrossFadeAlpha(0, 0.01f, false);

		right_collider_1_1.enabled = false;
		right_collider_1_2.enabled = false;
		right_collider_1_3.enabled = false;
		right_collider_1_4.enabled = false;
		right_collider_1_5.enabled = false;
		right_collider_1_6.enabled = false;

		right_collider_2_1.enabled = false;
		right_collider_2_2.enabled = false;
		right_collider_2_3.enabled = false;
		right_collider_2_4.enabled = false;
		right_collider_2_5.enabled = false;
		right_collider_2_6.enabled = false;

		right_collider_3_1.enabled = false;
		right_collider_3_2.enabled = false;
		right_collider_3_3.enabled = false;
		right_collider_3_4.enabled = false;
		right_collider_3_5.enabled = false;
		right_collider_3_6.enabled = false;

		right_collider_4_1.enabled = false;
		right_collider_4_2.enabled = false;
		right_collider_4_3.enabled = false;
		right_collider_4_4.enabled = false;
		right_collider_4_5.enabled = false;
		right_collider_4_6.enabled = false;
	}

	private void BeginDrag_(PointerEventData data)
	{
		if(dragEnabled == true){
			objectDragged = true;
			startPosition = gameObject.transform.localPosition;
			startIndex = gameObject.GetComponent<RectTransform> ().GetSiblingIndex ();

			gameObject.GetComponent<RectTransform> ().SetAsLastSibling();

			_standPosition.enabled = false;
			_benchPosition.SetActive (true);

			_rightColliders.transform.SetAsLastSibling ();
			activateRightColliders ();

			_situation.GetComponent<PnematicTestBenchClass> ()._msgArea.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='" + textTag + "']").InnerText;
		}
	}

	private void ObjectDrag_(PointerEventData data)
	{
		if (objectDragged == true) {
			Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			gameObject.GetComponent<RectTransform>().position = new Vector3(mousePos.x, mousePos.y, 0);

			if(data.pointerEnter){
				switch(data.pointerEnter.name){
				case "over_1_1":
					Vector2[] _vector11 = new Vector2[4];
					_vector11 [0] = new Vector2 (1, 1);
					_vector11 [1] = new Vector2 (1, 2);
					_vector11 [2] = new Vector2 (1, 3);
					_vector11 [3] = new Vector2 (2, 1);
					deactivateRightOversWithException (_vector11);
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 1) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 2) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 3) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 1)) {
						right_over_1_1.CrossFadeAlpha(1, 0.2f, false);
						right_over_1_2.CrossFadeAlpha(1, 0.2f, false);
						right_over_1_3.CrossFadeAlpha(1, 0.2f, false);
						right_over_2_1.CrossFadeAlpha(1, 0.2f, false);
					}
					break;
				case "over_1_2":
					Vector2[] _vector12 = new Vector2[4];
					_vector12 [0] = new Vector2 (1, 1);
					_vector12 [1] = new Vector2 (1, 2);
					_vector12 [2] = new Vector2 (1, 3);
					_vector12 [3] = new Vector2 (2, 1);
					deactivateRightOversWithException (_vector12);
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 1) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 2) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 3) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 1)) {
						right_over_1_1.CrossFadeAlpha(1, 0.2f, false);
						right_over_1_2.CrossFadeAlpha(1, 0.2f, false);
						right_over_1_3.CrossFadeAlpha(1, 0.2f, false);
						right_over_2_1.CrossFadeAlpha(1, 0.2f, false);
					}
					break;
				case "over_1_3":
					Vector2[] _vector13 = new Vector2[4];
					_vector13 [0] = new Vector2 (1, 2);
					_vector13 [1] = new Vector2 (1, 3);
					_vector13 [2] = new Vector2 (1, 4);
					_vector13 [3] = new Vector2 (2, 2);
					deactivateRightOversWithException (_vector13);
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 2) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 3) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 4) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 2)) {
						right_over_1_2.CrossFadeAlpha(1, 0.2f, false);
						right_over_1_3.CrossFadeAlpha(1, 0.2f, false);
						right_over_1_4.CrossFadeAlpha(1, 0.2f, false);
						right_over_2_2.CrossFadeAlpha(1, 0.2f, false);
					}
					break;
				case "over_1_4":
					Vector2[] _vector14 = new Vector2[4];
					_vector14 [0] = new Vector2 (1, 3);
					_vector14 [1] = new Vector2 (1, 4);
					_vector14 [2] = new Vector2 (1, 5);
					_vector14 [3] = new Vector2 (2, 3);
					deactivateRightOversWithException (_vector14);
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 3) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 4) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 5) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 3)) {
						right_over_1_3.CrossFadeAlpha(1, 0.2f, false);
						right_over_1_4.CrossFadeAlpha(1, 0.2f, false);
						right_over_1_5.CrossFadeAlpha(1, 0.2f, false);
						right_over_2_3.CrossFadeAlpha(1, 0.2f, false);
					}
					break;
				case "over_1_5":
					Vector2[] _vector15 = new Vector2[4];
					_vector15 [0] = new Vector2 (1, 4);
					_vector15 [1] = new Vector2 (1, 5);
					_vector15 [2] = new Vector2 (1, 6);
					_vector15 [3] = new Vector2 (2, 4);
					deactivateRightOversWithException (_vector15);
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 4) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 5) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 6) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 4)) {
						right_over_1_4.CrossFadeAlpha(1, 0.2f, false);
						right_over_1_5.CrossFadeAlpha(1, 0.2f, false);
						right_over_1_6.CrossFadeAlpha(1, 0.2f, false);
						right_over_2_4.CrossFadeAlpha(1, 0.2f, false);
					}
					break;
				case "over_1_6":
					Vector2[] _vector16 = new Vector2[3];
					_vector16 [0] = new Vector2 (1, 5);
					_vector16 [1] = new Vector2 (1, 6);
					_vector16 [2] = new Vector2 (2, 5);
					deactivateRightOversWithException (_vector16);
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 5) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 6) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 5)) {
						right_over_1_5.CrossFadeAlpha(1, 0.2f, false);
						right_over_1_6.CrossFadeAlpha(1, 0.2f, false);
						right_over_2_5.CrossFadeAlpha(1, 0.2f, false);
					}
					break;
				case "over_2_1":
					Vector2[] _vector21 = new Vector2[4];
					_vector21 [0] = new Vector2 (2, 1);
					_vector21 [1] = new Vector2 (2, 2);
					_vector21 [2] = new Vector2 (2, 3);
					_vector21 [3] = new Vector2 (3, 1);
					deactivateRightOversWithException (_vector21);
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 1) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 2) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 3) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 1)) {
						right_over_2_1.CrossFadeAlpha(1, 0.2f, false);
						right_over_2_2.CrossFadeAlpha(1, 0.2f, false);
						right_over_2_3.CrossFadeAlpha(1, 0.2f, false);
						right_over_3_1.CrossFadeAlpha(1, 0.2f, false);
					}
					break;
				case "over_2_2":
					Vector2[] _vector22 = new Vector2[4];
					_vector22 [0] = new Vector2 (2, 1);
					_vector22 [1] = new Vector2 (2, 2);
					_vector22 [2] = new Vector2 (2, 3);
					_vector22 [3] = new Vector2 (3, 1);
					deactivateRightOversWithException (_vector22);
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 1) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 2) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 3) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 1)) {
						right_over_2_1.CrossFadeAlpha(1, 0.2f, false);
						right_over_2_2.CrossFadeAlpha(1, 0.2f, false);
						right_over_2_3.CrossFadeAlpha(1, 0.2f, false);
						right_over_3_1.CrossFadeAlpha(1, 0.2f, false);
					}
					break;
				case "over_2_3":
					Vector2[] _vector23 = new Vector2[4];
					_vector23 [0] = new Vector2 (2, 2);
					_vector23 [1] = new Vector2 (2, 3);
					_vector23 [2] = new Vector2 (2, 4);
					_vector23 [3] = new Vector2 (3, 2);
					deactivateRightOversWithException (_vector23);
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 2) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 3) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 4) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 2)) {
						right_over_2_2.CrossFadeAlpha(1, 0.2f, false);
						right_over_2_3.CrossFadeAlpha(1, 0.2f, false);
						right_over_2_4.CrossFadeAlpha(1, 0.2f, false);
						right_over_3_2.CrossFadeAlpha(1, 0.2f, false);
					}
					break;
				case "over_2_4":
					Vector2[] _vector24 = new Vector2[4];
					_vector24 [0] = new Vector2 (2, 3);
					_vector24 [1] = new Vector2 (2, 4);
					_vector24 [2] = new Vector2 (2, 5);
					_vector24 [3] = new Vector2 (3, 3);
					deactivateRightOversWithException (_vector24);
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 3) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 4) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 5) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 3)) {
						right_over_2_3.CrossFadeAlpha(1, 0.2f, false);
						right_over_2_4.CrossFadeAlpha(1, 0.2f, false);
						right_over_2_5.CrossFadeAlpha(1, 0.2f, false);
						right_over_3_3.CrossFadeAlpha(1, 0.2f, false);
					}
					break;
				case "over_2_5":
					Vector2[] _vector25 = new Vector2[4];
					_vector25 [0] = new Vector2 (2, 4);
					_vector25 [1] = new Vector2 (2, 5);
					_vector25 [2] = new Vector2 (2, 6);
					_vector25 [3] = new Vector2 (3, 4);
					deactivateRightOversWithException (_vector25);
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 4) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 5) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 6) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 4)) {
						right_over_2_4.CrossFadeAlpha(1, 0.2f, false);
						right_over_2_5.CrossFadeAlpha(1, 0.2f, false);
						right_over_2_6.CrossFadeAlpha(1, 0.2f, false);
						right_over_3_4.CrossFadeAlpha(1, 0.2f, false);
					}
					break;
				case "over_2_6":
					Vector2[] _vector26 = new Vector2[3];
					_vector26 [0] = new Vector2 (2, 5);
					_vector26 [1] = new Vector2 (2, 6);
					_vector26 [2] = new Vector2 (3, 5);
					deactivateRightOversWithException (_vector26);
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 5) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 6) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 5)) {
						right_over_2_5.CrossFadeAlpha(1, 0.2f, false);
						right_over_2_6.CrossFadeAlpha(1, 0.2f, false);
						right_over_3_5.CrossFadeAlpha(1, 0.2f, false);
					}
					break;
				case "over_3_1":
					Vector2[] _vector31 = new Vector2[4];
					_vector31 [0] = new Vector2 (3, 1);
					_vector31 [1] = new Vector2 (3, 2);
					_vector31 [2] = new Vector2 (3, 3);
					_vector31 [3] = new Vector2 (4, 1);
					deactivateRightOversWithException (_vector31);
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 1) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 2) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 3) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 1)) {
						right_over_3_1.CrossFadeAlpha(1, 0.2f, false);
						right_over_3_2.CrossFadeAlpha(1, 0.2f, false);
						right_over_3_3.CrossFadeAlpha(1, 0.2f, false);
						right_over_4_1.CrossFadeAlpha(1, 0.2f, false);
					}
					break;
				case "over_3_2":
					Vector2[] _vector32 = new Vector2[4];
					_vector32 [0] = new Vector2 (3, 1);
					_vector32 [1] = new Vector2 (3, 2);
					_vector32 [2] = new Vector2 (3, 3);
					_vector32 [3] = new Vector2 (4, 1);
					deactivateRightOversWithException (_vector32);
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 1) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 2) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 3) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 1)) {
						right_over_3_1.CrossFadeAlpha(1, 0.2f, false);
						right_over_3_2.CrossFadeAlpha(1, 0.2f, false);
						right_over_3_3.CrossFadeAlpha(1, 0.2f, false);
						right_over_4_1.CrossFadeAlpha(1, 0.2f, false);
					}
					break;
				case "over_3_3":
					Vector2[] _vector33 = new Vector2[4];
					_vector33 [0] = new Vector2 (3, 2);
					_vector33 [1] = new Vector2 (3, 3);
					_vector33 [2] = new Vector2 (3, 4);
					_vector33 [3] = new Vector2 (4, 2);
					deactivateRightOversWithException (_vector33);
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 2) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 3) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 4) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 2)) {
						right_over_3_2.CrossFadeAlpha(1, 0.2f, false);
						right_over_3_3.CrossFadeAlpha(1, 0.2f, false);
						right_over_3_4.CrossFadeAlpha(1, 0.2f, false);
						right_over_4_2.CrossFadeAlpha(1, 0.2f, false);
					}
					break;
				case "over_3_4":
					Vector2[] _vector34 = new Vector2[4];
					_vector34 [0] = new Vector2 (3, 3);
					_vector34 [1] = new Vector2 (3, 4);
					_vector34 [2] = new Vector2 (3, 5);
					_vector34 [3] = new Vector2 (4, 3);
					deactivateRightOversWithException (_vector34);
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 3) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 4) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 5) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 3)) {
						right_over_3_3.CrossFadeAlpha(1, 0.2f, false);
						right_over_3_4.CrossFadeAlpha(1, 0.2f, false);
						right_over_3_5.CrossFadeAlpha(1, 0.2f, false);
						right_over_4_3.CrossFadeAlpha(1, 0.2f, false);
					}
					break;
				case "over_3_5":
					Vector2[] _vector35 = new Vector2[4];
					_vector35 [0] = new Vector2 (3, 4);
					_vector35 [1] = new Vector2 (3, 5);
					_vector35 [2] = new Vector2 (3, 6);
					_vector35 [3] = new Vector2 (4, 4);
					deactivateRightOversWithException (_vector35);
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 4) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 5) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 6) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 4)) {
						right_over_3_4.CrossFadeAlpha(1, 0.2f, false);
						right_over_3_5.CrossFadeAlpha(1, 0.2f, false);
						right_over_3_6.CrossFadeAlpha(1, 0.2f, false);
						right_over_4_4.CrossFadeAlpha(1, 0.2f, false);
					}
					break;
				case "over_3_6":
					Vector2[] _vector36 = new Vector2[3];
					_vector36 [0] = new Vector2 (3, 5);
					_vector36 [1] = new Vector2 (3, 6);
					_vector36 [2] = new Vector2 (4, 5);
					deactivateRightOversWithException (_vector36);
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 5) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 6) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 5)) {
						right_over_3_5.CrossFadeAlpha(1, 0.2f, false);
						right_over_3_6.CrossFadeAlpha(1, 0.2f, false);
						right_over_4_5.CrossFadeAlpha(1, 0.2f, false);
					}
					break;
				case "over_4_1":
					Vector2[] _vector41 = new Vector2[4];
					_vector41 [0] = new Vector2 (4, 1);
					_vector41 [1] = new Vector2 (4, 2);
					_vector41 [2] = new Vector2 (4, 3);
					_vector41 [3] = new Vector2 (3, 1);
					deactivateRightOversWithException (_vector41);
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 1) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 2) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 3) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 1)) {
						right_over_4_1.CrossFadeAlpha(1, 0.2f, false);
						right_over_4_2.CrossFadeAlpha(1, 0.2f, false);
						right_over_4_3.CrossFadeAlpha(1, 0.2f, false);
						right_over_3_1.CrossFadeAlpha(1, 0.2f, false);
					}
					break;
				case "over_4_2":
					Vector2[] _vector42 = new Vector2[4];
					_vector42 [0] = new Vector2 (4, 1);
					_vector42 [1] = new Vector2 (4, 2);
					_vector42 [2] = new Vector2 (4, 3);
					_vector42 [3] = new Vector2 (3, 1);
					deactivateRightOversWithException (_vector42);
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 1) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 2) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 3) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 1)) {
						right_over_4_1.CrossFadeAlpha(1, 0.2f, false);
						right_over_4_2.CrossFadeAlpha(1, 0.2f, false);
						right_over_4_3.CrossFadeAlpha(1, 0.2f, false);
						right_over_3_1.CrossFadeAlpha(1, 0.2f, false);
					}
					break;
				case "over_4_3":
					Vector2[] _vector43 = new Vector2[4];
					_vector43 [0] = new Vector2 (4, 2);
					_vector43 [1] = new Vector2 (4, 3);
					_vector43 [2] = new Vector2 (4, 4);
					_vector43 [3] = new Vector2 (3, 2);
					deactivateRightOversWithException (_vector43);
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 2) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 3) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 4) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 2)) {
						right_over_4_2.CrossFadeAlpha(1, 0.2f, false);
						right_over_4_3.CrossFadeAlpha(1, 0.2f, false);
						right_over_4_4.CrossFadeAlpha(1, 0.2f, false);
						right_over_3_2.CrossFadeAlpha(1, 0.2f, false);
					}
					break;
				case "over_4_4":
					Vector2[] _vector44 = new Vector2[4];
					_vector44 [0] = new Vector2 (4, 3);
					_vector44 [1] = new Vector2 (4, 4);
					_vector44 [2] = new Vector2 (4, 5);
					_vector44 [3] = new Vector2 (3, 3);
					deactivateRightOversWithException (_vector44);
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 3) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 4) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 5) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 3)) {
						right_over_4_3.CrossFadeAlpha(1, 0.2f, false);
						right_over_4_4.CrossFadeAlpha(1, 0.2f, false);
						right_over_4_5.CrossFadeAlpha(1, 0.2f, false);
						right_over_3_3.CrossFadeAlpha(1, 0.2f, false);
					}
					break;
				case "over_4_5":
					Vector2[] _vector45 = new Vector2[4];
					_vector45 [0] = new Vector2 (4, 4);
					_vector45 [1] = new Vector2 (4, 5);
					_vector45 [2] = new Vector2 (4, 6);
					_vector45 [3] = new Vector2 (3, 4);
					deactivateRightOversWithException (_vector45);
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 4) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 5) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 6) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 4)) {
						right_over_4_4.CrossFadeAlpha(1, 0.2f, false);
						right_over_4_5.CrossFadeAlpha(1, 0.2f, false);
						right_over_4_6.CrossFadeAlpha(1, 0.2f, false);
						right_over_3_4.CrossFadeAlpha(1, 0.2f, false);
					}
					break;
				case "over_4_6":
					Vector2[] _vector46 = new Vector2[3];
					_vector46 [0] = new Vector2 (4, 5);
					_vector46 [1] = new Vector2 (4, 6);
					_vector46 [2] = new Vector2 (3, 5);
					deactivateRightOversWithException (_vector46);
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 5) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 6) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 5)) {
						right_over_4_5.CrossFadeAlpha(1, 0.2f, false);
						right_over_4_6.CrossFadeAlpha(1, 0.2f, false);
						right_over_3_5.CrossFadeAlpha(1, 0.2f, false);
					}
					break;
				default:
					deactivateOvers ();
					break;	
				}
			}

		}
	}

	private void ObjectDrop_(PointerEventData data)
	{
		if (objectDragged == true) {
			objectDragged = false;

			_situation.GetComponent<PnematicTestBenchClass> ()._msgArea.text = "";

			deactivateOvers ();
			deactivateRightColliders ();

			bool sensorsActuatorsAvailable = true;

			if (typeElement == typesActuators.SimpleEffect) {
				if (_situation.GetComponent<PnematicTestBenchClass> ().usedOutputs >= (_situation.GetComponent<PnematicTestBenchClass> ().simulatorOutputs)) {
					sensorsActuatorsAvailable = false;
				}
			} else if (typeElement == typesActuators.DoubleEffect) {
				if (_situation.GetComponent<PnematicTestBenchClass> ().usedOutputs >= (_situation.GetComponent<PnematicTestBenchClass> ().simulatorOutputs - 1)) {
					sensorsActuatorsAvailable = false;
				}
			} else if (typeElement == typesActuators.DoubleEffectWithSensors) {
				if ((_situation.GetComponent<PnematicTestBenchClass> ().usedInputs >= (_situation.GetComponent<PnematicTestBenchClass> ().simulatorInputs - 1)) || (_situation.GetComponent<PnematicTestBenchClass> ().usedOutputs >= (_situation.GetComponent<PnematicTestBenchClass> ().simulatorOutputs - 1))) {
					sensorsActuatorsAvailable = false;
				}
			}

			if (data.pointerEnter != null && sensorsActuatorsAvailable == true) {
				float y_offset = -6;
				switch (data.pointerEnter.name) {
				case "over_1_1":
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 1) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 2) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 3) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 1)) {
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(1, 1);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(1, 2);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(1, 3);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(2, 1);
						gameObject.transform.position = right_over_1_2.transform.position;
						gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x - 40, gameObject.transform.localPosition.y + y_offset, gameObject.transform.localPosition.z);

						addNewElement ();
						dragEnabled = false;
						inBench = true;
						rowsColumnsUsed = new Vector2[4];
						rowsColumnsUsed [0] = new Vector2 (1,1);
						rowsColumnsUsed [1] = new Vector2 (1,2);
						rowsColumnsUsed [2] = new Vector2 (1,3);
						rowsColumnsUsed [3] = new Vector2 (2,1);
						switch(typeElement){
						case typesActuators.DoubleEffect:
						case typesActuators.DoubleEffectWithSensors:
							_solenoidAttached = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttached.transform.position = right_over_2_1.transform.position;
							_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
							_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;	

							_solenoidAttachedB = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttachedB.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttachedB.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttachedB.transform.position = right_over_2_1.transform.position;
							_solenoidAttachedB.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttachedB.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
							_solenoidAttachedB.GetComponent<solenoidClass> ().numberSolenoid = 2;

							int _indexA = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
							int _indexB = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttachedB);

							if (_sensorAttachedA != null) {
								int _indexIA = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (_sensorAttachedA);
								_sensorAttachedA.GetComponent<dragElementPneumatic> ().setValue (1);
							}
							if (_sensorAttachedB != null) {
								int _indexIA = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (_sensorAttachedB);
							}
							setValueExternalSensorsA (1);
							break;
						case typesActuators.SimpleEffect:
							_solenoidAttached = Instantiate (_simpleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttached.transform.position = right_over_2_1.transform.position;
							_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Simple;
							_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;

							int _index = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
							setValueExternalSensorsA (1);
							break;
						}
							
						_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();

					} else {
						tweenToInitialPosition ();
					}
					break;
				case "over_1_2":
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 1) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 2) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 3) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 1)) {
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(1, 1);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(1, 2);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(1, 3);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(2, 1);
						gameObject.transform.position = right_over_1_2.transform.position;
						gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x - 40, gameObject.transform.localPosition.y+ y_offset, gameObject.transform.localPosition.z);

						addNewElement ();
						dragEnabled = false;
						inBench = true;
						rowsColumnsUsed = new Vector2[4];
						rowsColumnsUsed [0] = new Vector2 (1,1);
						rowsColumnsUsed [1] = new Vector2 (1,2);
						rowsColumnsUsed [2] = new Vector2 (1,3);
						rowsColumnsUsed [3] = new Vector2 (2,1);
						switch(typeElement){
						case typesActuators.DoubleEffect:
						case typesActuators.DoubleEffectWithSensors:
							_solenoidAttached = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttached.transform.position = right_over_2_1.transform.position;
							_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
							_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;	

							_solenoidAttachedB = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttachedB.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttachedB.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttachedB.transform.position = right_over_2_1.transform.position;
							_solenoidAttachedB.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttachedB.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
							_solenoidAttachedB.GetComponent<solenoidClass> ().numberSolenoid = 2;

							int _indexA = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
							int _indexB = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttachedB);
							if(_sensorAttachedA != null){
								int _indexIA = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (_sensorAttachedA);
								_sensorAttachedA.GetComponent<dragElementPneumatic> ().setValue (1);
							}
							if(_sensorAttachedB != null){
								int _indexIA = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (_sensorAttachedB);
							}
							setValueExternalSensorsA (1);
							break;
						case typesActuators.SimpleEffect:
							_solenoidAttached = Instantiate (_simpleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttached.transform.position = right_over_2_1.transform.position;
							_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Simple;
							_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;

							int _index = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
							setValueExternalSensorsA (1);
							break;
						}

						_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
					} else {
						tweenToInitialPosition ();
					}
					break;
				case "over_1_3":
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 2) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 3) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 4) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 2)) {
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(1, 2);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(1, 3);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(1, 4);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(2, 2);
						gameObject.transform.position = right_over_1_3.transform.position;
						gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x - 40, gameObject.transform.localPosition.y+ y_offset, gameObject.transform.localPosition.z);

						addNewElement ();
						dragEnabled = false;
						inBench = true;
						rowsColumnsUsed = new Vector2[4];
						rowsColumnsUsed [0] = new Vector2 (1,2);
						rowsColumnsUsed [1] = new Vector2 (1,3);
						rowsColumnsUsed [2] = new Vector2 (1,4);
						rowsColumnsUsed [3] = new Vector2 (2,2);
						switch(typeElement){
						case typesActuators.DoubleEffect:
						case typesActuators.DoubleEffectWithSensors:
							_solenoidAttached = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttached.transform.position = right_over_2_2.transform.position;
							_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
							_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;	

							_solenoidAttachedB = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttachedB.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttachedB.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttachedB.transform.position = right_over_2_2.transform.position;
							_solenoidAttachedB.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttachedB.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
							_solenoidAttachedB.GetComponent<solenoidClass> ().numberSolenoid = 2;

							int _indexA = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
							int _indexB = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttachedB);
							if(_sensorAttachedA != null){
								int _indexIA = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (_sensorAttachedA);
								_sensorAttachedA.GetComponent<dragElementPneumatic> ().setValue (1);
							}
							if(_sensorAttachedB != null){
								int _indexIA = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (_sensorAttachedB);
							}
							setValueExternalSensorsA (1);
							break;
						case typesActuators.SimpleEffect:
							_solenoidAttached = Instantiate (_simpleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttached.transform.position = right_over_2_2.transform.position;
							_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Simple;
							_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;

							int _index = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
							setValueExternalSensorsA (1);
							break;
						}

						_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
					} else {
						tweenToInitialPosition ();
					}
					break;
				case "over_1_4":
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 3) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 4) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 5) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 3)) {
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(1, 3);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(1, 4);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(1, 5);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(2, 3);
						gameObject.transform.position = right_over_1_4.transform.position;
						gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x - 40, gameObject.transform.localPosition.y+ y_offset, gameObject.transform.localPosition.z);

						addNewElement ();
						dragEnabled = false;
						inBench = true;
						rowsColumnsUsed = new Vector2[4];
						rowsColumnsUsed [0] = new Vector2 (1,3);
						rowsColumnsUsed [1] = new Vector2 (1,4);
						rowsColumnsUsed [2] = new Vector2 (1,5);
						rowsColumnsUsed [3] = new Vector2 (2,3);
						switch(typeElement){
						case typesActuators.DoubleEffect:
						case typesActuators.DoubleEffectWithSensors:
							_solenoidAttached = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttached.transform.position = right_over_2_3.transform.position;
							_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
							_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;	

							_solenoidAttachedB = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttachedB.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttachedB.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttachedB.transform.position = right_over_2_3.transform.position;
							_solenoidAttachedB.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttachedB.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
							_solenoidAttachedB.GetComponent<solenoidClass> ().numberSolenoid = 2;

							int _indexA = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
							int _indexB = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttachedB);
							if(_sensorAttachedA != null){
								int _indexIA = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (_sensorAttachedA);
								_sensorAttachedA.GetComponent<dragElementPneumatic> ().setValue (1);
							}
							if(_sensorAttachedB != null){
								int _indexIA = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (_sensorAttachedB);
							}
							setValueExternalSensorsA (1);
							break;
						case typesActuators.SimpleEffect:
							_solenoidAttached = Instantiate (_simpleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttached.transform.position = right_over_2_3.transform.position;
							_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Simple;
							_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;

							int _index = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
							setValueExternalSensorsA (1);
							break;
						}

						_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
					} else {
						tweenToInitialPosition ();
					}
					break;
				case "over_1_5":
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 4) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 5) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 6) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 4)) {
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(1, 4);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(1, 5);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(1, 6);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(2, 4);
						gameObject.transform.position = right_over_1_5.transform.position;
						gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x - 40, gameObject.transform.localPosition.y+ y_offset, gameObject.transform.localPosition.z);

						addNewElement ();
						dragEnabled = false;
						inBench = true;
						rowsColumnsUsed = new Vector2[4];
						rowsColumnsUsed [0] = new Vector2 (1,4);
						rowsColumnsUsed [1] = new Vector2 (1,5);
						rowsColumnsUsed [2] = new Vector2 (1,6);
						rowsColumnsUsed [3] = new Vector2 (2,4);
						switch(typeElement){
						case typesActuators.DoubleEffect:
						case typesActuators.DoubleEffectWithSensors:
							_solenoidAttached = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttached.transform.position = right_over_2_4.transform.position;
							_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
							_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;	

							_solenoidAttachedB = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttachedB.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttachedB.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttachedB.transform.position = right_over_2_4.transform.position;
							_solenoidAttachedB.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttachedB.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
							_solenoidAttachedB.GetComponent<solenoidClass> ().numberSolenoid = 2;

							int _indexA = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
							int _indexB = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttachedB);
							if(_sensorAttachedA != null){
								int _indexIA = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (_sensorAttachedA);
								_sensorAttachedA.GetComponent<dragElementPneumatic> ().setValue (1);
							}
							if(_sensorAttachedB != null){
								int _indexIA = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (_sensorAttachedB);
							}
							setValueExternalSensorsA (1);
							break;
						case typesActuators.SimpleEffect:
							_solenoidAttached = Instantiate (_simpleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttached.transform.position = right_over_2_4.transform.position;
							_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Simple;
							_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;

							int _index = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
							setValueExternalSensorsA (1);
							break;
						}

						_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
					} else {
						tweenToInitialPosition ();
					}
					break;
				case "over_1_6":
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 5) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 6) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 5)) {
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(1, 5);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(1, 6);
						//_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(1, 6);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(2, 5);
						gameObject.transform.position = right_over_1_6.transform.position;
						gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x - 40, gameObject.transform.localPosition.y + y_offset, gameObject.transform.localPosition.z);

						addNewElement ();
						dragEnabled = false;
						inBench = true;
						rowsColumnsUsed = new Vector2[3];
						rowsColumnsUsed [0] = new Vector2 (1,5);
						rowsColumnsUsed [1] = new Vector2 (1,6);
						rowsColumnsUsed [2] = new Vector2 (2,5);
						//rowsColumnsUsed [3] = new Vector2 (2,4);
						switch(typeElement){
						case typesActuators.DoubleEffect:
						case typesActuators.DoubleEffectWithSensors:
							_solenoidAttached = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttached.transform.position = right_over_2_5.transform.position;
							_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
							_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;	

							_solenoidAttachedB = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttachedB.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttachedB.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttachedB.transform.position = right_over_2_5.transform.position;
							_solenoidAttachedB.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttachedB.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
							_solenoidAttachedB.GetComponent<solenoidClass> ().numberSolenoid = 2;

							int _indexA = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
							int _indexB = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttachedB);

							if(_sensorAttachedA != null){
								int _indexIA = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (_sensorAttachedA);
								_sensorAttachedA.GetComponent<dragElementPneumatic> ().setValue (1);
							}
							if(_sensorAttachedB != null){
								int _indexIA = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (_sensorAttachedB);
							}
							setValueExternalSensorsA (1);
							break;
						case typesActuators.SimpleEffect:
							_solenoidAttached = Instantiate (_simpleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttached.transform.position = right_over_2_5.transform.position;
							_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Simple;
							_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;

							int _index = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
							setValueExternalSensorsA (1);
							break;
						}

						_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
					} else {
						tweenToInitialPosition ();
					}
					break;
				case "over_2_1":
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 1) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 2) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 3) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 1)) {
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(2, 1);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(2, 2);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(2, 3);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(3, 1);
						gameObject.transform.position = right_over_2_2.transform.position;
						gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x - 40, gameObject.transform.localPosition.y + y_offset, gameObject.transform.localPosition.z);

						addNewElement ();
						dragEnabled = false;
						inBench = true;
						rowsColumnsUsed = new Vector2[4];
						rowsColumnsUsed [0] = new Vector2 (2,1);
						rowsColumnsUsed [1] = new Vector2 (2,2);
						rowsColumnsUsed [2] = new Vector2 (2,3);
						rowsColumnsUsed [3] = new Vector2 (3,1);
						switch(typeElement){
						case typesActuators.DoubleEffect:
						case typesActuators.DoubleEffectWithSensors:
							_solenoidAttached = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttached.transform.position = right_over_3_1.transform.position;
							_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
							_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;	

							_solenoidAttachedB = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttachedB.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttachedB.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttachedB.transform.position = right_over_3_1.transform.position;
							_solenoidAttachedB.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttachedB.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
							_solenoidAttachedB.GetComponent<solenoidClass> ().numberSolenoid = 2;

							int _indexA = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
							int _indexB = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttachedB);

							if(_sensorAttachedA != null){
								int _indexIA = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (_sensorAttachedA);
								_sensorAttachedA.GetComponent<dragElementPneumatic> ().setValue (1);
							}
							if(_sensorAttachedB != null){
								int _indexIA = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (_sensorAttachedB);
							}
							setValueExternalSensorsA (1);
							break;
						case typesActuators.SimpleEffect:
							_solenoidAttached = Instantiate (_simpleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttached.transform.position = right_over_3_1.transform.position;
							_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Simple;
							_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;

							int _index = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
							setValueExternalSensorsA (1);
							break;
						}

						_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
					} else {
						tweenToInitialPosition ();
					}
					break;
				case "over_2_2":
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 1) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 2) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 3) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 1)) {
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(2, 1);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(2, 2);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(2, 3);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(3, 1);
						gameObject.transform.position = right_over_2_2.transform.position;
						gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x - 40, gameObject.transform.localPosition.y + y_offset, gameObject.transform.localPosition.z);

						addNewElement ();
						dragEnabled = false;
						inBench = true;
						rowsColumnsUsed = new Vector2[4];
						rowsColumnsUsed [0] = new Vector2 (2,1);
						rowsColumnsUsed [1] = new Vector2 (2,2);
						rowsColumnsUsed [2] = new Vector2 (2,3);
						rowsColumnsUsed [3] = new Vector2 (3,1);
						switch(typeElement){
						case typesActuators.DoubleEffect:
						case typesActuators.DoubleEffectWithSensors:
							_solenoidAttached = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttached.transform.position = right_over_3_1.transform.position;
							_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
							_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;	

							_solenoidAttachedB = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttachedB.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttachedB.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttachedB.transform.position = right_over_3_1.transform.position;
							_solenoidAttachedB.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttachedB.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
							_solenoidAttachedB.GetComponent<solenoidClass> ().numberSolenoid = 2;

							int _indexA = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
							int _indexB = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttachedB);

							if(_sensorAttachedA != null){
								int _indexIA = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (_sensorAttachedA);
								_sensorAttachedA.GetComponent<dragElementPneumatic> ().setValue (1);
							}
							if(_sensorAttachedB != null){
								int _indexIA = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (_sensorAttachedB);
							}
							setValueExternalSensorsA (1);
							break;
						case typesActuators.SimpleEffect:
							_solenoidAttached = Instantiate (_simpleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttached.transform.position = right_over_3_1.transform.position;
							_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Simple;
							_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;

							int _index = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
							setValueExternalSensorsA (1);
							break;
						}

						_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
					} else {
						tweenToInitialPosition ();
					}
					break;
				case "over_2_3":
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 2) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 3) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 4) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 2)) {
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(2, 2);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(2, 3);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(2, 4);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(3, 2);
						gameObject.transform.position = right_over_2_3.transform.position;
						gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x - 40, gameObject.transform.localPosition.y + y_offset, gameObject.transform.localPosition.z);

						addNewElement ();
						dragEnabled = false;
						inBench = true;
						rowsColumnsUsed = new Vector2[4];
						rowsColumnsUsed [0] = new Vector2 (2,2);
						rowsColumnsUsed [1] = new Vector2 (2,3);
						rowsColumnsUsed [2] = new Vector2 (2,4);
						rowsColumnsUsed [3] = new Vector2 (3,2);
						switch(typeElement){
						case typesActuators.DoubleEffect:
						case typesActuators.DoubleEffectWithSensors:
							_solenoidAttached = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttached.transform.position = right_over_3_2.transform.position;
							_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
							_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;	

							_solenoidAttachedB = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttachedB.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttachedB.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttachedB.transform.position = right_over_3_2.transform.position;
							_solenoidAttachedB.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttachedB.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
							_solenoidAttachedB.GetComponent<solenoidClass> ().numberSolenoid = 2;

							int _indexA = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
							int _indexB = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttachedB);

							if(_sensorAttachedA != null){
								int _indexIA = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (_sensorAttachedA);
								_sensorAttachedA.GetComponent<dragElementPneumatic> ().setValue (1);
							}
							if(_sensorAttachedB != null){
								int _indexIA = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (_sensorAttachedB);
							}
							setValueExternalSensorsA (1);
							break;
						case typesActuators.SimpleEffect:
							_solenoidAttached = Instantiate (_simpleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttached.transform.position = right_over_3_2.transform.position;
							_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Simple;
							_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;

							int _index = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
							setValueExternalSensorsA (1);
							break;
						}

						_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
					} else {
						tweenToInitialPosition ();
					}
					break;
				case "over_2_4":
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 3) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 4) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 5) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 3)) {
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(2, 3);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(2, 4);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(2, 5);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(3, 3);
						gameObject.transform.position = right_over_2_4.transform.position;
						gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x - 40, gameObject.transform.localPosition.y + y_offset, gameObject.transform.localPosition.z);

						addNewElement ();
						dragEnabled = false;
						inBench = true;
						rowsColumnsUsed = new Vector2[4];
						rowsColumnsUsed [0] = new Vector2 (2,3);
						rowsColumnsUsed [1] = new Vector2 (2,4);
						rowsColumnsUsed [2] = new Vector2 (2,5);
						rowsColumnsUsed [3] = new Vector2 (3,3);
						switch(typeElement){
						case typesActuators.DoubleEffect:
						case typesActuators.DoubleEffectWithSensors:
							_solenoidAttached = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttached.transform.position = right_over_3_3.transform.position;
							_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
							_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;	

							_solenoidAttachedB = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttachedB.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttachedB.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttachedB.transform.position = right_over_3_3.transform.position;
							_solenoidAttachedB.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttachedB.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
							_solenoidAttachedB.GetComponent<solenoidClass> ().numberSolenoid = 2;

							int _indexA = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
							int _indexB = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttachedB);

							if(_sensorAttachedA != null){
								int _indexIA = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (_sensorAttachedA);
								_sensorAttachedA.GetComponent<dragElementPneumatic> ().setValue (1);
							}
							if(_sensorAttachedB != null){
								int _indexIA = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (_sensorAttachedB);
							}
							setValueExternalSensorsA (1);
							break;
						case typesActuators.SimpleEffect:
							_solenoidAttached = Instantiate (_simpleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttached.transform.position = right_over_3_3.transform.position;
							_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Simple;
							_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;

							int _index = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
							setValueExternalSensorsA (1);
							break;
						}

						_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
					} else {
						tweenToInitialPosition ();
					}
					break;
				case "over_2_5":
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 4) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 5) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 6) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 4)) {
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(2, 4);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(2, 5);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(2, 6);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(3, 4);
						gameObject.transform.position = right_over_2_5.transform.position;
						gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x - 40, gameObject.transform.localPosition.y + y_offset, gameObject.transform.localPosition.z);

						addNewElement ();
						dragEnabled = false;
						inBench = true;
						rowsColumnsUsed = new Vector2[4];
						rowsColumnsUsed [0] = new Vector2 (2,4);
						rowsColumnsUsed [1] = new Vector2 (2,5);
						rowsColumnsUsed [2] = new Vector2 (2,6);
						rowsColumnsUsed [3] = new Vector2 (3,4);
						switch(typeElement){
						case typesActuators.DoubleEffect:
						case typesActuators.DoubleEffectWithSensors:
							_solenoidAttached = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttached.transform.position = right_over_3_4.transform.position;
							_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
							_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;	

							_solenoidAttachedB = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttachedB.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttachedB.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttachedB.transform.position = right_over_3_4.transform.position;
							_solenoidAttachedB.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttachedB.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
							_solenoidAttachedB.GetComponent<solenoidClass> ().numberSolenoid = 2;

							int _indexA = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
							int _indexB = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttachedB);

							if(_sensorAttachedA != null){
								int _indexIA = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (_sensorAttachedA);
								_sensorAttachedA.GetComponent<dragElementPneumatic> ().setValue (1);
							}
							if(_sensorAttachedB != null){
								int _indexIA = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (_sensorAttachedB);
							}
							setValueExternalSensorsA (1);
							break;
						case typesActuators.SimpleEffect:
							_solenoidAttached = Instantiate (_simpleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttached.transform.position = right_over_3_4.transform.position;
							_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Simple;
							_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;

							int _index = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
							setValueExternalSensorsA (1);
							break;
						}

						_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
					} else {
						tweenToInitialPosition ();
					}
					break;
				case "over_2_6":
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 5) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 6) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 5)) {
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(2, 5);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(2, 6);
						//_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(2, 6);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(3, 5);
						gameObject.transform.position = right_over_2_6.transform.position;
						gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x - 40, gameObject.transform.localPosition.y + y_offset, gameObject.transform.localPosition.z);

						addNewElement ();
						dragEnabled = false;
						inBench = true;
						rowsColumnsUsed = new Vector2[3];
						rowsColumnsUsed [0] = new Vector2 (2,5);
						rowsColumnsUsed [1] = new Vector2 (2,6);
						rowsColumnsUsed [2] = new Vector2 (3,5);
						switch(typeElement){
						case typesActuators.DoubleEffect:
						case typesActuators.DoubleEffectWithSensors:
							_solenoidAttached = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttached.transform.position = right_over_3_5.transform.position;
							_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
							_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;	

							_solenoidAttachedB = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttachedB.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttachedB.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttachedB.transform.position = right_over_3_5.transform.position;
							_solenoidAttachedB.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttachedB.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
							_solenoidAttachedB.GetComponent<solenoidClass> ().numberSolenoid = 2;

							int _indexA = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
							int _indexB = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttachedB);

							if(_sensorAttachedA != null){
								int _indexIA = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (_sensorAttachedA);
								_sensorAttachedA.GetComponent<dragElementPneumatic> ().setValue (1);
							}
							if(_sensorAttachedB != null){
								int _indexIA = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (_sensorAttachedB);
							}
							setValueExternalSensorsA (1);
							break;
						case typesActuators.SimpleEffect:
							_solenoidAttached = Instantiate (_simpleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttached.transform.position = right_over_3_5.transform.position;
							_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Simple;
							_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;

							int _index = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
							setValueExternalSensorsA (1);
							break;
						}

						_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
					} else {
						tweenToInitialPosition ();
					}
					break;
				case "over_3_1":
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 1) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 2) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 3) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 1)) {
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(3, 1);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(3, 2);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(3, 3);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(4, 1);
						gameObject.transform.position = right_over_3_2.transform.position;
						gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x - 40, gameObject.transform.localPosition.y + y_offset, gameObject.transform.localPosition.z);

						addNewElement ();
						dragEnabled = false;
						inBench = true;
						rowsColumnsUsed = new Vector2[4];
						rowsColumnsUsed [0] = new Vector2 (3,1);
						rowsColumnsUsed [1] = new Vector2 (3,2);
						rowsColumnsUsed [2] = new Vector2 (3,3);
						rowsColumnsUsed [3] = new Vector2 (4,1);
						switch(typeElement){
						case typesActuators.DoubleEffect:
						case typesActuators.DoubleEffectWithSensors:
							_solenoidAttached = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttached.transform.position = right_over_4_1.transform.position;
							_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
							_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;	

							_solenoidAttachedB = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttachedB.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttachedB.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttachedB.transform.position = right_over_4_1.transform.position;
							_solenoidAttachedB.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttachedB.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
							_solenoidAttachedB.GetComponent<solenoidClass> ().numberSolenoid = 2;

							int _indexA = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
							int _indexB = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttachedB);

							if(_sensorAttachedA != null){
								int _indexIA = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (_sensorAttachedA);
								_sensorAttachedA.GetComponent<dragElementPneumatic> ().setValue (1);
							}
							if(_sensorAttachedB != null){
								int _indexIA = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (_sensorAttachedB);
							}
							setValueExternalSensorsA (1);
							break;
						case typesActuators.SimpleEffect:
							_solenoidAttached = Instantiate (_simpleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttached.transform.position = right_over_4_1.transform.position;
							_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Simple;
							_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;

							int _index = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
							setValueExternalSensorsA (1);
							break;
						}

						_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
					} else {
						tweenToInitialPosition ();
					}
					break;
				case "over_3_2":
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 1) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 2) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 3) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 1)) {
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(3, 1);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(3, 2);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(3, 3);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(4, 1);
						gameObject.transform.position = right_over_3_2.transform.position;
						gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x - 40, gameObject.transform.localPosition.y + y_offset, gameObject.transform.localPosition.z);

						addNewElement ();
						dragEnabled = false;
						inBench = true;
						rowsColumnsUsed = new Vector2[4];
						rowsColumnsUsed [0] = new Vector2 (3,1);
						rowsColumnsUsed [1] = new Vector2 (3,2);
						rowsColumnsUsed [2] = new Vector2 (3,3);
						rowsColumnsUsed [3] = new Vector2 (4,1);
						switch(typeElement){
						case typesActuators.DoubleEffect:
						case typesActuators.DoubleEffectWithSensors:
							_solenoidAttached = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttached.transform.position = right_over_4_1.transform.position;
							_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
							_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;	

							_solenoidAttachedB = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttachedB.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttachedB.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttachedB.transform.position = right_over_4_1.transform.position;
							_solenoidAttachedB.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttachedB.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
							_solenoidAttachedB.GetComponent<solenoidClass> ().numberSolenoid = 2;

							int _indexA = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
							int _indexB = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttachedB);

							if(_sensorAttachedA != null){
								int _indexIA = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (_sensorAttachedA);
								_sensorAttachedA.GetComponent<dragElementPneumatic> ().setValue (1);
							}
							if(_sensorAttachedB != null){
								int _indexIA = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (_sensorAttachedB);
							}
							setValueExternalSensorsA (1);
							break;
						case typesActuators.SimpleEffect:
							_solenoidAttached = Instantiate (_simpleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttached.transform.position = right_over_4_1.transform.position;
							_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Simple;
							_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;

							int _index = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
							setValueExternalSensorsA (1);
							break;
						}

						_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
					} else {
						tweenToInitialPosition ();
					}
					break;
				case "over_3_3":
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 2) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 3) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 4) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 2)) {
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(3, 2);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(3, 3);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(3, 4);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(4, 2);
						gameObject.transform.position = right_over_3_3.transform.position;
						gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x - 40, gameObject.transform.localPosition.y + y_offset, gameObject.transform.localPosition.z);

						addNewElement ();
						dragEnabled = false;
						inBench = true;
						rowsColumnsUsed = new Vector2[4];
						rowsColumnsUsed [0] = new Vector2 (3,2);
						rowsColumnsUsed [1] = new Vector2 (3,3);
						rowsColumnsUsed [2] = new Vector2 (3,4);
						rowsColumnsUsed [3] = new Vector2 (4,2);
						switch(typeElement){
						case typesActuators.DoubleEffect:
						case typesActuators.DoubleEffectWithSensors:
							_solenoidAttached = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttached.transform.position = right_over_4_2.transform.position;
							_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
							_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;	

							_solenoidAttachedB = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttachedB.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttachedB.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttachedB.transform.position = right_over_4_2.transform.position;
							_solenoidAttachedB.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttachedB.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
							_solenoidAttachedB.GetComponent<solenoidClass> ().numberSolenoid = 2;

							int _indexA = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
							int _indexB = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttachedB);

							if(_sensorAttachedA != null){
								int _indexIA = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (_sensorAttachedA);
								_sensorAttachedA.GetComponent<dragElementPneumatic> ().setValue (1);
							}
							if(_sensorAttachedB != null){
								int _indexIA = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (_sensorAttachedB);
							}
							setValueExternalSensorsA (1);
							break;
						case typesActuators.SimpleEffect:
							_solenoidAttached = Instantiate (_simpleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttached.transform.position = right_over_4_2.transform.position;
							_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Simple;
							_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;

							int _index = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
							setValueExternalSensorsA (1);
							break;
						}

						_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
					} else {
						tweenToInitialPosition ();
					}
					break;
				case "over_3_4":
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 3) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 4) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 5) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 3)) {
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(3, 3);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(3, 4);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(3, 5);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(4, 3);
						gameObject.transform.position = right_over_3_4.transform.position;
						gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x - 40, gameObject.transform.localPosition.y + y_offset, gameObject.transform.localPosition.z);

						addNewElement ();
						dragEnabled = false;
						inBench = true;
						rowsColumnsUsed = new Vector2[4];
						rowsColumnsUsed [0] = new Vector2 (3,3);
						rowsColumnsUsed [1] = new Vector2 (3,4);
						rowsColumnsUsed [2] = new Vector2 (3,5);
						rowsColumnsUsed [3] = new Vector2 (4,3);
						switch(typeElement){
						case typesActuators.DoubleEffect:
						case typesActuators.DoubleEffectWithSensors:
							_solenoidAttached = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttached.transform.position = right_over_4_3.transform.position;
							_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
							_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;	

							_solenoidAttachedB = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttachedB.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttachedB.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttachedB.transform.position = right_over_4_3.transform.position;
							_solenoidAttachedB.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttachedB.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
							_solenoidAttachedB.GetComponent<solenoidClass> ().numberSolenoid = 2;

							int _indexA = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
							int _indexB = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttachedB);

							if(_sensorAttachedA != null){
								int _indexIA = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (_sensorAttachedA);
								_sensorAttachedA.GetComponent<dragElementPneumatic> ().setValue (1);
							}
							if(_sensorAttachedB != null){
								int _indexIA = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (_sensorAttachedB);
							}
							setValueExternalSensorsA (1);
							break;
						case typesActuators.SimpleEffect:
							_solenoidAttached = Instantiate (_simpleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttached.transform.position = right_over_4_3.transform.position;
							_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Simple;
							_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;

							int _index = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
							setValueExternalSensorsA (1);
							break;
						}

						_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
					} else {
						tweenToInitialPosition ();
					}
					break;
				case "over_3_5":
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 4) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 5) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 6) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 4)) {
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(3, 4);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(3, 5);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(3, 6);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(4, 4);
						gameObject.transform.position = right_over_3_5.transform.position;
						gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x - 40, gameObject.transform.localPosition.y + y_offset, gameObject.transform.localPosition.z);

						addNewElement ();
						dragEnabled = false;
						inBench = true;
						rowsColumnsUsed = new Vector2[4];
						rowsColumnsUsed [0] = new Vector2 (3,4);
						rowsColumnsUsed [1] = new Vector2 (3,5);
						rowsColumnsUsed [2] = new Vector2 (3,6);
						rowsColumnsUsed [3] = new Vector2 (4,4);
						switch(typeElement){
						case typesActuators.DoubleEffect:
						case typesActuators.DoubleEffectWithSensors:
							_solenoidAttached = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttached.transform.position = right_over_4_4.transform.position;
							_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
							_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;	

							_solenoidAttachedB = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttachedB.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttachedB.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttachedB.transform.position = right_over_4_4.transform.position;
							_solenoidAttachedB.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttachedB.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
							_solenoidAttachedB.GetComponent<solenoidClass> ().numberSolenoid = 2;

							int _indexA = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
							int _indexB = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttachedB);

							if(_sensorAttachedA != null){
								int _indexIA = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (_sensorAttachedA);
								_sensorAttachedA.GetComponent<dragElementPneumatic> ().setValue (1);
							}
							if(_sensorAttachedB != null){
								int _indexIA = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (_sensorAttachedB);
							}
							setValueExternalSensorsA (1);
							break;
						case typesActuators.SimpleEffect:
							_solenoidAttached = Instantiate (_simpleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttached.transform.position = right_over_4_4.transform.position;
							_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Simple;
							_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;

							int _index = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
							setValueExternalSensorsA (1);
							break;
						}

						_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
					} else {
						tweenToInitialPosition ();
					}
					break;
				case "over_3_6":
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 5) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 6) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 5)) {
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(3, 5);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(3, 6);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(4, 5);
						gameObject.transform.position = right_over_3_6.transform.position;
						gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x - 40, gameObject.transform.localPosition.y + y_offset, gameObject.transform.localPosition.z);

						addNewElement ();
						dragEnabled = false;
						inBench = true;
						rowsColumnsUsed = new Vector2[3];
						rowsColumnsUsed [0] = new Vector2 (3,5);
						rowsColumnsUsed [1] = new Vector2 (3,6);
						rowsColumnsUsed [2] = new Vector2 (4,5);
						switch(typeElement){
						case typesActuators.DoubleEffect:
						case typesActuators.DoubleEffectWithSensors:
							_solenoidAttached = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttached.transform.position = right_over_4_5.transform.position;
							_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
							_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;	

							_solenoidAttachedB = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttachedB.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttachedB.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttachedB.transform.position = right_over_4_5.transform.position;
							_solenoidAttachedB.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttachedB.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
							_solenoidAttachedB.GetComponent<solenoidClass> ().numberSolenoid = 2;

							int _indexA = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
							int _indexB = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttachedB);

							if(_sensorAttachedA != null){
								int _indexIA = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (_sensorAttachedA);
								_sensorAttachedA.GetComponent<dragElementPneumatic> ().setValue (1);
							}
							if(_sensorAttachedB != null){
								int _indexIA = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (_sensorAttachedB);
							}
							setValueExternalSensorsA (1);
							break;
						case typesActuators.SimpleEffect:
							_solenoidAttached = Instantiate (_simpleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttached.transform.position = right_over_4_5.transform.position;
							_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Simple;
							_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;

							int _index = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
							setValueExternalSensorsA (1);
							break;
						}

						_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
					} else {
						tweenToInitialPosition ();
					}
					break;
				case "over_4_1":
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 1) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 2) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 3) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 1)) {
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(4, 1);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(4, 2);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(4, 3);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(3, 1);
						gameObject.transform.position = right_over_4_2.transform.position;
						gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x - 40, gameObject.transform.localPosition.y + y_offset, gameObject.transform.localPosition.z);

						addNewElement ();
						dragEnabled = false;
						inBench = true;
						rowsColumnsUsed = new Vector2[4];
						rowsColumnsUsed [0] = new Vector2 (4,1);
						rowsColumnsUsed [1] = new Vector2 (4,2);
						rowsColumnsUsed [2] = new Vector2 (4,3);
						rowsColumnsUsed [3] = new Vector2 (3,1);
						switch(typeElement){
						case typesActuators.DoubleEffect:
						case typesActuators.DoubleEffectWithSensors:
							_solenoidAttached = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttached.transform.position = right_over_3_1.transform.position;
							_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
							_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;	

							_solenoidAttachedB = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttachedB.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttachedB.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttachedB.transform.position = right_over_3_1.transform.position;
							_solenoidAttachedB.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttachedB.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
							_solenoidAttachedB.GetComponent<solenoidClass> ().numberSolenoid = 2;

							int _indexA = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
							int _indexB = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttachedB);

							if(_sensorAttachedA != null){
								int _indexIA = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (_sensorAttachedA);
								_sensorAttachedA.GetComponent<dragElementPneumatic> ().setValue (1);
							}
							if(_sensorAttachedB != null){
								int _indexIA = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (_sensorAttachedB);
							}
							setValueExternalSensorsA (1);
							break;
						case typesActuators.SimpleEffect:
							_solenoidAttached = Instantiate (_simpleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttached.transform.position = right_over_3_1.transform.position;
							_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Simple;
							_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;

							int _index = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
							setValueExternalSensorsA (1);
							break;
						}

						_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
					} else {
						tweenToInitialPosition ();
					}
					break;
				case "over_4_2":
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 1) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 2) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 3) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 1)) {
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(4, 1);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(4, 2);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(4, 3);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(3, 1);
						gameObject.transform.position = right_over_4_2.transform.position;
						gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x - 40, gameObject.transform.localPosition.y + y_offset, gameObject.transform.localPosition.z);

						addNewElement ();
						dragEnabled = false;
						inBench = true;
						rowsColumnsUsed = new Vector2[4];
						rowsColumnsUsed [0] = new Vector2 (4,1);
						rowsColumnsUsed [1] = new Vector2 (4,2);
						rowsColumnsUsed [2] = new Vector2 (4,3);
						rowsColumnsUsed [3] = new Vector2 (3,1);
						switch(typeElement){
						case typesActuators.DoubleEffect:
						case typesActuators.DoubleEffectWithSensors:
							_solenoidAttached = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttached.transform.position = right_over_3_1.transform.position;
							_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
							_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;	

							_solenoidAttachedB = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttachedB.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttachedB.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttachedB.transform.position = right_over_3_1.transform.position;
							_solenoidAttachedB.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttachedB.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
							_solenoidAttachedB.GetComponent<solenoidClass> ().numberSolenoid = 2;

							int _indexA = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
							int _indexB = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttachedB);

							if(_sensorAttachedA != null){
								int _indexIA = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (_sensorAttachedA);
								_sensorAttachedA.GetComponent<dragElementPneumatic> ().setValue (1);
							}
							if(_sensorAttachedB != null){
								int _indexIA = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (_sensorAttachedB);
							}
							setValueExternalSensorsA (1);
							break;
						case typesActuators.SimpleEffect:
							_solenoidAttached = Instantiate (_simpleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttached.transform.position = right_over_3_1.transform.position;
							_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Simple;
							_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;

							int _index = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
							setValueExternalSensorsA (1);
							break;
						}

						_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
					} else {
						tweenToInitialPosition ();
					}
					break;
				case "over_4_3":
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 2) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 3) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 4) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 2)) {
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(4, 2);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(4, 3);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(4, 4);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(3, 2);
						gameObject.transform.position = right_over_4_3.transform.position;
						gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x - 40, gameObject.transform.localPosition.y + y_offset, gameObject.transform.localPosition.z);

						addNewElement ();
						dragEnabled = false;
						inBench = true;
						rowsColumnsUsed = new Vector2[4];
						rowsColumnsUsed [0] = new Vector2 (4,2);
						rowsColumnsUsed [1] = new Vector2 (4,3);
						rowsColumnsUsed [2] = new Vector2 (4,4);
						rowsColumnsUsed [3] = new Vector2 (3,2);
						switch(typeElement){
						case typesActuators.DoubleEffect:
						case typesActuators.DoubleEffectWithSensors:
							_solenoidAttached = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttached.transform.position = right_over_3_2.transform.position;
							_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
							_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;	

							_solenoidAttachedB = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttachedB.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttachedB.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttachedB.transform.position = right_over_3_2.transform.position;
							_solenoidAttachedB.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttachedB.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
							_solenoidAttachedB.GetComponent<solenoidClass> ().numberSolenoid = 2;

							int _indexA = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
							int _indexB = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttachedB);

							if(_sensorAttachedA != null){
								int _indexIA = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (_sensorAttachedA);
								_sensorAttachedA.GetComponent<dragElementPneumatic> ().setValue (1);
							}
							if(_sensorAttachedB != null){
								int _indexIA = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (_sensorAttachedB);
							}
							setValueExternalSensorsA (1);
							break;
						case typesActuators.SimpleEffect:
							_solenoidAttached = Instantiate (_simpleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttached.transform.position = right_over_3_2.transform.position;
							_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Simple;
							_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;

							int _index = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
							setValueExternalSensorsA (1);
							break;
						}

						_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
					} else {
						tweenToInitialPosition ();
					}
					break;
				case "over_4_4":
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 3) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 4) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 5) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 3)) {
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(4, 3);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(4, 4);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(4, 5);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(3, 3);
						gameObject.transform.position = right_over_4_4.transform.position;
						gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x - 40, gameObject.transform.localPosition.y + y_offset, gameObject.transform.localPosition.z);

						addNewElement ();
						dragEnabled = false;
						inBench = true;
						rowsColumnsUsed = new Vector2[4];
						rowsColumnsUsed [0] = new Vector2 (4,3);
						rowsColumnsUsed [1] = new Vector2 (4,4);
						rowsColumnsUsed [2] = new Vector2 (4,5);
						rowsColumnsUsed [3] = new Vector2 (3,3);
						switch(typeElement){
						case typesActuators.DoubleEffect:
						case typesActuators.DoubleEffectWithSensors:
							_solenoidAttached = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttached.transform.position = right_over_3_3.transform.position;
							_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
							_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;	

							_solenoidAttachedB = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttachedB.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttachedB.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttachedB.transform.position = right_over_3_3.transform.position;
							_solenoidAttachedB.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttachedB.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
							_solenoidAttachedB.GetComponent<solenoidClass> ().numberSolenoid = 2;

							int _indexA = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
							int _indexB = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttachedB);

							if(_sensorAttachedA != null){
								int _indexIA = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (_sensorAttachedA);
								_sensorAttachedA.GetComponent<dragElementPneumatic> ().setValue (1);
							}
							if(_sensorAttachedB != null){
								int _indexIA = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (_sensorAttachedB);
							}
							setValueExternalSensorsA (1);
							break;
						case typesActuators.SimpleEffect:
							_solenoidAttached = Instantiate (_simpleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttached.transform.position = right_over_3_3.transform.position;
							_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Simple;
							_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;

							int _index = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
							setValueExternalSensorsA (1);
							break;
						}

						_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
					} else {
						tweenToInitialPosition ();
					}
					break;
				case "over_4_5":
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 4) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 5) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 6) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 4)) {
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(4, 4);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(4, 5);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(4, 6);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(3, 4);
						gameObject.transform.position = right_over_4_5.transform.position;
						gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x - 40, gameObject.transform.localPosition.y + y_offset, gameObject.transform.localPosition.z);

						addNewElement ();
						dragEnabled = false;
						inBench = true;
						rowsColumnsUsed = new Vector2[4];
						rowsColumnsUsed [0] = new Vector2 (4,4);
						rowsColumnsUsed [1] = new Vector2 (4,5);
						rowsColumnsUsed [2] = new Vector2 (4,6);
						rowsColumnsUsed [3] = new Vector2 (3,4);
						switch(typeElement){
						case typesActuators.DoubleEffect:
						case typesActuators.DoubleEffectWithSensors:
							_solenoidAttached = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttached.transform.position = right_over_3_4.transform.position;
							_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
							_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;	

							_solenoidAttachedB = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttachedB.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttachedB.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttachedB.transform.position = right_over_3_4.transform.position;
							_solenoidAttachedB.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttachedB.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
							_solenoidAttachedB.GetComponent<solenoidClass> ().numberSolenoid = 2;

							int _indexA = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
							int _indexB = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttachedB);

							if(_sensorAttachedA != null){
								int _indexIA = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (_sensorAttachedA);
								_sensorAttachedA.GetComponent<dragElementPneumatic> ().setValue (1);
							}
							if(_sensorAttachedB != null){
								int _indexIA = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (_sensorAttachedB);
							}
							setValueExternalSensorsA (1);
							break;
						case typesActuators.SimpleEffect:
							_solenoidAttached = Instantiate (_simpleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttached.transform.position = right_over_3_4.transform.position;
							_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Simple;
							_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;

							int _index = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
							setValueExternalSensorsA (1);
							break;
						}

						_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
					} else {
						tweenToInitialPosition ();
					}
					break;
				case "over_4_6":
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 5) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 6) && _rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 5)) {
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(4, 5);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(4, 6);
						_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(3, 5);
						gameObject.transform.position = right_over_4_6.transform.position;
						gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x - 40, gameObject.transform.localPosition.y + y_offset, gameObject.transform.localPosition.z);

						addNewElement ();
						dragEnabled = false;
						inBench = true;
						rowsColumnsUsed = new Vector2[3];
						rowsColumnsUsed [0] = new Vector2 (4,5);
						rowsColumnsUsed [1] = new Vector2 (4,6);
						rowsColumnsUsed [2] = new Vector2 (3,5);
						switch(typeElement){
						case typesActuators.DoubleEffect:
						case typesActuators.DoubleEffectWithSensors:
							_solenoidAttached = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttached.transform.position = right_over_3_5.transform.position;
							_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
							_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;	

							_solenoidAttachedB = Instantiate (_doubleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttachedB.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttachedB.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttachedB.transform.position = right_over_3_5.transform.position;
							_solenoidAttachedB.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttachedB.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Double;
							_solenoidAttachedB.GetComponent<solenoidClass> ().numberSolenoid = 2;

							int _indexA = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
							int _indexB = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttachedB);

							if(_sensorAttachedA != null){
								int _indexIA = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (_sensorAttachedA);
								_sensorAttachedA.GetComponent<dragElementPneumatic> ().setValue (1);
							}
							if(_sensorAttachedB != null){
								int _indexIA = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (_sensorAttachedB);
							}
							setValueExternalSensorsA (1);
							break;
						case typesActuators.SimpleEffect:
							_solenoidAttached = Instantiate (_simpleSolenoidPrefab, initialPosition, Quaternion.identity) as GameObject;
							_solenoidAttached.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
							_solenoidAttached.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex ());
							_solenoidAttached.transform.position = right_over_3_5.transform.position;
							_solenoidAttached.GetComponent<solenoidClass> ()._parent = gameObject;
							_solenoidAttached.GetComponent<solenoidClass> ()._typeSolenoid = solenoidClass.typesSolenoid.Simple;
							_solenoidAttached.GetComponent<solenoidClass> ().numberSolenoid = 1;

							int _index = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (_solenoidAttached);
							setValueExternalSensorsA (1);
							break;
						}

						_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
					} else {
						tweenToInitialPosition ();
					}
					break;
				default:
					tweenToInitialPosition ();
					break;	
				}
			} else {
				tweenToInitialPosition ();
			}
		}
	}

	private void ObjectMouseOver_(PointerEventData data){
		if(objectDragged == false){
			_mouseOver = true;
			_situation.GetComponent<PnematicTestBenchClass> ()._msgArea.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='" + textTag + "']").InnerText;	
		}
	}

	private void ObjectMouseOut_(PointerEventData data){
		if(_mouseOver == true){
			_mouseOver = false;
			_situation.GetComponent<PnematicTestBenchClass> ()._msgArea.text = "";
		}
	}

	private void deactivateRightOversWithException(Vector2[] _positions){
		bool _used_1_1 = false;
		bool _used_1_2 = false;
		bool _used_1_3 = false;
		bool _used_1_4 = false;
		bool _used_1_5 = false;
		bool _used_1_6 = false;
		bool _used_2_1 = false;
		bool _used_2_2 = false;
		bool _used_2_3 = false;
		bool _used_2_4 = false;
		bool _used_2_5 = false;
		bool _used_2_6 = false;
		bool _used_3_1 = false;
		bool _used_3_2 = false;
		bool _used_3_3 = false;
		bool _used_3_4 = false;
		bool _used_3_5 = false;
		bool _used_3_6 = false;
		bool _used_4_1 = false;
		bool _used_4_2 = false;
		bool _used_4_3 = false;
		bool _used_4_4 = false;
		bool _used_4_5 = false;
		bool _used_4_6 = false;

		for(int i = 0; i < _positions.Length; i++){
			if(_positions[i].x == 1 && _positions[i].y == 1){
				_used_1_1 = true;
			} else if(_positions[i].x == 1 && _positions[i].y == 2){
				_used_1_2 = true;
			} else if(_positions[i].x == 1 && _positions[i].y == 3){
				_used_1_3 = true;
			} else if(_positions[i].x == 1 && _positions[i].y == 4){
				_used_1_4 = true;
			} else if(_positions[i].x == 1 && _positions[i].y == 5){
				_used_1_5 = true;
			} else if(_positions[i].x == 1 && _positions[i].y == 6){
				_used_1_6 = true;
			} else if(_positions[i].x == 2 && _positions[i].y == 1){
				_used_2_1 = true;
			} else if(_positions[i].x == 2 && _positions[i].y == 2){
				_used_2_2 = true;
			} else if(_positions[i].x == 2 && _positions[i].y == 3){
				_used_2_3 = true;
			} else if(_positions[i].x == 2 && _positions[i].y == 4){
				_used_2_4 = true;
			} else if(_positions[i].x == 2 && _positions[i].y == 5){
				_used_2_5 = true;
			} else if(_positions[i].x == 2 && _positions[i].y == 6){
				_used_2_6 = true;
			} else if(_positions[i].x == 3 && _positions[i].y == 1){
				_used_3_1 = true;
			} else if(_positions[i].x == 3 && _positions[i].y == 2){
				_used_3_2 = true;
			} else if(_positions[i].x == 3 && _positions[i].y == 3){
				_used_3_3 = true;
			} else if(_positions[i].x == 3 && _positions[i].y == 4){
				_used_3_4 = true;
			} else if(_positions[i].x == 3 && _positions[i].y == 5){
				_used_3_5 = true;
			} else if(_positions[i].x == 3 && _positions[i].y == 6){
				_used_3_6 = true;
			} else if(_positions[i].x == 4 && _positions[i].y == 1){
				_used_4_1 = true;
			} else if(_positions[i].x == 4 && _positions[i].y == 2){
				_used_4_2 = true;
			} else if(_positions[i].x == 4 && _positions[i].y == 3){
				_used_4_3 = true;
			} else if(_positions[i].x == 4 && _positions[i].y == 4){
				_used_4_4 = true;
			} else if(_positions[i].x == 4 && _positions[i].y == 5){
				_used_4_5 = true;
			} else if(_positions[i].x == 4 && _positions[i].y == 6){
				_used_4_6 = true;
			}
		}

		if(_used_1_1 == false){
			right_over_1_1.CrossFadeAlpha(0, 0.2f, false);
		}
		if(_used_1_2 == false){
			right_over_1_2.CrossFadeAlpha(0, 0.2f, false);
		}
		if(_used_1_3 == false){
			right_over_1_3.CrossFadeAlpha(0, 0.2f, false);
		}
		if(_used_1_4 == false){
			right_over_1_4.CrossFadeAlpha(0, 0.2f, false);
		}
		if(_used_1_5 == false){
			right_over_1_5.CrossFadeAlpha(0, 0.2f, false);
		}
		if(_used_1_6 == false){
			right_over_1_6.CrossFadeAlpha(0, 0.2f, false);
		}
		if(_used_2_1 == false){
			right_over_2_1.CrossFadeAlpha(0, 0.2f, false);
		}
		if(_used_2_2 == false){
			right_over_2_2.CrossFadeAlpha(0, 0.2f, false);
		}
		if(_used_2_3 == false){
			right_over_2_3.CrossFadeAlpha(0, 0.2f, false);
		}
		if(_used_2_4 == false){
			right_over_2_4.CrossFadeAlpha(0, 0.2f, false);
		}
		if(_used_2_5 == false){
			right_over_2_5.CrossFadeAlpha(0, 0.2f, false);
		}
		if(_used_2_6 == false){
			right_over_2_6.CrossFadeAlpha(0, 0.2f, false);
		}
		if(_used_3_1 == false){
			right_over_3_1.CrossFadeAlpha(0, 0.2f, false);
		}
		if(_used_3_2 == false){
			right_over_3_2.CrossFadeAlpha(0, 0.2f, false);
		}
		if(_used_3_3 == false){
			right_over_3_3.CrossFadeAlpha(0, 0.2f, false);
		}
		if(_used_3_4 == false){
			right_over_3_4.CrossFadeAlpha(0, 0.2f, false);
		}
		if(_used_3_5 == false){
			right_over_3_5.CrossFadeAlpha(0, 0.2f, false);
		}
		if(_used_3_6 == false){
			right_over_3_6.CrossFadeAlpha(0, 0.2f, false);
		}
		if(_used_4_1 == false){
			right_over_4_1.CrossFadeAlpha(0, 0.2f, false);
		}
		if(_used_4_2 == false){
			right_over_4_2.CrossFadeAlpha(0, 0.2f, false);
		}
		if(_used_4_3 == false){
			right_over_4_3.CrossFadeAlpha(0, 0.2f, false);
		}
		if(_used_4_4 == false){
			right_over_4_4.CrossFadeAlpha(0, 0.2f, false);
		}
		if(_used_4_5 == false){
			right_over_4_5.CrossFadeAlpha(0, 0.2f, false);
		}
		if(_used_4_6 == false){
			right_over_4_6.CrossFadeAlpha(0, 0.2f, false);
		}
	}

	private void deactivateOvers(){
		right_over_1_1.CrossFadeAlpha(0, 0.2f, false);
		right_over_1_2.CrossFadeAlpha(0, 0.2f, false);
		right_over_1_3.CrossFadeAlpha(0, 0.2f, false);
		right_over_1_4.CrossFadeAlpha(0, 0.2f, false);
		right_over_1_5.CrossFadeAlpha(0, 0.2f, false);
		right_over_1_6.CrossFadeAlpha(0, 0.2f, false);

		right_over_2_1.CrossFadeAlpha(0, 0.2f, false);
		right_over_2_2.CrossFadeAlpha(0, 0.2f, false);
		right_over_2_3.CrossFadeAlpha(0, 0.2f, false);
		right_over_2_4.CrossFadeAlpha(0, 0.2f, false);
		right_over_2_5.CrossFadeAlpha(0, 0.2f, false);
		right_over_2_6.CrossFadeAlpha(0, 0.2f, false);

		right_over_3_1.CrossFadeAlpha(0, 0.2f, false);
		right_over_3_2.CrossFadeAlpha(0, 0.2f, false);
		right_over_3_3.CrossFadeAlpha(0, 0.2f, false);
		right_over_3_4.CrossFadeAlpha(0, 0.2f, false);
		right_over_3_5.CrossFadeAlpha(0, 0.2f, false);
		right_over_3_6.CrossFadeAlpha(0, 0.2f, false);

		right_over_4_1.CrossFadeAlpha(0, 0.2f, false);
		right_over_4_2.CrossFadeAlpha(0, 0.2f, false);
		right_over_4_3.CrossFadeAlpha(0, 0.2f, false);
		right_over_4_4.CrossFadeAlpha(0, 0.2f, false);
		right_over_4_5.CrossFadeAlpha(0, 0.2f, false);
		right_over_4_6.CrossFadeAlpha(0, 0.2f, false);
	}

	private void activateRightColliders(){
		right_collider_1_1.enabled = true;
		right_collider_1_2.enabled = true;
		right_collider_1_3.enabled = true;
		right_collider_1_4.enabled = true;
		right_collider_1_5.enabled = true;
		right_collider_1_6.enabled = true;

		right_collider_2_1.enabled = true;
		right_collider_2_2.enabled = true;
		right_collider_2_3.enabled = true;
		right_collider_2_4.enabled = true;
		right_collider_2_5.enabled = true;
		right_collider_2_6.enabled = true;

		right_collider_3_1.enabled = true;
		right_collider_3_2.enabled = true;
		right_collider_3_3.enabled = true;
		right_collider_3_4.enabled = true;
		right_collider_3_5.enabled = true;
		right_collider_3_6.enabled = true;

		right_collider_4_1.enabled = true;
		right_collider_4_2.enabled = true;
		right_collider_4_3.enabled = true;
		right_collider_4_4.enabled = true;
		right_collider_4_5.enabled = true;
		right_collider_4_6.enabled = true;
	}

	private void deactivateRightColliders(){
		right_collider_1_1.enabled = false;
		right_collider_1_2.enabled = false;
		right_collider_1_3.enabled = false;
		right_collider_1_4.enabled = false;
		right_collider_1_5.enabled = false;
		right_collider_1_6.enabled = false;

		right_collider_2_1.enabled = false;
		right_collider_2_2.enabled = false;
		right_collider_2_3.enabled = false;
		right_collider_2_4.enabled = false;
		right_collider_2_5.enabled = false;
		right_collider_2_6.enabled = false;

		right_collider_3_1.enabled = false;
		right_collider_3_2.enabled = false;
		right_collider_3_3.enabled = false;
		right_collider_3_4.enabled = false;
		right_collider_3_5.enabled = false;
		right_collider_3_6.enabled = false;

		right_collider_4_1.enabled = false;
		right_collider_4_2.enabled = false;
		right_collider_4_3.enabled = false;
		right_collider_4_4.enabled = false;
		right_collider_4_5.enabled = false;
		right_collider_4_6.enabled = false;
	}

	private void tweenToInitialPosition(){
		iTween.MoveTo(
			gameObject,
			iTween.Hash(
				"position", initialPosition,
				"looktarget", Camera.main,
				"easeType", iTween.EaseType.easeOutExpo,
				"time", 0.2f,
				"islocal",true
			)
		);

		Invoke ("goToInitialState", 0.1f);
	}

	private void goToInitialState(){
		_standPosition.enabled = true;
		_benchPosition.SetActive (false);
		gameObject.GetComponent<RectTransform> ().SetSiblingIndex (initialIndex);
	}

	private void addNewElement (){
		GameObject newElement = Instantiate (gameObject, initialPosition, Quaternion.identity) as GameObject;
		newElement.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
		newElement.transform.SetSiblingIndex (initialIndex);
		newElement.GetComponent<actuatorElement> ()._standPosition.enabled = true;
		newElement.GetComponent<actuatorElement> ()._benchPosition.SetActive (false);
	}



	public void activateRightOver(Vector2[] _rows_columns_elements = null, bool _hideAutomatically = true){
		bool feedbackActivated = true;

		if(_rows_columns_elements == null){
			feedbackActivated = false;
			_rows_columns_elements = rowsColumnsUsed;
		}

		int _row = 0;
		int _column = 0;
		for(int i = 0; i < _rows_columns_elements.Length; i++){
			_row = (int)_rows_columns_elements [i].x;
			_column = (int)_rows_columns_elements [i].y;

			switch(_row){
			case 1:
				switch(_column){
				case 1:
					right_over_1_1.CrossFadeAlpha(1, 0.2f, false);
					break;
				case 2:
					right_over_1_2.CrossFadeAlpha(1, 0.2f, false);
					break;
				case 3:
					right_over_1_3.CrossFadeAlpha(1, 0.2f, false);
					break;
				case 4:
					right_over_1_4.CrossFadeAlpha(1, 0.2f, false);
					break;
				case 5:
					right_over_1_5.CrossFadeAlpha(1, 0.2f, false);
					break;
				case 6:
					right_over_1_6.CrossFadeAlpha(1, 0.2f, false);
					break;
				}
				break;
			case 2:
				switch(_column){
				case 1:
					right_over_2_1.CrossFadeAlpha(1, 0.2f, false);
					break;
				case 2:
					right_over_2_2.CrossFadeAlpha(1, 0.2f, false);
					break;
				case 3:
					right_over_2_3.CrossFadeAlpha(1, 0.2f, false);
					break;
				case 4:
					right_over_2_4.CrossFadeAlpha(1, 0.2f, false);
					break;
				case 5:
					right_over_2_5.CrossFadeAlpha(1, 0.2f, false);
					break;
				case 6:
					right_over_2_6.CrossFadeAlpha(1, 0.2f, false);
					break;
				}
				break;
			case 3:
				switch(_column){
				case 1:
					right_over_3_1.CrossFadeAlpha(1, 0.2f, false);
					break;
				case 2:
					right_over_3_2.CrossFadeAlpha(1, 0.2f, false);
					break;
				case 3:
					right_over_3_3.CrossFadeAlpha(1, 0.2f, false);
					break;
				case 4:
					right_over_3_4.CrossFadeAlpha(1, 0.2f, false);
					break;
				case 5:
					right_over_3_5.CrossFadeAlpha(1, 0.2f, false);
					break;
				case 6:
					right_over_3_6.CrossFadeAlpha(1, 0.2f, false);
					break;
				}
				break;
			case 4:
				switch(_column){
				case 1:
					right_over_4_1.CrossFadeAlpha(1, 0.2f, false);
					break;
				case 2:
					right_over_4_2.CrossFadeAlpha(1, 0.2f, false);
					break;
				case 3:
					right_over_4_3.CrossFadeAlpha(1, 0.2f, false);
					break;
				case 4:
					right_over_4_4.CrossFadeAlpha(1, 0.2f, false);
					break;
				case 5:
					right_over_4_5.CrossFadeAlpha(1, 0.2f, false);
					break;
				case 6:
					right_over_4_6.CrossFadeAlpha(1, 0.2f, false);
					break;
				}
				break;
			}
		}

		if(_hideAutomatically == true){
			Invoke ("deactivateOvers", 0.3f);
			if(feedbackActivated == true){
				Invoke ("PostactivateRightOver", 0.7f);	
			}
		}
	}

	private void PostactivateRightOver(){
		activateRightOver ();
	}

	IEnumerator goForwardAction(){
		animationEnabled = true;

		switch(actualStatus){
		case 1:
			//deactivate sensors if exists
			if (_sensorAttachedA != null) {
				_sensorAttachedA.GetComponent<dragElementPneumatic> ().setValue (0);
			}

			setValueExternalSensorsA (0, true);

			actualStatus = 2;
			_status1.enabled = false;
			_status2.enabled = true;
			_status3.enabled = false;
			_status4.enabled = false;
			_status5.enabled = false;
			_status6.enabled = false;
			_status7.enabled = false;
			_status8.enabled = false;
			_status9.enabled = false;
			_status10.enabled = false;
			break;
		case 2:
			actualStatus = 3;
			_status1.enabled = false;
			_status2.enabled = false;
			_status3.enabled = true;
			_status4.enabled = false;
			_status5.enabled = false;
			_status6.enabled = false;
			_status7.enabled = false;
			_status8.enabled = false;
			_status9.enabled = false;
			_status10.enabled = false;
			break;
		case 3:
			actualStatus = 4;
			_status1.enabled = false;
			_status2.enabled = false;
			_status3.enabled = false;
			_status4.enabled = true;
			_status5.enabled = false;
			_status6.enabled = false;
			_status7.enabled = false;
			_status8.enabled = false;
			_status9.enabled = false;
			_status10.enabled = false;
			break;
		case 4:
			actualStatus = 5;
			_status1.enabled = false;
			_status2.enabled = false;
			_status3.enabled = false;
			_status4.enabled = false;
			_status5.enabled = true;
			_status6.enabled = false;
			_status7.enabled = false;
			_status8.enabled = false;
			_status9.enabled = false;
			_status10.enabled = false;
			break;
		case 5:
			actualStatus = 6;
			_status1.enabled = false;
			_status2.enabled = false;
			_status3.enabled = false;
			_status4.enabled = false;
			_status5.enabled = false;
			_status6.enabled = true;
			_status7.enabled = false;
			_status8.enabled = false;
			_status9.enabled = false;
			_status10.enabled = false;
			break;
		case 6:
			actualStatus = 7;
			_status1.enabled = false;
			_status2.enabled = false;
			_status3.enabled = false;
			_status4.enabled = false;
			_status5.enabled = false;
			_status6.enabled = false;
			_status7.enabled = true;
			_status8.enabled = false;
			_status9.enabled = false;
			_status10.enabled = false;
			break;
		case 7:
			actualStatus = 8;
			_status1.enabled = false;
			_status2.enabled = false;
			_status3.enabled = false;
			_status4.enabled = false;
			_status5.enabled = false;
			_status6.enabled = false;
			_status7.enabled = false;
			_status8.enabled = true;
			_status9.enabled = false;
			_status10.enabled = false;
			break;
		case 8:
			actualStatus = 9;
			_status1.enabled = false;
			_status2.enabled = false;
			_status3.enabled = false;
			_status4.enabled = false;
			_status5.enabled = false;
			_status6.enabled = false;
			_status7.enabled = false;
			_status8.enabled = false;
			_status9.enabled = true;
			_status10.enabled = false;
			break;
		case 9:
			actualStatus = 10;
			_status1.enabled = false;
			_status2.enabled = false;
			_status3.enabled = false;
			_status4.enabled = false;
			_status5.enabled = false;
			_status6.enabled = false;
			_status7.enabled = false;
			_status8.enabled = false;
			_status9.enabled = false;
			_status10.enabled = true;
			break;
		case 10:
			//activate sensors if exists
			if(_sensorAttachedB != null){
				_sensorAttachedB.GetComponent<dragElementPneumatic> ().setValue (1);
			}
			setValueExternalSensorsB (1);
			break;
		}

		yield return new WaitForSeconds (0.02f);

		if (goForward == true && goBack == false) {
			StartCoroutine (goForwardAction ());
		} else if (goForward == false && goBack == true) {
			StartCoroutine (goBackwardAction ());
		} else {
			animationEnabled = false;
		}
	}

	IEnumerator goBackwardAction(){
		animationEnabled = true;

		switch(actualStatus){
		case 1:
			//activate sensors if exists
			if (_sensorAttachedA != null) {
				_sensorAttachedA.GetComponent<dragElementPneumatic> ().setValue (1);
			}
			setValueExternalSensorsA (1, false);
			break;
		case 2:
			actualStatus = 1;
			_status1.enabled = true;
			_status2.enabled = false;
			_status3.enabled = false;
			_status4.enabled = false;
			_status5.enabled = false;
			_status6.enabled = false;
			_status7.enabled = false;
			_status8.enabled = false;
			_status9.enabled = false;
			_status10.enabled = false;
			break;
		case 3:
			actualStatus = 2;
			_status1.enabled = false;
			_status2.enabled = true;
			_status3.enabled = false;
			_status4.enabled = false;
			_status5.enabled = false;
			_status6.enabled = false;
			_status7.enabled = false;
			_status8.enabled = false;
			_status9.enabled = false;
			_status10.enabled = false;
			break;
		case 4:
			actualStatus = 3;
			_status1.enabled = false;
			_status2.enabled = false;
			_status3.enabled = true;
			_status4.enabled = false;
			_status5.enabled = false;
			_status6.enabled = false;
			_status7.enabled = false;
			_status8.enabled = false;
			_status9.enabled = false;
			_status10.enabled = false;
			break;
		case 5:
			actualStatus = 4;
			_status1.enabled = false;
			_status2.enabled = false;
			_status3.enabled = false;
			_status4.enabled = true;
			_status5.enabled = false;
			_status6.enabled = false;
			_status7.enabled = false;
			_status8.enabled = false;
			_status9.enabled = false;
			_status10.enabled = false;
			break;
		case 6:
			actualStatus = 5;
			_status1.enabled = false;
			_status2.enabled = false;
			_status3.enabled = false;
			_status4.enabled = false;
			_status5.enabled = true;
			_status6.enabled = false;
			_status7.enabled = false;
			_status8.enabled = false;
			_status9.enabled = false;
			_status10.enabled = false;
			break;
		case 7:
			actualStatus = 6;
			_status1.enabled = false;
			_status2.enabled = false;
			_status3.enabled = false;
			_status4.enabled = false;
			_status5.enabled = false;
			_status6.enabled = true;
			_status7.enabled = false;
			_status8.enabled = false;
			_status9.enabled = false;
			_status10.enabled = false;
			break;
		case 8:
			actualStatus = 7;
			_status1.enabled = false;
			_status2.enabled = false;
			_status3.enabled = false;
			_status4.enabled = false;
			_status5.enabled = false;
			_status6.enabled = false;
			_status7.enabled = true;
			_status8.enabled = false;
			_status9.enabled = false;
			_status10.enabled = false;
			break;
		case 9:
			actualStatus = 8;
			_status1.enabled = false;
			_status2.enabled = false;
			_status3.enabled = false;
			_status4.enabled = false;
			_status5.enabled = false;
			_status6.enabled = false;
			_status7.enabled = false;
			_status8.enabled = true;
			_status9.enabled = false;
			_status10.enabled = false;
			break;
		case 10:
			actualStatus = 9;
			_status1.enabled = false;
			_status2.enabled = false;
			_status3.enabled = false;
			_status4.enabled = false;
			_status5.enabled = false;
			_status6.enabled = false;
			_status7.enabled = false;
			_status8.enabled = false;
			_status9.enabled = true;
			_status10.enabled = false;
			//deactivate sensors if exists
			if(_sensorAttachedB != null){
				_sensorAttachedB.GetComponent<dragElementPneumatic> ().setValue (0);
			}
			setValueExternalSensorsB (0);
			break;
		}

		yield return new WaitForSeconds (0.02f);

		if (goForward == true && goBack == false) {
			StartCoroutine (goForwardAction ());
		} else if (goForward == false && goBack == true) {
			StartCoroutine (goBackwardAction ());
		} else {
			animationEnabled = false;
		}
	}

	private void setValueExternalSensorsA(int _value, bool firstTime = true){
		for(int i = 0; i < _situation.GetComponent<PnematicTestBenchClass>().inputElements.Length; i++){
			if (_situation.GetComponent<PnematicTestBenchClass> ().inputElements [i] != null && _situation.GetComponent<PnematicTestBenchClass> ().inputElements [i].GetComponent<dragElementPneumatic> ()._typeInput == dragElementPneumatic.typesInputs.Right) {
				if (_situation.GetComponent<PnematicTestBenchClass> ().inputElements [i].GetComponent<dragElementPneumatic> ()._typeComponent == dragElementPneumatic.typesComponents.limit_switch_sensor) {
					if (_situation.GetComponent<PnematicTestBenchClass> ().inputElements [i].GetComponent<dragElementPneumatic> ().rowUsed == (rowsColumnsUsed [0].x + 1) && _situation.GetComponent<PnematicTestBenchClass> ().inputElements [i].GetComponent<dragElementPneumatic> ().columnUsed == (rowsColumnsUsed [0].y + 2)) {
						//set value
						_situation.GetComponent<PnematicTestBenchClass> ().inputElements [i].GetComponent<dragElementPneumatic> ().setValue (_value);
						if(firstTime == true){
							_situation.GetComponent<PnematicTestBenchClass> ().inputElements [i].GetComponent<dragElementPneumatic> ().setSibling (gameObject.transform.GetSiblingIndex () + 1);
						}
					}
				}
			}
		}
	}

	private void setValueExternalSensorsB(int _value){
		for(int i = 0; i < _situation.GetComponent<PnematicTestBenchClass>().inputElements.Length; i++){
			if (_situation.GetComponent<PnematicTestBenchClass> ().inputElements [i] != null && _situation.GetComponent<PnematicTestBenchClass> ().inputElements [i].GetComponent<dragElementPneumatic> ()._typeInput == dragElementPneumatic.typesInputs.Right) {
				if (_situation.GetComponent<PnematicTestBenchClass> ().inputElements [i].GetComponent<dragElementPneumatic> ()._typeComponent == dragElementPneumatic.typesComponents.inductive_sensor) {
					if (_situation.GetComponent<PnematicTestBenchClass> ().inputElements [i].GetComponent<dragElementPneumatic> ().rowUsed == (rowsColumnsUsed [0].x) && _situation.GetComponent<PnematicTestBenchClass> ().inputElements [i].GetComponent<dragElementPneumatic> ().columnUsed == (rowsColumnsUsed [0].y + 3)) {
						//set value
						_situation.GetComponent<PnematicTestBenchClass> ().inputElements [i].GetComponent<dragElementPneumatic> ().setValue (_value);
					}
				}
			}
		}
	}

	// Update is called once per frame
	void Update () {
		if(inBench == true){
			if (_solenoidAttached != null && _solenoidAttachedB != null && _solenoidAttached.GetComponent<solenoidClass> ().actualValue == 1 && _solenoidAttachedB.GetComponent<solenoidClass> ().actualValue == 0) {
				//open
				goForward = true;
				goBack = false;
				if (animationEnabled == false) {
					StartCoroutine (goForwardAction ());
				}
			} else if (_solenoidAttached != null && _solenoidAttachedB != null && _solenoidAttached.GetComponent<solenoidClass> ().actualValue == 0 && _solenoidAttachedB.GetComponent<solenoidClass> ().actualValue == 1) {
				//close
				goForward = false;
				goBack = true;
				if (animationEnabled == false) {
					StartCoroutine (goBackwardAction ());
				}
			} else if (_solenoidAttached != null && _solenoidAttachedB == null && _solenoidAttached.GetComponent<solenoidClass> ().actualValue == 1) {
				goForward = true;
				goBack = false;
				if (animationEnabled == false) {
					StartCoroutine (goForwardAction ());
				}
			} else if (_solenoidAttached != null && _solenoidAttachedB == null && _solenoidAttached.GetComponent<solenoidClass> ().actualValue == 0) {
				goForward = false;
				goBack = true;
				if (animationEnabled == false) {
					StartCoroutine (goForwardAction ());
				}
			} else {
				goForward = false;
				goBack = false;
			}
		}
	}
}
