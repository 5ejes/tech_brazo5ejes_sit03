﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class dragElementPneumatic : MonoBehaviour {

	public enum typesDragElement
	{
		Input,
		Output
	}

	public enum typesInputs{
		Left,
		Right
	}

	public enum typesComponents
	{
		capacitive_sensor,
		inductive_sensor,
		optical_sensor,
		red_button_sensor,
		green_button_sensor,
		limit_switch_sensor,
		switch_sensor,
		sound_actuator,
		green_light_actuator,
		yellow_light_actuator,
		red_light_actuator
	}

	public Image _actualImage;
	public Image _pressedImage;
	public Sprite _normalPosition;
	public Sprite _benchPosition;

	public typesDragElement _typeElement;
	public typesInputs _typeInput;
	public typesComponents _typeComponent;
	public string textTag;

	public bool isSwitch = false;
	public bool hideBaseImageOnActive = false;

	public int normalValue = 0;
	public int pressedValue = 0;

	public int actualValue = 0;

	public int rowUsed = 0;
	public int columnUsed = 0;

	public bool dragEnabled = true;
	public bool inBench = false;

	public AudioClip _downAudio;
	public AudioClip _upAudio;

	public GameObject _situation;

	public GameObject _redCablePrefab;
	public GameObject _blueCablePrefab;
	public GameObject _blackCablePrefab;
	public GameObject _whiteCablePrefab;
	public GameObject _grayCablePrefab;

	public GameObject _leftCable;
	public GameObject _rightCable;
	public GameObject _topCable;

	public bool manualActivation = true;

	private GameObject _leftOvers;
	private GameObject _leftColliders;

	private GameObject _rightOvers;
	private GameObject _rightColliders;

	private Image left_over_1_1;
	private Image left_over_1_2;
	private Image left_over_1_3;
	private Image left_over_1_4;

	private Image left_over_2_1;
	private Image left_over_2_2;
	private Image left_over_2_3;
	private Image left_over_2_4;

	private Image left_over_3_1;
	private Image left_over_3_2;
	private Image left_over_3_3;
	private Image left_over_3_4;

	private Image left_over_4_1;
	private Image left_over_4_2;
	private Image left_over_4_3;
	private Image left_over_4_4;

	private Image left_collider_1_1;
	private Image left_collider_1_2;
	private Image left_collider_1_3;
	private Image left_collider_1_4;

	private Image left_collider_2_1;
	private Image left_collider_2_2;
	private Image left_collider_2_3;
	private Image left_collider_2_4;

	private Image left_collider_3_1;
	private Image left_collider_3_2;
	private Image left_collider_3_3;
	private Image left_collider_3_4;

	private Image left_collider_4_1;
	private Image left_collider_4_2;
	private Image left_collider_4_3;
	private Image left_collider_4_4;

	private Image right_over_1_1;
	private Image right_over_1_2;
	private Image right_over_1_3;
	private Image right_over_1_4;
	private Image right_over_1_5;
	private Image right_over_1_6;

	private Image right_over_2_1;
	private Image right_over_2_2;
	private Image right_over_2_3;
	private Image right_over_2_4;
	private Image right_over_2_5;
	private Image right_over_2_6;

	private Image right_over_3_1;
	private Image right_over_3_2;
	private Image right_over_3_3;
	private Image right_over_3_4;
	private Image right_over_3_5;
	private Image right_over_3_6;

	private Image right_over_4_1;
	private Image right_over_4_2;
	private Image right_over_4_3;
	private Image right_over_4_4;
	private Image right_over_4_5;
	private Image right_over_4_6;

	private Image right_collider_1_1;
	private Image right_collider_1_2;
	private Image right_collider_1_3;
	private Image right_collider_1_4;
	private Image right_collider_1_5;
	private Image right_collider_1_6;

	private Image right_collider_2_1;
	private Image right_collider_2_2;
	private Image right_collider_2_3;
	private Image right_collider_2_4;
	private Image right_collider_2_5;
	private Image right_collider_2_6;

	private Image right_collider_3_1;
	private Image right_collider_3_2;
	private Image right_collider_3_3;
	private Image right_collider_3_4;
	private Image right_collider_3_5;
	private Image right_collider_3_6;

	private Image right_collider_4_1;
	private Image right_collider_4_2;
	private Image right_collider_4_3;
	private Image right_collider_4_4;
	private Image right_collider_4_5;
	private Image right_collider_4_6;

	private bool objectDragged;
	private Vector3 initialPosition;
	private Vector3 startPosition;
	private int initialIndex;
	private int startIndex;

	private bool _mouseOver = false;
	// Use this for initialization
	void Start () {

		actualValue = normalValue;

		initialPosition = gameObject.transform.localPosition;
		initialIndex = gameObject.GetComponent<RectTransform> ().GetSiblingIndex ();

		if (gameObject.GetComponent<EventTrigger> () != null) {
			EventTrigger _prev_trigger_Object = gameObject.GetComponent<EventTrigger> ();
			List<EventTrigger.Entry> entriesToRemove = new List<EventTrigger.Entry>();

			foreach (var entry in _prev_trigger_Object.triggers) {        
				entry.callback.RemoveAllListeners ();
				entriesToRemove.Add (entry);
			}

			foreach(var entry in entriesToRemove)
			{
				_prev_trigger_Object.triggers.Remove(entry);
			}
		} else {
			gameObject.AddComponent<EventTrigger> ();
		}

		EventTrigger trigger_Object = gameObject.GetComponent<EventTrigger>();

		EventTrigger.Entry entry_0 = new EventTrigger.Entry();
		entry_0.eventID = EventTriggerType.BeginDrag;
		entry_0.callback.AddListener((data) => { BeginDrag_((PointerEventData)data); });

		EventTrigger.Entry entry_1 = new EventTrigger.Entry();
		entry_1.eventID = EventTriggerType.Drag;
		entry_1.callback.AddListener((data) => { ObjectDrag_((PointerEventData)data); });

		EventTrigger.Entry entry_2 = new EventTrigger.Entry();
		entry_2.eventID = EventTriggerType.EndDrag;
		entry_2.callback.AddListener((data) => { ObjectDrop_((PointerEventData)data); });

		EventTrigger.Entry entry_3 = new EventTrigger.Entry();
		entry_3.eventID = EventTriggerType.PointerDown;
		entry_3.callback.AddListener((data) => { ObjectMouseDown_((PointerEventData)data); });

		EventTrigger.Entry entry_4 = new EventTrigger.Entry();
		entry_4.eventID = EventTriggerType.PointerUp;
		entry_4.callback.AddListener((data) => { ObjectMouseUp_((PointerEventData)data); });

		EventTrigger.Entry entry_5 = new EventTrigger.Entry();
		entry_5.eventID = EventTriggerType.PointerEnter;
		entry_5.callback.AddListener((data) => { ObjectMouseOver_((PointerEventData)data); });

		EventTrigger.Entry entry_6 = new EventTrigger.Entry();
		entry_6.eventID = EventTriggerType.PointerExit;
		entry_6.callback.AddListener((data) => { ObjectMouseOut_((PointerEventData)data); });

		trigger_Object.triggers.Add(entry_0);
		trigger_Object.triggers.Add(entry_1);
		trigger_Object.triggers.Add(entry_2);

		if(manualActivation == true){
			trigger_Object.triggers.Add(entry_3);
			trigger_Object.triggers.Add(entry_4);	
		}

		trigger_Object.triggers.Add(entry_5);
		trigger_Object.triggers.Add(entry_6);

		_situation = GameObject.Find ("PneumaticBench");

		_leftOvers = GameObject.Find ("Canvas/PneumaticBench/leftOvers");
		_leftColliders = GameObject.Find ("Canvas/PneumaticBench/leftColliders");

		_rightOvers = GameObject.Find ("Canvas/PneumaticBench/rightOvers");
		_rightColliders = GameObject.Find ("Canvas/PneumaticBench/rightColliders");

		left_over_1_1 = GameObject.Find ("Canvas/PneumaticBench/leftOvers/over_1_1").GetComponent<Image>();
		left_over_1_2 = GameObject.Find ("Canvas/PneumaticBench/leftOvers/over_1_2").GetComponent<Image>();

		left_over_2_1 = GameObject.Find ("Canvas/PneumaticBench/leftOvers/over_2_1").GetComponent<Image>();
		left_over_2_2 = GameObject.Find ("Canvas/PneumaticBench/leftOvers/over_2_2").GetComponent<Image>();

		left_over_3_1 = GameObject.Find ("Canvas/PneumaticBench/leftOvers/over_3_1").GetComponent<Image>();
		left_over_3_2 = GameObject.Find ("Canvas/PneumaticBench/leftOvers/over_3_2").GetComponent<Image>();

		left_over_4_1 = GameObject.Find ("Canvas/PneumaticBench/leftOvers/over_4_1").GetComponent<Image>();
		left_over_4_2 = GameObject.Find ("Canvas/PneumaticBench/leftOvers/over_4_2").GetComponent<Image>();

		left_collider_1_1 = GameObject.Find ("Canvas/PneumaticBench/leftColliders/over_1_1").GetComponent<Image>();
		left_collider_1_2 = GameObject.Find ("Canvas/PneumaticBench/leftColliders/over_1_2").GetComponent<Image>();

		left_collider_2_1 = GameObject.Find ("Canvas/PneumaticBench/leftColliders/over_2_1").GetComponent<Image>();
		left_collider_2_2 = GameObject.Find ("Canvas/PneumaticBench/leftColliders/over_2_2").GetComponent<Image>();

		left_collider_3_1 = GameObject.Find ("Canvas/PneumaticBench/leftColliders/over_3_1").GetComponent<Image>();
		left_collider_3_2 = GameObject.Find ("Canvas/PneumaticBench/leftColliders/over_3_2").GetComponent<Image>();

		left_collider_4_1 = GameObject.Find ("Canvas/PneumaticBench/leftColliders/over_4_1").GetComponent<Image>();
		left_collider_4_2 = GameObject.Find ("Canvas/PneumaticBench/leftColliders/over_4_2").GetComponent<Image>();


		right_over_1_1 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_1_1").GetComponent<Image>();
		right_over_1_2 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_1_2").GetComponent<Image>();
		right_over_1_3 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_1_3").GetComponent<Image>();
		right_over_1_4 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_1_4").GetComponent<Image>();
		right_over_1_5 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_1_5").GetComponent<Image>();
		right_over_1_6 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_1_6").GetComponent<Image>();

		right_over_2_1 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_2_1").GetComponent<Image>();
		right_over_2_2 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_2_2").GetComponent<Image>();
		right_over_2_3 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_2_3").GetComponent<Image>();
		right_over_2_4 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_2_4").GetComponent<Image>();
		right_over_2_5 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_2_5").GetComponent<Image>();
		right_over_2_6 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_2_6").GetComponent<Image>();

		right_over_3_1 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_3_1").GetComponent<Image>();
		right_over_3_2 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_3_2").GetComponent<Image>();
		right_over_3_3 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_3_3").GetComponent<Image>();
		right_over_3_4 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_3_4").GetComponent<Image>();
		right_over_3_5 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_3_5").GetComponent<Image>();
		right_over_3_6 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_3_6").GetComponent<Image>();

		right_over_4_1 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_4_1").GetComponent<Image>();
		right_over_4_2 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_4_2").GetComponent<Image>();
		right_over_4_3 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_4_3").GetComponent<Image>();
		right_over_4_4 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_4_4").GetComponent<Image>();
		right_over_4_5 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_4_5").GetComponent<Image>();
		right_over_4_6 = GameObject.Find ("Canvas/PneumaticBench/rightOvers/over_4_6").GetComponent<Image>();

		right_collider_1_1 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_1_1").GetComponent<Image>();
		right_collider_1_2 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_1_2").GetComponent<Image>();
		right_collider_1_3 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_1_3").GetComponent<Image>();
		right_collider_1_4 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_1_4").GetComponent<Image>();
		right_collider_1_5 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_1_5").GetComponent<Image>();
		right_collider_1_6 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_1_6").GetComponent<Image>();

		right_collider_2_1 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_2_1").GetComponent<Image>();
		right_collider_2_2 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_2_2").GetComponent<Image>();
		right_collider_2_3 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_2_3").GetComponent<Image>();
		right_collider_2_4 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_2_4").GetComponent<Image>();
		right_collider_2_5 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_2_5").GetComponent<Image>();
		right_collider_2_6 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_2_6").GetComponent<Image>();

		right_collider_3_1 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_3_1").GetComponent<Image>();
		right_collider_3_2 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_3_2").GetComponent<Image>();
		right_collider_3_3 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_3_3").GetComponent<Image>();
		right_collider_3_4 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_3_4").GetComponent<Image>();
		right_collider_3_5 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_3_5").GetComponent<Image>();
		right_collider_3_6 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_3_6").GetComponent<Image>();

		right_collider_4_1 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_4_1").GetComponent<Image>();
		right_collider_4_2 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_4_2").GetComponent<Image>();
		right_collider_4_3 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_4_3").GetComponent<Image>();
		right_collider_4_4 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_4_4").GetComponent<Image>();
		right_collider_4_5 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_4_5").GetComponent<Image>();
		right_collider_4_6 = GameObject.Find ("Canvas/PneumaticBench/rightColliders/over_4_6").GetComponent<Image>();

		/*
		left_over_1_1.enabled = false;
		left_over_1_2.enabled = false;
		left_over_1_3.enabled = false;
		left_over_1_4.enabled = false;

		left_over_2_1.enabled = false;
		left_over_2_2.enabled = false;
		left_over_2_3.enabled = false;
		left_over_2_4.enabled = false;

		left_over_3_1.enabled = false;
		left_over_3_2.enabled = false;
		left_over_3_3.enabled = false;
		left_over_3_4.enabled = false;

		left_over_4_1.enabled = false;
		left_over_4_2.enabled = false;
		left_over_4_3.enabled = false;
		left_over_4_4.enabled = false;
		*/

		left_over_1_1.CrossFadeAlpha(0, 0.01f, false);
		left_over_1_2.CrossFadeAlpha(0, 0.01f, false);
	
		left_over_2_1.CrossFadeAlpha(0, 0.01f, false);
		left_over_2_2.CrossFadeAlpha(0, 0.01f, false);

		left_over_3_1.CrossFadeAlpha(0, 0.01f, false);
		left_over_3_2.CrossFadeAlpha(0, 0.01f, false);

		left_over_4_1.CrossFadeAlpha(0, 0.01f, false);
		left_over_4_2.CrossFadeAlpha(0, 0.01f, false);

		left_collider_1_1.enabled = false;
		left_collider_1_2.enabled = false;

		left_collider_2_1.enabled = false;
		left_collider_2_2.enabled = false;

		left_collider_3_1.enabled = false;
		left_collider_3_2.enabled = false;

		left_collider_4_1.enabled = false;
		left_collider_4_2.enabled = false;

		/*
		right_over_1_1.enabled = false;
		right_over_1_2.enabled = false;
		right_over_1_3.enabled = false;
		right_over_1_4.enabled = false;

		right_over_2_1.enabled = false;
		right_over_2_2.enabled = false;
		right_over_2_3.enabled = false;
		right_over_2_4.enabled = false;

		right_over_3_1.enabled = false;
		right_over_3_2.enabled = false;
		right_over_3_3.enabled = false;
		right_over_3_4.enabled = false;

		right_over_4_1.enabled = false;
		right_over_4_2.enabled = false;
		right_over_4_3.enabled = false;
		right_over_4_4.enabled = false;
		*/

		right_over_1_1.CrossFadeAlpha(0, 0.01f, false);
		right_over_1_2.CrossFadeAlpha(0, 0.01f, false);
		right_over_1_3.CrossFadeAlpha(0, 0.01f, false);
		right_over_1_4.CrossFadeAlpha(0, 0.01f, false);
		right_over_1_5.CrossFadeAlpha(0, 0.01f, false);
		right_over_1_6.CrossFadeAlpha(0, 0.01f, false);

		right_over_2_1.CrossFadeAlpha(0, 0.01f, false);
		right_over_2_2.CrossFadeAlpha(0, 0.01f, false);
		right_over_2_3.CrossFadeAlpha(0, 0.01f, false);
		right_over_2_4.CrossFadeAlpha(0, 0.01f, false);
		right_over_2_5.CrossFadeAlpha(0, 0.01f, false);
		right_over_2_6.CrossFadeAlpha(0, 0.01f, false);

		right_over_3_1.CrossFadeAlpha(0, 0.01f, false);
		right_over_3_2.CrossFadeAlpha(0, 0.01f, false);
		right_over_3_3.CrossFadeAlpha(0, 0.01f, false);
		right_over_3_4.CrossFadeAlpha(0, 0.01f, false);
		right_over_3_5.CrossFadeAlpha(0, 0.01f, false);
		right_over_3_6.CrossFadeAlpha(0, 0.01f, false);

		right_over_4_1.CrossFadeAlpha(0, 0.01f, false);
		right_over_4_2.CrossFadeAlpha(0, 0.01f, false);
		right_over_4_3.CrossFadeAlpha(0, 0.01f, false);
		right_over_4_4.CrossFadeAlpha(0, 0.01f, false);
		right_over_4_5.CrossFadeAlpha(0, 0.01f, false);
		right_over_4_6.CrossFadeAlpha(0, 0.01f, false);

		right_collider_1_1.enabled = false;
		right_collider_1_2.enabled = false;
		right_collider_1_3.enabled = false;
		right_collider_1_4.enabled = false;
		right_collider_1_5.enabled = false;
		right_collider_1_6.enabled = false;

		right_collider_2_1.enabled = false;
		right_collider_2_2.enabled = false;
		right_collider_2_3.enabled = false;
		right_collider_2_4.enabled = false;
		right_collider_2_5.enabled = false;
		right_collider_2_6.enabled = false;

		right_collider_3_1.enabled = false;
		right_collider_3_2.enabled = false;
		right_collider_3_3.enabled = false;
		right_collider_3_4.enabled = false;
		right_collider_3_5.enabled = false;
		right_collider_3_6.enabled = false;

		right_collider_4_1.enabled = false;
		right_collider_4_2.enabled = false;
		right_collider_4_3.enabled = false;
		right_collider_4_4.enabled = false;
		right_collider_4_5.enabled = false;
		right_collider_4_6.enabled = false;

	}

	private void BeginDrag_(PointerEventData data)
	{
		if(dragEnabled == true){
			objectDragged = true;
			startPosition = gameObject.transform.localPosition;
			startIndex = gameObject.GetComponent<RectTransform> ().GetSiblingIndex ();

			gameObject.GetComponent<RectTransform> ().SetAsLastSibling();

			_actualImage.sprite = _benchPosition;
			_actualImage.SetNativeSize ();

			if (_typeElement == typesDragElement.Input) {
				if (_typeInput == typesInputs.Left) {
					_leftColliders.transform.SetAsLastSibling ();
					activateLeftColliders ();
				} else {
					_rightColliders.transform.SetAsLastSibling ();
					activateRightColliders ();
				}

			} else {
				_rightColliders.transform.SetAsLastSibling ();
				activateRightColliders ();
			}

			_situation.GetComponent<PnematicTestBenchClass> ()._msgArea.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='" + textTag + "']").InnerText;
		}
	}

	private void ObjectDrag_(PointerEventData data)
	{
		if (objectDragged == true) {
			Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			gameObject.GetComponent<RectTransform>().position = new Vector3(mousePos.x, mousePos.y, 0);

			if(data.pointerEnter){
				switch(data.pointerEnter.name){
				case "over_1_1":
					if (_typeElement == typesDragElement.Input) {
						if (_typeInput == typesInputs.Left) {
							deactivateLeftOversWithException (1, 1);
							if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (1, 1)) {
								if(left_over_1_1 != null){
									left_over_1_1.CrossFadeAlpha (1, 0.2f, false);	
								}
							}
						} else {
							deactivateRightOversWithException (1, 1);
							if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 1)) {
								if (right_over_1_1 != null) {
									right_over_1_1.CrossFadeAlpha(1, 0.2f, false);
								}
							}
						}
					} else {
						deactivateRightOversWithException (1, 1);
						if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 1)) {
							if(right_over_1_1 != null){
								right_over_1_1.CrossFadeAlpha (1, 0.2f, false);
							}
						}
					}
					break;
				case "over_1_2":
					if (_typeElement == typesDragElement.Input) {
						if (_typeInput == typesInputs.Left) {
							deactivateLeftOversWithException (1, 2);
							if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (1, 2)) {
								if (left_over_1_2 != null) {
									left_over_1_2.CrossFadeAlpha (1, 0.2f, false);
								}
							}
						} else {
							deactivateRightOversWithException (1, 2);
							if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 2)) {
								if (right_over_1_2 != null) {
									right_over_1_2.CrossFadeAlpha(1, 0.2f, false);
								}
							}
						}
					} else {
						deactivateRightOversWithException (1, 2);
						if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 2)) {
							if (right_over_1_2 != null) {
								right_over_1_2.CrossFadeAlpha(1, 0.2f, false);
							}
						}
					}
					break;
				case "over_1_3":
					if (_typeElement == typesDragElement.Input) {
						if (_typeInput == typesInputs.Left) {
							deactivateLeftOversWithException (1, 3);
							if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (1, 3)) {
								if (left_over_1_3 != null) {
									left_over_1_3.CrossFadeAlpha (1, 0.2f, false);
								}
							}
						} else {
							deactivateRightOversWithException (1, 3);
							if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 3)) {
								if (right_over_1_3 != null) {
									right_over_1_3.CrossFadeAlpha(1, 0.2f, false);
								}
							}
						}
					} else {
						deactivateRightOversWithException (1, 3);
						if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 3)) {
							if (right_over_1_3 != null) {
								right_over_1_3.CrossFadeAlpha(1, 0.2f, false);
							}
						}
					}
					break;
				case "over_1_4":
					if (_typeElement == typesDragElement.Input) {
						if (_typeInput == typesInputs.Left) {
							deactivateLeftOversWithException (1, 4);
							if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (1, 4)) {
								if (left_over_1_4 != null) {
									left_over_1_4.CrossFadeAlpha (1, 0.2f, false);
								}
							}
						} else {
							deactivateRightOversWithException (1, 4);
							if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 4)) {
								if (right_over_1_4 != null) {
									right_over_1_4.CrossFadeAlpha(1, 0.2f, false);
								}
							}
						}
					} else {
						deactivateRightOversWithException (1, 4);
						if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 4)) {
							if (right_over_1_4 != null) {
								right_over_1_4.CrossFadeAlpha(1, 0.2f, false);
							}
						}
					}
					break;
				case "over_1_5":
					deactivateRightOversWithException (1, 5);
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 5)) {
						if (right_over_1_5 != null) {
							right_over_1_5.CrossFadeAlpha(1, 0.2f, false);
						}
					}
					break;
				case "over_1_6":
					deactivateRightOversWithException (1, 6);
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 6)) {
						if (right_over_1_6 != null) {
							right_over_1_6.CrossFadeAlpha(1, 0.2f, false);
						}
					}
					break;
				case "over_2_1":
					if (_typeElement == typesDragElement.Input) {
						if (_typeInput == typesInputs.Left) {
							deactivateLeftOversWithException (2, 1);
							if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (2, 1)) {
								if (left_over_2_1 != null) {
									left_over_2_1.CrossFadeAlpha (1, 0.2f, false);
								}
							}
						} else {
							deactivateRightOversWithException (2, 1);
							if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 1)) {
								if (right_over_2_1 != null) {
									right_over_2_1.CrossFadeAlpha(1, 0.2f, false);
								}
							}
						}
					} else {
						deactivateRightOversWithException (2, 1);
						if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 1) && right_over_2_1 != null) {
							right_over_2_1.CrossFadeAlpha(1, 0.2f, false);
						}
					}
					break;
				case "over_2_2":
					if (_typeElement == typesDragElement.Input) {
						if (_typeInput == typesInputs.Left) {
							deactivateLeftOversWithException (2, 2);
							if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (2, 2) && left_over_2_2 != null) {
								left_over_2_2.CrossFadeAlpha (1, 0.2f, false);
							}
						} else {
							deactivateRightOversWithException (2, 2);
							if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 2) && right_over_2_2 != null) {
								right_over_2_2.CrossFadeAlpha(1, 0.2f, false);
							}
						}
					} else {
						deactivateRightOversWithException (2, 2);
						if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 2) && right_over_2_2 != null) {
							right_over_2_2.CrossFadeAlpha(1, 0.2f, false);
						}
					}
					break;
				case "over_2_3":
					if (_typeElement == typesDragElement.Input) {
						if (_typeInput == typesInputs.Left) {
							deactivateLeftOversWithException (2, 3);
							if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (2, 3) && left_over_2_3 != null) {
								left_over_2_3.CrossFadeAlpha (1, 0.2f, false);
							}
						} else {
							deactivateRightOversWithException (2, 3);
							if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 3) && right_over_2_3 != null) {
								right_over_2_3.CrossFadeAlpha(1, 0.2f, false);
							}
						}
					} else {
						deactivateRightOversWithException (2, 3);
						if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 3) && right_over_2_3 != null) {
							right_over_2_3.CrossFadeAlpha(1, 0.2f, false);
						}
					}
					break;
				case "over_2_4":
					if (_typeElement == typesDragElement.Input) {
						if (_typeInput == typesInputs.Left) {
							deactivateLeftOversWithException (2, 4);
							if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (2, 4) && left_over_2_4 != null) {
								left_over_2_4.CrossFadeAlpha (1, 0.2f, false);
							}
						} else {
							deactivateRightOversWithException (2, 4);
							if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 4) && right_over_2_4 != null) {
								right_over_2_4.CrossFadeAlpha(1, 0.2f, false);
							}
						}
					} else {
						deactivateRightOversWithException (2, 4);
						if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 4) && right_over_2_4 != null) {
							right_over_2_4.CrossFadeAlpha(1, 0.2f, false);
						}
					}
					break;
				case "over_2_5":
					deactivateRightOversWithException (2, 5);
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 5) && right_over_2_5 != null) {
						right_over_2_5.CrossFadeAlpha(1, 0.2f, false);
					}
					break;
				case "over_2_6":
					deactivateRightOversWithException (2, 6);
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 6) && right_over_2_6 != null) {
						right_over_2_6.CrossFadeAlpha(1, 0.2f, false);
					}
					break;
				case "over_3_1":
					if (_typeElement == typesDragElement.Input) {
						if (_typeInput == typesInputs.Left) {
							deactivateLeftOversWithException (3, 1);
							if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (3, 1) && left_over_3_1 != null) {
								left_over_3_1.CrossFadeAlpha (1, 0.2f, false);
							}
						} else {
							deactivateRightOversWithException (3, 1);
							if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 1) && right_over_3_1 != null) {
								right_over_3_1.CrossFadeAlpha(1, 0.2f, false);
							}
						}
					} else {
						deactivateRightOversWithException (3, 1);
						if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 1) && right_over_3_1 != null) {
							right_over_3_1.CrossFadeAlpha(1, 0.2f, false);
						}
					}
					break;
				case "over_3_2":
					if (_typeElement == typesDragElement.Input) {
						if (_typeInput == typesInputs.Left) {
							deactivateLeftOversWithException (3, 2);
							if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (3, 2) && left_over_3_2 != null) {
								left_over_3_2.CrossFadeAlpha (1, 0.2f, false);
							}
						} else {
							deactivateRightOversWithException (3, 2);
							if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 2) && right_over_3_2 != null) {
								right_over_3_2.CrossFadeAlpha(1, 0.2f, false);
							}
						}
					} else {
						deactivateRightOversWithException (3, 2);
						if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 2) && right_over_3_2 != null) {
							right_over_3_2.CrossFadeAlpha(1, 0.2f, false);
						}
					}
					break;
				case "over_3_3":
					if (_typeElement == typesDragElement.Input) {
						if (_typeInput == typesInputs.Left) {
							deactivateLeftOversWithException (3, 3);
							if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (3, 3) && left_over_3_3 != null) {
								left_over_3_3.CrossFadeAlpha (1, 0.2f, false);
							}
						} else {
							deactivateRightOversWithException (3, 3);
							if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 3) && right_over_3_3 != null) {
								right_over_3_3.CrossFadeAlpha(1, 0.2f, false);
							}
						}
					} else {
						deactivateRightOversWithException (3, 3);
						if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 3) && right_over_3_3 != null) {
							right_over_3_3.CrossFadeAlpha(1, 0.2f, false);
						}
					}
					break;
				case "over_3_4":
					if (_typeElement == typesDragElement.Input) {
						if (_typeInput == typesInputs.Left) {
							deactivateLeftOversWithException (3, 4);
							if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (3, 4) && left_over_3_4 != null) {
								left_over_3_4.CrossFadeAlpha (1, 0.2f, false);
							}
						} else {
							deactivateRightOversWithException (3, 4);
							if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 4) && right_over_3_4 != null) {
								right_over_3_4.CrossFadeAlpha(1, 0.2f, false);
							}
						}
					} else {
						deactivateRightOversWithException (3, 4);
						if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 4) && right_over_3_4 != null) {
							right_over_3_4.CrossFadeAlpha(1, 0.2f, false);
						}
					}
					break;
				case "over_3_5":
					deactivateRightOversWithException (3, 5);
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 5) && right_over_3_5 != null) {
						right_over_3_5.CrossFadeAlpha(1, 0.2f, false);
					}
					break;
				case "over_3_6":
					deactivateRightOversWithException (3, 6);
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 6) && right_over_3_6 != null) {
						right_over_3_6.CrossFadeAlpha(1, 0.2f, false);
					}
					break;
				case "over_4_1":
					if (_typeElement == typesDragElement.Input) {
						if (_typeInput == typesInputs.Left) {
							deactivateLeftOversWithException (4, 1);
							if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (4, 1) && left_over_4_1 != null) {
								left_over_4_1.CrossFadeAlpha (1, 0.2f, false);
							}
						} else {
							deactivateRightOversWithException (4, 1);
							if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 1) && right_over_4_1 != null) {
								right_over_4_1.CrossFadeAlpha(1, 0.2f, false);
							}
						}
					} else {
						deactivateRightOversWithException (4, 1);
						if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 1) && right_over_4_1 != null) {
							right_over_4_1.CrossFadeAlpha(1, 0.2f, false);
						}
					}
					break;
				case "over_4_2":
					if (_typeElement == typesDragElement.Input) {
						if (_typeInput == typesInputs.Left) {
							deactivateLeftOversWithException (4, 2);
							if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (4, 2) && left_over_4_2 != null) {
								left_over_4_2.CrossFadeAlpha (1, 0.2f, false);
							}
						} else {
							deactivateRightOversWithException (4, 2);
							if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 2) && right_over_4_2 != null) {
								right_over_4_2.CrossFadeAlpha(1, 0.2f, false);
							}
						}
					} else {
						deactivateRightOversWithException (4, 2);
						if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 2) && right_over_4_2 != null) {
							right_over_4_2.CrossFadeAlpha(1, 0.2f, false);
						}
					}
					break;
				case "over_4_3":
					if (_typeElement == typesDragElement.Input) {
						if (_typeInput == typesInputs.Left) {
							deactivateLeftOversWithException (4, 3);
							if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (4, 3) && left_over_4_3 != null) {
								left_over_4_3.CrossFadeAlpha (1, 0.2f, false);
							}
						} else {
							deactivateRightOversWithException (4, 3);
							if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 3) && right_over_4_3 != null) {
								right_over_4_3.CrossFadeAlpha(1, 0.2f, false);
							}
						}
					} else {
						deactivateRightOversWithException (4, 3);
						if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 3) && right_over_4_3 != null) {
							right_over_4_3.CrossFadeAlpha(1, 0.2f, false);
						}
					}
					break;
				case "over_4_4":
					if (_typeElement == typesDragElement.Input) {
						if (_typeInput == typesInputs.Left) {
							deactivateLeftOversWithException (4, 4);
							if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (4, 4) && left_over_4_4 != null) {
								left_over_4_4.CrossFadeAlpha (1, 0.2f, false);
							}
						} else {
							deactivateRightOversWithException (4, 4);
							if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 4) && right_over_4_4 != null) {
								right_over_4_4.CrossFadeAlpha(1, 0.2f, false);
							}
						}
					} else {
						deactivateRightOversWithException (4, 4);
						if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 4) && right_over_4_4 != null) {
							right_over_4_4.CrossFadeAlpha(1, 0.2f, false);
						}
					}
					break;
				case "over_4_5":
					deactivateRightOversWithException (4, 5);
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 5) && right_over_4_5 != null) {
						right_over_4_5.CrossFadeAlpha(1, 0.2f, false);
					}
					break;
				case "over_4_6":
					deactivateRightOversWithException (4, 6);
					if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 6) && right_over_4_6 != null) {
						right_over_4_6.CrossFadeAlpha(1, 0.2f, false);
					}
					break;
				default:
					deactivateOvers ();
					break;	
				}
			}

		}
	}

	private void ObjectDrop_(PointerEventData data)
	{
		if (objectDragged == true) {
			objectDragged = false;

			_situation.GetComponent<PnematicTestBenchClass> ()._msgArea.text = "";

			deactivateOvers ();
			deactivateLeftColliders ();
			deactivateRightColliders ();

			bool sensorsActuatorsAvailable = true;

			if (_typeElement == typesDragElement.Input) {
				if (_situation.GetComponent<PnematicTestBenchClass> ().usedInputs >= (_situation.GetComponent<PnematicTestBenchClass> ().simulatorInputs)) {
					sensorsActuatorsAvailable = false;
				}
			} else {
				if (_situation.GetComponent<PnematicTestBenchClass> ().usedOutputs >= (_situation.GetComponent<PnematicTestBenchClass> ().simulatorOutputs)) {
					sensorsActuatorsAvailable = false;
				}
			}

			if (data.pointerEnter != null && sensorsActuatorsAvailable == true) {
				switch (data.pointerEnter.name) {
				case "over_1_1":
					if (_typeElement == typesDragElement.Input) {
						if (_typeInput == typesInputs.Left) {
							if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (1, 1)) {
								_leftOvers.GetComponent<benchAvailability> ().setAreaBusy (1, 1);
								gameObject.transform.position = left_over_1_1.transform.position;
								addNewElement ();
								dragEnabled = false;
								inBench = true;
								rowUsed = 1;
								columnUsed = 1;
								int _index = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (gameObject);
								_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
								setRedCable (rowUsed, columnUsed, _index);
								setBlackCable (rowUsed, columnUsed, _index);
								if (_grayCablePrefab) {
									setGrayCable (rowUsed, columnUsed, _index);	
								}
							} else {
								tweenToInitialPosition ();
							}
						} else {
							if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 1)) {
								_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (1, 1);
								gameObject.transform.position = right_over_1_1.transform.position;
								if(_typeComponent == typesComponents.inductive_sensor){
									gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x + 10f, gameObject.transform.localPosition.y, gameObject.transform.localPosition.z);
								}
								addNewElement ();
								dragEnabled = false;
								inBench = true;
								rowUsed = 1;
								columnUsed = 1;
								int _index = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (gameObject);
								_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
								setRedCable (rowUsed, columnUsed, _index, true);
								setBlackCable (rowUsed, columnUsed, _index, true);
								setInitialValue (rowUsed, columnUsed);
							} else {
								tweenToInitialPosition ();
							}
						}
					} else {
						if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 1)) {
							_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (1, 1);
							gameObject.transform.position = right_over_1_1.transform.position;
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 1;
							columnUsed = 1;
							int _index = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (gameObject);
							_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
							setBlueCable (rowUsed, columnUsed, _index);
							setWhiteCable (rowUsed, columnUsed, _index);
						} else {
							tweenToInitialPosition ();
						}
					}
					break;
				case "over_1_2":
					if (_typeElement == typesDragElement.Input) {
						if (_typeInput == typesInputs.Left) {
							if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (1, 2)) {
								_leftOvers.GetComponent<benchAvailability> ().setAreaBusy(1, 2);
								gameObject.transform.position = left_over_1_2.transform.position;
								addNewElement ();
								dragEnabled = false;
								inBench = true;
								rowUsed = 1;
								columnUsed = 2;
								int _index = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (gameObject);
								_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
								setRedCable (rowUsed, columnUsed, _index);
								setBlackCable (rowUsed, columnUsed, _index);
								if(_grayCablePrefab){
									setGrayCable(rowUsed, columnUsed, _index);	
								}
							} else {
								tweenToInitialPosition ();
							}
						} else {
							if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 2)) {
								_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (1, 2);
								gameObject.transform.position = right_over_1_2.transform.position;
								if(_typeComponent == typesComponents.inductive_sensor){
									gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x + 10f, gameObject.transform.localPosition.y, gameObject.transform.localPosition.z);
								}
								addNewElement ();
								dragEnabled = false;
								inBench = true;
								rowUsed = 1;
								columnUsed = 2;
								int _index = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (gameObject);
								_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
								setRedCable (rowUsed, columnUsed, _index, true);
								setBlackCable (rowUsed, columnUsed, _index, true);
								setInitialValue (rowUsed, columnUsed);
							} else {
								tweenToInitialPosition ();
							}
						}
					} else {
						if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 2)) {
							_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(1, 2);
							gameObject.transform.position = right_over_1_2.transform.position;
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 1;
							columnUsed = 2;
							int _index = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (gameObject);
							_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
							setBlueCable (rowUsed, columnUsed, _index);
							setWhiteCable (rowUsed, columnUsed, _index);
						} else {
							tweenToInitialPosition ();
						}
					}
					break;
				case "over_1_3":
					if (_typeElement == typesDragElement.Input) {
						if (_typeInput == typesInputs.Left) {
							if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (1, 3)) {
								_leftOvers.GetComponent<benchAvailability> ().setAreaBusy(1, 3);
								gameObject.transform.position = left_over_1_3.transform.position;
								addNewElement ();
								dragEnabled = false;
								inBench = true;
								rowUsed = 1;
								columnUsed = 3;
								int _index = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (gameObject);
								_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
								setRedCable (rowUsed, columnUsed, _index);
								setBlackCable (rowUsed, columnUsed, _index);
								if(_grayCablePrefab){
									setGrayCable(rowUsed, columnUsed, _index);	
								}
							} else {
								tweenToInitialPosition ();
							}
						} else {
							if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 3)) {
								_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (1, 3);
								gameObject.transform.position = right_over_1_3.transform.position;
								if(_typeComponent == typesComponents.inductive_sensor){
									gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x + 10f, gameObject.transform.localPosition.y, gameObject.transform.localPosition.z);
								}
								addNewElement ();
								dragEnabled = false;
								inBench = true;
								rowUsed = 1;
								columnUsed = 3;
								int _index = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (gameObject);
								_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
								setRedCable (rowUsed, columnUsed, _index, true);
								setBlackCable (rowUsed, columnUsed, _index, true);
								setInitialValue (rowUsed, columnUsed);
							} else {
								tweenToInitialPosition ();
							}
						}
					} else {
						if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 3)) {
							_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(1, 3);
							gameObject.transform.position = right_over_1_3.transform.position;
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 1;
							columnUsed = 3;
							int _index = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (gameObject);
							_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
							setBlueCable (rowUsed, columnUsed, _index);
							setWhiteCable (rowUsed, columnUsed, _index);
						} else {
							tweenToInitialPosition ();
						}
					}
					break;
				case "over_1_4":
					if (_typeElement == typesDragElement.Input) {
						if (_typeInput == typesInputs.Left) {
							if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (1, 4)) {
								_leftOvers.GetComponent<benchAvailability> ().setAreaBusy(1, 4);
								gameObject.transform.position = left_over_1_4.transform.position;
								addNewElement ();
								dragEnabled = false;
								inBench = true;
								rowUsed = 1;
								columnUsed = 4;
								int _index = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (gameObject);
								_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
								setRedCable (rowUsed, columnUsed, _index);
								setBlackCable (rowUsed, columnUsed, _index);
								if(_grayCablePrefab){
									setGrayCable(rowUsed, columnUsed, _index);	
								}
							} else {
								tweenToInitialPosition ();
							}
						} else {
							if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 4)) {
								_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (1, 4);
								gameObject.transform.position = right_over_1_4.transform.position;
								if(_typeComponent == typesComponents.inductive_sensor){
									gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x + 10f, gameObject.transform.localPosition.y, gameObject.transform.localPosition.z);
								}
								addNewElement ();
								dragEnabled = false;
								inBench = true;
								rowUsed = 1;
								columnUsed = 4;
								int _index = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (gameObject);
								_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
								setRedCable (rowUsed, columnUsed, _index, true);
								setBlackCable (rowUsed, columnUsed, _index, true);
								setInitialValue (rowUsed, columnUsed);
							} else {
								tweenToInitialPosition ();
							}
						}
					} else {
						if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 4)) {
							_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(1, 4);
							gameObject.transform.position = right_over_1_4.transform.position;
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 1;
							columnUsed = 4;
							int _index = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (gameObject);
							_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
							setBlueCable (rowUsed, columnUsed, _index);
							setWhiteCable (rowUsed, columnUsed, _index);
						} else {
							tweenToInitialPosition ();
						}
					}
					break;
				case "over_1_5":
					if (_typeElement == typesDragElement.Input) {
						if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 5)) {
							_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (1, 5);
							gameObject.transform.position = right_over_1_5.transform.position;
							if(_typeComponent == typesComponents.inductive_sensor){
								gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x + 10f, gameObject.transform.localPosition.y, gameObject.transform.localPosition.z);
							}
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 1;
							columnUsed = 5;
							int _index = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (gameObject);
							_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
							setRedCable (rowUsed, columnUsed, _index, true);
							setBlackCable (rowUsed, columnUsed, _index, true);
							setInitialValue (rowUsed, columnUsed);
						} else {
							tweenToInitialPosition ();
						}
					} else {
						if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 5)) {
							_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(1, 5);
							gameObject.transform.position = right_over_1_5.transform.position;
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 1;
							columnUsed = 5;
							int _index = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (gameObject);
							_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
							setBlueCable (rowUsed, columnUsed, _index);
							setWhiteCable (rowUsed, columnUsed, _index);
						} else {
							tweenToInitialPosition ();
						}
					}
					break;
				case "over_1_6":
					if (_typeElement == typesDragElement.Input) {
						if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 6)) {
							_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (1, 6);
							gameObject.transform.position = right_over_1_6.transform.position;
							if(_typeComponent == typesComponents.inductive_sensor){
								gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x + 10f, gameObject.transform.localPosition.y, gameObject.transform.localPosition.z);
							}
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 1;
							columnUsed = 6;
							int _index = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (gameObject);
							_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
							setRedCable (rowUsed, columnUsed, _index, true);
							setBlackCable (rowUsed, columnUsed, _index, true);
							setInitialValue (rowUsed, columnUsed);
						} else {
							tweenToInitialPosition ();
						}
					} else {
						if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (1, 6)) {
							_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(1, 6);
							gameObject.transform.position = right_over_1_6.transform.position;
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 1;
							columnUsed = 6;
							int _index = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (gameObject);
							_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
							setBlueCable (rowUsed, columnUsed, _index);
							setWhiteCable (rowUsed, columnUsed, _index);
						} else {
							tweenToInitialPosition ();
						}
					}
					break;
				case "over_2_1":
					if (_typeElement == typesDragElement.Input) {
						if (_typeInput == typesInputs.Left) {
							if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (2, 1)) {
								_leftOvers.GetComponent<benchAvailability> ().setAreaBusy(2, 1);
								gameObject.transform.position = left_over_2_1.transform.position;
								addNewElement ();
								dragEnabled = false;
								inBench = true;
								rowUsed = 2;
								columnUsed = 1;
								int _index = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (gameObject);
								_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
								setRedCable (rowUsed, columnUsed, _index);
								setBlackCable (rowUsed, columnUsed, _index);
								if(_grayCablePrefab){
									setGrayCable(rowUsed, columnUsed, _index);	
								}
							} else {
								tweenToInitialPosition ();
							}
						} else {
							if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 1)) {
								_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (2, 1);
								gameObject.transform.position = right_over_2_1.transform.position;
								if(_typeComponent == typesComponents.inductive_sensor){
									gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x + 10f, gameObject.transform.localPosition.y, gameObject.transform.localPosition.z);
								}
								addNewElement ();
								dragEnabled = false;
								inBench = true;
								rowUsed = 2;
								columnUsed = 1;
								int _index = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (gameObject);
								_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
								setRedCable (rowUsed, columnUsed, _index, true);
								setBlackCable (rowUsed, columnUsed, _index, true);
								setInitialValue (rowUsed, columnUsed);
							} else {
								tweenToInitialPosition ();
							}
						}
					} else {
						if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 1)) {
							_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(2, 1);
							gameObject.transform.position = right_over_2_1.transform.position;
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 2;
							columnUsed = 1;
							int _index = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (gameObject);
							_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
							setBlueCable (rowUsed, columnUsed, _index);
							setWhiteCable (rowUsed, columnUsed, _index);
						} else {
							tweenToInitialPosition ();
						}
					}
					break;
				case "over_2_2":
					if (_typeElement == typesDragElement.Input) {
						if (_typeInput == typesInputs.Left) {
							if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (2, 2)) {
								_leftOvers.GetComponent<benchAvailability> ().setAreaBusy(2, 2);
								gameObject.transform.position = left_over_2_2.transform.position;
								addNewElement ();
								dragEnabled = false;
								inBench = true;
								rowUsed = 2;
								columnUsed = 2;
								int _index = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (gameObject);
								_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
								setRedCable (rowUsed, columnUsed, _index);
								setBlackCable (rowUsed, columnUsed, _index);
								if(_grayCablePrefab){
									setGrayCable(rowUsed, columnUsed, _index);	
								}
							} else {
								tweenToInitialPosition ();
							}
						} else {
							if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 2)) {
								_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (2, 2);
								gameObject.transform.position = right_over_2_2.transform.position;
								if(_typeComponent == typesComponents.inductive_sensor){
									gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x + 10f, gameObject.transform.localPosition.y, gameObject.transform.localPosition.z);
								}
								addNewElement ();
								dragEnabled = false;
								inBench = true;
								rowUsed = 2;
								columnUsed = 2;
								int _index = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (gameObject);
								_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
								setRedCable (rowUsed, columnUsed, _index, true);
								setBlackCable (rowUsed, columnUsed, _index, true);
								setInitialValue (rowUsed, columnUsed);
							} else {
								tweenToInitialPosition ();
							}
						}
					} else {
						if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 2)) {
							_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(2, 2);
							gameObject.transform.position = right_over_2_2.transform.position;
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 2;
							columnUsed = 2;
							int _index = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (gameObject);
							_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
							setBlueCable (rowUsed, columnUsed, _index);
							setWhiteCable (rowUsed, columnUsed, _index);
						} else {
							tweenToInitialPosition ();
						}
					}
					break;
				case "over_2_3":
					if (_typeElement == typesDragElement.Input) {
						if (_typeInput == typesInputs.Left) {
							if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (2, 3)) {
								_leftOvers.GetComponent<benchAvailability> ().setAreaBusy(2, 3);
								gameObject.transform.position = left_over_2_3.transform.position;
								addNewElement ();
								dragEnabled = false;
								inBench = true;
								rowUsed = 2;
								columnUsed = 3;
								int _index = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (gameObject);
								_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
								setRedCable (rowUsed, columnUsed, _index);
								setBlackCable (rowUsed, columnUsed, _index);
								if(_grayCablePrefab){
									setGrayCable(rowUsed, columnUsed, _index);	
								}
							} else {
								tweenToInitialPosition ();
							}
						} else {
							if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 3)) {
								_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (2, 3);
								gameObject.transform.position = right_over_2_3.transform.position;
								if(_typeComponent == typesComponents.inductive_sensor){
									gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x + 10f, gameObject.transform.localPosition.y, gameObject.transform.localPosition.z);
								}
								addNewElement ();
								dragEnabled = false;
								inBench = true;
								rowUsed = 2;
								columnUsed = 3;
								int _index = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (gameObject);
								_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
								setRedCable (rowUsed, columnUsed, _index, true);
								setBlackCable (rowUsed, columnUsed, _index, true);
								setInitialValue (rowUsed, columnUsed);
							} else {
								tweenToInitialPosition ();
							}
						}
					} else {
						if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 3)) {
							_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(2, 3);
							gameObject.transform.position = right_over_2_3.transform.position;
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 2;
							columnUsed = 3;
							int _index = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (gameObject);
							_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
							setBlueCable (rowUsed, columnUsed, _index);
							setWhiteCable (rowUsed, columnUsed, _index);
						} else {
							tweenToInitialPosition ();
						}
					}
					break;
				case "over_2_4":
					if (_typeElement == typesDragElement.Input) {
						if (_typeInput == typesInputs.Left) {
							if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (2, 4)) {
								_leftOvers.GetComponent<benchAvailability> ().setAreaBusy(2, 4);
								gameObject.transform.position = left_over_2_4.transform.position;
								addNewElement ();
								dragEnabled = false;
								inBench = true;
								rowUsed = 2;
								columnUsed = 4;
								int _index = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (gameObject);
								_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
								setRedCable (rowUsed, columnUsed, _index);
								setBlackCable (rowUsed, columnUsed, _index);
								if(_grayCablePrefab){
									setGrayCable(rowUsed, columnUsed, _index);	
								}
							} else {
								tweenToInitialPosition ();
							}
						} else {
							if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 4)) {
								_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (2, 4);
								gameObject.transform.position = right_over_2_4.transform.position;
								if(_typeComponent == typesComponents.inductive_sensor){
									gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x + 10f, gameObject.transform.localPosition.y, gameObject.transform.localPosition.z);
								}
								addNewElement ();
								dragEnabled = false;
								inBench = true;
								rowUsed = 2;
								columnUsed = 4;
								int _index = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (gameObject);
								_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
								setRedCable (rowUsed, columnUsed, _index, true);
								setBlackCable (rowUsed, columnUsed, _index, true);
								setInitialValue (rowUsed, columnUsed);
							} else {
								tweenToInitialPosition ();
							}
						}
					} else {
						if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 4)) {
							_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(2, 4);
							gameObject.transform.position = right_over_2_4.transform.position;
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 2;
							columnUsed = 4;
							int _index = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (gameObject);
							_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
							setBlueCable (rowUsed, columnUsed, _index);
							setWhiteCable (rowUsed, columnUsed, _index);
						} else {
							tweenToInitialPosition ();
						}
					}
					break;
				case "over_2_5":
					if (_typeElement == typesDragElement.Input) {
						if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 5)) {
							_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (2, 5);
							gameObject.transform.position = right_over_2_5.transform.position;
							if(_typeComponent == typesComponents.inductive_sensor){
								gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x + 10f, gameObject.transform.localPosition.y, gameObject.transform.localPosition.z);
							}
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 2;
							columnUsed = 5;
							int _index = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (gameObject);
							_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
							setRedCable (rowUsed, columnUsed, _index, true);
							setBlackCable (rowUsed, columnUsed, _index, true);
							setInitialValue (rowUsed, columnUsed);
						} else {
							tweenToInitialPosition ();
						}
					} else {
						if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 5)) {
							_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(2, 5);
							gameObject.transform.position = right_over_2_5.transform.position;
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 2;
							columnUsed = 5;
							int _index = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (gameObject);
							_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
							setBlueCable (rowUsed, columnUsed, _index);
							setWhiteCable (rowUsed, columnUsed, _index);
						} else {
							tweenToInitialPosition ();
						}
					}
					break;
				case "over_2_6":
					if (_typeElement == typesDragElement.Input) {
						if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 6)) {
							_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (2, 6);
							gameObject.transform.position = right_over_2_6.transform.position;
							if(_typeComponent == typesComponents.inductive_sensor){
								gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x + 10f, gameObject.transform.localPosition.y, gameObject.transform.localPosition.z);
							}
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 2;
							columnUsed = 6;
							int _index = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (gameObject);
							_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
							setRedCable (rowUsed, columnUsed, _index, true);
							setBlackCable (rowUsed, columnUsed, _index, true);
							setInitialValue (rowUsed, columnUsed);
						} else {
							tweenToInitialPosition ();
						}
					} else {
						if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (2, 6)) {
							_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(2, 6);
							gameObject.transform.position = right_over_2_6.transform.position;
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 2;
							columnUsed = 6;
							int _index = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (gameObject);
							_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
							setBlueCable (rowUsed, columnUsed, _index);
							setWhiteCable (rowUsed, columnUsed, _index);
						} else {
							tweenToInitialPosition ();
						}
					}
					break;
				case "over_3_1":
					if (_typeElement == typesDragElement.Input) {
						if (_typeInput == typesInputs.Left) {
							if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (3, 1)) {
								_leftOvers.GetComponent<benchAvailability> ().setAreaBusy(3, 1);
								gameObject.transform.position = left_over_3_1.transform.position;
								addNewElement ();
								dragEnabled = false;
								inBench = true;
								rowUsed = 3;
								columnUsed = 1;
								int _index = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (gameObject);
								_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
								setRedCable (rowUsed, columnUsed, _index);
								setBlackCable (rowUsed, columnUsed, _index);
								if(_grayCablePrefab){
									setGrayCable(rowUsed, columnUsed, _index);	
								}
							} else {
								tweenToInitialPosition ();
							}
						} else {
							if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 1)) {
								_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (3, 1);
								gameObject.transform.position = right_over_3_1.transform.position;
								if(_typeComponent == typesComponents.inductive_sensor){
									gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x + 10f, gameObject.transform.localPosition.y, gameObject.transform.localPosition.z);
								}
								addNewElement ();
								dragEnabled = false;
								inBench = true;
								rowUsed = 3;
								columnUsed = 1;
								int _index = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (gameObject);
								_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
								setRedCable (rowUsed, columnUsed, _index, true);
								setBlackCable (rowUsed, columnUsed, _index, true);
								setInitialValue (rowUsed, columnUsed);
							} else {
								tweenToInitialPosition ();
							}
						}
					} else {
						if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 1)) {
							_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(3, 1);
							gameObject.transform.position = right_over_3_1.transform.position;
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 3;
							columnUsed = 1;
							int _index = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (gameObject);
							_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
							setBlueCable (rowUsed, columnUsed, _index);
							setWhiteCable (rowUsed, columnUsed, _index);
						} else {
							tweenToInitialPosition ();
						}
					}
					break;
				case "over_3_2":
					if (_typeElement == typesDragElement.Input) {
						if (_typeInput == typesInputs.Left) {
							if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (3, 2)) {
								_leftOvers.GetComponent<benchAvailability> ().setAreaBusy(3, 2);
								gameObject.transform.position = left_over_3_2.transform.position;
								addNewElement ();
								dragEnabled = false;
								inBench = true;
								rowUsed = 3;
								columnUsed = 2;
								int _index = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (gameObject);
								_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
								setRedCable (rowUsed, columnUsed, _index);
								setBlackCable (rowUsed, columnUsed, _index);
								if(_grayCablePrefab){
									setGrayCable(rowUsed, columnUsed, _index);	
								}
							} else {
								tweenToInitialPosition ();
							}
						} else {
							if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 2)) {
								_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (3, 2);
								gameObject.transform.position = right_over_3_2.transform.position;
								if(_typeComponent == typesComponents.inductive_sensor){
									gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x + 10f, gameObject.transform.localPosition.y, gameObject.transform.localPosition.z);
								}
								addNewElement ();
								dragEnabled = false;
								inBench = true;
								rowUsed = 3;
								columnUsed = 2;
								int _index = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (gameObject);
								_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
								setRedCable (rowUsed, columnUsed, _index, true);
								setBlackCable (rowUsed, columnUsed, _index, true);
								setInitialValue (rowUsed, columnUsed);
							} else {
								tweenToInitialPosition ();
							}
						}
					} else {
						if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 2)) {
							_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(3, 2);
							gameObject.transform.position = right_over_3_2.transform.position;
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 3;
							columnUsed = 2;
							int _index = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (gameObject);
							_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
							setBlueCable (rowUsed, columnUsed, _index);
							setWhiteCable (rowUsed, columnUsed, _index);
						} else {
							tweenToInitialPosition ();
						}
					}
					break;
				case "over_3_3":
					if (_typeElement == typesDragElement.Input) {
						if (_typeInput == typesInputs.Left) {
							if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (3, 3)) {
								_leftOvers.GetComponent<benchAvailability> ().setAreaBusy(3, 3);
								gameObject.transform.position = left_over_3_3.transform.position;
								addNewElement ();
								dragEnabled = false;
								inBench = true;
								rowUsed = 3;
								columnUsed = 3;
								int _index = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (gameObject);
								_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
								setRedCable (rowUsed, columnUsed, _index);
								setBlackCable (rowUsed, columnUsed, _index);
								if(_grayCablePrefab){
									setGrayCable(rowUsed, columnUsed, _index);	
								}
							} else {
								tweenToInitialPosition ();
							}
						} else {
							if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 3)) {
								_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (3, 3);
								gameObject.transform.position = right_over_3_3.transform.position;
								if(_typeComponent == typesComponents.inductive_sensor){
									gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x + 10f, gameObject.transform.localPosition.y, gameObject.transform.localPosition.z);
								}
								addNewElement ();
								dragEnabled = false;
								inBench = true;
								rowUsed = 3;
								columnUsed = 3;
								int _index = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (gameObject);
								_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
								setRedCable (rowUsed, columnUsed, _index, true);
								setBlackCable (rowUsed, columnUsed, _index, true);
								setInitialValue (rowUsed, columnUsed);
							} else {
								tweenToInitialPosition ();
							}
						}
					} else {
						if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 3)) {
							_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(3, 3);
							gameObject.transform.position = right_over_3_3.transform.position;
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 3;
							columnUsed = 3;
							int _index = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (gameObject);
							_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
							setBlueCable (rowUsed, columnUsed, _index);
							setWhiteCable (rowUsed, columnUsed, _index);
						} else {
							tweenToInitialPosition ();
						}
					}
					break;
				case "over_3_4":
					if (_typeElement == typesDragElement.Input) {
						if (_typeInput == typesInputs.Left) {
							if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (3, 4)) {
								_leftOvers.GetComponent<benchAvailability> ().setAreaBusy(3, 4);
								gameObject.transform.position = left_over_3_4.transform.position;
								addNewElement ();
								dragEnabled = false;
								inBench = true;
								rowUsed = 3;
								columnUsed = 4;
								int _index = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (gameObject);
								_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
								setRedCable (rowUsed, columnUsed, _index);
								setBlackCable (rowUsed, columnUsed, _index);
								if(_grayCablePrefab){
									setGrayCable(rowUsed, columnUsed, _index);	
								}
							} else {
								tweenToInitialPosition ();
							}
						} else {
							if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 4)) {
								_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (3, 4);
								gameObject.transform.position = right_over_3_4.transform.position;
								if(_typeComponent == typesComponents.inductive_sensor){
									gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x + 10f, gameObject.transform.localPosition.y, gameObject.transform.localPosition.z);
								}
								addNewElement ();
								dragEnabled = false;
								inBench = true;
								rowUsed = 3;
								columnUsed = 4;
								int _index = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (gameObject);
								_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
								setRedCable (rowUsed, columnUsed, _index, true);
								setBlackCable (rowUsed, columnUsed, _index, true);
								setInitialValue (rowUsed, columnUsed);
							} else {
								tweenToInitialPosition ();
							}
						}
					} else {
						if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 4)) {
							_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(3, 4);
							gameObject.transform.position = right_over_3_4.transform.position;
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 3;
							columnUsed = 4;
							int _index = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (gameObject);
							_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
							setBlueCable (rowUsed, columnUsed, _index);
							setWhiteCable (rowUsed, columnUsed, _index);
						} else {
							tweenToInitialPosition ();
						}
					}
					break;
				case "over_3_5":
					if (_typeElement == typesDragElement.Input) {
						if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 5)) {
							_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (3, 5);
							gameObject.transform.position = right_over_3_5.transform.position;
							if(_typeComponent == typesComponents.inductive_sensor){
								gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x + 10f, gameObject.transform.localPosition.y, gameObject.transform.localPosition.z);
							}
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 3;
							columnUsed = 5;
							int _index = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (gameObject);
							_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
							setRedCable (rowUsed, columnUsed, _index, true);
							setBlackCable (rowUsed, columnUsed, _index, true);
							setInitialValue (rowUsed, columnUsed);
						} else {
							tweenToInitialPosition ();
						}
					} else {
						if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 5)) {
							_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(3, 5);
							gameObject.transform.position = right_over_3_5.transform.position;
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 3;
							columnUsed = 5;
							int _index = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (gameObject);
							_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
							setBlueCable (rowUsed, columnUsed, _index);
							setWhiteCable (rowUsed, columnUsed, _index);
						} else {
							tweenToInitialPosition ();
						}
					}
					break;
				case "over_3_6":
					if (_typeElement == typesDragElement.Input) {
						if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 6)) {
							_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (3, 6);
							gameObject.transform.position = right_over_3_6.transform.position;
							if(_typeComponent == typesComponents.inductive_sensor){
								gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x + 10f, gameObject.transform.localPosition.y, gameObject.transform.localPosition.z);
							}
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 3;
							columnUsed = 6;
							int _index = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (gameObject);
							_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
							setRedCable (rowUsed, columnUsed, _index, true);
							setBlackCable (rowUsed, columnUsed, _index, true);
							setInitialValue (rowUsed, columnUsed);
						} else {
							tweenToInitialPosition ();
						}
					} else {
						if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (3, 6)) {
							_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (3, 6);
							gameObject.transform.position = right_over_3_6.transform.position;
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 3;
							columnUsed = 6;
							int _index = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (gameObject);
							_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
							setBlueCable (rowUsed, columnUsed, _index);
							setWhiteCable (rowUsed, columnUsed, _index);
						} else {
							tweenToInitialPosition ();
						}
					}
					break;
				case "over_4_1":
					if (_typeElement == typesDragElement.Input) {
						if (_typeInput == typesInputs.Left) {
							if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (4, 1)) {
								_leftOvers.GetComponent<benchAvailability> ().setAreaBusy(4, 1);
								gameObject.transform.position = left_over_4_1.transform.position;
								addNewElement ();
								dragEnabled = false;
								inBench = true;
								rowUsed = 4;
								columnUsed = 1;
								int _index = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (gameObject);
								_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
								setRedCable (rowUsed, columnUsed, _index);
								setBlackCable (rowUsed, columnUsed, _index);
								if(_grayCablePrefab){
									setGrayCable(rowUsed, columnUsed, _index);	
								}
							} else {
								tweenToInitialPosition ();
							}
						} else {
							if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 1)) {
								_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (4, 1);
								gameObject.transform.position = right_over_4_1.transform.position;
								if(_typeComponent == typesComponents.inductive_sensor){
									gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x + 10f, gameObject.transform.localPosition.y, gameObject.transform.localPosition.z);
								}
								addNewElement ();
								dragEnabled = false;
								inBench = true;
								rowUsed = 4;
								columnUsed = 1;
								int _index = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (gameObject);
								_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
								setRedCable (rowUsed, columnUsed, _index, true);
								setBlackCable (rowUsed, columnUsed, _index, true);
								setInitialValue (rowUsed, columnUsed);
							} else {
								tweenToInitialPosition ();
							}
						}
					} else {
						if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 1)) {
							_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(4, 1);
							gameObject.transform.position = right_over_4_1.transform.position;
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 4;
							columnUsed = 1;
							int _index = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (gameObject);
							_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
							setBlueCable (rowUsed, columnUsed, _index);
							setWhiteCable (rowUsed, columnUsed, _index);
						} else {
							tweenToInitialPosition ();
						}
					}
					break;
				case "over_4_2":
					if (_typeElement == typesDragElement.Input) {
						if (_typeInput == typesInputs.Left) {
							if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (4, 2)) {
								_leftOvers.GetComponent<benchAvailability> ().setAreaBusy(4, 2);
								gameObject.transform.position = left_over_4_2.transform.position;
								addNewElement ();
								dragEnabled = false;
								inBench = true;
								rowUsed = 4;
								columnUsed = 2;
								int _index = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (gameObject);
								_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
								setRedCable (rowUsed, columnUsed, _index);
								setBlackCable (rowUsed, columnUsed, _index);
								if(_grayCablePrefab){
									setGrayCable(rowUsed, columnUsed, _index);	
								}
							} else {
								tweenToInitialPosition ();
							}
						} else {
							if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 2)) {
								_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (4, 2);
								gameObject.transform.position = right_over_4_2.transform.position;
								if(_typeComponent == typesComponents.inductive_sensor){
									gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x + 10f, gameObject.transform.localPosition.y, gameObject.transform.localPosition.z);
								}
								addNewElement ();
								dragEnabled = false;
								inBench = true;
								rowUsed = 4;
								columnUsed = 2;
								int _index = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (gameObject);
								_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
								setRedCable (rowUsed, columnUsed, _index, true);
								setBlackCable (rowUsed, columnUsed, _index, true);
								setInitialValue (rowUsed, columnUsed);
							} else {
								tweenToInitialPosition ();
							}
						}
					} else {
						if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 2)) {
							_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(4, 2);
							gameObject.transform.position = right_over_4_2.transform.position;
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 4;
							columnUsed = 2;
							int _index = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (gameObject);
							_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
							setBlueCable (rowUsed, columnUsed, _index);
							setWhiteCable (rowUsed, columnUsed, _index);
						} else {
							tweenToInitialPosition ();
						}
					}
					break;
				case "over_4_3":
					if (_typeElement == typesDragElement.Input) {
						if (_typeInput == typesInputs.Left) {
							if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (4, 3)) {
								_leftOvers.GetComponent<benchAvailability> ().setAreaBusy(4, 3);
								gameObject.transform.position = left_over_4_3.transform.position;
								addNewElement ();
								dragEnabled = false;
								inBench = true;
								rowUsed = 4;
								columnUsed = 3;
								int _index = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (gameObject);
								_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
								setRedCable (rowUsed, columnUsed, _index);
								setBlackCable (rowUsed, columnUsed, _index);
								if(_grayCablePrefab){
									setGrayCable(rowUsed, columnUsed, _index);	
								}
							} else {
								tweenToInitialPosition ();
							}
						} else {
							if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 3)) {
								_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (4, 3);
								gameObject.transform.position = right_over_4_3.transform.position;
								if(_typeComponent == typesComponents.inductive_sensor){
									gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x + 10f, gameObject.transform.localPosition.y, gameObject.transform.localPosition.z);
								}
								addNewElement ();
								dragEnabled = false;
								inBench = true;
								rowUsed = 4;
								columnUsed = 3;
								int _index = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (gameObject);
								_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
								setRedCable (rowUsed, columnUsed, _index, true);
								setBlackCable (rowUsed, columnUsed, _index, true);
								setInitialValue (rowUsed, columnUsed);
							} else {
								tweenToInitialPosition ();
							}
						}
					} else {
						if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 3)) {
							_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(4, 3);
							gameObject.transform.position = right_over_4_3.transform.position;
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 4;
							columnUsed = 3;
							int _index = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (gameObject);
							_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
							setBlueCable (rowUsed, columnUsed, _index);
							setWhiteCable (rowUsed, columnUsed, _index);
						} else {
							tweenToInitialPosition ();
						}
					}
					break;
				case "over_4_4":
					if (_typeElement == typesDragElement.Input) {
						if (_typeInput == typesInputs.Left) {
							if (_leftOvers.GetComponent<benchAvailability> ().areaIsAvailable (4, 4)) {
								_leftOvers.GetComponent<benchAvailability> ().setAreaBusy(4, 4);
								gameObject.transform.position = left_over_4_4.transform.position;
								addNewElement ();
								dragEnabled = false;
								inBench = true;
								rowUsed = 4;
								columnUsed = 4;
								int _index = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (gameObject);
								_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
								setRedCable (rowUsed, columnUsed, _index);
								setBlackCable (rowUsed, columnUsed, _index);
								if(_grayCablePrefab){
									setGrayCable(rowUsed, columnUsed, _index);	
								}
							} else {
								tweenToInitialPosition ();
							}
						} else {
							if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 4)) {
								_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (4, 4);
								gameObject.transform.position = right_over_4_4.transform.position;
								if(_typeComponent == typesComponents.inductive_sensor){
									gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x + 10f, gameObject.transform.localPosition.y, gameObject.transform.localPosition.z);
								}
								addNewElement ();
								dragEnabled = false;
								inBench = true;
								rowUsed = 4;
								columnUsed = 4;
								int _index = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (gameObject);
								_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
								setRedCable (rowUsed, columnUsed, _index, true);
								setBlackCable (rowUsed, columnUsed, _index, true);
								setInitialValue (rowUsed, columnUsed);
							} else {
								tweenToInitialPosition ();
							}
						}
					} else {
						if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 4)) {
							_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(4, 4);
							gameObject.transform.position = right_over_4_4.transform.position;
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 4;
							columnUsed = 4;
							int _index = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (gameObject);
							_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
							setBlueCable (rowUsed, columnUsed, _index);
							setWhiteCable (rowUsed, columnUsed, _index);
						} else {
							tweenToInitialPosition ();
						}
					}
					break;
				case "over_4_5":
					if (_typeElement == typesDragElement.Input) {
						if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 5)) {
							_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (4, 5);
							gameObject.transform.position = right_over_4_5.transform.position;
							if(_typeComponent == typesComponents.inductive_sensor){
								gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x + 10f, gameObject.transform.localPosition.y, gameObject.transform.localPosition.z);
							}
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 4;
							columnUsed = 5;
							int _index = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (gameObject);
							_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
							setRedCable (rowUsed, columnUsed, _index, true);
							setBlackCable (rowUsed, columnUsed, _index, true);
							setInitialValue (rowUsed, columnUsed);
						} else {
							tweenToInitialPosition ();
						}
					} else {
						if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 5)) {
							_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy(4, 5);
							gameObject.transform.position = right_over_4_5.transform.position;
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 4;
							columnUsed = 5;
							int _index = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (gameObject);
							_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
							setBlueCable (rowUsed, columnUsed, _index);
							setWhiteCable (rowUsed, columnUsed, _index);
						} else {
							tweenToInitialPosition ();
						}
					}
					break;
				case "over_4_6":
					if (_typeElement == typesDragElement.Input) {
						if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 6)) {
							_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (4, 6);
							gameObject.transform.position = right_over_4_6.transform.position;
							if(_typeComponent == typesComponents.inductive_sensor){
								gameObject.transform.localPosition = new Vector3 (gameObject.transform.localPosition.x + 10f, gameObject.transform.localPosition.y, gameObject.transform.localPosition.z);
							}
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 4;
							columnUsed = 6;
							int _index = _situation.GetComponent<PnematicTestBenchClass> ().addInputElement (gameObject);
							_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
							setRedCable (rowUsed, columnUsed, _index, true);
							setBlackCable (rowUsed, columnUsed, _index, true);
							setInitialValue (rowUsed, columnUsed);
						} else {
							tweenToInitialPosition ();
						}
					} else {
						if (_rightOvers.GetComponent<benchAvailabilityPneumatic> ().areaIsAvailable (4, 6)) {
							_rightOvers.GetComponent<benchAvailabilityPneumatic> ().setAreaBusy (4, 6);
							gameObject.transform.position = right_over_4_6.transform.position;
							addNewElement ();
							dragEnabled = false;
							inBench = true;
							rowUsed = 4;
							columnUsed = 6;
							int _index = _situation.GetComponent<PnematicTestBenchClass> ().addOutputElement (gameObject);
							_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setOutputValueIndicators ();
							setBlueCable (rowUsed, columnUsed, _index);
							setWhiteCable (rowUsed, columnUsed, _index);
						} else {
							tweenToInitialPosition ();
						}
					}
					break;
				default:
					tweenToInitialPosition ();
					break;	
				}
			} else {
				tweenToInitialPosition ();
			}
		}
	}

	private void ObjectMouseDown_(PointerEventData data){
		if(inBench == true && _typeElement == typesDragElement.Input){
			if (isSwitch == true) {
				if (actualValue == normalValue) {

					actualValue = pressedValue;

					if( gameObject.GetComponent<AudioSource> () ){
						if (_downAudio) {
							gameObject.GetComponent<AudioSource> ().clip = _downAudio;
							gameObject.GetComponent<AudioSource> ().Play ();
						}
					}

					if(_pressedImage){
						_pressedImage.enabled = true;
						if(hideBaseImageOnActive == true){
							_actualImage.enabled = false;
						}
					}

					_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
				} else {
					actualValue = normalValue;

					if( gameObject.GetComponent<AudioSource> () ){
						if (_upAudio) {
							gameObject.GetComponent<AudioSource> ().clip = _upAudio;
							gameObject.GetComponent<AudioSource> ().Play ();
						}
					}

					if(_pressedImage){
						_pressedImage.enabled = false;
						if(hideBaseImageOnActive == true){
							_actualImage.enabled = true;
						}
					}

					_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
				}
			} else {
				actualValue = pressedValue;

				if( gameObject.GetComponent<AudioSource> () ){
					if (_downAudio) {
						gameObject.GetComponent<AudioSource> ().clip = _downAudio;
						gameObject.GetComponent<AudioSource> ().Play ();
					}
				}

				if(_pressedImage){
					_pressedImage.enabled = true;
					if(hideBaseImageOnActive == true){
						_actualImage.enabled = false;
					}
				}

				_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
			}
		}
	}

	private void ObjectMouseUp_(PointerEventData data){
		if(inBench == true && _typeElement == typesDragElement.Input){
			if (isSwitch == false) {

				actualValue = normalValue;

				if( gameObject.GetComponent<AudioSource> () ){
					if (_upAudio) {
						gameObject.GetComponent<AudioSource> ().clip = _upAudio;
						gameObject.GetComponent<AudioSource> ().Play ();
					}
				}

				if(_pressedImage){
					_pressedImage.enabled = false;
					if(hideBaseImageOnActive == true){
						_actualImage.enabled = true;
					}
				}

				_situation.GetComponent<PnematicTestBenchClass> ()._upperBars.GetComponent<upperBars> ().setInputValueIndicators ();
			}
		}
	}

	private void ObjectMouseOver_(PointerEventData data){
		if(objectDragged == false){
			_mouseOver = true;
			_situation.GetComponent<PnematicTestBenchClass> ()._msgArea.text = Manager.Instance.globalTexts.SelectSingleNode ("/data/element[@title='" + textTag + "']").InnerText;	
		}
	}

	private void ObjectMouseOut_(PointerEventData data){
		if(_mouseOver == true){
			_mouseOver = false;
			_situation.GetComponent<PnematicTestBenchClass> ()._msgArea.text = "";
		}
	}

	private void tweenToInitialPosition(){
		iTween.MoveTo(
			gameObject,
			iTween.Hash(
				"position", initialPosition,
				"looktarget", Camera.main,
				"easeType", iTween.EaseType.easeOutExpo,
				"time", 0.2f,
				"islocal",true
			)
		);

		Invoke ("goToInitialState", 0.1f);
	}

	private void goToInitialState(){
		_actualImage.sprite = _normalPosition;
		_actualImage.SetNativeSize ();
		gameObject.GetComponent<RectTransform> ().SetSiblingIndex (initialIndex);
	}

	private void activateLeftColliders(){
		left_collider_1_1.enabled = true;
		left_collider_1_2.enabled = true;

		left_collider_2_1.enabled = true;
		left_collider_2_2.enabled = true;

		left_collider_3_1.enabled = true;
		left_collider_3_2.enabled = true;

		left_collider_4_1.enabled = true;
		left_collider_4_2.enabled = true;
	}

	private void deactivateLeftColliders(){
		left_collider_1_1.enabled = false;
		left_collider_1_2.enabled = false;

		left_collider_2_1.enabled = false;
		left_collider_2_2.enabled = false;

		left_collider_3_1.enabled = false;
		left_collider_3_2.enabled = false;

		left_collider_4_1.enabled = false;
		left_collider_4_2.enabled = false;
	}

	private void activateRightColliders(){
		right_collider_1_1.enabled = true;
		right_collider_1_2.enabled = true;
		right_collider_1_3.enabled = true;
		right_collider_1_4.enabled = true;
		right_collider_1_5.enabled = true;
		right_collider_1_6.enabled = true;

		right_collider_2_1.enabled = true;
		right_collider_2_2.enabled = true;
		right_collider_2_3.enabled = true;
		right_collider_2_4.enabled = true;
		right_collider_2_5.enabled = true;
		right_collider_2_6.enabled = true;

		right_collider_3_1.enabled = true;
		right_collider_3_2.enabled = true;
		right_collider_3_3.enabled = true;
		right_collider_3_4.enabled = true;
		right_collider_3_5.enabled = true;
		right_collider_3_6.enabled = true;

		right_collider_4_1.enabled = true;
		right_collider_4_2.enabled = true;
		right_collider_4_3.enabled = true;
		right_collider_4_4.enabled = true;
		right_collider_4_5.enabled = true;
		right_collider_4_6.enabled = true;
	}

	private void deactivateRightColliders(){
		right_collider_1_1.enabled = false;
		right_collider_1_2.enabled = false;
		right_collider_1_3.enabled = false;
		right_collider_1_4.enabled = false;
		right_collider_1_5.enabled = false;
		right_collider_1_6.enabled = false;

		right_collider_2_1.enabled = false;
		right_collider_2_2.enabled = false;
		right_collider_2_3.enabled = false;
		right_collider_2_4.enabled = false;
		right_collider_2_5.enabled = false;
		right_collider_2_6.enabled = false;

		right_collider_3_1.enabled = false;
		right_collider_3_2.enabled = false;
		right_collider_3_3.enabled = false;
		right_collider_3_4.enabled = false;
		right_collider_3_5.enabled = false;
		right_collider_3_6.enabled = false;

		right_collider_4_1.enabled = false;
		right_collider_4_2.enabled = false;
		right_collider_4_3.enabled = false;
		right_collider_4_4.enabled = false;
		right_collider_4_5.enabled = false;
		right_collider_4_6.enabled = false;
	}

	private void deactivateOvers(){

		left_over_1_1.CrossFadeAlpha(0, 0.2f, false);
		left_over_1_2.CrossFadeAlpha(0, 0.2f, false);

		left_over_2_1.CrossFadeAlpha(0, 0.2f, false);
		left_over_2_2.CrossFadeAlpha(0, 0.2f, false);

		left_over_3_1.CrossFadeAlpha(0, 0.2f, false);
		left_over_3_2.CrossFadeAlpha(0, 0.2f, false);

		left_over_4_1.CrossFadeAlpha(0, 0.2f, false);
		left_over_4_2.CrossFadeAlpha(0, 0.2f, false);

		right_over_1_1.CrossFadeAlpha(0, 0.2f, false);
		right_over_1_2.CrossFadeAlpha(0, 0.2f, false);
		right_over_1_3.CrossFadeAlpha(0, 0.2f, false);
		right_over_1_4.CrossFadeAlpha(0, 0.2f, false);
		right_over_1_5.CrossFadeAlpha(0, 0.2f, false);
		right_over_1_6.CrossFadeAlpha(0, 0.2f, false);

		right_over_2_1.CrossFadeAlpha(0, 0.2f, false);
		right_over_2_2.CrossFadeAlpha(0, 0.2f, false);
		right_over_2_3.CrossFadeAlpha(0, 0.2f, false);
		right_over_2_4.CrossFadeAlpha(0, 0.2f, false);
		right_over_2_5.CrossFadeAlpha(0, 0.2f, false);
		right_over_2_6.CrossFadeAlpha(0, 0.2f, false);

		right_over_3_1.CrossFadeAlpha(0, 0.2f, false);
		right_over_3_2.CrossFadeAlpha(0, 0.2f, false);
		right_over_3_3.CrossFadeAlpha(0, 0.2f, false);
		right_over_3_4.CrossFadeAlpha(0, 0.2f, false);
		right_over_3_5.CrossFadeAlpha(0, 0.2f, false);
		right_over_3_6.CrossFadeAlpha(0, 0.2f, false);

		right_over_4_1.CrossFadeAlpha(0, 0.2f, false);
		right_over_4_2.CrossFadeAlpha(0, 0.2f, false);
		right_over_4_3.CrossFadeAlpha(0, 0.2f, false);
		right_over_4_4.CrossFadeAlpha(0, 0.2f, false);
		right_over_4_5.CrossFadeAlpha(0, 0.2f, false);
		right_over_4_6.CrossFadeAlpha(0, 0.2f, false);
	}

	public void activateLeftOver(int _row = 0, int _column = 0, bool _hideAutomatically = true){

		bool feedbackActivated = true;

		if(_row == 0 && _column == 0){
			feedbackActivated = false;
			_row = rowUsed;
			_column = columnUsed;
		}

		switch(_row){
		case 1:
			switch(_column){
			case 1:
				left_over_1_1.CrossFadeAlpha(1, 0.2f, false);
				break;
			case 2:
				left_over_1_2.CrossFadeAlpha(1, 0.2f, false);
				break;
			case 3:
				left_over_1_3.CrossFadeAlpha(1, 0.2f, false);
				break;
			case 4:
				left_over_1_4.CrossFadeAlpha(1, 0.2f, false);
				break;
			}
			break;
		case 2:
			switch(_column){
			case 1:
				left_over_2_1.CrossFadeAlpha(1, 0.2f, false);
				break;
			case 2:
				left_over_2_2.CrossFadeAlpha(1, 0.2f, false);
				break;
			case 3:
				left_over_2_3.CrossFadeAlpha(1, 0.2f, false);
				break;
			case 4:
				left_over_2_4.CrossFadeAlpha(1, 0.2f, false);
				break;
			}
			break;
		case 3:
			switch(_column){
			case 1:
				left_over_3_1.CrossFadeAlpha(1, 0.2f, false);
				break;
			case 2:
				left_over_3_2.CrossFadeAlpha(1, 0.2f, false);
				break;
			case 3:
				left_over_3_3.CrossFadeAlpha(1, 0.2f, false);
				break;
			case 4:
				left_over_3_4.CrossFadeAlpha(1, 0.2f, false);
				break;
			}
			break;
		case 4:
			switch(_column){
			case 1:
				left_over_4_1.CrossFadeAlpha(1, 0.2f, false);
				break;
			case 2:
				left_over_4_2.CrossFadeAlpha(1, 0.2f, false);
				break;
			case 3:
				left_over_4_3.CrossFadeAlpha(1, 0.2f, false);
				break;
			case 4:
				left_over_4_4.CrossFadeAlpha(1, 0.2f, false);
				break;
			}
			break;
		}

		if(_hideAutomatically == true){
			Invoke ("deactivateOvers", 0.3f);
			if(feedbackActivated == true){
				Invoke ("PostactivateLeftOver", 0.7f);	
			}
		}
	}

	private void PostactivateLeftOver(){
		activateLeftOver ();
	}

	public void activateRightOver(int _row = 0, int _column = 0, bool _hideAutomatically = true){
		bool feedbackActivated = true;

		if(_row == 0 && _column == 0){
			feedbackActivated = false;
			_row = rowUsed;
			_column = columnUsed;
		}

		switch(_row){
		case 1:
			switch(_column){
			case 1:
				right_over_1_1.CrossFadeAlpha(1, 0.2f, false);
				break;
			case 2:
				right_over_1_2.CrossFadeAlpha(1, 0.2f, false);
				break;
			case 3:
				right_over_1_3.CrossFadeAlpha(1, 0.2f, false);
				break;
			case 4:
				right_over_1_4.CrossFadeAlpha(1, 0.2f, false);
				break;
			case 5:
				right_over_1_5.CrossFadeAlpha(1, 0.2f, false);
				break;
			case 6:
				right_over_1_6.CrossFadeAlpha(1, 0.2f, false);
				break;
			}
			break;
		case 2:
			switch(_column){
			case 1:
				right_over_2_1.CrossFadeAlpha(1, 0.2f, false);
				break;
			case 2:
				right_over_2_2.CrossFadeAlpha(1, 0.2f, false);
				break;
			case 3:
				right_over_2_3.CrossFadeAlpha(1, 0.2f, false);
				break;
			case 4:
				right_over_2_4.CrossFadeAlpha(1, 0.2f, false);
				break;
			case 5:
				right_over_2_5.CrossFadeAlpha(1, 0.2f, false);
				break;
			case 6:
				right_over_2_6.CrossFadeAlpha(1, 0.2f, false);
				break;
			}
			break;
		case 3:
			switch(_column){
			case 1:
				right_over_3_1.CrossFadeAlpha(1, 0.2f, false);
				break;
			case 2:
				right_over_3_2.CrossFadeAlpha(1, 0.2f, false);
				break;
			case 3:
				right_over_3_3.CrossFadeAlpha(1, 0.2f, false);
				break;
			case 4:
				right_over_3_4.CrossFadeAlpha(1, 0.2f, false);
				break;
			case 5:
				right_over_3_5.CrossFadeAlpha(1, 0.2f, false);
				break;
			case 6:
				right_over_3_6.CrossFadeAlpha(1, 0.2f, false);
				break;
			}
			break;
		case 4:
			switch(_column){
			case 1:
				right_over_4_1.CrossFadeAlpha(1, 0.2f, false);
				break;
			case 2:
				right_over_4_2.CrossFadeAlpha(1, 0.2f, false);
				break;
			case 3:
				right_over_4_3.CrossFadeAlpha(1, 0.2f, false);
				break;
			case 4:
				right_over_4_4.CrossFadeAlpha(1, 0.2f, false);
				break;
			case 5:
				right_over_4_5.CrossFadeAlpha(1, 0.2f, false);
				break;
			case 6:
				right_over_4_6.CrossFadeAlpha(1, 0.2f, false);
				break;
			}
			break;
		}

		if(_hideAutomatically == true){
			Invoke ("deactivateOvers", 0.3f);
			if(feedbackActivated == true){
				Invoke ("PostactivateRightOver", 0.7f);	
			}
		}
	}

	private void PostactivateRightOver(){
		activateRightOver ();
	}

	public void setValue(int _value){
		if(actualValue != _value){
			actualValue = _value;
			if (actualValue == 1) {
				if( gameObject.GetComponent<AudioSource> () ){
					if (_downAudio) {
						gameObject.GetComponent<AudioSource> ().clip = _downAudio;
						if(gameObject.GetComponent<AudioSource> ().isPlaying == false){
							gameObject.GetComponent<AudioSource> ().Play ();
						}
					}
				}
				if(_pressedImage){
					_pressedImage.enabled = true;
					if(hideBaseImageOnActive == true){
						_actualImage.enabled = false;
					}
				}

			} else {
				if( gameObject.GetComponent<AudioSource> () ){
					if (_downAudio) {
						gameObject.GetComponent<AudioSource> ().Stop ();
					}
				}
				if(_pressedImage){
					_pressedImage.enabled = false;
					if(hideBaseImageOnActive == true){
						_actualImage.enabled = true;
					}
				}
			}	
		}
	}

	private void deactivateLeftOversWithException(int _row, int _column){
		if(!(_row == 1 && _column == 1)){
			left_over_1_1.CrossFadeAlpha(0, 0.2f, false);
		}
		if(!(_row == 1 && _column == 2)){
			left_over_1_2.CrossFadeAlpha(0, 0.2f, false);
		}

		if(!(_row == 2 && _column == 1)){
			left_over_2_1.CrossFadeAlpha(0, 0.2f, false);
		}
		if(!(_row == 2 && _column == 2)){
			left_over_2_2.CrossFadeAlpha(0, 0.2f, false);
		}

		if(!(_row == 3 && _column == 1)){
			left_over_3_1.CrossFadeAlpha(0, 0.2f, false);
		}
		if(!(_row == 3 && _column == 2)){
			left_over_3_2.CrossFadeAlpha(0, 0.2f, false);
		}

		if(!(_row == 4 && _column == 1)){
			left_over_4_1.CrossFadeAlpha(0, 0.2f, false);
		}
		if(!(_row == 4 && _column == 2)){
			left_over_4_2.CrossFadeAlpha(0, 0.2f, false);
		}
	}

	private void deactivateRightOversWithException(int _row, int _column){
		if(!(_row == 1 && _column == 1)){
			right_over_1_1.CrossFadeAlpha(0, 0.2f, false);
		}
		if(!(_row == 1 && _column == 2)){
			right_over_1_2.CrossFadeAlpha(0, 0.2f, false);
		}
		if(!(_row == 1 && _column == 3)){
			right_over_1_3.CrossFadeAlpha(0, 0.2f, false);
		}
		if(!(_row == 1 && _column == 4)){
			right_over_1_4.CrossFadeAlpha(0, 0.2f, false);
		}
		if(!(_row == 1 && _column == 5)){
			right_over_1_5.CrossFadeAlpha(0, 0.2f, false);
		}
		if(!(_row == 1 && _column == 6)){
			right_over_1_6.CrossFadeAlpha(0, 0.2f, false);
		}

		if(!(_row == 2 && _column == 1)){
			right_over_2_1.CrossFadeAlpha(0, 0.2f, false);
		}
		if(!(_row == 2 && _column == 2)){
			right_over_2_2.CrossFadeAlpha(0, 0.2f, false);
		}
		if(!(_row == 2 && _column == 3)){
			right_over_2_3.CrossFadeAlpha(0, 0.2f, false);
		}
		if(!(_row == 2 && _column == 4)){
			right_over_2_4.CrossFadeAlpha(0, 0.2f, false);
		}
		if(!(_row == 2 && _column == 5)){
			right_over_2_5.CrossFadeAlpha(0, 0.2f, false);
		}
		if(!(_row == 2 && _column == 6)){
			right_over_2_6.CrossFadeAlpha(0, 0.2f, false);
		}

		if(!(_row == 3 && _column == 1)){
			right_over_3_1.CrossFadeAlpha(0, 0.2f, false);
		}
		if(!(_row == 3 && _column == 2)){
			right_over_3_2.CrossFadeAlpha(0, 0.2f, false);
		}
		if(!(_row == 3 && _column == 3)){
			right_over_3_3.CrossFadeAlpha(0, 0.2f, false);
		}
		if(!(_row == 3 && _column == 4)){
			right_over_3_4.CrossFadeAlpha(0, 0.2f, false);
		}
		if(!(_row == 3 && _column == 5)){
			right_over_3_5.CrossFadeAlpha(0, 0.2f, false);
		}
		if(!(_row == 3 && _column == 6)){
			right_over_3_6.CrossFadeAlpha(0, 0.2f, false);
		}

		if(!(_row == 4 && _column == 1)){
			right_over_4_1.CrossFadeAlpha(0, 0.2f, false);
		}
		if(!(_row == 4 && _column == 2)){
			right_over_4_2.CrossFadeAlpha(0, 0.2f, false);
		}
		if(!(_row == 4 && _column == 3)){
			right_over_4_3.CrossFadeAlpha(0, 0.2f, false);
		}
		if(!(_row == 4 && _column == 4)){
			right_over_4_4.CrossFadeAlpha(0, 0.2f, false);
		}
		if(!(_row == 4 && _column == 5)){
			right_over_4_5.CrossFadeAlpha(0, 0.2f, false);
		}
		if(!(_row == 4 && _column == 6)){
			right_over_4_6.CrossFadeAlpha(0, 0.2f, false);
		}
	}

	private void addNewElement (){
		GameObject newElement = Instantiate (gameObject, initialPosition, Quaternion.identity) as GameObject;
		newElement.transform.SetParent (GameObject.Find ("Canvas/PneumaticBench").transform, false);
		newElement.transform.SetSiblingIndex (initialIndex);
		newElement.GetComponent<dragElementPneumatic> ()._actualImage.sprite = newElement.GetComponent<dragElementPneumatic> ()._normalPosition;
		newElement.GetComponent<dragElementPneumatic> ()._actualImage.SetNativeSize ();
	}

	private void setRedCable (int _row, int _column, int _input, bool _isRight = false){
		_rightCable = Instantiate(_redCablePrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
		_rightCable.transform.SetParent(GameObject.Find("Canvas/PneumaticBench").transform, false);
		_rightCable.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex () + 1);
		_rightCable.GetComponent<redCable> ()._parentElement = gameObject;
		_rightCable.GetComponent<redCable> ().setPosition (_row, _column, _input, _isRight);
	}

	private void setBlueCable (int _row, int _column, int _input){
		_leftCable = Instantiate(_blueCablePrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
		_leftCable.transform.SetParent(GameObject.Find("Canvas/PneumaticBench").transform, false);
		_leftCable.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex () + 1);
		_leftCable.GetComponent<blueCable> ()._parentElement = gameObject;
		_leftCable.GetComponent<blueCable> ().setPosition (_row, _column, _input);
	}

	private void setBlackCable (int _row, int _column, int _input, bool _isRight = false){
		_leftCable = Instantiate(_blackCablePrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
		_leftCable.transform.SetParent(GameObject.Find("Canvas/PneumaticBench").transform, false);
		_leftCable.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex () + 1);
		_leftCable.GetComponent<blackCable> ()._parentElement = gameObject;
		_leftCable.GetComponent<blackCable> ().setPosition (_row, _column, _input, _isRight);
	}

	private void setWhiteCable (int _row, int _column, int _input){
		_rightCable = Instantiate(_whiteCablePrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
		_rightCable.transform.SetParent(GameObject.Find("Canvas/PneumaticBench").transform, false);
		_rightCable.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex () + 1);
		_rightCable.GetComponent<whiteCable> ()._parentElement = gameObject;
		_rightCable.GetComponent<whiteCable> ().setPosition (_row, _column, _input);
	}

	private void setGrayCable (int _row, int _column, int _input){
		_topCable = Instantiate(_grayCablePrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
		_topCable.transform.SetParent(GameObject.Find("Canvas/PneumaticBench").transform, false);
		_topCable.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex () + 1);
		_topCable.GetComponent<grayCable> ()._parentElement = gameObject;
		_topCable.GetComponent<grayCable> ().setPosition (_row, _column, _input);
	}

	private void setInitialValue(int _row, int _column){
		if(_typeComponent == typesComponents.limit_switch_sensor){
			for (int i = 0; i < _situation.GetComponent<PnematicTestBenchClass> ().outputsElements.Length; i++) {
				if(_situation.GetComponent<PnematicTestBenchClass> ().outputsElements[i] != null){
					Debug.Log (_situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].name);
					if(_situation.GetComponent<PnematicTestBenchClass> ().outputsElements[i].GetComponent<solenoidClass>() != null && _situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<solenoidClass> ()._parent.GetComponent<actuatorElement>() != null){
						Debug.Log ("is actuator");
						if ((int)_situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<solenoidClass> ()._parent.GetComponent<actuatorElement>().rowsColumnsUsed [0].x == (_row - 1) && (int)_situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<solenoidClass> ()._parent.GetComponent<actuatorElement>().rowsColumnsUsed [0].y == (_column - 2)) {
							if(_situation.GetComponent<PnematicTestBenchClass> ().outputsElements [i].GetComponent<solenoidClass> ()._parent.GetComponent<actuatorElement>().actualStatus == 1 ){
								setValue (1);
							}	
						}
					}
				}	
			}
		}
	}

	public void setSibling(int _newIndex){
		gameObject.transform.SetSiblingIndex (_newIndex);
		if (_rightCable != null) {
			_rightCable.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex () + 1);
		}
		if (_leftCable != null) {
			_leftCable.transform.SetSiblingIndex (gameObject.transform.GetSiblingIndex () + 1);
		}
	}

	// Update is called once per frame
	void Update () {

	}
}
